/**
 * Created by amadeyr on 10/13/15.
 */
package com.amadeyr.amadeyrsalesrepresentative;

        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteException;
        import android.os.Bundle;
        import android.support.v4.widget.CursorAdapter;
        import android.support.v7.app.ActionBar;
        import android.support.v7.app.ActionBarActivity;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
        import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
        import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRTeamLeaderCursorAdapter;
        import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
        import com.amadeyr.amadeyrsalesrepresentative.models.CustomerInfo;

        import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
        import com.amadeyr.amadeyrsalesrepresentative.models.EmployeeInfo;

        import java.util.List;

/**
 * Created by acl on 5/17/15.
 */
public class ASRTeamLeaderActivity extends ActionBarActivity {

    //private EditText edtTextEmployeeID;

    private ListView lvSalesRepList;
    private EmployeeInfo cInfo;
    private APCProductDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private ASRErrorMessageView errorMsgObj;
    private APCCustomAutoCompleteView salesRepSearch;
    private ASRTeamLeaderCursorAdapter apsCursorAdapter;
    private ImageView imgViewSalesRepCross;



    private String user_id, source_activity;
    private String SCREEN_TITLE = "";


    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] salesRepArray;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_rep_selection_screen);

        SCREEN_TITLE            = getResources().getString(R.string.barTitleSalesRep);
        ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();



        SharedPreferences sp    = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        getURL                  = sp.getString("getURL","")+"/autodata/description";
        postURL                 = sp.getString("postURL","")+"/index.php/pos_request/datapush";
        user_id                 = sp.getString("username", "");
        source_activity         = getIntent().getStringExtra("source_activity");

        errorMsgObj             = new ASRErrorMessageView(ASRTeamLeaderActivity.this);
        apcDateHelper           = new APCDateHelper();

        salesRepSearch          = (APCCustomAutoCompleteView) findViewById(R.id.actSalesRepSearch);
        salesRepSearch.clearFocus();
        imgViewSalesRepCross    = (ImageView) findViewById(R.id.imgViewSalesRepCross);
        lvSalesRepList          = (ListView) findViewById(R.id.lstViewSalesRepList);

        salesRepSearch.setThreshold(1);

        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        dbHelper                = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());
        //user_id                 = "SUP-100";

        Cursor salesRepresentatives = null;

        try
        {
            salesRepresentatives = dbHelper.getEmployeesUnderSupervisor(user_id);

        }catch(SQLiteException e)
        {

            e.printStackTrace();
            errorMsgObj.showErrorMessage("Error 101", "No database present ! Please select sync option from menu to download database");

        }


        apsCursorAdapter        = new ASRTeamLeaderCursorAdapter(getApplicationContext(),R.layout.sales_rep_row_layout,salesRepresentatives,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvSalesRepList.setAdapter(apsCursorAdapter);
        //btnPSSubmit             = (Button)   findViewById(R.id.btnPSSubmit);
        //btnPSSearch             = (Button)   findViewById(R.id.btnPSSearch);

        imgViewSalesRepCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                salesRepSearch.setText("");
                Cursor allEntries = dbHelper.getEmployeesUnderSupervisor(user_id);
                apsCursorAdapter.swapCursor(allEntries);
                apsCursorAdapter.notifyDataSetChanged();
            }
        });

        lvSalesRepList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor selectedCustomer = (Cursor) apsCursorAdapter.getItem(position);

                //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();

                if(source_activity.contentEquals("chemist"))
                {
                    Intent i = new Intent(ASRTeamLeaderActivity.this, APSCustomerSelectionActivity.class);
                    i.putExtra("employee_id", selectedCustomer.getString(selectedCustomer.getColumnIndex("employee_id")));
                    i.putExtra("source_activity",source_activity);
                    startActivity(i);

                }else if(source_activity.contentEquals("doctor"))
                {

                    //Cursor selectedCustomer = (Cursor) apsCursorAdapter.getItem(position);

                    //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();

                    Intent i = new Intent(ASRTeamLeaderActivity.this, ASRDoctorSelectionActivity.class);
                    i.putExtra("employee_id", selectedCustomer.getString(selectedCustomer.getColumnIndex("employee_id")));
                    i.putExtra("institute_code", "");
                    i.putExtra("institute_name", "");
                    i.putExtra("source_activity", source_activity);

                    startActivity(i);


//                    Intent i = new Intent(ASRTeamLeaderActivity.this, ASRInstituteSelectionActivity.class);
//                    i.putExtra("employee_id", selectedCustomer.getString(selectedCustomer.getColumnIndex("employee_id")));
//                    i.putExtra("source_activity",source_activity);
//                    startActivity(i);

                }


                //finish();

            }
        });

        salesRepSearch.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));

        salesRepSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String searchKey    = salesRepSearch.getText().toString();

                Cursor searchCursor = dbHelper.getEmployeeList(searchKey);

                apsCursorAdapter.swapCursor(searchCursor);
                apsCursorAdapter.notifyDataSetChanged();


            }
        });




    }




    public void populateSearchAdapter(String searchItem)
    {

        String supervisor_id        = this.user_id;
        List<EmployeeInfo> eInfo    = dbHelper.getAutoCompleteEmployeeInfo(searchItem,user_id);
        salesRepArray               = new String[eInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < eInfo.size();++index)
        {
            salesRepArray[index]        = new String(eInfo.get(index).getEmployeeId());
            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());

            //str_index+=2;

            //System.out.println("SearchActivity: "+jObj.getString("description"));
        }


        ArrayAdapter adapter         = new ArrayAdapter(ASRTeamLeaderActivity.this, android.R.layout.simple_list_item_1, salesRepArray);

        salesRepSearch.setAdapter(adapter);
        salesRepSearch.setThreshold(1);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {
            String source_activity  = "";

            Intent main_menu = new Intent(ASRTeamLeaderActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();


            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

       super.onBackPressed();




    }




}
