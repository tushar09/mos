package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by acl on 5/12/15.
 */
public class APCSimpleCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;
    public APCSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {


        if(layout_id==R.layout.des_row_layout) {

            TextView txtViewDESRowSerialID = (TextView) view.findViewById(R.id.txtViewDESRowSerialID);
            TextView txtViewDESRowTag = (TextView) view.findViewById(R.id.txtViewDESRowTag);
            TextView txtViewDESRowCount = (TextView) view.findViewById(R.id.txtViewDESRowCount);
            TextView txtViewDESRowStatus = (TextView) view.findViewById(R.id.txtViewDESRowStatus);
            TextView txtViewDESRowTime = (TextView) view.findViewById(R.id.txtViewDESRowTime);

            if (cursor != null)
            {
                //
                txtViewDESRowSerialID.setText(cursor.getPosition()+1+".");
                txtViewDESRowTag.setText(cursor.getString(cursor.getColumnIndex("trade_name")));
                txtViewDESRowCount.setText("" + cursor.getDouble(cursor.getColumnIndex("product_count")));
                txtViewDESRowStatus.setText(cursor.getString(cursor.getColumnIndex("product_tag")));


                long time_in_milliseconds = cursor.getLong(cursor.getColumnIndex("entry_time"));
                String time_in_milliseconds_str = apcDate.getTimeFromMilliseconds(time_in_milliseconds);

                txtViewDESRowTime.setText(time_in_milliseconds_str);

                String entry_status = cursor.getString(cursor.getColumnIndex("entry_status"));

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
            }
        }else
        {
//
//            TextView txtViewPSRowSerialID = (TextView) view.findViewById(R.id.txtViewPSRowSerialID);
//            TextView txtViewPSRowName = (TextView) view.findViewById(R.id.txtViewPSRowName);
//            TextView txtViewPSRowTag = (TextView) view.findViewById(R.id.txtViewPSRowTag);
//            TextView txtViewPSRowCount = (TextView) view.findViewById(R.id.txtViewPSRowCount);
//
//            TextView txtViewPSRowTime = (TextView) view.findViewById(R.id.txtViewPSRowTime);
//
//            if (cursor != null) {
//                //
//                txtViewPSRowSerialID.setText(cursor.getPosition()+1+".");
//                txtViewPSRowTag.setText(cursor.getString(cursor.getColumnIndex("product_tag")));
//                txtViewPSRowName.setText(cursor.getString(cursor.getColumnIndex("trade_name")));
//                txtViewPSRowCount.setText("" + cursor.getDouble(cursor.getColumnIndex("product_count")));
//
//                long time_in_milliseconds = cursor.getLong(cursor.getColumnIndex("entry_time"));
//                String time_in_milliseconds_str = apcDate.getTimeFromMilliseconds(time_in_milliseconds);
//
//                txtViewPSRowTime.setText(time_in_milliseconds_str);
//
//
//            }


        }



    }
}
