package com.amadeyr.amadeyrsalesrepresentative.managers;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by acl on 2/18/16.
 */
public class ASRLocationManager implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public Context appCtx;


    private Location mLastLocation, mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private boolean updated;
    private double latitude, longitude;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;


    public ASRLocationManager(Context c)
    {

        this.appCtx = c;
        updated = false;

        if(checkPlayServices())
        {
            buildGoogleApiClient();


        }

        if(mGoogleApiClient!=null)
        {

            mGoogleApiClient.connect();
        }
    }


    public void setAppContext(Context ctx)
    {

        appCtx = ctx;
    }

    protected void createLocationRequest()
    {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }

    public boolean getLocationReadyStatus()
    {
        return updated;

    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }


    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {

            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }

        }



    }

    private boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(appCtx);

        if(resultCode != ConnectionResult.SUCCESS)
        {

            return false;
        }

        return true;

    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(appCtx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            latitude  = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
        }

        updated = true;

        //stopLocationUpdates();


    }

    @Override
    public void onConnected(Bundle bundle)
    {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location)
    {

        if(location!=null)
        {

            mCurrentLocation = location;
            recordLocation();
            stopLocationUpdates();

        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
