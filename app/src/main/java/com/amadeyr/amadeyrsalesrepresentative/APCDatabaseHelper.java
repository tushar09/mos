package com.amadeyr.amadeyrsalesrepresentative;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.models.CustomerInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acl on 5/12/15.
 */
public class APCDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME               = "apsDB.db";
    public static final String DATABASE_NAME_2             = "apsDB";

    private static final String PRODUCT_DATABASE_NAME      = "apsProductDB";
    public static final int DATABASE_VERSION               = 13;

    private Context application_ctx;
    // Initializing table names

    private static final String ORDER_TABLE                     = "aps_order";
    private static final String ORDER_ENTRIES_TABLE             = "aps_order_entries";
    private static final String DOCTOR_VISIT_MASTER_TABLE       = "aps_doctor_visit_master";
    private static final String DOCTOR_VISIT_DETAILS_TABLE      = "aps_doctor_visit_details";
    private static final String table_name3                     = "aps_sync";
    private static final String MESSAGE_TABLE                   = "aps_messages";
    private static final String LAST_MESSAGE_TABLE              = "aps_last_messages";
    private static final String PRESCRIPTION_GROUP_TABLE        = "asr_prescription_group";
    private static final String PRESCRIPTION_DETAILS_TABLE      = "asr_prescription_details";
    private static final String PRESCRIPTION_IMAGES_TABLE       = "asr_prescription_images";

    private static final String EMPLOYEE_ATTENDANCE             = "asr_employee_attendance";
    // Query Strings for creation of tables


    private static final String CREATE_ORDERS_TABLE         = "create table "+ORDER_TABLE+"(_id integer primary key,order_no text,customer_id text, customer_name text, delivery_date text, collection_date text, order_date text, order_time text,order_status text, submit_status text, total numeric, total_vat numeric, total_commission numeric, remarks text, invoice_type text, start_lat double, start_lon double, submit_lat double, submit_lon double, taken_by text, delete_status int)";
    private static final String CREATE_ORDER_ENTRIES_TABLE  = "create table "+ ORDER_ENTRIES_TABLE +"(_id integer primary key,order_no text,product_id text, product_name text, price numeric, entry_time text, qty numeric, product_vat numeric, product_commission numeric, bonus numeric, comments text)";

    private static final String CREATE_DOCTOR_VISIT_MASTER_TABLE  = "create table "+DOCTOR_VISIT_MASTER_TABLE+"(_id integer primary key,visit_id text, doctor_code text, doctor_name text, institute_name text, visit_date text, visit_time text, visit_status text, visit_submit_status text,visit_remarks text, visit_start_lat double, visit_start_lon double, visit_submit_lat double, visit_submit_lon double, taken_by text, visited_with text, visit_type text, delete_status int)";
    private static final String CREATE_DOCTOR_VISIT_DETAILS_TABLE = "create table "+ DOCTOR_VISIT_DETAILS_TABLE +"(_id integer primary key,visit_id text,category_name text,item_code text, item_name text, qty numeric, comments text)";

    private static final String CREATE_SYNC_TABLE                       = "create table "+table_name3+"(_id integer primary key,database_tag text, hashcode_latest text, employee_code text)";
    private static final String CREATE_MSG_TABLE                        = "CREATE TABLE "+ MESSAGE_TABLE +"(_id INTEGER PRIMARY KEY AUTOINCREMENT,msg_id TEXT,msg_title TEXT, msg_body TEXT, publish_date TEXT, pub_time TEXT, deleted INTEGER, read INTEGER, sent INTEGER, msg_user_id INTEGER, parent_msg_id INTEGER, employee_code TEXT)";
    private static final String CREATE_LAST_MSG_TABLE                   = "CREATE TABLE "+ LAST_MESSAGE_TABLE+"(_id INTEGER PRIMARY KEY AUTOINCREMENT,msg_id TEXT, u_id TEXT);";
    private static final String CREATE_PRESCRIPTION_GROUP_TABLE         = "CREATE TABLE "+ PRESCRIPTION_GROUP_TABLE+"(_id INTEGER PRIMARY KEY AUTOINCREMENT,group_id INTEGER, creation_time INTEGER, doctor_code TEXT, doctor_name TEXT, start_lat DOUBLE, start_lon DOUBLE, submit_lat DOUBLE, submit_lon DOUBLE, submit_status TEXT, completion_status TEXT, taken_by TEXT, image_upload_path TEXT);";
    private static final String CREATE_PRESCRIPTION_DETAILS_TABLE       = "CREATE TABLE "+ PRESCRIPTION_DETAILS_TABLE+"(_id INTEGER PRIMARY KEY AUTOINCREMENT,master_group_id INTEGER, product_code TEXT, product_name TEXT);";
    private static final String CREATE_PRESCRIPTION_IMAGES_TABLE        = "CREATE TABLE "+ PRESCRIPTION_IMAGES_TABLE+"(_id INTEGER PRIMARY KEY AUTOINCREMENT,group_id INTEGER, image_name TEXT, date_time INTEGER, deleted INTEGER, submit_status TEXT);";
    private static final String CREATE_EMPLOYEE_ATTENDANCE_TABLE        = "CREATE TABLE "+ EMPLOYEE_ATTENDANCE+"(_id INTEGER PRIMARY KEY AUTOINCREMENT,employee_code TEXT, clock_type TEXT, date_time LONG, date TEXT, lat DOUBLE, lon DOUBLE, submit_status TEXT);";

    // DEFAULT TAGS FOR FIELDS IN ORDER & ORDER_ENTRIES TABLE

    private final String ORDER_STATUS_COMPLETE              = "COMPLETE";
    private final String ORDER_STATUS_INCOMPLETE            = "INCOMPLETE";
    private static final String SUBMIT_STATUS_PENDING       = "PENDING";
    private static final String SUBMIT_STATUS_SUBMITTED     = "SUBMITTED";
    public  static final String VISIT_TYPE_LONE             = "NO-OFFER";
    public  static final String VISIT_TYPE_NORMAL           = "NORMAL";

    public static final String START_ATTENDANCE_SHIFT       = "START";
    public static final String STOP_ATTENDANCE_SHIFT        = "END";    // Default tags for fields in the aps_sync table

    public static final String PRODUCT_TAG_DB_SYNC                  = "PRODUCT";
    public static final String OTHER_PRODUCTS_TAG_DB_SYNC           = "OTHER_PRODUCTS";
    public static final String USER_TAG_DB_SYNC                     = "USER";
    public static final String ALL_TAG_DB_SYNC                      = "ALL";



    private final String LOG_TAG                            = "APCDatabaseHelper";

    private static APCDatabaseHelper sInstance;

    //private static final String CREATE_SYNC_TABLE          = "create table "+table_name3+"(_id integer primary key,sync_version int)";

    public static synchronized  APCDatabaseHelper getInstance(Context c)
    {
        if(sInstance==null)
        {
            sInstance = new APCDatabaseHelper(c,DATABASE_NAME,null,DATABASE_VERSION);
        }
        return sInstance;

    }

    public APCDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        application_ctx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {

        Toast.makeText(application_ctx,"Order Database Created", Toast.LENGTH_LONG).show();
        database.execSQL(CREATE_ORDERS_TABLE);
        database.execSQL(CREATE_ORDER_ENTRIES_TABLE);
        database.execSQL(CREATE_DOCTOR_VISIT_MASTER_TABLE);
        database.execSQL(CREATE_DOCTOR_VISIT_DETAILS_TABLE);
        database.execSQL(CREATE_SYNC_TABLE);
        database.execSQL(CREATE_MSG_TABLE);
        database.execSQL(CREATE_LAST_MSG_TABLE);
        database.execSQL(CREATE_PRESCRIPTION_GROUP_TABLE);
        database.execSQL(CREATE_PRESCRIPTION_IMAGES_TABLE);
        database.execSQL(CREATE_PRESCRIPTION_DETAILS_TABLE);
        database.execSQL(CREATE_EMPLOYEE_ATTENDANCE_TABLE);
        //initializeSyncTableData(database);
        fillWithMessageData(database);
        //database.execSQL("ATTACH DATABASE "+application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME)+" as productDB");


    }




    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });

            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }


    }

    private void  initializeSyncTableData(SQLiteDatabase db)
    {

        ContentValues cvalues   =   new ContentValues();
        cvalues.put("db_type", "product");
        cvalues.put("hashcode", "");
        db.insert(MESSAGE_TABLE, null, cvalues);


    }

    public void fillWithMessageData(SQLiteDatabase db)
    {


        ContentValues cvalues   =   new ContentValues();
        cvalues.put("msg_id", "1");
        cvalues.put("msg_title","Message 1");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","10 Oct 2015");
        cvalues.put("pub_time","4:30 pm");
        cvalues.put("deleted",0);
        cvalues.put("read",0);
        cvalues.put("sent",0);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);


        long category_id=db.insert(MESSAGE_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("msg_id", "2");
        cvalues.put("msg_title","Message 2");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","11 Oct 2015");
        cvalues.put("pub_time","10:30 am");
        cvalues.put("deleted",0);
        cvalues.put("read",1);
        cvalues.put("sent",0);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);

        category_id=db.insert(MESSAGE_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("msg_id", "3");
        cvalues.put("msg_title","Message 3");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","09 Oct 2015");
        cvalues.put("pub_time","12:30 pm");
        cvalues.put("deleted",0);
        cvalues.put("read",0);
        cvalues.put("sent",0);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);


        category_id=db.insert(MESSAGE_TABLE, null, cvalues);


        cvalues   =   new ContentValues();
        cvalues.put("msg_id", "4");
        cvalues.put("msg_title","Message 4");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","08 Oct 2015");
        cvalues.put("pub_time","09:30 am");
        cvalues.put("deleted",0);
        cvalues.put("read",0);
        cvalues.put("sent",0);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);

        category_id=db.insert(MESSAGE_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("msg_id", "4");
        cvalues.put("msg_title","Message 5");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","08 Oct 2015");
        cvalues.put("pub_time","09:30 am");
        cvalues.put("deleted",0);
        cvalues.put("read",1);
        cvalues.put("sent",1);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);

        category_id=db.insert(MESSAGE_TABLE, null, cvalues);

        category_id=db.insert(MESSAGE_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("msg_id", "3");
        cvalues.put("msg_title","Message 6");
        cvalues.put("msg_body","Lorem Ipsum");
        cvalues.put("publish_date","09 Oct 2015");
        cvalues.put("pub_time","12:30 pm");
        cvalues.put("deleted",0);
        cvalues.put("read",1);
        cvalues.put("sent",1);
        cvalues.put("msg_user_id",1);
        cvalues.put("parent_msg_id",1);


        category_id=db.insert(MESSAGE_TABLE, null, cvalues);




    }




    public void fillPrescriptionWithDummyData() {
        SQLiteDatabase db = this.getWritableDatabase();

        //String doctor_code,String trade_name, String product_barcode,String product_tag, double product_count , String entry_date, long entry_time, String entry_status, String comments

        ContentValues cvalues = new ContentValues();
        cvalues.put("group_id", "1");
        db.insert(PRESCRIPTION_GROUP_TABLE,null,cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", "2");
        db.insert(PRESCRIPTION_GROUP_TABLE,null,cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", "3");
        db.insert(PRESCRIPTION_GROUP_TABLE,null,cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", "4");
        db.insert(PRESCRIPTION_GROUP_TABLE,null,cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", "5");
        db.insert(PRESCRIPTION_GROUP_TABLE,null,cvalues);
    }


    public void fillPrescriptionImagesWithDummyData() {
        SQLiteDatabase db = this.getWritableDatabase();

        //String doctor_code,String trade_name, String product_barcode,String product_tag, double product_count , String entry_date, long entry_time, String entry_status, String comments

        ContentValues cvalues = new ContentValues();
        cvalues.put("group_id", 1);
        cvalues.put("image_name", "Image 1");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", 1);
        cvalues.put("image_name", "Image 2");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);


        cvalues = new ContentValues();
        cvalues.put("group_id", 2);
        cvalues.put("image_name", "Image 3");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", 2);
        cvalues.put("image_name", "Image 4");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", 3);
        cvalues.put("image_name", "Image 5");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", 3);
        cvalues.put("image_name", "Image 6");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);
        cvalues = new ContentValues();
        cvalues.put("group_id", 4);
        cvalues.put("image_name", "Image 7");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        cvalues = new ContentValues();
        cvalues.put("group_id", 4);
        cvalues.put("image_name", "Image 8");
        cvalues.put("date_time", 12341223);
        cvalues.put("deleted", 0);

        db.insert(PRESCRIPTION_IMAGES_TABLE,null,cvalues);



    }



    public void fillWithDummyData()
    {
        SQLiteDatabase db       =   this.getWritableDatabase();

        //String doctor_code,String trade_name, String product_barcode,String product_tag, double product_count , String entry_date, long entry_time, String entry_status, String comments

        ContentValues cvalues   =   new ContentValues();
        cvalues.put("doctor_code", "24");
        cvalues.put("trade_name","T-Shirts");
        cvalues.put("product_barcode","132768165");
        cvalues.put("product_tag","shelf");
        cvalues.put("product_count",10.0);
        cvalues.put("entry_date","2015-05-12");
        cvalues.put("entry_time", 1234);
        cvalues.put("entry_status","pending");
        cvalues.put("comments","ok");

        long category_id=db.insert(ORDER_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("doctor_code", "24");
        cvalues.put("trade_name","T-Shirts");
        cvalues.put("product_barcode", "132768165");
        cvalues.put("product_tag","backstore");
        cvalues.put("product_count",15.0);
        cvalues.put("entry_date","2015-05-12");
        cvalues.put("entry_time",1234);
        cvalues.put("entry_status","pending");
        cvalues.put("comments", "ok");

        category_id=db.insert(ORDER_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("doctor_code", "24");
        cvalues.put("trade_name","T-Shirts");
        cvalues.put("product_barcode","132768165");
        cvalues.put("product_tag", "front");
        cvalues.put("product_count",20.0);
        cvalues.put("entry_date","2015-05-12");
        cvalues.put("entry_time",1235);
        cvalues.put("entry_status","submitted");
        cvalues.put("comments","ok");

        category_id= db.insert(ORDER_TABLE, null, cvalues);

        category_id=db.insert(ORDER_TABLE, null, cvalues);

        cvalues   =   new ContentValues();
        cvalues.put("doctor_code", "24");
        cvalues.put("trade_name","T-Shirts");
        cvalues.put("product_barcode","132768165");
        cvalues.put("product_tag", "front");
        cvalues.put("product_count",27.0);
        cvalues.put("entry_date","2015-05-12");
        cvalues.put("entry_time",1235);
        cvalues.put("entry_status","submitted");
        cvalues.put("comments","ok");

        category_id=db.insert(ORDER_TABLE, null, cvalues);


    }

    public Cursor getAllOrders()
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM "+ORDER_TABLE;

        Cursor results      = db.rawQuery(query, null);

        return results;


    }


    public Cursor getSyncInfo()
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM aps_sync";

        Cursor results      = db.rawQuery(query,null);

        return results;

    }

    public void updateSyncVersion(String hashcode)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "UPDATE aps_sync SET hash_code=\""+hashcode+"\" WHERE _id=\"1\"";

        db.execSQL(query);

    }



    public void insertProduct(ProductInfo p)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("customer_id", p.getProductID());
        cvalues.put("trade_name", p.getProductName());
        //cvalues.put("product_barcode",p.getProductBarcode());

        db.insert(ORDER_ENTRIES_TABLE, null, cvalues);
        //db.close();


    }

    public Cursor getAllProductInfo()
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_product";

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getProductInfoFromBarcode(String barcode)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_product WHERE product_barcode=\""+barcode+"\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getProductInfoFromName(String name)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_product WHERE trade_name LIKE \"%"+name+"%\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getPrescriptionList()
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT asr_prescription_group._id,asr_prescription_group.group_id, creation_time, image_name FROM "+PRESCRIPTION_GROUP_TABLE+" JOIN "+PRESCRIPTION_IMAGES_TABLE+" ON asr_prescription_group.group_id=asr_prescription_images.group_id GROUP BY asr_prescription_group._id ORDER BY asr_prescription_group._id DESC";

        Cursor results      = db.rawQuery(query,null);

        return results;
    }


    public Cursor getIncompletePrescriptions()
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT asr_prescription_group._id,asr_prescription_group.doctor_name,asr_prescription_group.group_id, creation_time, image_name FROM "+PRESCRIPTION_GROUP_TABLE+" JOIN "+PRESCRIPTION_IMAGES_TABLE+" ON asr_prescription_group.group_id=asr_prescription_images.group_id WHERE asr_prescription_group.completion_status=\"INCOMPLETE\" GROUP BY asr_prescription_group._id ORDER BY asr_prescription_group._id DESC";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }
    public Cursor getPendingPrescriptionList(String user_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query;

        if(user_id.isEmpty())
        {
            query        =  "SELECT asr_prescription_group._id,asr_prescription_group.doctor_name,asr_prescription_group.group_id, creation_time, image_name FROM "+PRESCRIPTION_GROUP_TABLE+" JOIN "+PRESCRIPTION_IMAGES_TABLE+" ON asr_prescription_group.group_id=asr_prescription_images.group_id WHERE asr_prescription_group.submit_status=\"PENDING\" AND asr_prescription_group.completion_status=\"COMPLETE\" GROUP BY asr_prescription_group._id ORDER BY asr_prescription_group._id DESC";

        }else
        {
            query        =  "SELECT asr_prescription_group._id,asr_prescription_group.doctor_name,asr_prescription_group.group_id, creation_time, image_name FROM "+PRESCRIPTION_GROUP_TABLE+" JOIN "+PRESCRIPTION_IMAGES_TABLE+" ON asr_prescription_group.group_id=asr_prescription_images.group_id WHERE asr_prescription_group.submit_status=\"PENDING\" AND asr_prescription_group.completion_status=\"COMPLETE\" WHERE taken_by="+user_id+" GROUP BY asr_prescription_group._id ORDER BY asr_prescription_group._id DESC";
        }


        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getPendingPrescriptionGroup()
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+PRESCRIPTION_GROUP_TABLE+" WHERE asr_prescription_group.submit_status=\"PENDING\" AND completion_status=\"COMPLETE\" ORDER BY asr_prescription_group._id DESC";

        Cursor results      = db.rawQuery(query,null);

        return results;

    }



    public void insertEntry(String product_id, String product_name, String product_barcode, String product_tag, double product_count, String entry_date, long entry_time, String entry_status,String comments)
    {
            SQLiteDatabase db       =   this.getWritableDatabase();
            ContentValues cvalues   =   new ContentValues();

            cvalues.put("customer_id", product_id);
            cvalues.put("trade_name",product_name);
            cvalues.put("product_barcode",product_barcode);
            cvalues.put("product_tag",product_tag);
            cvalues.put("product_count",product_count);
            cvalues.put("entry_date",entry_date);
            cvalues.put("entry_time",entry_time);
            cvalues.put("entry_status",entry_status);
            cvalues.put("comments",comments);

            db.insert(ORDER_TABLE, null, cvalues);
            db.close();

    }
    public Cursor getAllEntries()
    {


        return null;
    }

    public Cursor getLastTenEntries()
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_entries WHERE entry_status= \"Pending\" ORDER BY _id DESC LIMIT 0,10";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getEntryForProduct(String barCodeStr)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_entries WHERE product_barcode="+"\""+barCodeStr+"\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getEntryForProductPending(String barCodeStr)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_entries WHERE product_barcode="+"\""+barCodeStr+"\" AND entry_status=\"Pending\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getEntryForName(String order_no)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT * FROM aps_order WHERE (order_no = \""+order_no+"\" OR customer_name = \""+order_no+"\") AND submit_status=\"PENDING\" AND delete_status=0";


        Cursor results      =  db.rawQuery(query,null);


        return results;

    }

    public Cursor getEntryForNameFromPrescription(String doctor_name)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT * FROM asr_prescription_group WHERE doctor_name = \""+doctor_name+"\" AND submit_status=\"PENDING\"";


        Cursor results      =  db.rawQuery(query,null);


        return results;

    }

    public Cursor getEntryForVisit(String order_no)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM aps_doctor_visit_master WHERE (visit_id = \""+order_no+"\" OR doctor_name = \""+order_no+"\") AND visit_submit_status=\"PENDING\" AND delete_status=0";
        Cursor results      =  db.rawQuery(query,null);

        return results;

    }





    public void addEntry(String product_id,String product_name, String product_barcode,String product_tag, double product_count , String entry_date, long entry_time, String entry_status, String comments)
    {


    }

    public void updateStatus(int entry_id)
    {


    }

    public Cursor getDistinctEntries()
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT DISTINCT * FROM aps_order WHERE submission_status=\"PENDING\"";

        Cursor results      =  db.rawQuery(query,null);


        return results;

    }

    public Cursor getEntriesFromBarcodeOrProductName(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT DISTINCT * FROM apc_entries WHERE entry_status=\"Pending\" AND (trade_name LIKE \"%"+searchTerm+"%\" OR product_barcode LIKE \"%"+searchTerm+"%\") ORDER BY trade_name DESC LIMIT 0,10";
        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getOrders(String searchTerm)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT DISTINCT customer_name FROM aps_order WHERE submit_status=\"PENDING\" AND order_status=\"COMPLETE\" AND customer_name LIKE \"%"+searchTerm+"%\" AND delete_status=0) ORDER BY order_date DESC LIMIT 0,10";
        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getPendingSubmissions(String taken_by)
    {
        SQLiteDatabase db   = this.getWritableDatabase();
        String query        = "";

        if(taken_by.isEmpty())
        {
            query = "SELECT * FROM aps_order WHERE submit_status=\"PENDING\" AND order_status=\"COMPLETE\" AND delete_status=0";

        }else
        {

            query = "SELECT * FROM aps_order WHERE submit_status=\"PENDING\" AND order_status=\"COMPLETE\" AND delete_status=0 AND taken_by='" + taken_by + "'";
        }


        Cursor results      = db.rawQuery(query,null);


        return results;

    }

    public Cursor getPendingVisits(String userId)
    {
        SQLiteDatabase db   = this.getWritableDatabase();
        String query;

        if(userId.isEmpty())
        {
            query        = "SELECT * FROM aps_doctor_visit_master WHERE visit_submit_status=\"PENDING\" AND visit_status=\"COMPLETE\" AND delete_status=0";

        }else
        {
            query        = "SELECT * FROM aps_doctor_visit_master WHERE visit_submit_status=\"PENDING\" AND visit_status=\"COMPLETE\" AND delete_status=0 AND taken_by='"+userId+"'";

        }



        Cursor results      = db.rawQuery(query,null);


        return results;

    }



    public Cursor getOrderByID(String order_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM aps_order WHERE order_no=\""+order_id+"\"";
        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getVisitByID(String order_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM aps_doctor_visit_master WHERE visit_id=\""+order_id+"\"";
        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public int getLastGroupID()
    {

        int last_id         = 1;

        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT _id FROM "+PRESCRIPTION_GROUP_TABLE +" ORDER by _id DESC limit 1";

        Cursor results      =  db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_id     = results.getInt(results.getColumnIndex("_id"));
            last_id     += 1;
        }

        return last_id;


    }


    public int getLastInsertID()
    {
        int last_id         = 1;

        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT _id FROM "+ORDER_TABLE +" ORDER by _id DESC limit 1";

        Cursor results      =  db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_id     = results.getInt(results.getColumnIndex("_id"));
            last_id     += 1;
        }

        return last_id;

    }


    public int getLastVisitInsertID()
    {
        int last_id         = 1;

        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "SELECT _id FROM "+DOCTOR_VISIT_MASTER_TABLE +" ORDER by _id DESC limit 1";

        Cursor results      =  db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_id     = results.getInt(results.getColumnIndex("_id"));
            last_id     +=1;

        }

        return last_id;

    }




    public String getLastIncompleteOrderNo(String customer_id)
    {
        String last_id          = "";

        SQLiteDatabase db       = this.getWritableDatabase();

        String query            = "SELECT order_no FROM "+ORDER_TABLE +" WHERE order_status='INCOMPLETE' AND customer_id='"+customer_id+"' AND delete_status=0 ORDER by _id DESC limit 1";

        Cursor results          = db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_id         = results.getString(results.getColumnIndex("order_no"));

        }

        return last_id;

    }


    public String getLastIncompleteVisitOrderNo(String doc_code)
    {
        String last_id          = "";

        SQLiteDatabase db       = this.getWritableDatabase();

        String query            = "SELECT visit_id FROM "+DOCTOR_VISIT_MASTER_TABLE +" WHERE visit_status='INCOMPLETE' AND doctor_code='"+doc_code+"' AND delete_status=0 ORDER by _id DESC limit 1";

        Cursor results          = db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_id         = results.getString(results.getColumnIndex("visit_id"));

        }

        return last_id;

    }



    public String getLastOrderDate(String product_id, String customer_id)
    {
        SQLiteDatabase db       =  this.getWritableDatabase();

        String last_order_date  = "";

        String query            = "SELECT aps_order.order_date FROM "+ORDER_TABLE +" JOIN aps_order_entries ON aps_order_entries.order_no = aps_order.order_no WHERE aps_order_entries.product_id='"+product_id+"' AND aps_order.customer_id='"+customer_id+"' AND delete_status=0 ORDER by aps_order.order_date DESC limit 1";

        Cursor results          =  db.rawQuery(query,null);

        if(results.getCount()>0)
        {
            results.moveToFirst();
            last_order_date     =  results.getString(results.getColumnIndex("order_date"));

        }

        return last_order_date;

    }

    public void updateStatusToSubmit(int product_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "UPDATE apc_entries SET entry_status=\"Submitted\" WHERE customer_id="+product_id;

        db.execSQL(query);

    }






    public void updateStatusToSubmitWithEntryID(String entry_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "UPDATE aps_order SET submit_status=\"SUBMITTED\" WHERE order_no='"+entry_id+"'";

        db.execSQL(query);

    }



    public void updatePrescriptionStatusToSubmitWithID(String prescription_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "UPDATE "+PRESCRIPTION_GROUP_TABLE+" SET submit_status=\"SUBMITTED\" WHERE group_id='"+prescription_id+"'";

        db.execSQL(query);

    }

    public void updatePrescriptionSubmitLocation(String group_id,double submit_lat, double submit_lon)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("submit_lat", submit_lat);
        cvalues.put("submit_lon", submit_lon);

        db.update(PRESCRIPTION_GROUP_TABLE, cvalues, "group_id='" + group_id + "'", null);

    }



    public void updateVisitStatusToSubmitWithEntryID(String entry_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "UPDATE "+DOCTOR_VISIT_MASTER_TABLE+" SET visit_submit_status=\"SUBMITTED\" WHERE visit_id='"+entry_id+"'";

        db.execSQL(query);

    }

    public void updateStatusToSubmitWithOrderNo(String order_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "UPDATE aps_order SET submit_status=\"SUBMITTED\" WHERE order_no="+order_id;

        db.execSQL(query);

    }


    public void updateVisitStatusToSubmitWithVisitID(String visit_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "UPDATE "+DOCTOR_VISIT_MASTER_TABLE+" SET visit_status=\"SUBMITTED\" WHERE visit_id='"+visit_id+"'";

        db.execSQL(query);

    }

    public void removeEntry(int entry_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "DELETE FROM aps_order_entries WHERE _id="+entry_id;
        db.execSQL(query);

    }




    public void editEntry(int entry_id,String order_no,String comments,double qty,double bonus)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "UPDATE aps_order_entries SET comments="+"\""+comments+"\", "+"qty="+"\""+qty+"\", "+"bonus="+bonus+ " WHERE _id="+entry_id;
        db.execSQL(query);


    }

    public void removeVisitEntry(int entry_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "DELETE FROM "+DOCTOR_VISIT_DETAILS_TABLE+" WHERE _id="+entry_id;
        db.execSQL(query);

    }

    public void editVisitEntry(int entry_id,String visit_no,String comments,int qty)
    {
        SQLiteDatabase db   = this.getWritableDatabase();
        String query        = "UPDATE "+DOCTOR_VISIT_DETAILS_TABLE+" SET comments="+"\""+comments+"\", "+"qty="+"\""+qty+"\" WHERE _id="+entry_id;
        db.execSQL(query);

    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2)
    {
        // Changes in table due to db version 3

        if(i<=2)
        {
            sqLiteDatabase.execSQL(CREATE_PRESCRIPTION_GROUP_TABLE);
            sqLiteDatabase.execSQL(CREATE_PRESCRIPTION_IMAGES_TABLE);

        }

        if(i<=3)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN creation_time INTEGER");

        }

        if(i<=4)
        {
            sqLiteDatabase.execSQL(CREATE_PRESCRIPTION_DETAILS_TABLE);
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN doctor_code TEXT");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN start_lat DOUBLE");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN start_lon DOUBLE");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN submit_lat DOUBLE");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN submit_lon DOUBLE");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN taken_by TEXT");


        }


        if(i<=5)
        {

            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN doctor_name TEXT");
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_DETAILS_TABLE+" ADD COLUMN product_name TEXT");

        }

        if(i<=6)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN submit_status TEXT");

        }

        if(i<=7)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN image_upload_path TEXT");

        }

        if(i<=8)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_GROUP_TABLE+" ADD COLUMN completion_status TEXT");
        }

        if(i<=9)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+PRESCRIPTION_IMAGES_TABLE+" ADD COLUMN submit_status TEXT");
        }

        if(i<=10)
        {
            sqLiteDatabase.execSQL(CREATE_EMPLOYEE_ATTENDANCE_TABLE);

        }

        if(i<=11)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+EMPLOYEE_ATTENDANCE+" ADD COLUMN submit_status TEXT");
        }

        if(i<=12)
        {
            sqLiteDatabase.execSQL("ALTER TABLE "+EMPLOYEE_ATTENDANCE+" ADD COLUMN lat double");
            sqLiteDatabase.execSQL("ALTER TABLE "+EMPLOYEE_ATTENDANCE+" ADD COLUMN lon double");
        }


    }

    public boolean orderExists(String order_id, String mode) {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "";

        if(mode.contentEquals("EDIT"))
        {
            query        =  "SELECT * FROM "+ORDER_TABLE+" WHERE order_no=\'"+order_id+"\' AND order_status='COMPLETE' AND delete_status=0";

        }else
        {
            query        =  "SELECT * FROM "+ORDER_TABLE+" WHERE order_no=\'"+order_id+"\' AND order_status='INCOMPLETE' AND delete_status=0";

        }


        Cursor entry        =  db.rawQuery(query,null);

        if(entry.getCount()>0)
        {
            return true;
        }

        return false;



    }

    public boolean visitExists(String visit_id, String mode) {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "";

        if(mode.contentEquals("EDIT"))
        {
            query        =  "SELECT * FROM "+DOCTOR_VISIT_MASTER_TABLE+" WHERE visit_id=\'"+visit_id+"\' AND visit_status='COMPLETE' AND delete_status=0";

        }else
        {
            query        =  "SELECT * FROM "+DOCTOR_VISIT_MASTER_TABLE+" WHERE visit_id=\'"+visit_id+"\' AND visit_status='INCOMPLETE' AND delete_status=0";

        }


        Cursor entry        =  db.rawQuery(query,null);

        if(entry.getCount()>0)
        {
            return true;

        }

        return false;



    }

    public void getVisitTableDump() {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "";

        query               =  "SELECT * FROM "+DOCTOR_VISIT_MASTER_TABLE;


        Cursor results      =  db.rawQuery(query, null);

        if(results!=null)
        {

            results.moveToFirst();


            while(!results.isAfterLast())
            {
                System.out.println("Visit ID: "+results.getString(results.getColumnIndex("visit_id"))+" "+results.getString(results.getColumnIndex("doctor_code"))+" "+results.getString(results.getColumnIndex("doctor_name"))+" "+results.getString(results.getColumnIndex("visit_status"))+ " "+results.getString(results.getColumnIndex("delete_status")));

                results.moveToNext();
            }


        }



    }





    public void insertOrderEntry(OrderEntries o)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("order_no", o.getOrderNo());
        cvalues.put("product_id",o.getProductID());
        cvalues.put("product_name",o.getProductName());
        cvalues.put("price",o.getProductPrice());
        cvalues.put("qty",o.getQuantity());
        cvalues.put("product_vat",o.getProductVAT());
        cvalues.put("product_commission",o.getProductCommission());
        cvalues.put("bonus",o.getBonus());
        cvalues.put("comments",o.getComments());

        db.insert(ORDER_ENTRIES_TABLE, null, cvalues);
        db.close();

    }

    public void insertOrder(OrderInfo o)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("order_no", o.getOrderNo());
        cvalues.put("delivery_date", o.getDeliveryDate());
        cvalues.put("customer_id",o.getCustomerId());
        cvalues.put("customer_name",o.getCustomerName());
        cvalues.put("order_date",o.getOrderDate());
        cvalues.put("order_time",o.getOrderTime());
        cvalues.put("total",o.getTotal());
        cvalues.put("invoice_type",o.getInvoiceType());
        cvalues.put("order_status",o.getOrderStatus());
        cvalues.put("submit_status",o.getSubmitStatus());
        cvalues.put("start_lat",o.getStartLat());
        cvalues.put("start_lon",o.getStartLon());
        cvalues.put("submit_lat",o.getSubmitLat());
        cvalues.put("submit_lon",o.getSubmitLon());
        cvalues.put("delete_status",0);
        cvalues.put("taken_by",o.getTakenBy());

        db.insert(ORDER_TABLE, null, cvalues);
        db.close();

    }



    public void insertVisitEntry(VisitEntries o)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("visit_id", o.getVisitID());
        cvalues.put("item_code",o.getProductID());
        cvalues.put("item_name",o.getProductName());
        cvalues.put("qty",o.getQuantity());
        cvalues.put("comments",o.getComments());
        cvalues.put("category_name",o.getCategoryName());

        db.insert(DOCTOR_VISIT_DETAILS_TABLE, null, cvalues);
        db.close();

    }



    public void insertVisit(VisitInfo o)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("visit_id", o.getVisitNo());
        cvalues.put("doctor_code",o.getDoctorCode());
        cvalues.put("doctor_name",o.getDoctorName());
        cvalues.put("institute_name",o.getInstituteName());
        cvalues.put("visit_date",o.getVisitDate());
        cvalues.put("visit_time",o.getVisitTime());
//        cvalues.put("total",o.getTotal());
        cvalues.put("visit_status",o.getVisitStatus());
        cvalues.put("visit_submit_status",o.getSubmitStatus());
        cvalues.put("visit_remarks",o.getNotes());
        cvalues.put("visit_start_lat",o.getStartLat());
        cvalues.put("visit_start_lon",o.getStartLon());
        cvalues.put("visit_submit_lat",o.getSubmitLat());
        cvalues.put("visit_submit_lon",o.getSubmitLon());
        cvalues.put("visited_with",o.getVisitedWith());
        cvalues.put("delete_status",0);
        cvalues.put("taken_by",o.getTakenBy());
        cvalues.put("visit_type",o.getVisitType());

        db.insert(DOCTOR_VISIT_MASTER_TABLE, null, cvalues);
        db.close();

    }



    public void removeOrder(String order_no)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();
        cvalues.put("delete_status", 1);
        db.update(ORDER_TABLE, cvalues, "order_no='" + order_no + "'", null);

    }

    public void removeVisit(String order_no)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();
        cvalues.put("delete_status", 1);
        db.update(DOCTOR_VISIT_MASTER_TABLE, cvalues, "visit_id='" + order_no + "'", null);

    }



    public Cursor getOrderDetailsEntries(String order_no)
    {
        SQLiteDatabase db = this.getWritableDatabase();


        //db.execSQL("ATTACH DATABASE '" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME) + "' AS productDB");

        //db.execSQL("ATTACH DATABASE "+PRODUCT_DATABASE_NAME+" as productDB");

        String query           = "SELECT * FROM aps_order_entries WHERE order_no='"+order_no+"'";

        //String query        = "SELECT * FROM aps_order_entries WHERE order_no=\'"+order_no+"\' ORDER BY _id DESC";
        //String query        = "SELECT p.product_name, p.price, o.qty FROM aps_order_entries AS o JOIN productDB.aps_product as p ON p.product_id=o.product_id";

        Cursor results      =  db.rawQuery(query, null);


        return results;


    }

    public Cursor getVisitDetailsEntries(String order_no)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        String query           = "SELECT * FROM aps_doctor_visit_details WHERE visit_id='"+order_no+"'";
        Cursor results      =  db.rawQuery(query, null);
        return results;


    }


    public Cursor getOrderEntries(String order_no)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(LOG_TAG, "" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME));

        //db.execSQL("ATTACH DATABASE '" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME) + "' AS productDB");

        //db.execSQL("ATTACH DATABASE "+PRODUCT_DATABASE_NAME+" as productDB");
        String joinConditions  = "FROM aps_order_entries JOIN aps_order ON aps_order_entries.order_no=aps_order.order_no WHERE aps_order_entries.order_no='"+order_no+"' AND aps_order.delete_status=0";
        String query           = "SELECT aps_order_entries.order_no,aps_order.customer_name,aps_order_entries._id, aps_order_entries.product_name, aps_order_entries.qty, aps_order_entries.price, aps_order_entries.product_vat,aps_order_entries.product_commission, aps_order_entries.bonus, aps_order_entries.comments, aps_order.total, aps_order.total_vat, aps_order.total_commission, aps_order.delivery_date, aps_order.order_date  "+joinConditions+" ORDER BY aps_order_entries._id DESC";

        //String query        = "SELECT * FROM aps_order_entries WHERE order_no=\'"+order_no+"\' ORDER BY _id DESC";
        //String query        = "SELECT p.product_name, p.price, o.qty FROM aps_order_entries AS o JOIN productDB.aps_product as p ON p.product_id=o.product_id";

        Cursor results      =  db.rawQuery(query, null);


        return results;


    }


    public Cursor getVisitEntries(String order_no)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(LOG_TAG, "" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME));

        //db.execSQL("ATTACH DATABASE '" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME) + "' AS productDB");

        //db.execSQL("ATTACH DATABASE "+PRODUCT_DATABASE_NAME+" as productDB");
        String joinConditions  = "FROM aps_doctor_visit_details JOIN aps_doctor_visit_master ON aps_doctor_visit_master.visit_id=aps_doctor_visit_details.visit_id WHERE aps_doctor_visit_master.visit_id='"+order_no+"'";
        String query           = "SELECT aps_doctor_visit_details.visit_id, aps_doctor_visit_master.doctor_name,aps_doctor_visit_details._id, aps_doctor_visit_details.item_name, aps_doctor_visit_details.category_name, aps_doctor_visit_details.qty, aps_doctor_visit_details.comments, aps_doctor_visit_master.visit_date  "+joinConditions+" ORDER BY aps_doctor_visit_details.category_name DESC";

        //String query        = "SELECT * FROM aps_order_entries WHERE visit_id=\'"+visit_id+"\' ORDER BY _id DESC";
        //String query        = "SELECT p.product_name, p.price, o.qty FROM aps_order_entries AS o JOIN productDB.aps_product as p ON p.product_id=o.product_id";

        Cursor results      =  db.rawQuery(query, null);


        return results;


    }



    public Cursor getSortedOrderEntries(String order_no, String sort_field, String sort_order)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(LOG_TAG, "" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME));

        //db.execSQL("ATTACH DATABASE '" + application_ctx.getDatabasePath(PRODUCT_DATABASE_NAME) + "' AS productDB");

        //db.execSQL("ATTACH DATABASE "+PRODUCT_DATABASE_NAME+" as productDB");
        String joinConditions  = "FROM aps_order_entries JOIN aps_order ON aps_order_entries.order_no=aps_order.order_no WHERE aps_order_entries.order_no='"+order_no+"' ";
        String query           = "SELECT aps_order_entries.order_no,aps_order.customer_name,aps_order_entries._id, aps_order_entries.product_name, aps_order_entries.qty, aps_order_entries.price, aps_order_entries.product_vat,aps_order_entries.product_commission, aps_order_entries.bonus, aps_order_entries.comments, aps_order.total, aps_order.total_vat, aps_order.total_commission, aps_order.delivery_date, aps_order.order_date  "+joinConditions+" ORDER BY aps_order_entries."+sort_field+" "+sort_order;

        //String query        = "SELECT * FROM aps_order_entries WHERE order_no=\'"+order_no+"\' ORDER BY _id DESC";
        //String query        = "SELECT p.product_name, p.price, o.qty FROM aps_order_entries AS o JOIN productDB.aps_product as p ON p.product_id=o.product_id";

        Cursor results      =  db.rawQuery(query, null);


        return results;


    }

    public List<String> getCustomersWithIncompleteOrders(String sales_rep_id)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        List<String> outputIDs = new ArrayList();

        String query            =  "SELECT customer_id FROM aps_order WHERE order_status='"+ORDER_STATUS_INCOMPLETE+"' AND order_no LIKE '"+sales_rep_id+"%' ORDER BY customer_id ASC";
        Cursor results          =  db.rawQuery(query, null);

        if(results!=null)
        {
            results.moveToFirst();

            while(!results.isAfterLast())
            {
                outputIDs.add(results.getString(results.getColumnIndex("customer_id")));
                results.moveToNext();

            }

            return outputIDs;

        }


        return null;
    }



    public void updateOrderTotal(String order_no, double total, double total_vat, double total_commission)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("total", total);
        cvalues.put("total_vat", total_vat);
        cvalues.put("total_commission", total_commission);

        db.update(ORDER_TABLE,cvalues,"order_no='"+order_no+"'",null);


    }


    public void updateInvoiceType(String order_no, String invoice_type)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("invoice_type", invoice_type);

        db.update(ORDER_TABLE,cvalues,"order_no='"+order_no+"'",null);


    }

    public void updateSubmitLocation(String order_no,double submit_lat, double submit_lon)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("submit_lat", submit_lat);
        cvalues.put("submit_lon", submit_lon);

        db.update(ORDER_TABLE, cvalues, "order_no='" + order_no + "'", null);

    }

    public void updateVisitSubmitLocation(String order_no,double submit_lat, double submit_lon)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("visit_submit_lat", submit_lat);
        cvalues.put("visit_submit_lon", submit_lon);

        db.update(DOCTOR_VISIT_MASTER_TABLE, cvalues, "visit_id='" + order_no + "'", null);

    }


    public void updateCompleteStatus(String order_id, String order_status_complete)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("order_status", order_status_complete);
        cvalues.put("submit_status", SUBMIT_STATUS_PENDING);
        db.update(ORDER_TABLE,cvalues,"order_no='"+order_id+"'",null);

    }

    public void updateVisitCompleteStatus(String order_id, String order_status_complete)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("visit_status", order_status_complete);
        cvalues.put("visit_submit_status", SUBMIT_STATUS_PENDING);
        db.update(DOCTOR_VISIT_MASTER_TABLE,cvalues,"visit_id='"+order_id+"'",null);

    }

    public void updatePrescriptionGroupTable(String group_id, String doc_code, String doc_name, String taken_by, String image_path, String completion_stat)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("doctor_code", doc_code);
        cvalues.put("doctor_name", doc_name);
        cvalues.put("taken_by", taken_by);
        cvalues.put("image_upload_path",image_path);
        cvalues.put("submit_status", "PENDING");
        cvalues.put("completion_status", completion_stat);

        db.update(PRESCRIPTION_GROUP_TABLE, cvalues, "group_id='" + group_id + "'", null);


    }

    public void addRemarks(String order_id, String remarks)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("remarks", remarks);
        db.update(ORDER_TABLE,cvalues,"order_no='"+order_id+"'",null);

    }

    public void addVisitRemarks(String order_id, String remarks, String visited_with)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   = new ContentValues();


        cvalues.put("visit_remarks", remarks);
        cvalues.put("visited_with", visited_with);
        db.update(DOCTOR_VISIT_MASTER_TABLE,cvalues,"visit_id='"+order_id+"'",null);

    }

    public Cursor getAutocompletePrescriptionInfoForPending(String searchTerm)
    {
        SQLiteDatabase db               =  this.getWritableDatabase();
        String query                    = "SELECT DISTINCT doctor_name FROM "+PRESCRIPTION_GROUP_TABLE+ " WHERE doctor_name LIKE \"%"+searchTerm+"%\" AND submit_status='PENDING' ORDER BY creation_time DESC LIMIT 0,10";
        Cursor results                  =  db.rawQuery(query, null);

        return results;
    }

    public Cursor getAllPendingPrescriptionImages()
    {
        SQLiteDatabase db               =  this.getWritableDatabase();
        String query                    = "SELECT * FROM "+PRESCRIPTION_IMAGES_TABLE+ " WHERE deleted=2 AND submit_status=\"UNSENT\"";
        Cursor results                  =  db.rawQuery(query, null);
        return results;

    }

    public List<OrderInfo> getAutoCompleteOrderInfoForPending(String searchTerm) {


        SQLiteDatabase db               =  this.getWritableDatabase();
        String query                    = "SELECT DISTINCT customer_name FROM "+ORDER_TABLE+ " WHERE (order_no LIKE \"%"+searchTerm+"%\" OR customer_name LIKE \"%"+searchTerm+"%\") AND submit_status='PENDING' AND order_status='COMPLETE' AND delete_status=0 ORDER BY order_date DESC LIMIT 0,10";
        Cursor results                  =  db.rawQuery(query, null);

        List<OrderInfo> searchResults   = new ArrayList<OrderInfo>();

        if(results.moveToFirst())
        {
            do
            {
                //String order_no                 = results.getString(results.getColumnIndex("order_no"));
                String trade_name               = results.getString(results.getColumnIndex("customer_name"));
                //String order_date               = results.getString(results.getColumnIndex("order_date"));
                //double total                    = results.getDouble(results.getColumnIndex("total"));


                OrderInfo oInfo              = new OrderInfo();

                //pInfo.setProductBarcode(product_barcode);
                //oInfo.setOrderNo(order_no);
                oInfo.setCustomerName(trade_name);
                //oInfo.setOrderDate(order_date);
                //oInfo.setTotal(total);


                searchResults.add(oInfo);

            }while(results.moveToNext());





        }

        results.close();
        //db.close();




        return searchResults;
    }

    public List<VisitInfo> getAutoCompleteVisitInfoForPending(String searchTerm) {


        SQLiteDatabase db               =  this.getWritableDatabase();
        String query                    = "SELECT DISTINCT doctor_name FROM "+DOCTOR_VISIT_MASTER_TABLE+ " WHERE (visit_id LIKE \"%"+searchTerm+"%\" OR doctor_name LIKE \"%"+searchTerm+"%\") AND visit_submit_status='PENDING' AND visit_status='COMPLETE' AND delete_status=0 ORDER BY visit_date DESC LIMIT 0,10";
        Cursor results                  =  db.rawQuery(query, null);

        List<VisitInfo> searchResults   = new ArrayList<VisitInfo>();

        if(results.moveToFirst())
        {
            do
            {
                //String order_no                 = results.getString(results.getColumnIndex("order_no"));
                String trade_name               = results.getString(results.getColumnIndex("doctor_name"));
                //String order_date               = results.getString(results.getColumnIndex("order_date"));
                //double total                    = results.getDouble(results.getColumnIndex("total"));


                VisitInfo oInfo              = new VisitInfo();

                //pInfo.setProductBarcode(product_barcode);
                //oInfo.setOrderNo(order_no);
                oInfo.setDoctorName(trade_name);
                //oInfo.setOrderDate(order_date);
                //oInfo.setTotal(total);


                searchResults.add(oInfo);

            }while(results.moveToNext());





        }

        results.close();
        //db.close();




        return searchResults;
    }

    public boolean entryExistsInOrder(String order_id, String searchItem)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+ORDER_ENTRIES_TABLE+" WHERE order_no=\'"+order_id+"\' AND product_name='"+searchItem+"'";

        Cursor entry        =  db.rawQuery(query, null);

        if(entry.getCount()>0)
        {
            return true;

        }

        return false;


    }

    public boolean visitsExistsInOrder(String order_id, String searchItem)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+DOCTOR_VISIT_DETAILS_TABLE+" WHERE visit_id=\'"+order_id+"\' AND item_name='"+searchItem+"'";

        Cursor entry        =  db.rawQuery(query, null);

        if(entry.getCount()>0)
        {
            return true;

        }

        return false;


    }

    public void updateDeliveryDateIfExists(String order_no, String delivery_date)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   = new ContentValues();

        cvalues.put("delivery_date", delivery_date);
        db.update(ORDER_TABLE, cvalues, "order_" +
                "no='" + order_no + "'", null);

        db.close();

    }

    public void updateCollectionDateIfExists(String order_no, String collection_date)
    {
        SQLiteDatabase db       = this.getWritableDatabase();
        ContentValues cvalues   = new ContentValues();

        cvalues.put("collection_date", collection_date);
        db.update(ORDER_TABLE, cvalues, "order_" +
                "no='" + order_no + "'", null);

        db.close();

    }



    public void addToMsgTable(String msg_id, String msg_title, String msg_body, String publish_date, String pub_time, int deleted, int read,int sent, int msg_user_id, int parent_msg_id, String emp_id)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();


        cvalues.put("msg_id", msg_id);
        cvalues.put("msg_title", msg_title);
        cvalues.put("msg_body", msg_body);
        cvalues.put("publish_date",publish_date);
        cvalues.put("pub_time", pub_time);
        cvalues.put("deleted", deleted);
        cvalues.put("read", read);
        cvalues.put("sent",sent);
        cvalues.put("msg_user_id",msg_user_id);
        cvalues.put("parent_msg_id",parent_msg_id);
        cvalues.put("employee_code",emp_id);

        db.insert(MESSAGE_TABLE, null, cvalues);

        db.close();

    }

    public void addToMsgTableDummy(SQLiteDatabase db, int _id, String msg_id, String msg_title, String msg_body, String publish_date, int deleted, int read, int msg_user_id)
    {


        db.execSQL("insert into aps_messages values('" + _id + "','" + msg_id + "','" + msg_title + "','" + msg_body + "','" + publish_date + "','" + deleted + "','" + read + "','" + msg_user_id + "');");


    }



    public Cursor getMsgInfo(String user_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        Cursor c            =  db.rawQuery("SELECT * from aps_messages WHERE msg_user_id="+user_id+" AND sent=0",new String [] {});


        return c;
    }

    public Cursor getMsgInfoWithEmpCode(String user_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        Cursor c            =  db.rawQuery("SELECT * from aps_messages WHERE employee_code='"+user_id+"' AND sent=0 ORDER BY msg_id DESC",new String [] {});


        return c;
    }

    public Cursor getLastMsgStatus()
    {
        SQLiteDatabase db	= this.getReadableDatabase();
        Cursor c 			= db.rawQuery("SELECT * from aps_last_messages",new String [] {});
        //db.close();
        return c;
    }

    public Cursor getLastMsgStatus(String user_id)
    {
        SQLiteDatabase db	= this.getWritableDatabase();
        Cursor c 			= db.rawQuery("SELECT * FROM aps_last_messages WHERE u_id='"+user_id+"'",null);
        //db.close();
        return c;
    }


    public void addToLastMsgTable(String _id, String msg_id, String u_id) {
        SQLiteDatabase db = getWritableDatabase();
        super.onOpen(db);
        db.execSQL("insert into aps_last_messages values(NULL,'" + msg_id + "','"
                + u_id + "');");
        db.close();

    }

    public void updateLastMsgTable(String new_msg_id, String last_msg_id,
                                   String uid) {
        SQLiteDatabase db = getWritableDatabase();
        super.onOpen(db);
        db.execSQL("UPDATE aps_last_messages SET msg_id=" + new_msg_id
                + " WHERE msg_id=" + last_msg_id + " AND u_id=" + "'" + uid
                + "'");
        db.close();

    }

    public Cursor getSentMessages(String user_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * from aps_messages WHERE msg_user_id="+user_id+" AND sent=1";
        Cursor entry        =  db.rawQuery(query, null);

        return entry;

    }

    public Cursor getSentMessagesWithEmpCode(String user_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * from aps_messages WHERE employee_code='"+user_id+"' AND sent=1";
        Cursor entry        =  db.rawQuery(query, null);

        return entry;

    }


    public Cursor getProductSyncHash()
    {

        SQLiteDatabase db      =  this.getWritableDatabase();
        String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+PRODUCT_TAG_DB_SYNC+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;



    }



    public void insertProductSyncHash(String hash)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();


        Cursor c  = getProductSyncHash();

        if(c!=null)
        {
            if(c.getCount()==0)
            {

                //Insert hash if no entry is present

                cvalues.put("database_tag",APCDatabaseHelper.PRODUCT_TAG_DB_SYNC);
                cvalues.put("hashcode_latest",hash);
                db.insert(table_name3,null,cvalues);

            }else
            {
                //cvalues.clear();
                cvalues.put("hashcode_latest",hash);

                db.update(table_name3,cvalues,"database_tag='"+APCDatabaseHelper.PRODUCT_TAG_DB_SYNC+"'",null);



            }

        }




        db.close();

    }




    public Cursor getOtherProductSyncHash()
    {
        SQLiteDatabase db      =  this.getWritableDatabase();
        String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+OTHER_PRODUCTS_TAG_DB_SYNC+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;
    }

    public void insertOtherProductSyncHash(String hash)
    {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();


        Cursor c  = getOtherProductSyncHash();



        if(c!=null)
        {
            if(c.getCount()==0)
            {

                //Insert hash if no entry is present

                cvalues.put("database_tag",APCDatabaseHelper.OTHER_PRODUCTS_TAG_DB_SYNC);
                cvalues.put("hashcode_latest",hash);
                db.insert(table_name3,null,cvalues);

            }else
            {

                cvalues.clear();
                cvalues.put("hashcode_latest",hash);

                db.update(table_name3,cvalues,"database_tag='"+APCDatabaseHelper.OTHER_PRODUCTS_TAG_DB_SYNC+"'",null);



            }

        }




        db.close();
    }


    public Cursor getTLHash(String employee_code)
    {
        SQLiteDatabase db      =  this.getWritableDatabase();
        String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+ALL_TAG_DB_SYNC+"' AND employee_code='"+employee_code+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;

    }

    public void insertTLHash(String employee_code, String hash)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        int dummy_count         = 0;

        Cursor c  = getTLHash(employee_code);

        if(c!=null)
        {

            if(c.getCount()==0)
            {

                //Insert hash if no entry is present

                cvalues.put("database_tag",APCDatabaseHelper.ALL_TAG_DB_SYNC);
                cvalues.put("employee_code",employee_code);
                cvalues.put("hashcode_latest",hash);
                db.insert(table_name3,null,cvalues);

            }else
            {

                dummy_count = c.getCount();
                c.moveToFirst();

                System.out.println("Count: "+c.getString(c.getColumnIndex("database_tag")));
                cvalues.clear();
                cvalues.put("hashcode_latest",hash);

                db.update(table_name3,cvalues,"database_tag='"+APCDatabaseHelper.ALL_TAG_DB_SYNC+"' AND employee_code='"+employee_code+"'",null);


            }

        }




        db.close();

    }




    public Cursor getSRUserDBHash(String employee_code)
    {

        SQLiteDatabase db      =  this.getWritableDatabase();
        //String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+USER_TAG_DB_SYNC+"' AND employee_code='"+employee_code+"'";
        String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+USER_TAG_DB_SYNC+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;


    }

    public Cursor getSRUserDBHash()
    {

        SQLiteDatabase db      =  this.getWritableDatabase();
        String query           =  "SELECT * from "+table_name3+" WHERE database_tag='"+USER_TAG_DB_SYNC+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;


    }

    public void insertUserHash(String employee_code,String hash)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();


        //Cursor c  = getSRUserDBHash(employee_code);
        Cursor c  = getSRUserDBHash();

        if(c!=null)
        {
            if(c.getCount()==0)
            {

                //Insert hash if no entry is present

                cvalues.put("database_tag",APCDatabaseHelper.USER_TAG_DB_SYNC);
                //cvalues.put("employee_code",employee_code);
                cvalues.put("hashcode_latest",hash);

                db.insert(table_name3,null,cvalues);

            }else
            {
                cvalues.clear();
                cvalues.put("hashcode_latest", hash);
                //db.update(table_name3,cvalues,"database_tag='"+APCDatabaseHelper.USER_TAG_DB_SYNC+"' AND employee_code='"+employee_code+"'",null);
                db.update(table_name3,cvalues,"database_tag='"+APCDatabaseHelper.USER_TAG_DB_SYNC+"'",null);



            }

        }




        db.close();


    }

    public void insertPrescriptionGroup(String id, double start_lat, double start_lon)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("group_id", Integer.parseInt(id));
        cvalues.put("start_lat", start_lat);
        cvalues.put("start_lon", start_lon);
        cvalues.put("creation_time", System.currentTimeMillis());
        cvalues.put("submit_status", "PENDING");
        cvalues.put("completion_status", "INCOMPLETE");

        db.insert(PRESCRIPTION_GROUP_TABLE, null, cvalues);

        db.close();

    }

    public void insertPrescriptionGroupImages(String group_id, String picturePath, long l, int i)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("group_id", Integer.parseInt(group_id));
        cvalues.put("image_name", picturePath);
        cvalues.put("date_time", l);
        cvalues.put("deleted", 0);
        cvalues.put("submit_status", "UNSENT");

        db.insert(PRESCRIPTION_IMAGES_TABLE, null, cvalues);

        db.close();
    }

    public Cursor getImageSetForPrescription(String group_id) {

        SQLiteDatabase db      =  this.getWritableDatabase();
        String query           =  "SELECT * from "+PRESCRIPTION_IMAGES_TABLE+" WHERE group_id='"+group_id+"'";
        Cursor entry           =  db.rawQuery(query, null);

        return entry;


    }

    public boolean entryExistsInPrescription(String group_id, String searchItem) {


        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+PRESCRIPTION_DETAILS_TABLE+" WHERE master_group_id=\'"+group_id+"\' AND product_name='"+searchItem+"'";

        Cursor entry        =  db.rawQuery(query, null);

        if(entry.getCount()>0)
        {
            return true;

        }

        return false;


    }

    public void insertPrescriptionDetails(String group_id, String product_id, String product_name)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("master_group_id", Integer.parseInt(group_id));
        cvalues.put("product_code", product_id);
        cvalues.put("product_name", product_name);
        db.insert(PRESCRIPTION_DETAILS_TABLE, null, cvalues);

        db.close();
    }

    public Cursor getPrescriptionEntry(String group_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+PRESCRIPTION_DETAILS_TABLE+" WHERE master_group_id=\'"+group_id+"\' ORDER BY master_group_id DESC";

        Cursor entry        =  db.rawQuery(query, null);


        return entry;
    }

    public void removeItemFromPrescription(String group_id, String item_name)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "DELETE FROM "+PRESCRIPTION_DETAILS_TABLE+" WHERE master_group_id="+group_id+" AND product_name='"+item_name+"'";
        db.execSQL(query);
    }

    public Cursor getPrescriptionGroup(String group_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+PRESCRIPTION_GROUP_TABLE+" WHERE group_id=\'"+group_id+"\'";

        Cursor entry        =  db.rawQuery(query, null);


        return entry;
    }


    public void removePrescription(String group_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        // Remove Prescription Entry from images table

        String query        = "DELETE FROM "+PRESCRIPTION_IMAGES_TABLE+" WHERE group_id="+group_id;
        db.execSQL(query);

        // Remove prescription details from details table

        query               = "DELETE FROM "+PRESCRIPTION_DETAILS_TABLE+" WHERE master_group_id="+group_id;
        db.execSQL(query);

        // Remove prescription group from prescription group table

        query               = "DELETE FROM "+PRESCRIPTION_GROUP_TABLE+" WHERE group_id="+group_id;
        db.execSQL(query);


        db.close();

    }

    public Cursor getPrescriptionImages(String prescription_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+PRESCRIPTION_IMAGES_TABLE+" WHERE group_id=\'"+prescription_id+"\'";

        Cursor entry        =  db.rawQuery(query, null);


        return entry;

    }

    public void updateSelectStatusOfPrescriptionImages(ArrayList<String> selectedListOfOrders)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();

        // Remove Prescription Entry from images table

        for(int i =0; i<selectedListOfOrders.size();++i)
        {
            String query        = "UPDATE "+PRESCRIPTION_IMAGES_TABLE+" SET deleted='2' WHERE _id=\""+selectedListOfOrders.get(i)+"\"";
            db.execSQL(query);
        }

        db.close();

    }

    public Cursor getPrescriptionImagesForSubmission(String group_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        =  "SELECT * FROM "+PRESCRIPTION_IMAGES_TABLE+" WHERE group_id=\'"+group_id+"\' AND deleted='2'";

        Cursor entry        =  db.rawQuery(query, null);


        return entry;


    }

    public void updatePrescriptionImageStatus(String image_entry_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "UPDATE "+PRESCRIPTION_IMAGES_TABLE+" SET submit_status=\"SENT\" WHERE _id=\""+image_entry_id+"\"";

        db.execSQL(query);
        db.close();

    }

    public void insertAttendanceEntry(String employee_code, String clock_type, Long time, String date)
    {
        SQLiteDatabase db       = getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("employee_code", employee_code);
        cvalues.put("clock_type", clock_type);
        cvalues.put("date_time", time);
        cvalues.put("date", date);
        cvalues.put("submit_status", "UNSENT");

        db.insert(EMPLOYEE_ATTENDANCE, null, cvalues);

        db.close();

    }

    public Cursor getAttendanceEntryID(String employee_code, String clock_type)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+EMPLOYEE_ATTENDANCE+" WHERE employee_code=\'"+employee_code+"\' AND submit_status='UNSENT' AND clock_type='"+clock_type+"' ORDER BY _id DESC";
        Cursor entry        =  db.rawQuery(query, null);

        return entry;

    }

    public Cursor getAttendanceEntriesFromDate(String employee_code, String date)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+EMPLOYEE_ATTENDANCE+" WHERE employee_code=\'"+employee_code+"\' AND submit_status='UNSENT' AND date LIKE '"+date+"'";
        Cursor entry        =  db.rawQuery(query, null);

        return entry;


    }



    public void updateAttendanceEntryLocationsWithEntryID(String entry_id, double lat, double lon)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "UPDATE "+EMPLOYEE_ATTENDANCE+" SET lat="+lat+", lon="+lon+" WHERE _id=\""+entry_id+"\"";

        db.execSQL(query);
        db.close();



    }

    public void removeImageFromTable(String entry_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();

        String query        = "DELETE FROM "+PRESCRIPTION_IMAGES_TABLE+" WHERE _id="+entry_id;
        db.execSQL(query);

        db.close();

    }


    public void messageRead(int entry_id)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "UPDATE "+MESSAGE_TABLE+" SET read=1 WHERE _id=\""+entry_id+"\"";

        db.execSQL(query);
        //db.close();
    }

    public List<String> getDoctorsWithIncompleteVisits(String employee_id)
    {


        SQLiteDatabase db = this.getWritableDatabase();
        List<String> outputIDs = new ArrayList();

        String query            =  "SELECT doctor_code,doctor_name FROM aps_doctor_visit_master WHERE visit_status='INCOMPLETE' AND visit_id LIKE '"+employee_id+"%' ORDER BY doctor_code ASC";
        Cursor results          =  db.rawQuery(query, null);

        if(results!=null)
        {
            results.moveToFirst();

            while(!results.isAfterLast())
            {
                outputIDs.add(results.getString(results.getColumnIndex("doctor_code")));
                results.moveToNext();

            }

            return outputIDs;

        }


        return null;
    }
}
