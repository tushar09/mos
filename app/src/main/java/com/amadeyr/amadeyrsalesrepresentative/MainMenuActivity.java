package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncAutoAttendanceUpdaterTask;
import com.amadeyr.amadeyrsalesrepresentative.tasks.UpdateUserTokenTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;


public class MainMenuActivity extends ActionBarActivity {

    //private EditText edtTextEmployeeID;
    private Button btnScan, btnSubmit, btnMessages, btnDoctorVisit, btnSchedule, btnPrescription, btnReports, btnShift;
    private APCNetworkHandler nHandler;
    private String pInfo;
    private com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo pInfoObj;
    private APCDatabaseHelper dbHelper;
    private String dbLastHashcode, dbHashcodeFromServer, latest_product_db_hash, latest_other_product_db_hash, latest_tl_hash, latest_user_db_hash, user_designation, employee_code, user_token;
    private ASRErrorMessageView errorMsgObj;
    private APCProductDatabaseHelper pdbHelper, userDBHelper;
    private String networkURL = "";// "http://202.51.189.20/rpo_latest/public/autodata/description";
    private String downloadURL = "";
    public int sync_count = 0;
    private SharedPreferences sp;
    private boolean sync_required, is_team_leader, shift_started;
    private int total_sync_required;
    public static final String TEAM_LEAD_ROLE = "SUPERVISOR";
    public static final String SALES_REP_ROLE = "SALESREP";
    public LinearLayout llShiftStartButton;
    public ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        //nHandler = new APCNetworkHandler("http://202.51.189.20/rpo_latest/public/autodata/description",this.getApplicationContext());
        //int pending_orders_count = dbHelper.getPendingSubmissions(employee_code).getCount();

        //dbHelper    = new APCDatabaseHelper(getApplicationContext(),"apsDB", null,2);
        initializeDatabaseConnections();

        sp = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        networkURL = sp.getString("getURL", "") + "/fetchdatabaseupdate";
        downloadURL = sp.getString("downloadURL", "") + "/apsProductDB.zip";
        user_designation = sp.getString("user_designation", "");
        employee_code = sp.getString("username", "");
        sync_required = sp.getBoolean("sync_required", false);
        user_token = sp.getString("user_token", "");
        shift_started = sp.getBoolean("shift_started", false);
        errorMsgObj = new ASRErrorMessageView(MainMenuActivity.this);
        pd = new ProgressDialog(MainMenuActivity.this);


        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pd.setIndeterminate(false);
        pd.setMax(100);
        pd.setCancelable(false);

        llShiftStartButton = (LinearLayout) findViewById(R.id.llShiftStart);
        btnShift = (Button) findViewById(R.id.btnStart);
        btnScan = (Button) findViewById(R.id.btnMainMenuScan);
        btnPrescription = (Button) findViewById(R.id.btnMainMenuPrescription);
        btnSubmit = (Button) findViewById(R.id.btnMainMenuSubmit);
        btnMessages = (Button) findViewById(R.id.btnMMMessages);
        btnDoctorVisit = (Button) findViewById(R.id.btnMainMenuDoctor);
        btnReports = (Button) findViewById(R.id.btnReport);

        if (shift_started) {

            llShiftStartButton.setBackgroundResource(R.drawable.shift_end_button_bg);
            btnShift.setText(getResources().getString(R.string.lblEndShift));
        }

        try {
            is_team_leader = userDBHelper.isTeamLeader(employee_code);
            setPendingSubmitCounts();
        } catch (SQLiteException e) {
            e.printStackTrace();
            is_team_leader = false;
        }


        dbLastHashcode = "sadlqjkwhqlnksd9i";

        String source_activity = getIntent().getExtras().getString("source_activity");

        ProgressDialog pd = new ProgressDialog(MainMenuActivity.this);


//        if(sync_required)
//        {
//            btnScan.setEnabled(false);
//            btnDoctorVisit.setEnabled(false);
//
//
//        }

        //sync_required                   = getIntent().getExtras().getBoolean("sync_required");
        latest_product_db_hash = "";
        latest_other_product_db_hash = "";
        latest_tl_hash = "";
        latest_user_db_hash = "";

        if (source_activity.contentEquals("LogIn")) {
            dbHashcodeFromServer = getIntent().getExtras().getString("newHash");

        } else if (source_activity.contentEquals("DeviceStatusActivity")) {
            //sync_required                   = getIntent().getExtras().getBoolean("sync_required");
            latest_product_db_hash = getIntent().getExtras().getString("latest_product_db_hash");
            latest_other_product_db_hash = getIntent().getExtras().getString("latest_other_product_db_hash");
            latest_tl_hash = getIntent().getExtras().getString("latest_tl_hash");
            latest_user_db_hash = getIntent().getExtras().getString("latest_user_db_hash");

        } else {

            dbHashcodeFromServer = "";
        }

        btnShift.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (sp.getBoolean("shift_started", false)) {
                    sp.edit().putBoolean("shift_started", false).commit();
                    llShiftStartButton.setBackgroundResource(R.drawable.prescription_bar_theme);
                    btnShift.setText(getResources().getString(R.string.lblStartShift));
                    long c_time = System.currentTimeMillis();
                    dbHelper.insertAttendanceEntry(employee_code, "END", c_time, APCDateHelper.getDateForPrescriptionFromMilliseconds(c_time));
                    setAttendanceLocation("END");


                } else {
                    llShiftStartButton.setBackgroundResource(R.drawable.shift_end_button_bg);
                    sp.edit().putBoolean("shift_started", true).commit();
                    btnShift.setText(getResources().getString(R.string.lblEndShift));
                    long c_time = System.currentTimeMillis();
                    dbHelper.insertAttendanceEntry(employee_code, "START", c_time, APCDateHelper.getDateForPrescriptionFromMilliseconds(c_time));
                    setAttendanceLocation("START");

                }

                return false;
            }
        });


        btnMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent msgBoxStarter = new Intent(MainMenuActivity.this, APCMessageActivity.class);
                startActivity(msgBoxStarter);

            }
        });

        btnReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getConnectionStatus()) {
                    Intent reportViewStarter = new Intent(MainMenuActivity.this, ASRReportViewActivity.class);
                    startActivity(reportViewStarter);
                } else {
                    new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage(getResources().getString(R.string.errorNoCannotOpen), getResources().getString(R.string.errorInternetConnectionUnavailable));

                }

            }
        });
        //dbHelper.fillWithDummyData();
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dbHelper.getWritableDatabase().close();

                boolean product_required = sp.getBoolean("product_required", true);
                boolean other_product_required = sp.getBoolean("other_product_required", true);
                boolean user_db_required = sp.getBoolean("user_db_required", true);

                //sync_required           = (pro);
                SharedPreferences sp2 = getSharedPreferences("apcpref", Context.MODE_PRIVATE);

                Log.e("p o us", product_required + " " + other_product_required + " " + user_db_required);
                if (product_required || other_product_required || user_db_required) {

                    //downloadProductDB(downloadURL);

                    final ASRErrorMessageView syncM = new ASRErrorMessageView(MainMenuActivity.this);
                    syncM.addExtraAction(getResources().getString(R.string.lblBtnSync));
                    Button sButton = syncM.getActionButton2Instance();
                    sButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Log.e("downloadProductDB", downloadURL);
                            downloadProductDB(downloadURL);
                            syncM.closeScreen();
                            //ASRDatabaseFactory.refreshDBConnections(getApplicationContext());

                        }
                    });

                    syncM.showErrorMessage(getResources().getString(R.string.errorDatabaseOutdated), getResources().getString(R.string.errorDatabaseOutdatedMessage));
                    return;
                }else {
                    //Log.e("downloadProductDB", downloadURL);
                }

                //initializeDatabaseConnections();
                //ASRDatabaseFactory.refreshDBConnections(getApplicationContext());
                ASRDatabaseFactory.refreshDBConnections(getApplicationContext());

                try

                {

                    is_team_leader = userDBHelper.isTeamLeader(employee_code);

                } catch (SQLiteException e) {
                    ASRErrorMessageView am = new ASRErrorMessageView(MainMenuActivity.this);

                    am.addExtraAction(getResources().getString(R.string.lblButtonDownloadAgain));

                    am.getActionButton2Instance().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            resetSyncStatus();
                            downloadProductDB(downloadURL);

                        }
                    });

                    am.showErrorMessage(getResources().getString(R.string.errorFileCorruptedTitle), getResources().getString(R.string.errorFileCorruptedMessage));
                    e.printStackTrace();
                    return;
                }


                //sp2.edit().putString("user_id","SP-100").commit();\
                Log.i("Employee Code: ", "" + employee_code);
                Log.i("Is Team Leader ?", "" + is_team_leader);

                if (is_team_leader) {

                    sp.edit().putString("user_role", TEAM_LEAD_ROLE).commit();

                } else {
                    sp.edit().putString("user_role", SALES_REP_ROLE).commit();

                }

                String user_role = sp2.getString("user_role", "");

                Log.i("User Role: ", user_role);

                boolean cameraModeEnabled = sp2.getBoolean("scanMode", false);

                if (cameraModeEnabled) {
                    scanNow();

                } else {
                    Intent HHSScreenStarter = null;

                    try {

                        if (user_role.contentEquals(TEAM_LEAD_ROLE)) {
                            HHSScreenStarter = new Intent(MainMenuActivity.this, ASRTeamLeaderActivity.class);

                        } else if (user_role.contentEquals(SALES_REP_ROLE)) {
                            HHSScreenStarter = new Intent(MainMenuActivity.this, APSCustomerSelectionActivity.class);

                        }
                        //Intent HHSScreenStarter = new Intent(MainMenuActivity.this,DataEntryScreenActivity.class);
                        //Intent HHSScreenStarter = new Intent(MainMenuActivity.this,APSCustomerSelectionActivity.class);

                        if (HHSScreenStarter != null) {
                            HHSScreenStarter.putExtra("source_activity", "chemist");
                            HHSScreenStarter.putExtra("employee_id", employee_code);
                            startActivity(HHSScreenStarter);
                        }

                    } catch (SQLiteException e) {
                        e.printStackTrace();
                        errorMsgObj.showErrorMessage("Error 101", "No database present ! Please select sync option from menu to download database");

                    }


                }


                //scanNow();
            }
        });


        btnDoctorVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean product_required = sp.getBoolean("product_required", true);
                boolean other_product_required = sp.getBoolean("other_product_required", true);
                boolean user_db_required = sp.getBoolean("user_db_required", true);

                //sync_required           = (pro);

                if (product_required || other_product_required || user_db_required) {
//                    new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage("Database not up to date","Please sync your database by pressing \"Sync\" on the Menu");
//                    return;
                    final ASRErrorMessageView syncM = new ASRErrorMessageView(MainMenuActivity.this);
                    syncM.addExtraAction(getResources().getString(R.string.lblBtnSync));
                    Button sButton = syncM.getActionButton2Instance();
                    sButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            downloadProductDB(downloadURL);
                            syncM.closeScreen();
                            //ASRDatabaseFactory.refreshDBConnections(getApplicationContext());
                        }
                    });

                    syncM.showErrorMessage(getResources().getString(R.string.errorDatabaseOutdated), getResources().getString(R.string.errorDatabaseOutdatedMessage));
                    return;
                }

                ASRDatabaseFactory.refreshDBConnections(getApplicationContext());


                try

                {

                    is_team_leader = userDBHelper.isTeamLeader(employee_code);

                } catch (SQLiteException e) {
                    ASRErrorMessageView am = new ASRErrorMessageView(MainMenuActivity.this);
                    am.addExtraAction(getResources().getString(R.string.lblButtonDownloadAgain));
                    am.getActionButton2Instance().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            resetSyncStatus();
                            downloadProductDB(downloadURL);

                        }
                    });

                    am.showErrorMessage(getResources().getString(R.string.errorFileCorruptedDatabaseTitle), getResources().getString(R.string.errorFileCorruptedDatabaseMessage));


                    e.printStackTrace();
                    return;
                }

                if (is_team_leader) {

                    sp.edit().putString("user_role", TEAM_LEAD_ROLE).commit();
                } else {
                    sp.edit().putString("user_role", SALES_REP_ROLE).commit();

                }

                dbHelper.getWritableDatabase().close();

                SharedPreferences sp2 = getSharedPreferences("apcpref", Context.MODE_PRIVATE);

                sp2.edit().putString("user_id", employee_code).commit();
                //sp2.edit().putString("user_role","SUPERVISOR").commit();

                String user_role = sp2.getString("user_role", "");


                boolean cameraModeEnabled = sp2.getBoolean("scanMode", false);

                // ASRDatabaseFactory.refreshDBConnections(getApplicationContext());


                if (cameraModeEnabled) {
                    scanNow();

                } else {
                    Intent HHSScreenStarter = null;

                    if (user_role.contentEquals(TEAM_LEAD_ROLE)) {
                        HHSScreenStarter = new Intent(MainMenuActivity.this, ASRTeamLeaderActivity.class);


                    } else if (user_role.contentEquals(SALES_REP_ROLE)) {
                        HHSScreenStarter = new Intent(MainMenuActivity.this, ASRDoctorSelectionActivity.class);

                        HHSScreenStarter.putExtra("employee_id", employee_code);

                    }


                    //Intent HHSScreenStarter = new Intent(MainMenuActivity.this,DataEntryScreenActivity.class);
                    //Intent HHSScreenStarter = new Intent(MainMenuActivity.this,APSCustomerSelectionActivity.class);
                    if (HHSScreenStarter != null) {
                        HHSScreenStarter.putExtra("source_activity", "doctor");
                        startActivity(HHSScreenStarter);
                    }

                }


            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent testScrn = new Intent(MainMenuActivity.this, ASRDualSubmissionActivity.class);
                startActivity(testScrn);


            }
        });


        btnPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean product_required = sp.getBoolean("product_required", true);
                boolean other_product_required = sp.getBoolean("other_product_required", true);
                boolean user_db_required = sp.getBoolean("user_db_required", true);

                //sync_required           = (pro);

                if (product_required || other_product_required || user_db_required) {
//                    new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage("Database not up to date","Please sync your database by pressing \"Sync\" on the Menu");
//                    return;
                    final ASRErrorMessageView syncM = new ASRErrorMessageView(MainMenuActivity.this);
                    syncM.addExtraAction(getResources().getString(R.string.lblBtnSync));
                    Button sButton = syncM.getActionButton2Instance();

                    sButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            downloadProductDB(downloadURL);
                            syncM.closeScreen();
                            //ASRDatabaseFactory.refreshDBConnections(getApplicationContext());
                        }
                    });

                    syncM.showErrorMessage(getResources().getString(R.string.errorDatabaseOutdated), getResources().getString(R.string.errorDatabaseOutdatedMessage));
                    return;
                }

                ASRDatabaseFactory.refreshDBConnections(getApplicationContext());

                Intent presScrn = new Intent(MainMenuActivity.this, ASRPrescriptionGroupActivity.class);
                startActivity(presScrn);

            }
        });


//        btnSchedule.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//
//                if(is_team_leader)
//                {
//
//                    sp.edit().putString("user_role",TEAM_LEAD_ROLE).commit();
//
//                }else
//                {
//                    sp.edit().putString("user_role",SALES_REP_ROLE).commit();
//
//                }
//
//
//
//                SharedPreferences sp2       = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
//
//                //sp2.edit().putString("user_role","SUPERVISOR").commit();
//
//                String user_role            = sp2.getString("user_role","");
//
//                //boolean cameraModeEnabled   = sp2.getBoolean("scanMode",false);
//
//
//                Intent i = new Intent(MainMenuActivity.this,ASRInstituteSelectionActivity.class);
//                i.putExtra("source_activity","MainMenuActivity");
//                i.putExtra("employee_id",employee_code);
//                startActivity(i);
//            }
//        });

        //if(sync_required) Toast.makeText(getApplicationContext(),"yes",Toast.LENGTH_LONG).show();

        //fetchProductInfoFromServer();
    }

    public void setAttendanceLocation(String type) {
        Cursor last_id = dbHelper.getAttendanceEntryID(employee_code, type);

        int entry_id;

        if (last_id != null) {
            last_id.moveToFirst();
            entry_id = last_id.getInt(last_id.getColumnIndex("_id"));
            new AsyncAutoAttendanceUpdaterTask(getApplicationContext(), "" + entry_id).execute();
        }

    }

    public void scanNow() {

        Log.d("test", "button works !");


    }

    private void initializeDatabaseConnections() {
        dbHelper = APCDatabaseHelper.getInstance(getApplicationContext());

        dbHelper.getWritableDatabase();
        //pdbHelper   = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCDatabaseHelper.DATABASE_VERSION);
        pdbHelper = ASRDatabaseFactory.getProductsDBInstance(getApplicationContext());
        //userDBHelper = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCDatabaseHelper.DATABASE_VERSION);
        userDBHelper = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());


    }

    private void setPendingSubmitCounts() {

        Cursor pendingSubmissionsCursor = (is_team_leader == true) ? dbHelper.getPendingSubmissions("") : dbHelper.getPendingSubmissions(employee_code);
        Cursor pendingVisitCursor = (is_team_leader == true) ? dbHelper.getPendingVisits("") : dbHelper.getPendingVisits(employee_code);
        Cursor pendingPrescriptionCursor = (is_team_leader == true) ? dbHelper.getPendingSubmissions("") : dbHelper.getPendingSubmissions("");

        int pending_orders_count = 0, pending_visits_count = 0, pending_prescription_count = 0;

        if (pendingSubmissionsCursor != null) {
            pending_orders_count = pendingSubmissionsCursor.getCount();

        }

        if (pendingVisitCursor != null) {
            pending_visits_count = pendingVisitCursor.getCount();
        }

        if (pendingPrescriptionCursor != null) {

            pending_prescription_count = pendingPrescriptionCursor.getCount();
        }

        if (pending_orders_count == 0 && pending_visits_count == 0 && pending_prescription_count == 0) {
            btnSubmit.setText(getString(R.string.lblMainMenuSubmitButton) + "");

        } else {
            btnSubmit.setText(getString(R.string.lblMainMenuSubmitButton) + " (" + pending_orders_count + "/" + pending_visits_count + "/" + pending_prescription_count + ")");

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
//        Intent nxtScrSearch     = new Intent(MainMenuActivity.this,APCSearchProductActivity.class);
//
//        if(scanningResult!=null)
//        {
//            //Toast.makeText(getApplicationContext(), scanningResult.getContents(), Toast.LENGTH_LONG).show();
//            String barcodeStr       = scanningResult.getContents();
//
//            Intent hhScannerEntryScreen = new Intent(MainMenuActivity.this,APCHandheldScannerEntryScreen.class);
//            hhScannerEntryScreen.putExtra("cameraMode",true);
//            hhScannerEntryScreen.putExtra("product_barcode",barcodeStr);
//
//            startActivity(hhScannerEntryScreen);
//
//            //fetchInfoFromServer(barcodeStr);
//
//
//        }else
//        {
//
//
//            Toast.makeText(getApplicationContext(),"No data",Toast.LENGTH_LONG).show();
//
//            //startActivity(MainMenuActivity.this,APCSearchProductActivity.class);
//
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu_orig, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.morig_action_settings) {
            Intent d = new Intent(MainMenuActivity.this, AndroidDatabaseManager.class);
            startActivity(d);

            return true;

        } else if (id == R.id.morig_action_logout) {
            sp.edit().putString("user_token", "").commit();
            sp.edit().putString("username", "").commit();
            sp.edit().putString("password", "").commit();
            sp.edit().putBoolean("product_required", false).commit();
            sp.edit().putBoolean("other_product_required", false).commit();
            sp.edit().putBoolean("user_db_required", false).commit();
            sp.edit().putBoolean("sync_required", false).commit();


            Intent d = new Intent(MainMenuActivity.this, LogInActivity.class);

            startActivity(d);
            finish();
            return true;

        } else if (id == R.id.morig_action_sync) {


            // to force sync all flags are set to false to create an unsynced scenario

            resetSyncStatus();
            downloadProductDB(downloadURL);


            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private boolean getConnectionStatus() {
        ConnectivityManager connMngr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = connMngr.getActiveNetworkInfo();


        if (nInfo != null && nInfo.isConnected()) {
            //Toast.makeText(context,"No Internet Connection Available",Toast.LENGTH_LONG).show();
            return true;
        }

        return false;


    }

    private boolean tokenExists() {
        if (sp.getString("user_token", "").isEmpty()) {
            return false;

        }

        return true;
    }


    private void resetSyncStatus() {

        sp.edit().putBoolean("product_required", true).commit();
        sp.edit().putBoolean("other_product_required", true).commit();
        sp.edit().putBoolean("user_db_required", true).commit();
    }


    public void downloadProductDB(String url) {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = connMgr.getActiveNetworkInfo();

        boolean product_required = sp.getBoolean("product_required", true);
        boolean other_product_required = sp.getBoolean("other_product_required", true);
        boolean tl_hash_required = sp.getBoolean("tl_hash_required", true);
        boolean user_db_required = sp.getBoolean("user_db_required", true);
        String bUrl = sp.getString("postURL", "");
        String u_token = sp.getString("user_token", "");
        //String u_token                             = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NSIsImlzcyI6Imh0dHA6XC9cLzIwMi41MS4xODkuMjBcL21lcmdlZF9zYWxlc1wvcHVibGljXC9hdXRobW9iIiwiaWF0IjoxNDUwMDA3NjYwLCJleHAiOjE0NTAwMTMwNjAsIm5iZiI6MTQ1MDAwNzY2MCwianRpIjoiMDlmZTc5MGJhZGVmMjkyZDc2MDQ3NDhiNjhhMjk2ZDMifQ.eqkGG7aXL66Wvo2ZcUc-2VqoNiKRkbER3_c8Wescjtk";
        total_sync_required = 0;

        if (nInfo != null && nInfo.isConnected()) {


            try {

                String product_url = bUrl + ASRConfig.PRODUCT_SYNC_URL_EXTENSION + URLEncoder.encode(u_token, "UTF-8");
                //Log.e("product_url", product_url);
                String other_product_url = bUrl + ASRConfig.OTHER_PRODUCT_SYNC_URL_EXTENSION + URLEncoder.encode(u_token, "UTF-8");
                String user_url = bUrl + ASRConfig.USER_SYNC_URL_EXTENSION + URLEncoder.encode(u_token, "UTF-8");


                if (product_required) {
                    total_sync_required++;
                    new AccessAPCServerTask(product_url, "URL", ASRConfig.PRODUCTS_DB_ZIP_FILE_NAME).execute();

                }

                if (other_product_required) {
                    total_sync_required++;
                    new AccessAPCServerTask(other_product_url, "URL", ASRConfig.OTHER_PRODUCTS_DB_ZIP_FILE_NAME).execute();

                }

                if (user_db_required) {
                    total_sync_required++;
                    new AccessAPCServerTask(user_url, "URL", ASRConfig.USER_DB_ZIP_FILE_NAME).execute();


                }

                if (!product_required && !other_product_required && !user_db_required) {
                    Toast.makeText(getApplicationContext(), "Database is up to date", Toast.LENGTH_LONG).show();

                }


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        } else {
            new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage(getResources().getString(R.string.errorNoInternetConnection), getResources().getString(R.string.errorNoInternetConnectionMessage));
            //Log.e("Outer if", "exe");

        }


    }

    @Override
    protected void onResume() {

        super.onResume();

        if (sp.getBoolean("shift_started", false)) {
            llShiftStartButton.setBackgroundResource(R.drawable.shift_end_button_bg);

        } else {
            llShiftStartButton.setBackgroundResource(R.drawable.prescription_bar_theme);

        }
        setPendingSubmitCounts();

    }

    private class AccessAPCServerTask extends AsyncTask<String, Integer, Object> {
        //private ProgressDialog pd;
        private String DATA_FETCH_TYPE;
        private String FETCH_TYPE_URL = "URL";
        private String FETCH_TYPE_FILE = "FILE";
        private String task_url;
        private String DL_FILE_NAME;


        public AccessAPCServerTask(String url, String fetch_type, String file_name) {
            task_url = new String(url);
            DATA_FETCH_TYPE = new String(fetch_type);
            DL_FILE_NAME = new String(file_name);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //pd.setCancelable(false);
            pd.setMessage("Downloading data updates from server. Please wait.");
            pd.show();
        }

        @Override
        protected Object doInBackground(String... urls) {

            UpdateUserTokenTask update_token = new UpdateUserTokenTask(getApplicationContext(), null);
            update_token.getNewUserToken();

            HashMap<String, String> postParams = new HashMap<>();
            postParams.put("user_token", user_token);


            if (this.DATA_FETCH_TYPE.contentEquals(FETCH_TYPE_URL)) {
                String dlUrl = "";
                String dlHash = "";
                String checkPreparationStatusUrl = sp.getString("postURL", "") + ASRConfig.CHECK_HASH_URL_EXTENSION + user_token + "&hash=";
                String queueStatusJSON = "";
                boolean queueStatus = false;
                String urlHashJSON = getDownloadURL(postParams);

//                System.out.println("Task Url: "+this.task_url);
                //Log.e("URL Hash JSON", "URL Hash JSON:" + urlHashJSON);
                int retry_count = 0;

                JSONArray response_array = null;
                JSONArray q_response_array = null;

                try {
                    Log.e("urlHashJSON", urlHashJSON + "nai");
                    response_array = new JSONArray(urlHashJSON);

                    JSONObject response_object = response_array.getJSONObject(0);
                    //Log.e("response_object", response_object.toString());


                    if (response_object.has("error")) {
                        //Log.e("token time out", "Token Timed Out or Invalid");
                        //pd.dismiss();

                        if (new UpdateUserTokenTask(getApplicationContext(), null).getNewUserToken()) {
                            //Log.e("token success", "Token Update Successfull");
                        }
                        Log.e("return", "return from here");
                        return false;

                    }


                    dlUrl = response_object.getString("url");
                    dlHash = response_object.getString("hash");
                    checkPreparationStatusUrl += dlHash;
                    //Log.e("checkPrepurl", checkPreparationStatusUrl);

                    while (retry_count < 3 && !queueStatus) {
                        queueStatusJSON = getDownloadStatus(checkPreparationStatusUrl);
                        q_response_array = new JSONArray(queueStatusJSON);

                        JSONObject qStatusJSONObj = q_response_array.getJSONObject(0);
                        //Log.e("qStatusJSONObj", qStatusJSONObj.toString());
                        queueStatus = qStatusJSONObj.getBoolean("status");

                        if (queueStatus) {

                            dlUrl += "?token=" + user_token + "&hash=" + dlHash;

                            if (downloadAndExtractFile(dlUrl, this.DL_FILE_NAME)) return true;


                        } else {

                            Thread.sleep(5000);
                            retry_count++;

                        }

                    }

                    return false;


                } catch (JSONException e) {
                    Log.e("JSONException", e.toString());
                    e.printStackTrace();
                    return false;
                } catch (InterruptedException e) {
                    Log.e("InterruptedException", e.toString());
                    e.printStackTrace();
                    return false;
                } catch (IOException e) {
                    Log.e("IO", e.toString());
                    new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage(getResources().getString(R.string.errorFileCorruptedTitle), getResources().getString(R.string.errorFileCorruptedMessage));
                    e.printStackTrace();
                    return false;
                }


            }else {
                //Log.e("AcesAPCSrTask outer if", "if");
            }


            return true;


        }

        @Override
        protected void onPostExecute(Object s) {

            if (this.DATA_FETCH_TYPE.contentEquals(FETCH_TYPE_URL)) {
                if (pd != null && sync_count == total_sync_required) {
                    sync_count = 0;
                    total_sync_required = 0;
                    pd.setProgress(0);
                    pd.dismiss();
                }

                boolean stat = (boolean) s;

                //Log.e("DB Sync: ", "Preparation Status : " + stat);

                //Log.e("DB Sync: ", "File Name: " + this.DL_FILE_NAME);
                if (stat) {


                    if (this.DL_FILE_NAME.contains("apsProductDB")) {
                        dbHelper.insertProductSyncHash(latest_product_db_hash);
                        sp.edit().putBoolean("product_required", false).commit();


                    } else if (this.DL_FILE_NAME.contains("asrOtherProductDB")) {
                        dbHelper.insertOtherProductSyncHash(latest_other_product_db_hash);
                        sp.edit().putBoolean("other_product_required", false).commit();


                    } else if (this.DL_FILE_NAME.contains("asrUserDB")) {
                        dbHelper.insertUserHash(employee_code, latest_user_db_hash);
                        sp.edit().putBoolean("user_db_required", false).commit();

                    }


                } else {
                    if (pd != null && pd.isShowing()) pd.dismiss();
                    new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage(getResources().getString(R.string.errorDatabaseNotReady), getResources().getString(R.string.errorDatabaseNotReadyMessage));

                }


            }


        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pd.setProgress(values[0]);
            pd.setMessage((sync_count + 1) + "/" + total_sync_required + " database being downloaded");

            //if(pd.getProgress()==100) pd.setProgress(0);

        }

        private String getFormattedPostDataString(HashMap<String, String> postParameters) {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (Map.Entry<String, String> entry : postParameters.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                try {
                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }

            return result.toString();

            //while(postParameters)


        }

        private String getDownloadURL(HashMap<String, String> postParameters) {

            if (!getConnectionStatus()) return "";

            StringBuilder response = new StringBuilder();
            HttpURLConnection urlConnection = null;
            InputStream is;

            try {
                //System.out.println("Task URL: "+this.task_url);

                URL url = new URL(this.task_url);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
                //urlConnection.setDoInput(true);
                // urlConnection.setDoOutput(false);

//                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
//                wr.writeBytes(getFormattedPostDataString(postParameters));
//                wr.flush();
//                wr.close();

                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    return "[{\"error\":\"token_expired\"}]";

                }
                ;
                System.out.println("Server Response Code:" + urlConnection.getResponseCode());
                is = urlConnection.getInputStream();


                BufferedReader rd = new BufferedReader(new InputStreamReader(is));

                String line = "";

                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "";

            } catch (IOException e) {
                e.printStackTrace();
                return "";

            } finally {
                if (urlConnection != null) urlConnection.disconnect();


            }


            return response.toString();
        }


        private String getDownloadStatus(String checkUrl) {

            if (!getConnectionStatus()) return "";

            StringBuilder response = new StringBuilder();
            HttpURLConnection urlConnection = null;
            InputStream is;

            try {
                Log.e("status url", checkUrl);
                URL url = new URL(checkUrl);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
                //urlConnection.setDoInput(true);
                // urlConnection.setDoOutput(false);

//                DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
//                wr.writeBytes(getFormattedPostDataString(postParameters));
//                wr.flush();
//                wr.close();

                is = urlConnection.getInputStream();

                BufferedReader rd = new BufferedReader(new InputStreamReader(is));

                String line;

                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "";

            } catch (IOException e) {
                e.printStackTrace();
                return "";

            } finally {
                if (urlConnection != null) urlConnection.disconnect();


            }


            return response.toString();
        }

        private boolean downloadAndExtractFile(String dUrl, String zip_file_name) throws IOException {
            //Log.e("downloadAndExtractFile", dUrl + " " + zip_file_name);
            FileOutputStream fos = null;
            FileInputStream fis = null;
            InputStream input = null;
            boolean unzipStat = false;
            try {

                // URL url 					= new URL(downloadBaseURL+ASRConfig.APP_UPDATE_URL_EXTENSION+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
                URL url = new URL(dUrl);
                Log.e("new url", url.toString());
                //File destinationFolderRoot 	= new File(Environment.getExternalStorageDirectory()+"/"+ASRConfig.DOWNLOAD_DIRECTORY_NAME);
                File downloadedDBFilePath = new File("/data/data/" + getPackageName() + "/files/");

                if (!downloadedDBFilePath.exists()) {
                    downloadedDBFilePath.mkdir();

                }

                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/files", zip_file_name);

                downloadedDBFile.createNewFile();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
                //conn.setRequestMethod("POST");
                //conn.setDoInput(true);
                //conn.setDoOutput(true);

//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
//            writer.write(getFormattedPostDataString(postParameters));
//            writer.flush();
//            writer.close();
//            os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode != HttpURLConnection.HTTP_FORBIDDEN) {
                    int read;


                    byte[] data = new byte[1024];

                    input = conn.getInputStream();
                    int data_length = conn.getContentLength();

                    fos = new FileOutputStream(downloadedDBFile);

                    long total = 0;

                    while ((read = input.read(data)) != -1) {
                        total += read;
                        fos.write(data, 0, read);
                        int current_progress = pd.getProgress();
                        //publishProgress((int)((current_progress+(total/data_length)*33.33));
                        try {

                            publishProgress((int) (current_progress + (total / data_length) * (100 / total_sync_required)));

                        } catch (ArithmeticException ar) {
                            ar.printStackTrace();

                        }

                    }


                }

                APCFileOperationsHelper apf = new APCFileOperationsHelper(getApplicationContext());
                String _zipFileLocation = "/data/data/" + getPackageName() + "/files/" + zip_file_name;
                String _zipExtractedFileLocation = "/data/data/" + getPackageName() + "/databases/";
                String _extractedFileName = new String(zip_file_name);
                _extractedFileName = _extractedFileName.replace(".zip", "");


                try {

                    apf.unzipFolder(_zipFileLocation, _zipExtractedFileLocation, _extractedFileName);

                    if (sync_count < 3) {
                        sync_count++;

                    } else {
                        sync_count = 0;
                        sync_required = false;
                        sp.edit().putBoolean("sync_required", sync_required).commit();
                    }

                    return true;

                } catch (IOException e) {
                    e.printStackTrace();
                    pd.dismiss();
                    sync_count = 0;
                    sync_required = true;
                    sp.edit().putBoolean("sync_required", sync_required).commit();
                    return false;

                }


//            URLConnection conn 		= url.openConnection();
//
//            input 					= conn.getInputStream();
//
//            fos						= new FileOutputStream(desinationFolder+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
//
//            Log.d(LOG_TAG, "Url DL Path" + url.getPath());
//
//            int read;
//            byte [] data = new byte[1024];
//
//            while((read=input.read(data))!=-1)
//            {
//
//                Log.d(LOG_TAG,"Data: "+read);
//                fos.write(data,0,read);
//
//            }


            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;

            } catch (IOException e) {
                // new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage("File Error","Problem at creating files/directories for downloading updates.");
                e.printStackTrace();


            } finally {
                if (fis != null) fis.close();
                if (input != null) input.close();


            }

            return false;

        }


        private boolean downloadAndExtractFile(HashMap<String, String> postParameters) throws IOException {
            FileOutputStream fos = null;
            FileInputStream fis = null;
            InputStream input = null;

            try {

                // URL url 					= new URL(downloadBaseURL+ASRConfig.APP_UPDATE_URL_EXTENSION+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
                URL url = new URL(this.task_url);
                //File destinationFolderRoot 	= new File(Environment.getExternalStorageDirectory()+"/"+ASRConfig.DOWNLOAD_DIRECTORY_NAME);
                File downloadedDBFilePath = new File("/data/data/" + getPackageName() + "/files/");

                if (!downloadedDBFilePath.exists()) {
                    downloadedDBFilePath.mkdir();

                }

                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/files", this.DL_FILE_NAME);

                downloadedDBFile.createNewFile();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
                //conn.setRequestMethod("POST");
                conn.setDoInput(true);
                //conn.setDoOutput(true);

//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
//            writer.write(getFormattedPostDataString(postParameters));
//            writer.flush();
//            writer.close();
//            os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode != HttpURLConnection.HTTP_FORBIDDEN) {
                    int read;


                    byte[] data = new byte[1024];

                    input = conn.getInputStream();
                    int data_length = conn.getContentLength();

                    fos = new FileOutputStream(downloadedDBFile);

                    long total = 0;

                    while ((read = input.read(data)) != -1) {
                        total += read;
                        fos.write(data, 0, read);
                        int current_progress = pd.getProgress();
                        //publishProgress((int)((current_progress+(total/data_length)*33.33));
                        publishProgress((int) (current_progress + (total / data_length) * (100 / total_sync_required)));
                    }


                }

                APCFileOperationsHelper apf = new APCFileOperationsHelper(getApplicationContext());
                String _zipFileLocation = "/data/data/" + getPackageName() + "/files/" + this.DL_FILE_NAME;
                String _zipExtractedFileLocation = "/data/data/" + getPackageName() + "/databases/";
                String _extractedFileName = new String(this.DL_FILE_NAME);
                _extractedFileName = _extractedFileName.replace(".zip", "");

                try {

                    apf.unzipFolder(_zipFileLocation, _zipExtractedFileLocation, _extractedFileName);

                    if (sync_count < 3) {
                        sync_count++;

                    } else {
                        sync_count = 0;
                        sync_required = false;
                        sp.edit().putBoolean("sync_required", sync_required).commit();
                    }

                    return true;

                } catch (IOException e) {
                    e.printStackTrace();
                    return false;

                }


//            URLConnection conn 		= url.openConnection();
//
//            input 					= conn.getInputStream();
//
//            fos						= new FileOutputStream(desinationFolder+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
//
//            Log.d(LOG_TAG, "Url DL Path" + url.getPath());
//
//            int read;
//            byte [] data = new byte[1024];
//
//            while((read=input.read(data))!=-1)
//            {
//
//                Log.d(LOG_TAG,"Data: "+read);
//                fos.write(data,0,read);
//
//            }


            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                // new ASRErrorMessageView(MainMenuActivity.this).showErrorMessage("File Error","Problem at creating files/directories for downloading updates.");
                e.printStackTrace();

            } finally {
                if (fis != null) fis.close();
                if (input != null) input.close();


            }


            return false;

        }


        private String getInfoWithUrl(String serverUrl) throws IOException {
            HashMap<String, String> postParams = new HashMap<>();


            postParams.put("username", sp.getString("username", ""));
            postParams.put("password", sp.getString("password", ""));
            postParams.put("dbHashCode", dbLastHashcode);

            URL url = null;
            String response = "";

            FileOutputStream output = null;
            FileInputStream fis = null;
            InputStream input = null;

            try {

                url = new URL(serverUrl);
                URLConnection connection = url.openConnection();

                input = connection.getInputStream();


                File downloadedDBFilePath = new File("/data/data/" + getPackageName() + "/files/");

                if (!downloadedDBFilePath.exists()) {
                    downloadedDBFilePath.mkdir();

                }

                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/files", "apsProductDB.zip");

                downloadedDBFile.createNewFile();

                output = new FileOutputStream(downloadedDBFile);
                int contentLength = connection.getContentLength();
                int read;
                byte[] data = new byte[1024];
                int total = 0;
                while ((read = input.read(data)) != -1) {
                    System.out.println("datavar: " + read);

                    output.write(data, 0, read);
                    total += read;

                    publishProgress((int) ((total * 100) / contentLength));

                }

                output.close();

                if (fis != null) fis.close();
                if (input != null) input.close();

                //pd.setMessage("Extracting database");

                APCFileOperationsHelper apf = new APCFileOperationsHelper(getApplicationContext());
                String _zipFileLocation = "/data/data/" + getPackageName() + "/files/apsProductDB.zip";
                String _zipExtractedFileLocation = "/data/data/" + getPackageName() + "/databases/";
                String _extractedFileName = "apsProductDB";

                try {
                    apf.unzipFolder(_zipFileLocation, _zipExtractedFileLocation, _extractedFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                    return "";

                }


//                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/databases","apsProductDB");
//
//                downloadedDBFile.createNewFile();
//
//                output  = new FileOutputStream(downloadedDBFile);
//
//                int read;
//                byte[] data = new byte[1024];
//
//                while ((read = input.read(data)) != -1)
//                {
//                    System.out.println("datavar: "+read);
//
//                    output.write(data, 0, read);
//
//                }

//                if(fis!=null) fis.close();
//                if (input != null) input.close();

                //pd.setMessage("Extracting database");


            } catch (Exception e) {
                e.printStackTrace();
                return "";

            } finally {

                if (fis != null) fis.close();
                if (input != null) input.close();


            }

            return "Done";

        }


    }


}
