package com.amadeyr.amadeyrsalesrepresentative;

/**
 * Created by amadeyr on 9/13/15.
 */

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.Button;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by acl on 5/12/15.
 */
public class APSSubmissionsCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;

    public APSSubmissionsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor)
    {



        //TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewOSRowSerialID);
        TextView txtViewOSProductName   = (TextView) view.findViewById(R.id.txtViewOSRowProductName);
        TextView txtViewOSPrice         = (TextView) view.findViewById(R.id.txtViewOSRowPrice);
        TextView txtViewOSQty           = (TextView) view.findViewById(R.id.txtViewOSRowQty);
        TextView txtViewOSTotal         = (TextView) view.findViewById(R.id.txtViewOSRowTotal);
      //  Button btnPreviousHistory     = (Button)   view.findViewById(R.id.btnCustomerHistory);

        if (cursor != null)
        {
            //


            double price        = cursor.getDouble(cursor.getColumnIndex("price"));
            double qty          = cursor.getDouble(cursor.getColumnIndex("qty"));
            double total        = price * qty;

            int position        = cursor.getPosition()+1;
//
//            if(position<10)
//            {
//                txtViewSerialNo.setText("0"+position);
//            }else
//            {
//                txtViewSerialNo.setText("0"+position);
//            }

            txtViewOSProductName.setText(cursor.getString(cursor.getColumnIndex("product_name")));
            txtViewOSPrice.setText(""+price);
            txtViewOSQty.setText(""+qty);
            txtViewOSTotal.setText(""+total);

//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
        }

    }
}
