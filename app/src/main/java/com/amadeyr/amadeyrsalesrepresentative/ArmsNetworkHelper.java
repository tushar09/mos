package com.amadeyr.amadeyrsalesrepresentative;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;

public class ArmsNetworkHelper {

    private static Activity theActivity = null;
    private static int itemCountForJsonString = 0;
    private static String newJSONstringFromServer = null;
    private static String oldJSONstringFromSDcard = null;
    private static String latestJSONstring = null;
    private static File newJSONfileInSDcard = null;
    private static File oldJSONfileInSDcard = null;

    public static HttpClient loginHttpClient;

    public static int getItemCountForJson() {
        return itemCountForJsonString;
    }

    private static void setItemCountForJsonString(String jsonString) {
        try {
            itemCountForJsonString = (new JSONArray(jsonString)).length();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static String checkServer(String requestURL, String imei,
                                      String phoneSim, String gpsLoc, String user_token) {
        InputStream iStream = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

        //nameValuePairs.add(new BasicNameValuePair("user_token", "" + user_token));
//        nameValuePairs.add(new BasicNameValuePair("imei", imei));
//        nameValuePairs.add(new BasicNameValuePair("simNumber", phoneSim));
//        nameValuePairs.add(new BasicNameValuePair("gpsLocation", gpsLoc));

        int timeOutValue = 5000;
        HttpClient myHttpClient = null;

        requestURL+= "?token="+user_token;
        // perform http post
        try {
            System.out.println("++++++ polling remote server at: " + requestURL
                    + " " + getCurrentTime());
            HttpParams myHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(myHttpParams,
                    timeOutValue);
            HttpConnectionParams.setSoTimeout(myHttpParams, timeOutValue);
            myHttpClient = new DefaultHttpClient(myHttpParams);

            HttpGet myHttpGet = new HttpGet(requestURL);

//            HttpPost myHttpPost = new HttpPost(requestURL);
//            myHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse myHttpResponse = myHttpClient.execute(myHttpGet);
            HttpEntity myHttpEntity = myHttpResponse.getEntity();
            iStream = myHttpEntity.getContent();
            // myHttpClient.getConnectionManager().shutdown();
        } catch (HttpHostConnectException e) {
            return "nonet";
            // displayErrorMsgOnNetworkException(e , "Internet not available");
        } catch (ConnectTimeoutException e) {
            return "nonet";
            // displayErrorMsgOnNetworkException(e , (timeOutValue/1000) +
            // " seconds: " + "Couldn't route to ACL server.");
        } catch (SocketException e) {
            return "nonet";
            // displayErrorMsgOnNetworkException(e ,
            // "Could not fetch complete data");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("++++++ converting HTTP response to string");
        String tst = "";

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    iStream));
            StringBuilder sb = new StringBuilder();
            sb.append(reader.readLine() + "\n");
            System.out.println("Test:" + sb.toString());
            String line = "0";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            iStream.close();

            tst = new String(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        myHttpClient.getConnectionManager().shutdown();
        System.out.println("HouseHoldJSONString: " + tst);

        return tst;
    }

    public static String retrieveJSONstringFromServer(String jsonURLinServer,
                                                      String staff_id) {
        InputStream iStream = null;
        StringBuilder sb = null;
        String retrievedJSONstringToReturn = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

        nameValuePairs.add(new BasicNameValuePair("json", staff_id));

        // httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // List <? extends NameValuePair> nvpList = new
        // ArrayList<MyNameValPairDefinition>();
        // MyNameValPairDefinition postVal = new MyNameValPairDefinition("json",
        // "1");

        // nvpList.add(postVal);

        int timeOutValue = ASRConfig.TIMEOUT_VALUE;
        int connectionTimeOutVal = ASRConfig.CONNECTION_TIMEOUT_VALUE;
        HttpClient myHttpClient = null;
        // perform http post
        try {
            System.out.println("++++++ polling remote server at: "
                    + getCurrentTime());
            HttpParams myHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(myHttpParams,
                    connectionTimeOutVal); // for establishing connection with
            // remote server
            HttpConnectionParams.setSoTimeout(myHttpParams, timeOutValue); // for
            // fetching
            // subsequent
            // data
            // from
            // the
            // connected
            // remote
            // server
            myHttpClient = new DefaultHttpClient(myHttpParams);
            HttpPost myHttpPost = new HttpPost(jsonURLinServer);
            myHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            // myHttpPost.setHeader("Accept", "application/json");
            // myHttpPost.setHeader("Content-type", "application/json");

            // myHttpPost.setEntity(new UrlEncodedFormEntity(nvpList)); // List
            // of NameValuePairs for retrieving JSON data
            HttpResponse myHttpResponse = myHttpClient.execute(myHttpPost);
            HttpEntity myHttpEntity = myHttpResponse.getEntity();
            iStream = myHttpEntity.getContent();
            // myHttpClient.getConnectionManager().shutdown();
        } catch (HttpHostConnectException e) {
            return null;
            // displayErrorMsgOnNetworkException(e , "Internet not available");
        } catch (ConnectTimeoutException e) {
            return null;
            // displayErrorMsgOnNetworkException(e , (timeOutValue/1000) +
            // " seconds: " + "Couldn't route to ACL server.");
        } catch (SocketException e) {
            return null;
            // displayErrorMsgOnNetworkException(e ,
            // "Could not fetch complete data");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // convert response to string
        System.out.println("++++++ converting HTTP response to string");
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    iStream));
            sb = new StringBuilder();
            sb.append(reader.readLine() + "\n");
            System.out.println("Test:" + sb.toString());
            String line = "0";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            iStream.close();

            retrievedJSONstringToReturn = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        myHttpClient.getConnectionManager().shutdown();
        System.out.println("JSONString: "
                + retrievedJSONstringToReturn);

        return retrievedJSONstringToReturn;
    }

    // Added by Tasbeer
    public static String getUserInfo(String jsonURLinServer, String identity,
                                     String password, int last_msg_id, String market_hash) {
        InputStream iStream = null;
        StringBuilder sb = null;
        String retrievedJSONstringToReturn = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

        nameValuePairs.add(new BasicNameValuePair("identity", identity));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("last_msg_id", ""
                + last_msg_id));
        nameValuePairs.add(new BasicNameValuePair("market_hash", market_hash));

        int timeOutValue = ASRConfig.TIMEOUT_VALUE;
        // loginHttpClient = null;
        // perform http post
        try {
            System.out.println("++++++ polling remote server at: "
                    + getCurrentTime());
            HttpParams myHttpParams = new BasicHttpParams();
            HttpConnectionParams.setSoTimeout(myHttpParams, timeOutValue); // for
            // fetching
            // subsequent
            // data
            // from
            // the
            // connected
            // remote
            // server
            loginHttpClient = new DefaultHttpClient(myHttpParams);
            HttpPost myHttpPost = new HttpPost(jsonURLinServer);
            myHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse myHttpResponse = loginHttpClient.execute(myHttpPost);
            List<Cookie> cookies = ((AbstractHttpClient) loginHttpClient)
                    .getCookieStore().getCookies();

            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    System.out.println("- " + cookies.get(i).toString());
                }
            }

            if (myHttpResponse != null) {
                HttpEntity myHttpEntity = myHttpResponse.getEntity();
                iStream = myHttpEntity.getContent();
            }
        } catch (HttpHostConnectException e) {
            return null;
            // displayErrorMsgOnNetworkException(e , "Internet not available");
        } catch (ConnectTimeoutException e) {
            return null;
            // displayErrorMsgOnNetworkException(e , (timeOutValue/1000) +
            // " seconds: " + "Couldn't route to ACL server.");
        } catch (SocketException e) {
            return null;
            // displayErrorMsgOnNetworkException(e ,
            // "Could not fetch complete data");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // convert response to string
        System.out.println("++++++ converting HTTP response to string");
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    iStream));
            sb = new StringBuilder();
            sb.append(reader.readLine() + "\n");
            System.out.println("Test:" + sb.toString());
            String line = "0";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            iStream.close();
            loginHttpClient.getConnectionManager().shutdown();
            retrievedJSONstringToReturn = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // loginHttpClient.getConnectionManager().shutdown();
        System.out.println("User Info: " + retrievedJSONstringToReturn);
        return retrievedJSONstringToReturn;
    }

    public static String testNetCheck(String jsonURLinServer) {

        InputStream iStream = null;
        StringBuilder sb = null;
        String retrievedJSONstringToReturn = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

        nameValuePairs.add(new BasicNameValuePair("test", "1"));

        int timeOutValue = ASRConfig.TIMEOUT_VALUE;
        // loginHttpClient = null;
        // perform http post
        try {

            HttpParams myHttpParams = new BasicHttpParams();
            HttpConnectionParams.setSoTimeout(myHttpParams, timeOutValue); // for
            // fetching
            // subsequent
            // data
            // from
            // the
            // connected
            // remote
            // server
            // loginHttpClient = new DefaultHttpClient(myHttpParams);
            HttpPost myHttpPost = new HttpPost(jsonURLinServer);
            myHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse myHttpResponse = loginHttpClient.execute(myHttpPost);

            System.out.println("Entered Mordor !!!!");

            HttpEntity myHttpEntity = myHttpResponse.getEntity();
            iStream = myHttpEntity.getContent();

        } catch (HttpHostConnectException e) {
            // return null;
            displayErrorMsgOnNetworkException(e, "Internet not available");
        } catch (ConnectTimeoutException e) {
            // return null;
            displayErrorMsgOnNetworkException(e, (timeOutValue / 1000)
                    + " seconds: " + "Couldn't route to ACL server.");
        } catch (SocketException e) {
            // return null;
            displayErrorMsgOnNetworkException(e,
                    "Could not fetch complete data");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // convert response to string
        System.out.println("++++++ converting HTTP response to string");
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    iStream));
            sb = new StringBuilder();
            sb.append(reader.readLine() + "\n");

            String line = "0";
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            iStream.close();
            loginHttpClient.getConnectionManager().shutdown();
            retrievedJSONstringToReturn = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // loginHttpClient.getConnectionManager().shutdown();

        System.out.println("LoggedIn session: " + retrievedJSONstringToReturn);
        return retrievedJSONstringToReturn;
    }

    // method for echoing backup data to server

//    public static void postBackupData(String data_json) {
//
//        // get server url
//
//        SharedPreferences settings = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
//
//        String serverUrl = settings.getString(
//                PreferencesActivity.KEY_SERVER_URL, Collect.getInstance()
//                        .getString(R.string.default_server_url));
//
//        InputStream iStream = null;
//        StringBuilder sb = null;
//        String retrievedJSONstringToReturn = null;
//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//
//        nameValuePairs.add(new BasicNameValuePair("data_json", data_json));
//
//        int timeOutValue = ASRConfig.TIMEOUT_VALUE;
//        // loginHttpClient = null;
//        // perform http post
//        try {
//            System.out.println("++++++ polling remote server at: "
//                    + getCurrentTime());
//            HttpParams myHttpParams = new BasicHttpParams();
//            HttpConnectionParams.setSoTimeout(myHttpParams, timeOutValue); // for
//            // fetching
//            // subsequent
//            // data
//            // from
//            // the
//            // connected
//            // remote
//            // server
//            loginHttpClient = new DefaultHttpClient(myHttpParams);
//            HttpPost myHttpPost = new HttpPost(serverUrl
//                    + ASRConfig.RMS_BACKUP_CONTROLLER);
//            myHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//            HttpResponse myHttpResponse = loginHttpClient.execute(myHttpPost);
//            List<Cookie> cookies = ((AbstractHttpClient) loginHttpClient)
//                    .getCookieStore().getCookies();
//
//            if (cookies.isEmpty()) {
//                System.out.println("None");
//            } else {
//                for (int i = 0; i < cookies.size(); i++) {
//                    System.out.println("- " + cookies.get(i).toString());
//                }
//            }
//
//            if (myHttpResponse != null) {
//                HttpEntity myHttpEntity = myHttpResponse.getEntity();
//                iStream = myHttpEntity.getContent();
//            }
//        } catch (HttpHostConnectException e) {
//            e.printStackTrace();
//            // return null;
//            // displayErrorMsgOnNetworkException(e , "Internet not available");
//        } catch (ConnectTimeoutException e) {
//            e.printStackTrace();
//            // return null;
//            // displayErrorMsgOnNetworkException(e , (timeOutValue/1000) +
//            // " seconds: " + "Couldn't route to ACL server.");
//        } catch (SocketException e) {
//            e.printStackTrace();
//            // return null;
//            // displayErrorMsgOnNetworkException(e ,
//            // "Could not fetch complete data");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // convert response to string
//        System.out.println("++++++ converting HTTP response to string");
//        try {
//
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    iStream));
//            sb = new StringBuilder();
//            sb.append(reader.readLine() + "\n");
//            System.out.println("Test:" + sb.toString());
//            String line = "0";
//            while ((line = reader.readLine()) != null) {
//                sb.append(line + "\n");
//            }
//            iStream.close();
//            loginHttpClient.getConnectionManager().shutdown();
//            retrievedJSONstringToReturn = sb.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // return retrievedJSONstringToReturn;
//
//    }

    // Added by Tasbeer

    private static int getItemID(int position, String jsonString) {
        // parsing json data
        JSONArray jArray = null;
        System.out.println("--> parsing JSON data for item_id at position: "
                + position);
        int ITEM_ID = 0;
        try {
            jArray = new JSONArray(jsonString);
            JSONObject json_data = null;
            json_data = jArray.getJSONObject(position);
            ITEM_ID = json_data.getInt("sl");

            System.out.println("item_id:(" + ITEM_ID + ") --- ");
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ITEM_ID;
    }

    public class MyNameValPairDefinition implements NameValuePair {

        public String name;
        public String id;

        public MyNameValPairDefinition(String name, String id) {
            // TODO Auto-generated constructor stub
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return id;
        }
    }

    private static void displayErrorMsgOnNetworkException(Exception e,
                                                          final String errorMsg) {
        System.out.println("++++++ remote server polling TIMED OUT at: "
                + getCurrentTime());
        System.out.println("!!!!!! ERROR-MSG: " + errorMsg);
        e.printStackTrace();
        theActivity.runOnUiThread(new Runnable() {

            public void run() {
                toast(errorMsg);
            }
        });
    }

    private static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL,
                DateFormat.MEDIUM);
        return (df.format(cal.getTime()));
    }

    private static void toast(String msg) {
        Toast.makeText(theActivity, msg, Toast.LENGTH_LONG).show();
    }

    private static void exitApplication() {
        theActivity.finish();
    }

    public static String Updatecheck(String url) {
        String version = "";
        try {
            JSONArray MainArray = new JSONArray(retrieveJSONstringFromServer(
                    url, ""));
            int CountNode = MainArray.length();
            for (int i = 0; i < CountNode; i++) {
                try {
                    JSONArray nameArray = MainArray.getJSONObject(i).names();
                    JSONArray valArray = MainArray.getJSONObject(i)
                            .toJSONArray(nameArray);

                    for (int j = 0; j < valArray.length(); j++) {
                        if (nameArray.getString(j).equalsIgnoreCase("version")) {
                            version = valArray.getString(j);

                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return version;
    }

    public static String Updateappgeturl(String url) {
        String updateurl = "";
        try {
            JSONArray MainArray = new JSONArray(retrieveJSONstringFromServer(
                    url, ""));
            int CountNode = MainArray.length();
            for (int i = 0; i < CountNode; i++) {
                try {
                    JSONArray nameArray = MainArray.getJSONObject(i).names();
                    JSONArray valArray = MainArray.getJSONObject(i)
                            .toJSONArray(nameArray);

                    for (int j = 0; j < valArray.length(); j++) {
                        if (nameArray.getString(j).equalsIgnoreCase(
                                "update_url")) {
                            updateurl = valArray.getString(j);

                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return updateurl;
    }

    public static boolean getNetStatus(Context appCtx)
    {

        ConnectivityManager connMgr = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected()) return true;

        return false;


    }

    public static boolean getGPSStatus(Context ctx)
    {
        String provider = android.provider.Settings.Secure.getString(ctx.getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) return true;

        return false;

    }

}
