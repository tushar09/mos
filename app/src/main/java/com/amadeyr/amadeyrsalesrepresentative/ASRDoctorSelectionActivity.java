package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRDoctorListCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.DoctorInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.EmployeeInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.DoctorInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by amadeyr on 11/5/15.
 */
public class ASRDoctorSelectionActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private ListView lvDoctorList;
    private DoctorInfo dInfo;
    private APCProductDatabaseHelper dbHelper;
    private APCDateHelper dateHelper,apcDateHelper;
    private APCCustomAutoCompleteView doctorSearch;
    private ASRDoctorListCursorAdapter apsCursorAdapter;

    private ImageView imgViewCross;
    private Button  btnSend;

    public ArrayList<String> selectedForSubmission;

    private String user_id, source_activity, institute_code, institute_name,employee_id;
    private final String SCREEN_TITLE = "Doctor's List";
    private String[] doctorArray;
    private Double start_latitude, start_longitude;
    private APCDatabaseHelper apsOrderDatabaseHelper;
    private Context appCtx;
    private DateTime jDTTimeHelper;
    private final String ORDER_STATUS_COMPLETE = "COMPLETE";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;


    private Location mLastLocation, mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_selection);

        ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        SharedPreferences sp    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        //user_id                 = sp.getString("user_id", "SUP-100");
        source_activity         = getIntent().getStringExtra("source_activity");

        apcDateHelper           = new APCDateHelper();
        jDTTimeHelper           = new DateTime();

        doctorSearch            = (APCCustomAutoCompleteView) findViewById(R.id.actDoctorSearch);
        doctorSearch.clearFocus();

        imgViewCross            = (ImageView) findViewById(R.id.imgViewDoctorClear);
        btnSend                 = (Button)  findViewById(R.id.btnDLSend);
        lvDoctorList            = (ListView) findViewById(R.id.lstViewDoctorsList);
        appCtx                  = getApplicationContext();

        doctorSearch.setThreshold(1);

        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        dbHelper                = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());
        //apsOrderDatabaseHelper  = new APCDatabaseHelper(appCtx,"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        apsOrderDatabaseHelper  = APCDatabaseHelper.getInstance(getApplicationContext());

        user_id                 = getIntent().getStringExtra("employee_id");
        //user_id                 = sp.getString("username","");
        institute_code          = getIntent().getStringExtra("institute_code");
        institute_name          = getIntent().getStringExtra("institute_name");

        //Cursor doctorNames      = dbHelper.getDoctorsList(institute_code);

        Cursor doctorNames      = dbHelper.getDoctorsListForSR(user_id);
        List<String> incompleteIDS  = apsOrderDatabaseHelper.getDoctorsWithIncompleteVisits(employee_id);

        apsCursorAdapter        = new ASRDoctorListCursorAdapter(getApplicationContext(),R.layout.doctor_list_row_layout,doctorNames,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        apsCursorAdapter.setIncompleteList(incompleteIDS);
        lvDoctorList.setAdapter(apsCursorAdapter);
        selectedForSubmission   = apsCursorAdapter.getSelectedListOfOrders();
        //btnPSSubmit             = (Button)   findViewById(R.id.btnPSSubmit);
        //btnPSSearch             = (Button)   findViewById(R.id.btnPSSearch);

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorSearch.setText("");
                Cursor allEntries = dbHelper.getDoctorsList(institute_code);
                apsCursorAdapter.swapCursor(allEntries);
                apsCursorAdapter.notifyDataSetChanged();
            }
        });

        lvDoctorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(ArmsNetworkHelper.getGPSStatus(getApplicationContext())) {
                    stopLocationUpdates();
                    Cursor selectedDoctor = (Cursor) apsCursorAdapter.getItem(position);

                    //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();

                    Intent i = new Intent(ASRDoctorSelectionActivity.this, DoctorVisitActivity.class);
                    i.putExtra("employee_id", user_id);
                    i.putExtra("mode", "");
                    //i.putExtra("employee_id", selectedDoctor.getString(selectedDoctor.getColumnIndex("e")));
                    i.putExtra("doctor_code", selectedDoctor.getString(selectedDoctor.getColumnIndex("doctor_code")));
                    i.putExtra("doctor_name", selectedDoctor.getString(selectedDoctor.getColumnIndex("doctor_name")));
                    //i.putExtra("institute_code", selectedDoctor.getString(selectedDoctor.getColumnIndex("institute_code")));
                    //i.putExtra("institute_name", institute_name);
                    i.putExtra("institute_code", "");
                    i.putExtra("institute_name", "");

                    i.putExtra("source_activity", source_activity);
                    //finish();
                    startActivity(i);

                }else
                {
                    final ASRErrorMessageView am = new ASRErrorMessageView(ASRDoctorSelectionActivity.this);
                    am.addExtraAction(getResources().getString(R.string.lblBtnTurnOn));
                    am.showErrorMessage(getResources().getString(R.string.errorMsgTitleLocationDisabled),getResources().getString(R.string.errorMsgBodyLocationDisabled));
                    Button gpsEnabledButton = am.getActionButton2Instance();
                    gpsEnabledButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            am.closeScreen();
                        }
                    });

                }
            }
        });

        doctorSearch.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));

        doctorSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String searchKey = doctorSearch.getText().toString();
                Cursor searchCursor = dbHelper.getDoctorInfo(searchKey);
                apsCursorAdapter.swapCursor(searchCursor);
                apsCursorAdapter.notifyDataSetChanged();


            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                selectedForSubmission   = apsCursorAdapter.getSelectedListOfOrders();

                if(!selectedForSubmission.isEmpty())
                {
                    Iterator docArrayIterator = selectedForSubmission.iterator();


                    prepareConfirmationScreen();




                    while(docArrayIterator.hasNext())
                    {

                        String dCode                = (String) docArrayIterator.next();

                        //saveToDatabase(dCode);


                        // Toast.makeText(getApplicationContext(), order_id, Toast.LENGTH_SHORT).show();


                    }


                }else
                {

                    new ASRErrorMessageView(ASRDoctorSelectionActivity.this).showErrorMessage(getResources().getString(R.string.errorNoItemsSelected),getResources().getString(R.string.errorNoItemsSelectedMessage));

                }


            }
        });



    }



    public void saveToDatabase(String vWith)
    {
        selectedForSubmission       = apsCursorAdapter.getSelectedListOfOrders();
        Iterator lIterator          = selectedForSubmission.iterator();
        LocalTime j                 = jDTTimeHelper.toLocalTime();

        ProgressDialog pd = new ProgressDialog(ASRDoctorSelectionActivity.this);
        pd.setMessage("Please wait ... saving data for submission");
        pd.show();

        recordLocation();
        //stopLocationUpdates();

        while(lIterator.hasNext())
        {
            String dcode = (String) lIterator.next();
            String [] doctor_info = dcode.split("<sep>");

//            System.out.println("Doc Code Unsplit: "+ dcode);
//            System.out.println("Doc Code 0: "+ doctor_info[0]);
//            System.out.println("Doc Code 1: "+ doctor_info[1]);


            int last_order_id           = apsOrderDatabaseHelper.getLastVisitInsertID();
            String order_id             = user_id+doctor_info[0]+ String.format("%04d",last_order_id+1);
            String visit_date           = jDTTimeHelper.getYear()+"-"+jDTTimeHelper.getMonthOfYear()+"-"+ jDTTimeHelper.getDayOfMonth();
            String strTime              = j.getHourOfDay()+":"+j.getMinuteOfHour()+":"+j.getSecondOfMinute();


            VisitInfo orderEntry        = new VisitInfo(order_id,doctor_info[0],doctor_info[1],visit_date,institute_name,ORDER_STATUS_COMPLETE,start_latitude,start_longitude,0,0,user_id,vWith,strTime,APCDatabaseHelper.VISIT_TYPE_LONE);

            apsOrderDatabaseHelper.insertVisit(orderEntry);

        }


        apsCursorAdapter.notifyDataSetChanged();

        pd.dismiss();

        Toast.makeText(getApplicationContext(),"Data saved successfully !",Toast.LENGTH_LONG).show();



        //String order_id             = user_id+dCode+ String.format("%04d",last_order_id+1);

//
//
//        VisitEntries entry          = new VisitEntries(visit_id,product_id,product_name,order_qty,order_comments,selected_cat_name);
//
//
//        if(!entry_status)
//        {
//            recordLocation();
//            LocalTime j = jDTTimeHelper.toLocalTime();
//            String strTime = j.getHourOfDay()+":"+j.getMinuteOfHour()+":"+j.getSecondOfMinute();
//
//            VisitInfo orderEntry = new VisitInfo(visit_id,doctor_code,doctor_name,this.order_date,institute_name,ORDER_STATUS_INCOMPLETE,start_latitude,start_longitude,0,0,employee_id,visited_with,strTime);
//            apsOrderDBHelper.insertVisit(orderEntry);
//
//        }
//
//        apsOrderDBHelper.insertVisitEntry(entry);
//
//
//
//        Cursor updatedCursor     = apsOrderDBHelper.getVisitEntries(visit_id);

       // apsOrderCursorAdapter.swapCursor(updatedCursor);
       // apsOrderCursorAdapter.notifyDataSetChanged();



      //  this.sort_toggle_on = false;


    }


    public void prepareConfirmationScreen()
    {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();



        final Dialog orderConfirmationScreen = new Dialog(ASRDoctorSelectionActivity.this);

        orderConfirmationScreen.setContentView(R.layout.confirm_order_screen);

        LinearLayout l1 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO1);
        LinearLayout l2 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO2);
        LinearLayout l3 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO3);
        LinearLayout l4 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO4);
        LinearLayout l5 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO5);
        LinearLayout l6 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO6);
        LinearLayout l7 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO7);
        LinearLayout l11 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.lC11);
        LinearLayout l10 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llC10);


        l1.setVisibility(View.GONE);
        l2.setVisibility(View.GONE);
        l3.setVisibility(View.GONE);
        l4.setVisibility(View.GONE);
        l5.setVisibility(View.GONE);
        l6.setVisibility(View.GONE);
        l7.setVisibility(View.VISIBLE);
        l11.setVisibility(View.GONE);
        l10.setVisibility(View.GONE);


        final EditText eTRemarks = (EditText) orderConfirmationScreen.findViewById(R.id.eTConfirmNotes);
        //eTRemarks.setEnabled(true);

        Button btnConfirmOrderYes           = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderYes);
        Button btnConfirmOrderNo            = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderNo);
        RadioButton rbSelf                  = (RadioButton) orderConfirmationScreen.findViewById(R.id.rbSelf);
        final RadioButton rbSupervisor      = (RadioButton) orderConfirmationScreen.findViewById(R.id.rbSupervisor);





        btnConfirmOrderYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                String visitedWith  = "Self";

                if(rbSupervisor.isChecked())
                {
                    visitedWith     = "Supervisor";

                }

                orderConfirmationScreen.dismiss();

                saveToDatabase(visitedWith);
                apsCursorAdapter.clearSelectedListOfOrders();
                selectedForSubmission.clear();
                lvDoctorList.invalidateViews();



            }
        });

        btnConfirmOrderNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderConfirmationScreen.dismiss();
            }
        });


        orderConfirmationScreen.setTitle("Confirm Order");
        orderConfirmationScreen.show();



    }

    public void populateSearchAdapter(String searchItem)
    {

        String supervisor_id        = this.user_id;
        List<DoctorInfo> eInfo      = dbHelper.getAutoCompleteDoctorInfo(searchItem, institute_code);
        doctorArray                 = new String[eInfo.size()];

        int str_index               = 0;

        for (int index = 0; index < eInfo.size();++index)
        {
            doctorArray[index]        = new String(eInfo.get(index).getDoctorName());
            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());

            //str_index+=2;

            //System.out.println("SearchActivity: "+jObj.getString("description"));
        }


        ArrayAdapter adapter         = new ArrayAdapter(ASRDoctorSelectionActivity.this, android.R.layout.simple_list_item_1, doctorArray);

        doctorSearch.setAdapter(adapter);
        doctorSearch.setThreshold(1);



    }

    public void addNewDoctorDialog()
    {
        final Dialog addNewDocDialog = new Dialog(ASRDoctorSelectionActivity.this);
        addNewDocDialog.setContentView(R.layout.add_new_doctor);

        final EditText etDocName               = (EditText)       addNewDocDialog.findViewById(R.id.edtTextAddDocNew);
        final EditText etInstituteName         = (EditText)       addNewDocDialog.findViewById(R.id.edtTextADInstituteName);
        final EditText etAddress               = (EditText)       addNewDocDialog.findViewById(R.id.edtTextADAddress);
        final EditText etContact               = (EditText)       addNewDocDialog.findViewById(R.id.edtTextDocPhone);


        Button btnRegister                     = (Button)         addNewDocDialog.findViewById(R.id.btnRegister);
        Button btnVisit                        = (Button)         addNewDocDialog.findViewById(R.id.btnNewDocVisit);



        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String visitedWith  = "Self";




                saveToDatabase(visitedWith);

                addNewDocDialog.dismiss();

            }
        });

        btnVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewDocDialog.dismiss();
            }
        });


        addNewDocDialog.setTitle("");
        addNewDocDialog.show();


        //apsOrderDBHelper.updateCompleteStatus(order_id,ORDER_STATUS_COMPLETE);
        //Intent i = new Intent(OrderScreenActivity.this, APSCustomerSelectionActivity.class);
        //startActivity(i);
        //finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.doctor_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {

            String source_activity  = "";

            Intent main_menu = new Intent(ASRDoctorSelectionActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();




            return true;

        }else if(id == R.id.add_new_doc)
        {

            addNewDoctorDialog();




            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

        super.onBackPressed();




    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            start_latitude  = mLastLocation.getLatitude();
            start_longitude = mLastLocation.getLongitude();

        }

        //stopLocationUpdates();


    }

    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {
            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }


        }



    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
//        createLocationRequest();
//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, this);
//        recordLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        if(location!=null)
        {
            mCurrentLocation = location;
            recordLocation();

        }

    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
