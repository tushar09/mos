package com.amadeyr.amadeyrsalesrepresentative;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by acl on 4/7/15.
 */
public class APCDateHelper {

    private String currentDateStr,firstDayOfWeek;
    private String month_str[] = {"","01","02","03","04","05","06","07","08","09","10","11","12"};
    private String date_str[] = {"","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};


    public APCDateHelper()
    {
        //currentDateStr = this.getCurrentDateStr();
        //firstDayOfWeek = this.getFirstDayOfWeek();

    }



    public String getCurrentDateStr()
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Date todaysDate     = Calendar.getInstance().getTime();
        String result       = df.format(todaysDate);


        System.out.println("Today's Date: "+result);

        return result;
    }

    public Date getCurrentDateObj()
    {
        Date todaysDate     = Calendar.getInstance().getTime();
        return todaysDate;
    }



    public long getCurrentTime()
    {

        return Calendar.getInstance().getTimeInMillis();
    }

    public static String getTimeFromMilliseconds(long time)
    {

        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("hh:mm aa");
        String dateFormated = formatter.format(date);

        return dateFormated;
    }

    public static String getDateForPrescriptionFromMilliseconds(long time)
    {
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String dateFormated = formatter.format(date);

        return dateFormated;


    }

    public static String getDateFromMilliseconds(long time)
    {
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("dd MMMM yyyy ");
        String dateFormated = formatter.format(date);

        return dateFormated;


    }

    public String getFirstDayOfWeek()
    {
        Calendar c = Calendar.getInstance();

        c.set(Calendar.DAY_OF_WEEK,1);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        String result       = df.format(c.getTime());

        return result;

    }

    public String getFirstDayOfWeek(int day, int month, int year)
    {
        Calendar c          = Calendar.getInstance();

        //c.set(year,month-1,day);

        c.set(Calendar.DATE,day);
        c.set(Calendar.MONTH,month-1);
        c.set(Calendar.YEAR,year);
        c.set(Calendar.DAY_OF_WEEK,1);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        String result       = df.format(c.getTime());

        return result;

    }

    public String getStringDateFromInt(int date, int month, int year)
    {
        String result = year+ "-"+month_str[month]+"-"+date_str[date];
        return result;

    }

    public Date getDateObjFromString(String date) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dt                 = format.parse(date);

        return dt;

    }

    public Date getDateObjFromStringConventional(String date) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dt                 = format.parse(date);

        return dt;

    }

}
