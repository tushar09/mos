package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by acl on 5/12/15.
 */
public class APCMessageCursorAdapter extends SimpleCursorAdapter {

    private Context appContext;
    private APCDateHelper apcDate;
    private int layout_id;
    public ArrayList<String> selectedForSubmission;

    public APCMessageCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        appContext = context;

    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        TextView serialNo       = (TextView) view.findViewById(R.id.txtViewMessageRowSerial);
        TextView msgTitle       = (TextView) view.findViewById(R.id.txtViewMessageTitle);
        TextView msgPubDate     = (TextView) view.findViewById(R.id.txtViewMsgDate);
        TextView msgTime        = (TextView) view.findViewById(R.id.txtViewMsgTime);
        LinearLayout llHolder   = (LinearLayout) view.findViewById(R.id.llMessageRowBack);

        if (cursor != null)
        {
            String msg_id           = cursor.getString(cursor.getColumnIndex("msg_id"));
            String msg_title        = cursor.getString(cursor.getColumnIndex("msg_title"));
            String msg_body         = cursor.getString(cursor.getColumnIndex("msg_body"));
            String msg_pub_date     = cursor.getString(cursor.getColumnIndex("publish_date"));
            String msg_pub_time     = cursor.getString(cursor.getColumnIndex("pub_time"));
            int msg_read_status     = cursor.getInt(cursor.getColumnIndex("read"));
            int msg_user_id         = cursor.getInt(cursor.getColumnIndex("msg_user_id"));

            //Double total        = cursor.getDouble(cursor.getColumnIndex("total"));
            int position            = cursor.getPosition()+1;

            if(position<10)
            {
                serialNo.setText("0"+position);
            }else
            {
                serialNo.setText(""+position);
            }


            msgTitle.setText(msg_title);
            msgPubDate.setText(msg_pub_date);
            msgTime.setText(msg_pub_time);

            if(msg_read_status==1)
            {

                llHolder.setBackgroundColor(appContext.getResources().getColor(R.color.unread_msg_row_bg));

            }else
            {
                serialNo.setTypeface(null, Typeface.BOLD);
                msgTitle.setTypeface(null, Typeface.BOLD);
                msgPubDate.setTypeface(null, Typeface.BOLD);
                msgTime.setTypeface(null, Typeface.BOLD);

            }


//
//            txtViewTradeName.setText(trade_name);
//            txtViewDate.setText(visit_date);
//            txtViewOrderNo.setText(visit_id);



//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
        }


    }


}
