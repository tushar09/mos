package com.amadeyr.amadeyrsalesrepresentative.adapters;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import java.io.File;

/**
 * Created by amadeyr on 1/13/16.
 */
public class ASRPrescriptionGroupListCursorAdapter extends SimpleCursorAdapter
{

    private Context appContext;
    private APCDatabaseHelper dbHelper;

    public ASRPrescriptionGroupListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        appContext = context;
        dbHelper   = APCDatabaseHelper.getInstance(appContext);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        ImageView thumbnail             = (ImageView) view.findViewById(R.id.prescriptionThumbnail);
        ImageView status                = (ImageView) view.findViewById(R.id.imgViewPresStatus);
        TextView groupNo                = (TextView) view.findViewById(R.id.txtViewGroupName);
        TextView groupTime              = (TextView) view.findViewById(R.id.txtViewPrescriptionTime);
        TextView imageCount             = (TextView) view.findViewById(R.id.txtViewNoOfImages);
        TextView groupT                 = (TextView) view.findViewById(R.id.txtViewTime);
        TextView prescriptionDr         = (TextView) view.findViewById(R.id.txtViewPrescriptionDr);

        status.setVisibility(View.INVISIBLE);

        if(cursor!=null)
        {
            String groupName                = "Prescription "+cursor.getString(cursor.getColumnIndex("group_id"));
            String imageFileName            = cursor.getString(cursor.getColumnIndex("image_name"));
            String creationDate             = APCDateHelper.getDateForPrescriptionFromMilliseconds(cursor.getLong(cursor.getColumnIndex("creation_time")));
            String creationTime             = APCDateHelper.getTimeFromMilliseconds(cursor.getLong(cursor.getColumnIndex("creation_time")));
            String count                    = "3";

            File imgFile                    = new  File(imageFileName);
            BitmapFactory.Options options   = new BitmapFactory.Options();
            //options.inJustDecodeBounds = true;
            options.inSampleSize       = 16;


            if(imgFile.exists())
            {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                thumbnail.setImageBitmap(myBitmap);
                thumbnail.setRotation(90f);

            }


            groupNo.setText(groupName);
            groupTime.setText(creationDate);
            imageCount.setText(count);
            groupT.setText(creationTime);
            prescriptionDr.setText(cursor.getString(cursor.getColumnIndex("doctor_name")));

//            Cursor count_cursor     = dbHelper.getPrescriptionEntry(cursor.getString(cursor.getColumnIndex("group_id")));
//            int t_count = 0;
//            if(count_cursor!=null) t_count = count_cursnor.getCount();
//
//            if(t_count==0) status.setVisibility(View.VISIBLE);

            //if(cursor.getString(cursor.getColumnIndex("completion_status")).contentEquals("INCOMPLETE")) status.setVisibility(View.VISIBLE);



        }







    }
}
