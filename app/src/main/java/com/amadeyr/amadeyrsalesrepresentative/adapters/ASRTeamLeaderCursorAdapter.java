package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by acl on 5/12/15.
 */
public class ASRTeamLeaderCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;
    public ASRTeamLeaderCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {



        TextView salesRepName        = (TextView) view.findViewById(R.id.txtViewSalesRepName);


        if (cursor != null) {
            //

            salesRepName.setText(cursor.getString(cursor.getColumnIndex("employee_id")));

        }

    }
}
