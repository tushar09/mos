package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;

/**
 * Created by amadeyr on 1/26/16.
 */
public class ASRPrescriptionSubmissionCursorAdapter extends SimpleCursorAdapter
{


    private Context appContext;
    private APCDateHelper apcDate;
    private int layout_id;
    public ArrayList<String> selectedForSubmission;
    public DateTime jDt;
    public DateTimeFormatter formatter;

    public ASRPrescriptionSubmissionCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        selectedForSubmission = new ArrayList<>();
        appContext  = context;
        layout_id   = layout;
        apcDate     = new APCDateHelper();


    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {



        TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewPSRowSerialID);
        TextView txtViewTradeName       = (TextView) view.findViewById(R.id.txtViewPSRowTradeName);
        TextView txtViewDate            = (TextView) view.findViewById(R.id.txtViewPSRowDate);
        TextView txtViewOrderNo         = (TextView) view.findViewById(R.id.txtViewPSRowOrderNo);
        final CheckBox chkSubmission    = (CheckBox) view.findViewById(R.id.chkBoxSelect);



        if(chkSubmission.isChecked())
        {

            chkSubmission.setChecked(false);
        }

        String group_id                 = "";

        if (cursor != null)
        {
            //

            group_id                                = cursor.getString(cursor.getColumnIndex("group_id"));
            String prescription_id                  = "Prescription "+group_id;
            long creation_time                      = cursor.getLong(cursor.getColumnIndex("creation_time"));
            String formatted_creation_time          = apcDate.getDateForPrescriptionFromMilliseconds(creation_time)+ " "+ APCDateHelper.getTimeFromMilliseconds(creation_time);



            int position        = cursor.getPosition()+1;

            if(position<10)
            {
                txtViewSerialNo.setText(""+position);
            }else
            {
                txtViewSerialNo.setText("0"+position);
            }

            txtViewTradeName.setText(prescription_id);
            txtViewDate.setText(formatted_creation_time);
            txtViewOrderNo.setText(group_id);




        }

        final String finalOrder_no = group_id;

        if(selectedForSubmission.contains(finalOrder_no)) chkSubmission.setChecked(true);

        chkSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(chkSubmission.isChecked())
                {

                    selectedForSubmission.add(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();


                }else
                {
                    selectedForSubmission.remove(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }

}
