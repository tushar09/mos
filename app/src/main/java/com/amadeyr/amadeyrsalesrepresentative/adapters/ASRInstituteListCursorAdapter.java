package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by amadeyr on 11/4/15.
 */
public class ASRInstituteListCursorAdapter extends SimpleCursorAdapter
{

    private APCDateHelper apcDate;
    private int layout_id;

    public ASRInstituteListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        String insName              = cursor.getString(cursor.getColumnIndex("institute_name"));
        String insCode              = cursor.getString(cursor.getColumnIndex("institute_code"));


        TextView instituteName      = (TextView) view.findViewById(R.id.txtViewInstituteName);

        if (cursor != null)
        {
            //

            instituteName.setText(insName+" | "+insCode);

        }

    }
}
