package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import java.text.DecimalFormat;

/**
 * Created by amadeyr on 11/9/15.
 */
public class ASRDoctorVisitCursorAdapter extends SimpleCursorAdapter
{

    private APCDateHelper apcDate;
    private int layout_id;
    private DecimalFormat decFormatter;

    public ASRDoctorVisitCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
        decFormatter    = new DecimalFormat("0.00");
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {



        //TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewOSRowSerialID);
        TextView txtViewProductName     = (TextView) view.findViewById(R.id.tvDVRowName);
        TextView txtViewQty             = (TextView) view.findViewById(R.id.tvDVRowQty);
        ImageView dvIcon                = (ImageView) view.findViewById(R.id.imgViewDVSItemIcon);
        String catName                  = cursor.getString(cursor.getColumnIndex("category_name"));

        if (cursor != null)
        {
            //

            String order_no     = cursor.getString(cursor.getColumnIndex("visit_id"));
            String trade_name   = cursor.getString(cursor.getColumnIndex("item_name"));
            //String order_date   = cursor.getString(cursor.getColumnIndex("order_date"));
            int qty             = (int) cursor.getDouble(cursor.getColumnIndex("qty"));
            dvIcon.setImageDrawable(null);

            if(catName.contentEquals("Samples"))
            {
                dvIcon.setImageResource(R.drawable.product);

            }else if(catName.contentEquals("Gifts"))
            {
                dvIcon.setImageResource(R.drawable.gift);


            }else if(catName.contentEquals("PPM"))
            {
                dvIcon.setImageResource(R.drawable.ppm);


            }


            txtViewProductName.setText(trade_name);
            //txtViewPrice.setText(""+price);
            txtViewQty.setText(""+qty);



        }

    }
}
