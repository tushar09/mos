package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.R;

import java.io.File;
import java.util.ArrayList;


public class ASRPrescriptionImageAdapter extends SimpleCursorAdapter {
    private Context context;
    public ArrayList<String> selectedForSubmission;

    //private final String[] mobileValues;

//    public ASRPrescriptionImageAdapter(Context context, String[] mobileValues) {
//        this.context = context;
//        this.mobileValues = mobileValues;
//    }

    public ASRPrescriptionImageAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        selectedForSubmission = new ArrayList<>();
    }



    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        final CheckBox gridCheckBox   = (CheckBox) view.findViewById(R.id.cellCheckBox);
        ImageView imageView     = (ImageView) view.findViewById(R.id.cellImage);

        if(gridCheckBox.isChecked())
        {

            gridCheckBox.setChecked(false);
        }

        if(cursor!=null)
        {

            String img_path         = cursor.getString(cursor.getColumnIndex("image_name"));
            File imgFile            = new  File(img_path);

            BitmapFactory.Options options   = new BitmapFactory.Options();
            //options.inJustDecodeBounds = true;
            options.inSampleSize       = 8;

            if(imgFile.exists())
            {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
                imageView.setImageBitmap(myBitmap);

            }

            final String entry_id = cursor.getString(cursor.getColumnIndex("_id"));

            if(selectedForSubmission.contains(entry_id)) gridCheckBox.setChecked(true);


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(gridCheckBox.isChecked())
                    {

                        gridCheckBox.setChecked(false);
                        selectedForSubmission.remove(entry_id);

                        //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();


                    }else
                    {
                        gridCheckBox.setChecked(true);
                        selectedForSubmission.add(entry_id);

                        //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                    }
                }
            });

            gridCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {

                    if(gridCheckBox.isChecked())
                    {

                        selectedForSubmission.add(entry_id);
                        //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();


                    }else
                    {
                        selectedForSubmission.remove(entry_id);
                        //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                    }

                }
            });

        }





    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}