package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amadeyr on 11/12/15.
 */
public class ASRVisitSCursorAdapter extends SimpleCursorAdapter {

    private Context appContext;
    private APCDateHelper apcDate;
    private int layout_id;
    public ArrayList<String> selectedForSubmission;
    private List<String> incompleteList;
    public DateTime jDt;
    public DateTimeFormatter formatter;

    public ASRVisitSCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        selectedForSubmission = new ArrayList<>();
        appContext  = context;
        layout_id   = layout;
        apcDate     = new APCDateHelper();


    }

    public void setIncompleteList(List a)
    {
        incompleteList = a;

    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {



        TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewPSRowSerialID);
        TextView txtViewTradeName       = (TextView) view.findViewById(R.id.txtViewPSRowTradeName);
        TextView txtViewDate            = (TextView) view.findViewById(R.id.txtViewPSRowDate);
        TextView txtViewOrderNo         = (TextView) view.findViewById(R.id.txtViewPSRowOrderNo);
        final CheckBox chkSubmission    = (CheckBox) view.findViewById(R.id.chkBoxSelect);
        //ImageView ivStatusIndicator     = (ImageView) view.findViewById(R.id.ivVisitStatusIndicator);


        if(chkSubmission.isChecked())
        {

            chkSubmission.setChecked(false);
        }

        String order_no                 = "";



        if (cursor != null)
        {
            //

            order_no            = cursor.getString(cursor.getColumnIndex("visit_id"));
            String trade_name   = cursor.getString(cursor.getColumnIndex("doctor_name"))+" | "+cursor.getString(cursor.getColumnIndex("doctor_code"));
            String order_date   = cursor.getString(cursor.getColumnIndex("visit_date"));
            //Double total        = cursor.getDouble(cursor.getColumnIndex("total"));

            formatter           = DateTimeFormat.forPattern("yyyy-MM-dd");
            jDt                 = formatter.parseDateTime(order_date);


            formatter           = DateTimeFormat.forPattern("dd MMM yy");


            int position        = cursor.getPosition()+1;

            if(position<10)
            {
                txtViewSerialNo.setText(""+position);
            }else
            {
                txtViewSerialNo.setText("0"+position);
            }

            txtViewTradeName.setText(trade_name);
            txtViewDate.setText(formatter.print(jDt));
            txtViewOrderNo.setText(order_no);




//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
        }

        final String finalOrder_no = order_no;

        if(selectedForSubmission.contains(finalOrder_no)) chkSubmission.setChecked(true);

        chkSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(chkSubmission.isChecked())
                {

                    selectedForSubmission.add(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();


                }else
                {
                    selectedForSubmission.remove(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }
}
