package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amadeyr on 11/5/15.
 */
public class ASRDoctorListCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;
    private List<String> incompleteList;
    public ArrayList<String> selectedForSubmission;
    public Context ctx;

    public ASRDoctorListCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
        selectedForSubmission = new ArrayList<>();
        ctx = context;

    }

    public void setIncompleteList(List a)
    {
        incompleteList = a;

    }


    @Override
    public void bindView(View view, final Context context, Cursor cursor)
    {

        final String docName            = cursor.getString(cursor.getColumnIndex("doctor_name"));
        final String docCode            = cursor.getString(cursor.getColumnIndex("doctor_code"));


        TextView doctorName             = (TextView) view.findViewById(R.id.txtViewDoctorListName);
        final CheckBox chkSubmission    = (CheckBox) view.findViewById(R.id.chkBox0DoctorSelect);
        ImageView ivStatusIndicator     = (ImageView) view.findViewById(R.id.ivVisitStatusIndicator);

        if(chkSubmission.isChecked())
        {

            chkSubmission.setChecked(false);
        }


        if (cursor != null) {
            //

            doctorName.setText(docName+" | "+docCode);

            ivStatusIndicator.setVisibility(View.INVISIBLE);

            if(incompleteList!=null)
            {

                if(incompleteList.contains(docCode))
                {
                    ivStatusIndicator.setBackground(ctx.getResources().getDrawable(R.drawable.orange_circle));
                    ivStatusIndicator.setVisibility(View.VISIBLE);
                }

//                    for(int lIterator = 0 ; lIterator< incompleteList.size(); ++lIterator)
//                    {
//                        String present_id = (String) incompleteList.get(lIterator);
//
//                        if(present_id.contentEquals(customer_id))
//                        {
//                            ivStatusIndicator.setBackground(appContext.getDrawable(R.drawable.orange_circle));
//                            ivStatusIndicator.setVisibility(View.VISIBLE);
//                            break;
//                        }
//
//                    }
            }


        }

        if(selectedForSubmission.contains(docCode+"<sep>"+docName)) chkSubmission.setChecked(true);

        chkSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(chkSubmission.isChecked())
                {
                    selectedForSubmission.add(docCode+"<sep>"+docName);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();

                }else
                {
                    selectedForSubmission.remove(docCode+"<sep>"+docName);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }

}
