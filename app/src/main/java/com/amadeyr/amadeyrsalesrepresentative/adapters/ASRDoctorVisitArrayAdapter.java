package com.amadeyr.amadeyrsalesrepresentative.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by amadeyr on 11/3/15.
 */
public class ASRDoctorVisitArrayAdapter extends ArrayAdapter<String> {
    public Context appContext;
    public String[] spinnerValues;
    public String[] spinnerSubs;
    public int total_images[];

    public ASRDoctorVisitArrayAdapter(Context context, int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
        appContext = context;

    }

    public void renderSpinnerValues(Cursor itemList)
    {
        if(itemList!=null)
        {
            itemList.moveToFirst();
            int cursorSize = itemList.getCount();

            spinnerValues   = new String[cursorSize];
            spinnerSubs     = new String[cursorSize];
            //total_images    = new int[cursorSize];
            total_images    = new int[]{R.drawable.product, R.drawable.gift, R.drawable.ppm};

            for(int idx = 0; idx<cursorSize;++idx)
            {

                spinnerValues[idx] = itemList.getString(itemList.getColumnIndex("category_code"));

                if(itemList.getString(itemList.getColumnIndex("category_name")).equals("Product"))
                {
                    spinnerSubs[idx] = "Sample";

                }else
                {
                    spinnerSubs[idx] = itemList.getString(itemList.getColumnIndex("category_name"));

                }

                itemList.moveToNext();

            }


        }

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent)
    {

        LayoutInflater inflater = (LayoutInflater) appContext.getSystemService(appContext.LAYOUT_INFLATER_SERVICE);

        View mySpinner = inflater.inflate(R.layout.doctor_visit_spinner, parent, false);
        TextView main_text = (TextView) mySpinner.findViewById(R.id.dv_item_caption);
        main_text.setText(spinnerSubs[position]);

        ImageView left_icon = (ImageView) mySpinner.findViewById(R.id.dv_item_icon);
        left_icon.setImageResource(total_images[position]);
        return mySpinner;
    }



}
