package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by acl on 6/2/15.
 */
public class APCFileOperationsHelper
{

    private Context appContext;
    public APCFileOperationsHelper(Context ctx)
    {
        appContext = ctx;


    }

    public void unzip(String _zipFile, String _targetLocation, String targetFileName) throws IOException {

        System.out.println("Inside Unzip method");
        //_zipFile = "data/data/"_zipFile
        File f              = new File(_targetLocation);

        FileInputStream fin = new FileInputStream(_zipFile);
        ZipInputStream  zin = new ZipInputStream(fin);

        ZipEntry ze         = null;

//        String fileName     = ze.getName();
//
//        if(fileName.contains(".zip"))
//        {
//            fileName        = fileName.substring(0,fileName.length()-4);
//
//        }



        while((ze = zin.getNextEntry())!= null)
        {



//            if(fileName.contains("."))
//            {
//                fileName        = fileName.substring(0,fileName.length()-4);
//
//            }

            FileOutputStream fout = new FileOutputStream(_targetLocation+targetFileName);
            byte[] buffer = new byte[8192];
            int c;
            while((c = zin.read(buffer))!=-1)
            {
                fout.write(buffer,0,c);

            }
            zin.closeEntry();
            fout.close();

        }

        zin.close();

//        if(f.exists() && f.isDirectory())
//        {
//            Log.v("Downloads Folder", "Exists");
//            FileInputStream fin = new FileInputStream(_zipFile);
//            ZipInputStream  zin = new ZipInputStream(fin);
//
//            ZipEntry ze         = null;
//
//            while((ze = zin.getNextEntry())!= null)
//            {
//                FileOutputStream fout = new FileOutputStream(_targetLocation+ze.getName());
//
//                for(int c = zin.read();c!=-1;c=zin.read())
//                {
//                    fout.write(c);
//
//                }
//                zin.closeEntry();
//                fout.close();
//
//            }
//
//            zin.close();
//
//        }else
//        {
//
//            Log.v("Downloads Folder", "Doesn't Exist Making One ... ");
//            //f.mkdir();
//
//
//
//
//        }


//        FileInputStream fin = new FileInputStream(_zipFile);
//        ZipInputStream  zin = new ZipInputStream(fin);
//
//        ZipEntry ze         = null;
//
//        while((ze=zin.getNextEntry())!=null)
//        {
//            if(ze.isDirectory())
//            {
//
//
//
//            }else
//            {
//
//
//            }
//
//        }

    }

    public void unzipFolder(String _zipFile, String _targetLocation, String targetFileName) throws IOException {

        System.out.println("Inside Unzip Folder method");

        //_zipFile = "data/data/"_zipFile

        File f              = new File(_targetLocation);
        FileInputStream fin = new FileInputStream(_zipFile);
        ZipInputStream  zin = new ZipInputStream(fin);

        ZipEntry ze         = null;


        while((ze = zin.getNextEntry())!= null)
        {

            String fileNames = ze.getName();

            if(ze.isDirectory())
            {
                File unzipperDir = new File(_targetLocation+"/"+fileNames);
                unzipperDir.mkdirs();

            }else
            {

                FileOutputStream fout = new FileOutputStream(_targetLocation+"/"+fileNames);
                int c;

                while((c = zin.read())!=-1)
                {
                    fout.write(c);

                }

                zin.closeEntry();
                fout.close();


            }
            //if(ze.isDirectory())



        }

        zin.close();

    }



}
