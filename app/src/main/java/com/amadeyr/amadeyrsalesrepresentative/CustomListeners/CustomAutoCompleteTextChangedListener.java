package com.amadeyr.amadeyrsalesrepresentative.CustomListeners;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.amadeyr.amadeyrsalesrepresentative.APCPendingSubmissionsActivity;
//import com.amadeyr.amadeyrsalesrepresentative.APCSearchProductActivity;
import com.amadeyr.amadeyrsalesrepresentative.APSCustomerSelectionActivity;
import com.amadeyr.amadeyrsalesrepresentative.ASRDoctorSelectionActivity;
import com.amadeyr.amadeyrsalesrepresentative.ASRDualSubmissionActivity;
import com.amadeyr.amadeyrsalesrepresentative.ASRInstituteSelectionActivity;
import com.amadeyr.amadeyrsalesrepresentative.ASRPrescriptionImageBrowser;
import com.amadeyr.amadeyrsalesrepresentative.ASRTeamLeaderActivity;
import com.amadeyr.amadeyrsalesrepresentative.DoctorVisitActivity;
import com.amadeyr.amadeyrsalesrepresentative.OrderScreenActivity;
import com.amadeyr.amadeyrsalesrepresentative.R;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRDoctorVisitArrayAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderEntries;

/**
 * Created by acl on 5/25/15.
 */
public class CustomAutoCompleteTextChangedListener implements TextWatcher {


    private Context appContext;
    private String type;
    private String fragmentName;

    public CustomAutoCompleteTextChangedListener(Context context)
    {

        this.appContext = context;
        type            = "";
        fragmentName    = "";
    }

    public CustomAutoCompleteTextChangedListener(Context context,String type, String fragmentName)
    {
        this.appContext = context;
        this.type       = type;
        this.fragmentName   = fragmentName;

    }



    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3)
    {

        if(type.contentEquals("")) {

//            if (appContext instanceof APCSearchProductActivity) {
//                APCSearchProductActivity searchActivity = ((APCSearchProductActivity) appContext);
//                searchActivity.populateSearchAdapter(charSequence.toString());
//
//            } else

            if (appContext instanceof OrderScreenActivity) {
                OrderScreenActivity searchActivity = ((OrderScreenActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof APCPendingSubmissionsActivity) {
                APCPendingSubmissionsActivity searchActivity = ((APCPendingSubmissionsActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof APSCustomerSelectionActivity) {
                APSCustomerSelectionActivity searchActivity = ((APSCustomerSelectionActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof ASRTeamLeaderActivity) {
                ASRTeamLeaderActivity searchActivity = ((ASRTeamLeaderActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof ASRDoctorSelectionActivity) {
                ASRDoctorSelectionActivity searchActivity = ((ASRDoctorSelectionActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof ASRInstituteSelectionActivity) {
                ASRInstituteSelectionActivity searchActivity = ((ASRInstituteSelectionActivity) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());


            } else if (appContext instanceof DoctorVisitActivity) {
                DoctorVisitActivity searchActivity = ((DoctorVisitActivity) appContext);
                searchActivity.populateSearchAdapter(searchActivity.spnrItemSelector.getSelectedItem().toString(), charSequence.toString());

            }else if (appContext instanceof ASRPrescriptionImageBrowser)
            {
                ASRPrescriptionImageBrowser searchActivity = ((ASRPrescriptionImageBrowser) appContext);
                searchActivity.populateSearchAdapter(charSequence.toString());

            }

        }else if(type.contentEquals("MULTIPLE"))
        {
            if (appContext instanceof ASRPrescriptionImageBrowser)
            {
                ASRPrescriptionImageBrowser searchActivity = ((ASRPrescriptionImageBrowser) appContext);

                if(fragmentName.contentEquals("PRODUCT"))
                {
                    searchActivity.populateSearchAdapter(charSequence.toString());

                }else if(fragmentName.contentEquals("DOCTOR"))
                {
                    AutoCompleteTextView b = (AutoCompleteTextView) ((ASRPrescriptionImageBrowser) appContext).findViewById(R.id.presDoctorSearch);

                    if(b.isPerformingCompletion()) return;
                    searchActivity.populateDoctorSearchAdapter(charSequence.toString());
                    Log.d("TextChangeListener: ","Still Functional");

                }


            }

        }else if(type.contentEquals("FRAGMENT"))
        {
            if (appContext instanceof ASRDualSubmissionActivity)
            {
                ASRDualSubmissionActivity searchActivity = ((ASRDualSubmissionActivity) appContext);
                searchActivity.invokeFragmentAutocompleteSearch(this.fragmentName);

            }

        }




    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
