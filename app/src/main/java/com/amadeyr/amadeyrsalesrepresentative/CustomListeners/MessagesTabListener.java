package com.amadeyr.amadeyrsalesrepresentative.CustomListeners;

import android.app.ActionBar;

import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;

import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by amadeyr on 10/6/15.
 */
public class MessagesTabListener<T extends Fragment> implements android.support.v7.app.ActionBar.TabListener {
    private Fragment fragment;

    public MessagesTabListener(Fragment frg)
    {
        this.fragment = frg;
    }


    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft)
    {
        
        ft.replace(R.id.container, fragment, null);


    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft)
    {

    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, android.support.v4.app.FragmentTransaction ft)
    {

    }
}
