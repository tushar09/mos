package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by amadeyr on 12/1/15.
 */
public class ASRConfig
{

    public static final String DOCTOR_VISIT_SAVE_EXTENSION = "/visits/save";
    public static final String REPORT_EXTENSION = "/esalesreport";
    public static final String POST_MSG_EXT = "/message/savemessage";
    public static String updateurl ="/arms_settings_controller/getapp_update_details";
    public final static String appstorepath = "/app_updates/";
    public final static int ARMS_DB_VERSION = 3;
    public final static int TIMEOUT_VALUE = 5000;													//MUST BE in milliseconds
    public final static int CONNECTION_TIMEOUT_VALUE = 5000;													//MUST BE in milliseconds

//    public final static String RMS_JSON_CONTROLLER 						= SERVER_IP_ADDRESS+"/ACL_Arms/index.php/rms_controller/json/";
//    final static String RMS_JSON_CONTROLLER_STAFF 						= SERVER_IP_ADDRESS+"/ACL_Arms/index.php/rms_controller/jsonStaff/";
//    public final static String RMS_APP_LOGIN_CONTROLLER					= SERVER_IP_ADDRESS+"/ACL_Arms/index.php/arms_main_controller/appUserLogin/";
//    public final static String APP_LOGIN_PATH					= "/app_login_controller/";
//    public final static String NET_CONNECTIVITY_PATH			= "/arms_main_controller/netConnectivity/";
    //final static String RMS_APP_NET_CONNECTIVITY_CONTROLLER = SERVER_IP_ADDRESS+"/ACL_Arms/index.php/arms_main_controller/netConnectivity/";
    public final static String RMS_APP_NET_CONNECTIVITY_CONTROLLER  = "/app_login_controller/netConnectivity";
    public final static String APP_UPDATE_LINK_EXTENSION             = "/index.php/asp_api/getAppUpdateUrl";
    public final static String GET_HASHES_EXTENSION                 = "/sync/gethashes";
    public final static String APP_UPDATE_URL_EXTENSION             = "/sync/getapk";
    public final static String PRODUCT_DB_DL_URL                    = "/getproducturl";
    public final static String OTHER_PRODUCT_DB_DL_URL              = "/getotherproducturl";
    public final static String USER_DB_DL_URL                       = "/getuserdburl";
    public final static String PRODUCT_SYNC_URL_EXTENSION           = "/sync/syncproduct?token=";
    public final static String PRODUCT_DL_URL_EXTENSION             = "/sync/downloadproducts?token=";

    public final static String USER_SYNC_URL_EXTENSION              = "/sync/syncuser?token=";
    public final static String USER_DL_URL_EXTENSION                = "/sync/downloadusers?token=";
    public final static String OTHER_PRODUCT_SYNC_URL_EXTENSION     = "/sync/otherproducts?token=";
    public final static String OTHER_PRODUCT_DL_URL_EXTENSION       = "/sync/downloadother?token=";


    public final static String CHECK_HASH_URL_EXTENSION             = "/sync/checkhash?token=";
    public final static String LOG_IN_EXTENSION                     = "/authmob";
    public final static String ORDER_SAVE_EXTENSION                 = "/orders/save";
    public final static String PULL_MESSAGE_EXT                     = "/message/getmessage";
    public final static int QUEUE_CHECK_WAIT_TIME                   = 15000;

    public final static String DEFAULT_PREFERENCES_NAME             = "apcpref";
    public final static String DOWNLOAD_DIRECTORY_NAME              = "ASRDownloads";
    public final static String DOWNLOAD_FILE_NAME                   = "amadeyrSalesRepresentative.apk";

    public final static String PRODUCTS_DB_ZIP_FILE_NAME                = "apsProductDB.zip";
    public final static String OTHER_PRODUCTS_DB_ZIP_FILE_NAME          = "asrOtherProductDB.zip";
    public final static String USER_DB_ZIP_FILE_NAME                    = "asrUserDB.zip";


    public final static String PRESCRIPTION_IMAGE_SUBMISSION_EXT    = "/prescription/uploadimage";
//    public final static String RMS_RULES_JSON							= "/arms_rules_handler/getRules";
//    public final static String RMS_RULES_UPDATE_JSON				    = "/arms_rules_handler/getRulesHash";
//    public final static String RMS_BACKUP_CONTROLLER				    = "/report_controller/data_backup";
//
//    final static String XML_FORM_DOWNLOAD_PATH 							= SERVER_IP_ADDRESS+"/downloads/armsTest.xml";
//    final static String FORM_NAME 										= "armsTest.xml";
//    public final static String FORM_SAVE_PATH 							= "/sdcard/odk/forms/";
//    //"/LocalDisk/odk/forms/";
//    final static String AMADEYR_FORM 									= "http://amadeyr.net:8080/ODKAggregate/www/formXml?readable=true&formId=build_Baseline-Survey-DTA-2_1328781759";
//
//    // Control Values
//
//    public final static int LIST_SCRL_DISTANCE_NORMAL					= 100 ;
//    public final static int LIST_SCRL_SPEED_NORMAL						= 350;
//
//    public final static int LIST_SCRL_DISTANCE_LONG						= 1000;
//    public final static int LIST_SCRL_SPEED_LONG						= 350;


}
