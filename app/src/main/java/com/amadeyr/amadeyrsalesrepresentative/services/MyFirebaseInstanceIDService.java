package com.amadeyr.amadeyrsalesrepresentative.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by acl on 5/31/16.
 */



public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{

    private final static String TAG = "MyFBInstanceIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);


    }




}
