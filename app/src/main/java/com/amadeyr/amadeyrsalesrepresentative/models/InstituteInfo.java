package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 11/3/15.
 */
public class InstituteInfo {
    public String Institute_id, institute_name, proprietary_name, employee_code, address;


    public InstituteInfo()
    {

    }

    public InstituteInfo(String id, String institute_name, String proprietary_name, String address, String employee_code)
    {
        this.Institute_id = id;
        this.institute_name = institute_name;
        this.proprietary_name = proprietary_name;
        this.address = address;
        this.employee_code  = employee_code;

    }


    public String getInstituteId()
    {
        return this.Institute_id;
    }

    public void setInstituteId(String id)
    {
        this.Institute_id = id;
    }


    public String getInstituteName()
    {
        return this.institute_name;
    }

    public void setInstituteName(String institute_name)
    {
        this.institute_name = institute_name;
    }


    public String getProprietaryName()
    {
        return this.proprietary_name;
    }

    public void setProprietaryName(String pName)
    {
        this.proprietary_name = pName;
    }

    public String getAddress()
    {
        return this.address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getEmployeeCode()
    {
        return this.employee_code;
    }

    public void setEmployeeCode(String EmployeeCode)
    {
        this.employee_code = EmployeeCode;
    }



}
