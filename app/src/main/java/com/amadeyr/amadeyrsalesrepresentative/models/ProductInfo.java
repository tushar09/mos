package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by acl on 5/12/15.
 */
public class ProductInfo {

    public String id, name, offer, generic_name,unit, category_code;
    public double stock_qty, price;


    public ProductInfo(String id, String name, String generic_name, String unit, double stockQty, double price, String offer,String category_code)
    {
        this.id = id;
        this.name = name;
        this.generic_name = generic_name;
        this.unit = unit;
        this.offer = offer;
        this.stock_qty = stockQty;
        this.price  = price;
        this.category_code = category_code;

    }

    public ProductInfo()
    {


    }


    public String getProductID()
    {
        return this.id;
    }

    public void setProductID(String product_id)
    {
        this.id = product_id;
    }

    public String getProductName()
    {
        return this.name;
    }

    public void setProductName(String product_name)
    {
        this.name = product_name;
    }

    public String getGenericName()
    {
        return this.generic_name;
    }

    public void setGenericName(String generic_name)
    {
        this.generic_name = generic_name;
    }

    public String getUnit(){ return this.unit;}

    public void setUnit(String unit){ this.unit= unit;}


    public double getProductStock()
    {
        return this.stock_qty;
    }

    public void setProductStock(double count)
    {
        this.stock_qty = count;
    }

    public double getProductPrice()
    {
        return this.price;
    }

    public void setProductPrice(double price)
    {
        this.price = price;
    }

    public String getOffer(){ return this.offer;}

    public void setOffer(String offer){ this.offer = offer;}

    public String getCategoryCode(){ return this.category_code;}

    public void setCategoryCode(String code){ this.category_code = code;}





}
