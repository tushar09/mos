package com.amadeyr.amadeyrsalesrepresentative.models;

import android.content.Context;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCProductDatabaseHelper;

/**
 * Created by amadeyr on 12/15/15.
 */
public class ASRDatabaseFactory
{
    private static APCProductDatabaseHelper productsDBConnection;
    private static ASROtherProductDatabaseHelper otherProductsDBConnection;
    private static APCProductDatabaseHelper userDBConnection;

    public ASRDatabaseFactory()
    {


    }

    public static APCProductDatabaseHelper getProductsDBInstance(Context c)
    {
        if(productsDBConnection==null)
        {

            productsDBConnection = new APCProductDatabaseHelper(c,"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);

        }

        return productsDBConnection;

    }

    public static ASROtherProductDatabaseHelper getOtherProductsDBInstance(Context c)
    {
        if(otherProductsDBConnection==null)
        {

            otherProductsDBConnection = new ASROtherProductDatabaseHelper(c,ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_NAME,null,ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_VERSION);

        }

        return otherProductsDBConnection;

    }

    public static APCProductDatabaseHelper getUserDBInstance(Context c)
    {
        if(userDBConnection==null)
        {

            userDBConnection = new APCProductDatabaseHelper(c,APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);

        }

        return userDBConnection;

    }

    public static void refreshDBConnections(Context c)
    {
        productsDBConnection = new APCProductDatabaseHelper(c,"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        otherProductsDBConnection = new ASROtherProductDatabaseHelper(c,ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_NAME,null,ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_VERSION);
        userDBConnection = new APCProductDatabaseHelper(c,APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);


    }





}
