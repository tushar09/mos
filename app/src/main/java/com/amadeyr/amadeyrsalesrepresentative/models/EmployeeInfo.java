package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 10/13/15.
 */
public class EmployeeInfo
{

    public String employee_id, employee_name, employee_role;
    public final String TEAM_LEAD_ROLE = "SUPERVISOR";
    public final String SALES_REP_ROLE = "SALESREP";

    public static final String TEAM_LEADER = "TL";
    public static final String SALES_REPRESENTATIVE = "SR";

    public EmployeeInfo()
    {

    }

    public EmployeeInfo(String id, String name, String role)
    {
        this.employee_id = id;
        this.employee_name = name;
        this.employee_role = role;

    }


    public String getEmployeeId()
    {
        return this.employee_id;
    }

    public void setEmployeeId(String id)
    {
        this.employee_id = id;
    }


    public String getEmployeeName()
    {
        return this.employee_name;
    }

    public void setEmployeeName(String name)
    {
        this.employee_name = name;
    }


    public String getEmployeeRole()
    {
        return this.employee_role;
    }

    public void setEmployeeRole(String role)
    {
        this.employee_role = role;
    }


}
