package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 8/27/15.
 */
public class CustomerInfo
{

    public String customer_id, trade_name, proprietary_name, contact_no, address;


    public CustomerInfo()
    {

    }

    public CustomerInfo(String id, String trade_name, String proprietary_name, String address, String contact_no)
    {
        this.customer_id = id;
        this.trade_name = trade_name;
        this.proprietary_name = proprietary_name;
        this.address = address;
        this.contact_no  = contact_no;

    }


    public String getCustomerId()
    {
        return this.customer_id;
    }

    public void setCustomerId(String id)
    {
        this.customer_id = id;
    }


    public String getTradeName()
    {
        return this.trade_name;
    }

    public void setTradeName(String trade_name)
    {
        this.trade_name = trade_name;
    }


    public String getProprietaryName()
    {
        return this.proprietary_name;
    }

    public void setProprietaryName(String pName)
    {
        this.proprietary_name = pName;
    }

    public String getAddress()
    {
        return this.address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getContactNo()
    {
        return this.contact_no;
    }

    public void setContactNo(String contactNo)
    {
        this.contact_no = contactNo;
    }



}
