package com.amadeyr.amadeyrsalesrepresentative.models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amadeyr on 11/26/15.
 */
public class ASROtherProductDatabaseHelper extends SQLiteOpenHelper
{
    public static final String  OTHER_PRODUCT_DB_NAME       = "asrOtherProductDB";
    public static final int     OTHER_PRODUCT_DB_VERSION    = 1;

    private static final String OTHER_PRODUCT_TABLE             = "aps_other_products";
    private static final String PRODUCT_CATEGORY_TABLE          = "aps_product_category";


    public Context appCtx;

    public ASROtherProductDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        appCtx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getOtherProductInfoFromName(String mode, String searchString)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "";

        query               = "SELECT * FROM "+OTHER_PRODUCT_TABLE+" JOIN aps_product_category ON aps_other_products.category_code=aps_product_category.category_code WHERE aps_product_category.category_name='"+mode+"' AND aps_other_products.product_name LIKE \"%"+searchString+"%\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;
    }

    public Cursor getAllCategory()
    {

        SQLiteDatabase db   = this.getWritableDatabase();
        String query        = "SELECT * FROM " + PRODUCT_CATEGORY_TABLE;

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public List<ProductInfo> getAutoCompleteOtherProductInfo(String type,String searchTerm)
    {

        SQLiteDatabase db       =  this.getWritableDatabase();
        String query            =  "";

        query                   = "SELECT * FROM "+OTHER_PRODUCT_TABLE+ " JOIN aps_product_category ON aps_other_products.category_code=aps_product_category.category_code WHERE aps_product_category.category_name='"+type+"' AND aps_other_products.product_name LIKE \"%"+searchTerm+"%\" ORDER BY aps_other_products.product_name DESC LIMIT 0,20";



        Cursor results      =  db.rawQuery(query,null);

        List<ProductInfo> searchResults = new ArrayList<ProductInfo>();

        if(results.moveToFirst())
        {
            do
            {

                ProductInfo pInfo               = new ProductInfo();

                    String product_id               = results.getString(results.getColumnIndex("product_code"));
                    String product_name             = results.getString(results.getColumnIndex("product_name"));




                    //pInfo.setProductBarcode(product_barcode);
                    pInfo.setProductID(product_id);
                    pInfo.setProductName(product_name);
//                    pInfo.setGenericName(product_generic_name);
//                    pInfo.setUnit(product_unit);
//                    pInfo.setProductStock(product_stock);
//                    pInfo.setProductPrice(product_price);
//                    pInfo.setOffer(product_offer);






                searchResults.add(pInfo);

            }while(results.moveToNext());


        }

        results.close();
        db.close();



        return searchResults;
    }


}
