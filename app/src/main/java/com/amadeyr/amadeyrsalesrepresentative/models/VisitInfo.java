package com.amadeyr.amadeyrsalesrepresentative.models;

import java.util.List;

/**
 * Created by amadeyr on 11/5/15.
 */
public class VisitInfo {

    public String visit_id, doctor_code, visit_date, visit_status, submit_status, doctor_name, institute_name,visit_notes,taken_by,visited_with, entry_time, visit_type;
    private final String ORDER_STATUS_SUBMITTED = "SUBMITTED";
    private final String ORDER_STATUS_PENDING   = "PENDING";

    public List<OrderEntries> entries;
    public double total,start_latitude, start_longitude, submit_latitude, submit_longitude;;

    public VisitInfo()
    {


    }



    public VisitInfo(String visit_id, String doctor_code, String doctor_name, String visit_date, String institute_name, String visit_status,double start_lat, double start_lon, double submit_lat,double submit_lon,String taken_by, String visited_with, String entry_time, String visit_type)
    {
        this.visit_id               = visit_id;
        this.doctor_code            = doctor_code;
        this.doctor_name            = doctor_name;
        this.visit_date             = visit_date;
        this.institute_name         = institute_name;
        this.visit_status           = visit_status;
        this.submit_status          = ORDER_STATUS_PENDING;
        this.start_latitude         = start_lat;
        this.start_longitude        = start_lon;
        this.submit_latitude        = submit_lat;
        this.submit_longitude       = submit_lon;
        this.visited_with           = visited_with;
        this.taken_by               = taken_by;
        this.entry_time             = entry_time;
        this.visit_type             = visit_type;


    }

    public String getVisitNo()
    {
        return this.visit_id;
    }

    public String getDoctorCode()
    {
        return this.doctor_code;
    }

    public String getDoctorName()
    {
        return this.doctor_name;
    }

    public String getVisitDate()
    {
        return this.visit_date;
    }

    public String getVisitTime()
    {
        return this.entry_time;
    }
    public String getVisitStatus()
    {
        return this.visit_status;
    }

    public List<OrderEntries> getOrderEntries()
    {
        return this.entries;
    }

    public String getInstituteName()
    {
        return this.institute_name;
    }

    public String getSubmitStatus()
    {
        return this.submit_status;

    }

    public String getNotes()
    {
        return this.visit_notes;
    }

    public String getTakenBy()
    {
        return this.taken_by;
    }

    public double getStartLat()
    {
        return this.start_latitude;
    }

    public double getStartLon()
    {
        return this.start_longitude;
    }

    public double getSubmitLat()
    {
        return this.submit_latitude;
    }

    public double getSubmitLon()
    {
        return this.submit_longitude;
    }

    public String getVisitedWith()
    {
        return this.visited_with;
    }
    public String getVisitType()
    {
        return this.visit_type;
    }




    public void setOrderNo(String order_no)
    {
        this.visit_id = order_no;
    }


    public void setDoctorCode(String customerId)
    {
        this.doctor_code = customerId;
    }

    public void setDoctorName(String customer_name)
    {
        this.doctor_name = customer_name;
    }


    public void setOrderStatus(String orderStatus)
    {
        this.visit_status = orderStatus;
    }

    public void setSubmitStatus(String status)
    {
        this.submit_status = status;

    }

    public void setOrderDate(String orderDate) {
        this.visit_date = orderDate;
    }



    public void setStartLat(double lat)
    {
        this.start_latitude = lat;
    }



    public void setStartLon(double lon)
    {
        this.start_longitude = lon;
    }




    public void setSubmittLat(double lat)
    {
        this.submit_latitude = lat;
    }


    public void setSubmitLon(double lon)
    {
        this.submit_longitude = lon;

    }



    public void setNotes(String remarks)
    {
        this.visit_notes = remarks;

    }

    public void setInstituteName(String insName)
    {
        this.institute_name = insName;
    }

    public void setTakenBy(String id)
    {
        taken_by  = id;

    }

    public void setVisitedWith(String visited_with)
    {
        this.visited_with = visited_with;

    }

    public void setVisitType(String type)
    {
        this.visit_type = type;

    }



}
