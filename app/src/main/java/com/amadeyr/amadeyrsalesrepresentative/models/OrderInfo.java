package com.amadeyr.amadeyrsalesrepresentative.models;

import java.util.List;

/**
 * Created by amadeyr on 8/30/15.
 */
public class OrderInfo
{


    public String order_no, customer_id, delivery_date, order_date,order_status, submit_status, customer_name, order_time,order_remarks,taken_by,collection_date, invoice_type;
    private final String ORDER_STATUS_SUBMITTED = "SUBMITTED";
    private final String ORDER_STATUS_PENDING   = "PENDING";
    public static String INVOICE_TYPE_CASH      = "CASH";
    public static String INVOICE_TYPE_CREDIT    = "CREDIT";

    public List<OrderEntries> entries;
    public double total,start_latitude, start_longitude, submit_latitude, submit_longitude;;

    public OrderInfo()
    {


    }

    public OrderInfo(String order_no, String customer_id, String customer_name,String delivery_date, String order_date,double total, List<OrderEntries> entries,double start_lat, double start_lon, double submit_lat,double submit_lon, String taken_by, String credit_type)
    {
        this.order_no       = order_no;
        this.customer_id    = customer_id;
        this.delivery_date  = delivery_date;
        this.total          = total;
        this.order_date     = order_date;
        this.entries        = entries;
        this.customer_name  = customer_name;
        this.start_latitude = start_lat;
        this.start_longitude = start_lon;
        this.submit_latitude = submit_lat;
        this.submit_longitude = submit_lon;
        this.taken_by           = taken_by;
        this.invoice_type       = credit_type;
    }

    public OrderInfo(String order_no, String customer_id, String customer_name, String delivery_date, String order_date,double total,String order_status,double start_lat, double start_lon, double submit_lat,double submit_lon,String taken_by, String collection_date, String credit_type)
    {
        this.order_no       = order_no;
        this.customer_id    = customer_id;
        this.customer_name  = customer_name;
        this.delivery_date  = delivery_date;
        this.total          = total;
        this.order_date     = order_date;
        this.order_status   = order_status;
        this.submit_status  = ORDER_STATUS_PENDING;
        this.start_latitude = start_lat;
        this.start_longitude = start_lon;
        this.submit_latitude = submit_lat;
        this.submit_longitude = submit_lon;
        this.taken_by           = taken_by;
        this.collection_date    = collection_date;
        this.invoice_type        = credit_type;

    }

    public String getOrderNo()
    {
        return this.order_no;
    }

    public String getCustomerId()
    {
        return this.customer_id;
    }

    public String getCustomerName()
    {
        return this.customer_name;
    }

    public String getDeliveryDate()
    {
        return this.delivery_date;
    }

    public List<OrderEntries> getOrderEntries()
    {
        return this.entries;
    }

    public String getOrderDate()
    {
        return this.order_date;
    }
    public String getCollectionDate()
    {
        return this.collection_date;
    }

    public double getTotal()
    {
        return this.total;
    }

    public String getOrderStatus()
    {
        return this.order_status;
    }

    public String getInvoiceType()
    {
        return this.invoice_type;
    }

    public void setOrderNo(String order_no)
    {
        this.order_no = order_no;
    }

    public void setCustomerId(String customerId)
    {
        this.customer_id = customerId;
    }

    public void setCustomerName(String customer_name)
    {
        this.customer_name = customer_name;
    }

    public void setDeliveryDate(String deliveryDate)
    {
        this.delivery_date = deliveryDate;
    }

    public void setOrderEntries(List<OrderEntries> entries)
    {
        this.entries = entries;
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public void setOrderStatus(String orderStatus)
    {
        this.order_status = orderStatus;
    }

    public void setSubmitStatus(String status)
    {
        this.submit_status = status;

    }

    public String getSubmitStatus()
    {
        return this.submit_status;

    }


    public void setOrderDate(String orderDate) {
        this.order_date = orderDate;
    }
    public void setOrderTime(String orderTime)
    {
        this.order_time = orderTime;
    }

    public String getOrderTime()
    {
        return this.order_time;
    }

    public void setCollectionDate(String dt) {
        this.collection_date = dt;
    }

    public double getStartLat()
    {
        return this.start_latitude;
    }

    public void setStartLat(double lat)
    {
        this.start_latitude = lat;
    }

    public double getStartLon()
    {
        return this.start_longitude;
    }

    public void setStartLon(double lon)
    {
        this.start_longitude = lon;
    }


    public double getSubmitLat()
    {
        return this.submit_latitude;
    }

    public void setSubmittLat(double lat)
    {
        this.submit_latitude = lat;
    }

    public double getSubmitLon()
    {
        return this.submit_longitude;
    }

    public void setSubmitLon(double lon)
    {
        this.submit_longitude = lon;

    }

    public void setInvoiceType(String credit_type)
    {

        this.invoice_type = credit_type;
    }

    public String getRemarks()
    {
        return this.order_remarks;
    }

    public void setRemarks(String remarks)
    {
        this.order_remarks = remarks;

    }

    public void setTakenBy(String id)
    {
        taken_by  = id;

    }

    public String getTakenBy() {
        return taken_by;
    }
}
