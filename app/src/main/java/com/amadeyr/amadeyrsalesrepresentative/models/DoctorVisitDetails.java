package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 11/5/15.
 */
public class DoctorVisitDetails
{
    private String visit_id, item_code, item_name, category_name, comments;
    private double qty;

    public DoctorVisitDetails()
    {

    }

    public DoctorVisitDetails(String visit_id, String item_code,String item_name, String category_name, String comments, double qty)
    {
        this.visit_id       = visit_id;
        this.item_code      = item_code;
        this.item_name      = item_name;
        this.category_name  = category_name;
        this.qty            = qty;
        this.comments       = comments;

    }

    public void setVisitId(String order_no)
    {

        this.visit_id = order_no;

    }
    public String getVisitId()
    {

        return this.visit_id;
    }

    public String getItemCode()
    {
        return this.item_code;
    }

    public String getItemName()
    {

        return this.item_name;
    }

    public double getQuantity()
    {
        return this.qty;
    }


    public String getComments()
    {
        return this.comments;
    }

    public void setItemCode(String product_id)
    {
        this.item_code = product_id;
    }

    public void setItemName(String name)
    {

        this.item_name = name;
    }

    public void setQuantity(double qty)
    {
        this.qty = qty;
    }


    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getCategoryName()
    {
        return this.category_name;

    }

    public void setCategoryName(String name)
    {
        this.category_name = name;

    }



}
