package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 8/30/15.
 */
public class OrderEntries
{

    private String order_no, product_id,product_name, comments;
    private double qty, bonus, price, vat, commission;

    public OrderEntries()
    {

    }

    public OrderEntries(String order_no, String product_id,String product_name, double qty, double price, double productVAT, double productCommission,double bonus, String comments)
    {
        this.order_no       = order_no;
        this.product_id     = product_id;
        this.product_name   = product_name;
        this.qty            = qty;
        this.price          = price;
        this.bonus          = bonus;
        this.comments       = comments;
        this.vat            = vat;
        this.commission     = commission;
     //   this.start_latitude  = start_latitude;
      //  this.start_longitude = start_longitude;

    }

    public void setOrderNo(String order_no)
    {

        this.order_no = order_no;

    }
    public String getOrderNo()
    {

        return this.order_no;
    }

    public String getProductID()
    {
        return this.product_id;
    }

    public double getQuantity()
    {
        return this.qty;
    }

    public double getBonus()
    {
        return this.bonus;
    }

    public String getComments()
    {
        return this.comments;
    }

    public void setProductID(String product_id)
    {
        this.product_id = product_id;
    }

    public void setQuantity(double qty)
    {
        this.qty = qty;
    }

    public void setBonus(double bonus)
    {
        this.bonus = bonus;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getProductName()
    {
        return this.product_name;

    }

    public void setProductName(String name)
    {
        this.product_name = name;

    }

    public double getProductPrice()
    {

        return this.price;
    }

    public void setProductPrice(double price)
    {
        this.price = price;
    }

    public double getProductVAT()
    {

        return this.vat;
    }

    public void setProductVAT(double vat)
    {
        this.vat = vat;
    }

    public double getProductCommission()
    {

        return this.commission;
    }

    public void setProductCommission(double commission)
    {
        this.commission = commission;
    }








}
