package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 11/8/15.
 */
public class VisitEntries
{

    private String visit_id, product_id,product_name,category_name, comments;
    private double qty, bonus, price, vat, commission;

    public VisitEntries()
    {

    }

    public VisitEntries(String order_no, String product_id,String product_name, double qty, String comments, String category_name)
    {
        this.visit_id       = order_no;
        this.product_id     = product_id;
        this.product_name   = product_name;
        this.qty            = qty;
        this.category_name  = category_name;
        //this.price          = price;
        //this.bonus          = bonus;
        this.comments       = comments;
        //this.vat            = vat;
        //this.commission     = commission;
        //   this.start_latitude  = start_latitude;
        //  this.start_longitude = start_longitude;

    }

    public void setVisitID(String order_no)
    {

        this.visit_id = order_no;

    }
    public String getVisitID()
    {

        return this.visit_id;
    }

    public String getProductID()
    {
        return this.product_id;
    }

    public double getQuantity()
    {
        return this.qty;
    }

    public double getBonus()
    {
        return this.bonus;
    }

    public String getComments()
    {
        return this.comments;
    }

    public String getCategoryName()
    {

        return this.category_name;
    }

    public void setCategoryName(String cName)
    {
        this.category_name = cName;
    }
    public void setProductID(String product_id)
    {
        this.product_id = product_id;
    }

    public void setQuantity(double qty)
    {
        this.qty = qty;
    }

    public void setBonus(double bonus)
    {
        this.bonus = bonus;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getProductName()
    {
        return this.product_name;

    }

    public void setProductName(String name)
    {
        this.product_name = name;

    }

    public double getProductPrice()
    {

        return this.price;
    }

    public void setProductPrice(double price)
    {
        this.price = price;
    }

    public double getProductVAT()
    {

        return this.vat;
    }

    public void setProductVAT(double vat)
    {
        this.vat = vat;
    }

    public double getProductCommission()
    {

        return this.commission;
    }

    public void setProductCommission(double commission)
    {
        this.commission = commission;
    }



}
