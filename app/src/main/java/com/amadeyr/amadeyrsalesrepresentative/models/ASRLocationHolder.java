package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by acl on 2/18/16.
 */
public class ASRLocationHolder
{
    private double latitude;
    private double longitude;

    public void ASRLocationHolder()
    {
            latitude    = 0;
            longitude   = 0;
    }

    public double getLatitude()
    {

        return latitude;
    }

    public double getLongitude()
    {

        return longitude;
    }

    public void setLatitude(double lat)
    {

        this.latitude = lat;
    }

    public void setLongitude(double lon)
    {

        this.longitude = lon;
    }




}
