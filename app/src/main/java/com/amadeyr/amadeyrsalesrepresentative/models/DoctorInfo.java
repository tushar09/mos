package com.amadeyr.amadeyrsalesrepresentative.models;

/**
 * Created by amadeyr on 11/3/15.
 */
public class DoctorInfo
{

    public String doctor_code, doctor_name, institute_code;


    public DoctorInfo()
    {

    }

    public DoctorInfo(String id, String doctor_name, String institute_code) {

        this.doctor_code = id;
        this.doctor_name = doctor_name;
        this.institute_code = institute_code;

    }


    public String getDoctorId()
    {
        return this.doctor_code;
    }

    public void setDoctorId(String id)
    {
        this.doctor_code = id;
    }


    public String getDoctorName()
    {
        return this.doctor_name;
    }

    public void setDoctorName(String doctor_name)
    {
        this.doctor_name = doctor_name;
    }


    public String getInstituteCode()
    {
        return this.institute_code;
    }

    public void setInstituteCode(String pName)
    {
        this.institute_code = pName;
    }


}
