package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.CustomerInfo;

import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by acl on 5/17/15.
 */
public class APSCustomerSelectionActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //private EditText edtTextEmployeeID;

    private ListView lvCustomerLIst;
    private CustomerInfo cInfo;
    private APCProductDatabaseHelper dbHelper;
    private APCDatabaseHelper orderDBHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private APCCustomAutoCompleteView actCSSearch;
    private APSCustomerCursorAdapter apsCursorAdapter;
    private ASRErrorMessageView errorMsgObj;
    private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;
    private ImageView imgViewCustomerSelCross;
    private String product_name, product_id, product_barcode, employee_id, source_activity;
    private String SCREEN_TITLE = "";
    private Button btnPSSearch, btnPSSubmit, btnSend;

    private ArrayList<String> selectedForSubmission;
    private final int REQUEST_CODE      = 100;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] customerArray;

    private HashMap<String,String> productNameMapper;

    private Double start_latitude, start_longitude;
    private APCDatabaseHelper apsOrderDatabaseHelper;
    private Context appCtx;
    private DateTime jDTTimeHelper;
    private final String ORDER_STATUS_COMPLETE = "COMPLETE";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;


    private Location mLastLocation, mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private Cursor lastSubmitted;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_selection);

        SCREEN_TITLE = getResources().getString(R.string.barTitleCustomerList);

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();


        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        SharedPreferences sp    = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        jDTTimeHelper           = new DateTime();
        employee_id             = getIntent().getStringExtra("employee_id");
        //employee_id             = sp.getString("username","");
        getURL                  = sp.getString("getURL","")+"/autodata/description";
        postURL                 = sp.getString("postURL","")+"/index.php/pos_request/datapush";
        source_activity         = getIntent().getStringExtra("source_activity");

        imgViewCustomerSelCross = (ImageView) findViewById(R.id.imgViewCustomerSelectionCross);
        btnSend                 = (Button) findViewById(R.id.btnCLSend);
        apcDateHelper           = new APCDateHelper();


        actCSSearch             = (APCCustomAutoCompleteView) findViewById(R.id.actPSSearch);

        lvCustomerLIst          = (ListView) findViewById(R.id.lstViewCustomerList);
//        txtViewPSSProductID      = (TextView) findViewById(R.id.txtViewPSSProductID);
//        txtViewPSSProductName    = (TextView) findViewById(R.id.txtViewPSSProductName);
//        txtViewPSSProductBarcode = (TextView) findViewById(R.id.txtViewPSSProductBarcode);
        actCSSearch.setThreshold(1);

        //dbHelper                    = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        //dbHelper                    = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        dbHelper                    = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());

        //orderDBHelper               = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        orderDBHelper               = APCDatabaseHelper.getInstance(getApplicationContext());
        errorMsgObj                 = new ASRErrorMessageView(APSCustomerSelectionActivity.this);
        List<String> incompleteIDS  = orderDBHelper.getCustomersWithIncompleteOrders(employee_id);



        Cursor customerEntries   = null;

        try
        {
            customerEntries   = dbHelper.getCustomerList(employee_id);

        }catch(SQLiteException e)
        {
            e.printStackTrace();
            errorMsgObj.showErrorMessage(getResources().getString(R.string.errorCode101), getResources().getString(R.string.errorCode101Message));

        }




        apsCursorAdapter        = new APSCustomerCursorAdapter(getApplicationContext(),R.layout.customer_row_layout,customerEntries,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        apsCursorAdapter.setIncompleteList(incompleteIDS);
        lvCustomerLIst.setAdapter(apsCursorAdapter);
        //btnPSSubmit             = (Button)   findViewById(R.id.btnPSSubmit);
        //btnPSSearch             = (Button)   findViewById(R.id.btnPSSearch);
//        TextView noDataView = new TextView(this);
//        noDataView.setText("No Data");s
//        lvCustomerLIst.setEmptyView(noDataView);

        lvCustomerLIst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

//                if(source_activity.contentEquals("doctor"))
//                {
//                    Intent i = new Intent(APSCustomerSelectionActivity.this,ASRInstituteSelectionActivity.class);
//                    i.putExtra("employee_id", employee_id);
//                    i.putExtra("mode","");
//                    //finish();
//                    startActivity(i);
//
//                }else if(source_activity.contentEquals("chemist"))
//                {

                if(ArmsNetworkHelper.getGPSStatus(getApplicationContext()))
                {

                    stopLocationUpdates();
                    Cursor selectedCustomer  = (Cursor) lvCustomerLIst.getAdapter().getItem(position);
                    //selectedCustomer.moveToFirst();
                    //Toast.makeText(getApplicationContext(),"Test", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(APSCustomerSelectionActivity.this,OrderScreenActivity.class);
                    i.putExtra("employee_id", employee_id);
                    i.putExtra("customer_id", selectedCustomer.getString(selectedCustomer.getColumnIndex("customer_id")));
                    i.putExtra("customer_name", selectedCustomer.getString(selectedCustomer.getColumnIndex("trade_name")));
                    i.putExtra("trade_name", selectedCustomer.getString(selectedCustomer.getColumnIndex("trade_name")));
                    i.putExtra("address", selectedCustomer.getString(selectedCustomer.getColumnIndex("address")));
                    i.putExtra("mode","");
                    //finish();
                    startActivity(i);

                }else
                {
                    final ASRErrorMessageView am = new ASRErrorMessageView(APSCustomerSelectionActivity.this);
                    am.addExtraAction(getResources().getString(R.string.lblBtnTurnOn));
                    am.showErrorMessage(getResources().getString(R.string.errorMsgTitleLocationDisabled),getResources().getString(R.string.errorMsgBodyLocationDisabled));
                    Button gpsEnabledButton = am.getActionButton2Instance();
                    gpsEnabledButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            am.closeScreen();
                        }
                    });

                }





            }
        });

        actCSSearch.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));

        actCSSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String searchKey = actCSSearch.getText().toString();

                Cursor searchCursor = dbHelper.getCustomerInfo(searchKey);


                apsCursorAdapter.swapCursor(searchCursor);
                apsCursorAdapter.notifyDataSetChanged();


            }
        });

        imgViewCustomerSelCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                actCSSearch.setText("");
                Cursor allEntries = dbHelper.getCustomerList(employee_id);
                apsCursorAdapter.swapCursor(allEntries);
                apsCursorAdapter.notifyDataSetChanged();
            }
        });



//        actPSBarcode.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//
//                actPSBarcode.setText("");
//                return true;
//            }
//        });

        //btnPSSOk.setEnabled(false);



//        btnPSSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//
//                Cursor allEntries   = dbHelper.getPendingSubmissions();
//
//                apcCursorAdapter.swapCursor(allEntries);
//                apcCursorAdapter.notifyDataSetChanged();
//
//
//            }
//
//
//
//
//        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                selectedForSubmission   = apsCursorAdapter.getSelectedListOfOrders();

                if(!selectedForSubmission.isEmpty())
                {


                    saveToDatabase("");
//                    Iterator cusArrayIterator = selectedForSubmission.iterator();
//
//
//
//
//                    while(cusArrayIterator.hasNext())
//                    {
//
//                        String cCode                = (String) cusArrayIterator.next();
//
//                        //saveToDatabase(dCode);
//
//
//                        // Toast.makeText(getApplicationContext(), order_id, Toast.LENGTH_SHORT).show();
//
//
//                    }
//

                }else
                {

                    new ASRErrorMessageView(APSCustomerSelectionActivity.this).showErrorMessage(getResources().getString(R.string.errorNoItemsSelected),getResources().getString(R.string.errorNoItemsSelectedMessage));

                }


            }
        });




    }


    public void saveToDatabase(String vWith)
    {
        selectedForSubmission       = apsCursorAdapter.getSelectedListOfOrders();
        Iterator lIterator          = selectedForSubmission.iterator();
        LocalTime j                 = jDTTimeHelper.toLocalTime();

        ProgressDialog pd = new ProgressDialog(APSCustomerSelectionActivity.this);
        pd.setMessage("Please wait ... saving data for submission");
        pd.show();

        recordLocation();
        //stopLocationUpdates();

        while(lIterator.hasNext())
        {
            String dcode = (String) lIterator.next();
            String [] doctor_info = dcode.split("<sep>");

//            System.out.println("Doc Code Unsplit: "+ dcode);
//            System.out.println("Doc Code 0: "+ doctor_info[0]);
//            System.out.println("Doc Code 1: "+ doctor_info[1]);


            int last_order_id           = orderDBHelper.getLastInsertID();
            String order_id             = employee_id+doctor_info[0]+ String.format("%04d", last_order_id + 1);
            String order_date           = jDTTimeHelper.getYear()+"-"+jDTTimeHelper.getMonthOfYear()+"-"+ jDTTimeHelper.getDayOfMonth();
            String strTime              = j.getHourOfDay()+":"+j.getMinuteOfHour()+":"+j.getSecondOfMinute();


            OrderInfo orderEntry        = new OrderInfo(order_id,doctor_info[0],doctor_info[1],"",order_date,0,"COMPLETE",start_latitude,start_longitude,0,0,employee_id,"","");
            orderDBHelper.insertOrder(orderEntry);

        }
        apsCursorAdapter.clearSelectedListOfOrders();
        apsCursorAdapter.notifyDataSetChanged();

        pd.dismiss();

        Toast.makeText(getApplicationContext(),"Data saved successfully !",Toast.LENGTH_LONG).show();




    }


    public void populateAPCAutocompleteArray(String searchTerm)
    {
        Cursor currentEntries = null;

        if(searchTerm.equals(""))
        {
            currentEntries = dbHelper.getAllCustomerInfo();

        }



        if(currentEntries!=null)
        {
            currentEntries.moveToFirst();

            String[] customerArray = new String[currentEntries.getCount()];


            int string_arr_index = 0;

            for (int json_arr_index = 0; json_arr_index < currentEntries.getCount();++json_arr_index)
            {
                customerArray[json_arr_index]        = currentEntries.getString(currentEntries.getColumnIndex("trade_name"));
                currentEntries.moveToNext();
                //System.out.println("SearchActivity: "+jObj.getString("description"));
            }

            ArrayAdapter adapter = new ArrayAdapter(APSCustomerSelectionActivity.this, android.R.layout.simple_list_item_1, customerArray);
            actCSSearch.setAdapter(adapter);

        }

    }

    public void populateSearchAdapter(String searchItem)
    {
        List<CustomerInfo> oInfo   = dbHelper.getAutoCompleteCustomerInfo(searchItem,employee_id);
        customerArray              = new String[oInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < oInfo.size();++index)
        {
            customerArray[index]        = new String(oInfo.get(index).getTradeName());
            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());

            //str_index+=2;

            //System.out.println("SearchActivity: "+jObj.getString("description"));
        }


        ArrayAdapter adapter         = new ArrayAdapter(APSCustomerSelectionActivity.this, android.R.layout.simple_list_item_1, customerArray);

        actCSSearch.setAdapter(adapter);
        actCSSearch.setThreshold(1);



    }

    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
            if(currentPending!=null) currentPending.moveToFirst();


            JSONArray dataArray = new JSONArray();


            while(!currentPending.isAfterLast())
            {
                JSONObject dataForSubmission = new JSONObject();

                //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
                //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
                //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();

                dataForSubmission.put("customer_id",currentPending.getString(currentPending.getColumnIndex("customer_id")).trim());
                dataForSubmission.put("trade_name",currentPending.getString(currentPending.getColumnIndex("trade_name")));
                dataForSubmission.put("product_barcode", currentPending.getString(currentPending.getColumnIndex("product_barcode")));
                dataForSubmission.put("product_tag",currentPending.getString(currentPending.getColumnIndex("product_tag")));
                dataForSubmission.put("product_count",currentPending.getDouble(currentPending.getColumnIndex("product_count")));
                dataForSubmission.put("entry_date",currentPending.getString(currentPending.getColumnIndex("entry_date")));
                dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
                //dataForSubmission.put("entry_status",urls[8]);
                dataForSubmission.put("entry_comment",currentPending.getString(currentPending.getColumnIndex("comments")));
                dataArray.put(dataForSubmission);

                currentPending.moveToNext();
            }

            new AccessAPCServerTask().execute(postURL, dataArray.toString());

        }else
        {
            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }



    public void refreshTextViews()
    {
//        txtViewPSSProductID.setText("No Data");
//        txtViewPSSProductName.setText("No Data");
//        txtViewPSSProductBarcode.setText("No Data");
//        btnPSSOk.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;

        }else if(id == android.R.id.home)
        {
            this.finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {
            String source_activity  = "";

            Intent main_menu = new Intent(APSCustomerSelectionActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {


        //Toast.makeText(getApplicationContext(),"Back pressed", Toast.LENGTH_SHORT).show();

//        if(btnOSAdd.getVisibility()==View.VISIBLE)
//        {
//            btnOSAdd.setVisibility(View.GONE);
//            btnOSFinish.setVisibility(View.VISIBLE);
//            return;
//        }

        super.onBackPressed();




    }

    @Override
    protected void onResume() {

        super.onResume();

        List<String> incompleteIDS         = orderDBHelper.getCustomersWithIncompleteOrders(employee_id);
        //Cursor customerEntries          = dbHelper.getCustomerList(employee_id);

        apsCursorAdapter.setIncompleteList(incompleteIDS);
        //apsCursorAdapter.swapCursor(customerEntries);
        apsCursorAdapter.notifyDataSetChanged();


    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            start_latitude  = mLastLocation.getLatitude();
            start_longitude = mLastLocation.getLongitude();

        }

        //stopLocationUpdates();


    }

    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {
            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }


        }



    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();
//        createLocationRequest();
//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, this);
//        recordLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location)
    {
        if(location!=null)
        {
            mCurrentLocation = location;
            recordLocation();

        }

    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {

        private ProgressDialog pd;
        private boolean getAll = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(APSCustomerSelectionActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Submitting data to server  ...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls) {

            try
            {

                String result  = submitInfoToUrl(urls[0], urls[1]);

                return result;

            }catch (IOException e)
            {

                return "Unsuccessfull";
            }

        }




        @Override
        protected void onPostExecute(String s)
        {
            System.out.println("Server Response: "+s);
            pd.dismiss();

            if(s!=null)
            {

                JSONArray jArray = null;

                try {


                    if(s.contentEquals("success"))
                    {
                        if(lastSubmitted!=null) lastSubmitted.moveToFirst();

                        while(!lastSubmitted.isAfterLast())
                        {
                            //dbHelper.updateStatusToSubmitWithEntryID(lastSubmitted.getString(lastSubmitted.getColumnIndex("_id")));

                            lastSubmitted.moveToNext();
                        }


                       // Cursor productEntries   = dbHelper.getPendingSubmissions();

                       // apsCursorAdapter.swapCursor(productEntries);
                        apsCursorAdapter.notifyDataSetChanged();


                    }





                }catch(Exception e )
                {


                }


            }


        }


        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException{

            //URL url = new URL(serverUrl);

            //barCode                 = "132768165";
            HttpClient httpClient   = new DefaultHttpClient();
            HttpContext httpContext = new BasicHttpContext();
            String postUrl          = networkURL;
            HttpPost request        = new HttpPost(postUrl);




            HttpResponse response   = null;

            BufferedReader in=null;
            String data = null;

            try
            {

                StringEntity se = new StringEntity(dataArray);
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("apc",dataArray));
                //nameValuePairs.add(new BasicNameValuePair("status","1"));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                //request.setEntity(se);
                //request.setHeader("Accept", "application/json");
                //request.setHeader("Content-type","application/json");
                System.out.println("Submit pack: "+dataArray);
                response = httpClient.execute(request, httpContext);
                HttpEntity entity = response.getEntity();

                String responseData     = EntityUtils.toString(entity);
//                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//                StringBuffer sb = new StringBuffer("");
//                String l = "";
//                String nl = System.getProperty("line.separator");
//
//                while((l=in.readLine())!=null)
//                {
//                    sb.append(l+nl);
//
//                }
//
//                in.close();
//                data = sb.toString();
                return responseData;

            }finally
            {


            }




        }
    }


}
