package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.DoctorInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.util.List;
import java.util.StringTokenizer;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by amadeyr on 1/17/16.
 */
public class ASRPrescriptionImageBrowser extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Cursor imageSetCursor;
    private SimpleCursorAdapter presAdapter;
    private SharedPreferences sp;
    private APCDatabaseHelper dbHelper;
    private APCProductDatabaseHelper productDBHelper;
    private APCProductDatabaseHelper userDBHelper;
    private String entry_mode, prescription_id, username, post_url, user_token, group_id, doc_code, doc_name;
    private String[] productArray, doctorArray;
    private Context appContext;
    private ImageView imageHolder, doctorCross, productCross;
    private Button btnNext, btnPrev, btnRotate, btnFinish;
    private ListView lvPrescriptionItems;
    private final String ENTRY_MODE_EDIT = "EDIT";
    private APCCustomAutoCompleteView productAutoComplete, doctorAutoComplete;
    private Bitmap rotatedBitmap;
    private double start_latitude, start_longitude;
    private static final String SCREEN_TITLE = "Prescription";

    private final int REQUEST_IMAGE_PICKER = 10;
    // Initializing variables for location services


    private Location mLastLocation,mCurrentLocation;;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private PhotoViewAttacher mAttacher;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prescription_image_browser);
        group_id            = getIntent().getExtras().getString("group_id");
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setTitle("Prescription No. "+group_id);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        appContext          = getApplicationContext();
        dbHelper            = APCDatabaseHelper.getInstance(appContext);
        productDBHelper     = ASRDatabaseFactory.getProductsDBInstance(appContext);
        userDBHelper        = ASRDatabaseFactory.getUserDBInstance(appContext);
        imageHolder         = (ImageView) findViewById(R.id.imgViewSlideImage);
        doctorCross         = (ImageView) findViewById(R.id.presDrClear);
        productCross        = (ImageView) findViewById(R.id.presClear);
        btnNext             = (Button) findViewById(R.id.btnSlideNext);
        btnPrev             = (Button) findViewById(R.id.btnSlideLeft);
        btnFinish           = (Button) findViewById(R.id.btnPresFinish);
        btnRotate           = (Button) findViewById(R.id.btnRotate);

        productAutoComplete = (APCCustomAutoCompleteView) findViewById(R.id.presProductSearch);
        doctorAutoComplete  = (APCCustomAutoCompleteView) findViewById(R.id.presDoctorSearch);
        lvPrescriptionItems = (ListView) findViewById(R.id.lstViewPrescriptionItems);

        productAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));
        doctorAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this, "MULTIPLE", "DOCTOR"));


        productAutoComplete.setFocusable(true);
        doctorAutoComplete.setFocusable(true);

        prescription_id     = getIntent().getExtras().getString("group_id");
        sp = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        username            = sp.getString("username", "nouser");
        post_url            = sp.getString("postURL", "");
        user_token          = sp.getString("user_token", "");


        Cursor enteredProduct       = dbHelper.getPrescriptionEntry(group_id);
        Cursor enteredProductGroup  = dbHelper.getPrescriptionGroup(group_id);

        if(enteredProductGroup !=null)
        {
            if(enteredProductGroup.getCount()==1)
            {
                enteredProductGroup.moveToFirst();

                doc_name = enteredProductGroup.getString(enteredProductGroup.getColumnIndex("doctor_name"));

                if(doc_name!=null)
                {
                    doc_code = doc_name.substring(doc_name.lastIndexOf("|")+2,doc_name.length());
                    doctorAutoComplete.setText(doc_name);
                    doctorAutoComplete.setEnabled(false);
                    productAutoComplete.requestFocus();
                }

            }

        }

        presAdapter         = new SimpleCursorAdapter(appContext,R.layout.prescription_row_layout,enteredProduct,new String[]{"product_name"},new int[]{R.id.txtViewPrescriptionName});
        lvPrescriptionItems.setAdapter(presAdapter);
        entry_mode = "";


        productCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productAutoComplete.setEnabled(true);
                productAutoComplete.setText("");
                productAutoComplete.requestFocus();
            }
        });

        doctorCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorAutoComplete.setEnabled(true);
                doctorAutoComplete.setText("");
                doctorAutoComplete.requestFocus();
            }
        });


        doctorAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //doctorAutoComplete.clearFocus();
                doc_name = (String) doctorAutoComplete.getAdapter().getItem(position);

                doc_code = doc_name.substring(doc_name.lastIndexOf("|") + 2, doc_name.length());
                //Toast.makeText(appContext,doc_code,Toast.LENGTH_LONG).show();

                dbHelper.updatePrescriptionGroupTable(group_id, doc_code, doc_name, username, "","INCOMPLETE");

                doctorAutoComplete.setEnabled(false);
                productAutoComplete.requestFocus();

            }
        });




        productAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String searchItem = productAutoComplete.getText().toString();

                boolean entered = dbHelper.entryExistsInPrescription(group_id, searchItem);

                if (entered) {

                    new ASRErrorMessageView(ASRPrescriptionImageBrowser.this).showErrorMessage(getResources().getString(R.string.errorDuplicateEntryTitle), getResources().getString(R.string.errorDuplicateEntryMessage));


                    //Toast.makeText(getApplicationContext(), "You have already entered this product once !", Toast.LENGTH_LONG).show();

                } else {

                    Cursor result = productDBHelper.getProductInfoFromNameOrBarcode(searchItem);

                    if (result != null)
                    {

                        result.moveToFirst();

                        String product_id = result.getString(result.getColumnIndex("product_id"));
                        String product_name = result.getString(result.getColumnIndex("trade_name"));
                        dbHelper.insertPrescriptionDetails(group_id, product_id, product_name);
                        //Cursor enteredProduct = dbHelper.getPrescriptionEntry(group_id);
                        presAdapter.swapCursor(dbHelper.getPrescriptionEntry(group_id));
                        presAdapter.notifyDataSetChanged();


                    }

                    productAutoComplete.setText("");
                    hideKeyboard();
                }


            }

        });

        productAutoComplete.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                int result = i & EditorInfo.IME_MASK_ACTION;

                if (result == EditorInfo.IME_ACTION_DONE || result == EditorInfo.IME_ACTION_GO || result == EditorInfo.IME_ACTION_SEND || result == EditorInfo.IME_ACTION_NEXT) {

                    if (productAutoComplete.getText().toString().isEmpty()) {

                        productAutoComplete.setError("Please enter product name.");

                    } else
                    {


                        Cursor result_cursor = productDBHelper.getProductInfoFromNameOrBarcode(productAutoComplete.getText().toString());

                        if (result_cursor != null) {

                            result_cursor.moveToFirst();

                            if(result_cursor.getCount()==0)
                            {
                                String product_id = "";
                                String product_name = productAutoComplete.getText().toString();
                                dbHelper.insertPrescriptionDetails(group_id, product_id, product_name);
                                presAdapter.swapCursor(dbHelper.getPrescriptionEntry(group_id));
                                presAdapter.notifyDataSetChanged();

                            }



                        }

                        productAutoComplete.setText("");
                        hideKeyboard();

                    }


//                    saveToDatabase();
//                    //edtTextDESBarcode.requestFocus();
//                    clearEntryFields();
//                    disableEntryFields();
                    return true;

                }


                return false;
            }
        });


        lvPrescriptionItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                final ASRErrorMessageView am = new ASRErrorMessageView(ASRPrescriptionImageBrowser.this);
                am.addExtraAction(getResources().getString(R.string.lblBtnYes));
                am.getActionButton2Instance().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        Cursor c = (Cursor) lvPrescriptionItems.getAdapter().getItem(position);
                        dbHelper.removeItemFromPrescription(group_id,c.getString(c.getColumnIndex("product_name")));
                        presAdapter.swapCursor(dbHelper.getPrescriptionEntry(group_id));
                        presAdapter.notifyDataSetChanged();
                        am.closeScreen();
                    }
                });

                am.showErrorMessage(getResources().getString(R.string.lblRemoveItemTitle),getResources().getString(R.string.lblRemoveItemMessage));

                return true;
            }
        });

//        if(entry_mode.contentEquals(ENTRY_MODE_EDIT))
//        {
//            prescription_id                      = getIntent().getStringExtra("order_no");

//        }else
//        {
//            String lastIncompleteOrderNo        = dbHelper.getLastIncompletePrescriptionNo(customer_id);
//
//            if(!lastIncompleteOrderNo.contentEquals(""))
//            {
//                order_id                    = lastIncompleteOrderNo;
//
//            }else
//            {
//                int last_order_id           = apsOrderDBHelper.getLastInsertID();
//                order_id                    = username+customer_id+ String.format("%04d",last_order_id);
//
//            }



        Log.d("Group ID at browser: ", group_id);
        imageSetCursor = dbHelper.getImageSetForPrescription(group_id);

        if (imageSetCursor != null) imageSetCursor.moveToFirst();
        changeImage();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imageSetCursor.isLast()) {
                    imageSetCursor.moveToFirst();

                } else {
                    imageSetCursor.moveToNext();

                }
                alterImage();
                //changeImage();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imageSetCursor.isFirst()) {
                    imageSetCursor.moveToLast();

                } else {
                    imageSetCursor.moveToPrevious();

                }

                alterImage();
                //changeImage();
            }
        });

        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateImage();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(lvPrescriptionItems.getCount()==0)
                {
                    new ASRErrorMessageView(ASRPrescriptionImageBrowser.this).showErrorMessage(getResources().getString(R.string.errorNoEntryFound), getResources().getString(R.string.errorNoEntryFoundBody));
                    return;
                }

                String image_path = imageSetCursor.getString(imageSetCursor.getColumnIndex("image_name"));
                Log.d("PrescriptionImgBrowser", "btnFinish Press: " + image_path);
                dbHelper.updatePrescriptionGroupTable(group_id, doc_code, doc_name, username, image_path, "COMPLETE");

                Intent choose_intent  = new Intent(ASRPrescriptionImageBrowser.this,PrescriptionImageChooserActivity.class);
                choose_intent.putExtra("prescription_id",group_id);

                startActivityForResult(choose_intent, REQUEST_IMAGE_PICKER);

                //finish();
                //onBackPressed();


            }
        });


        populateSearchAdapter("");
        populateDoctorSearchAdapter("");

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }


    }


    public void populateSearchAdapter(String searchItem)
    {
        List<ProductInfo> pInfo = productDBHelper.getAutoCompleteProductInfo(searchItem);
        productArray            = new String[pInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < pInfo.size();++index)
        {
            productArray[index]        = new String(pInfo.get(index).getProductName());

        }

        ArrayAdapter adapter            = new ArrayAdapter(ASRPrescriptionImageBrowser.this, android.R.layout.simple_list_item_1, productArray);
        productArray                    = null;
        productAutoComplete.setAdapter(adapter);
        productAutoComplete.setThreshold(1);


    }

    private void hideKeyboard()
    {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void rotateImage()
    {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(90);
//
//        rotatedBitmap = Bitmap.createBitmap(rotatedBitmap , 0, 0, rotatedBitmap.getWidth(), rotatedBitmap.getHeight(), matrix, true);
//
//        imageHolder.setImageBitmap(rotatedBitmap);

        mAttacher.setRotationBy(90f);

    }


    public void alterImage()
    {
        if(imageSetCursor!=null)
        {
            String imageFileName    = imageSetCursor.getString(imageSetCursor.getColumnIndex("image_name"));
            Log.d("Prescription img file: ", imageFileName);
            File imgFile            = new  File(imageFileName);

            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inJustDecodeBounds = true;
            options.inSampleSize       = 3;


            if(imgFile.exists())
            {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);

                Matrix matrix ;


                    matrix = new Matrix();
                    matrix.postRotate(90);



                rotatedBitmap = Bitmap.createBitmap(myBitmap , 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);



                //Bitmap scaledBitmap = Bitmap.createScaledBitmap(myBitmap, myBitmap.getHeight() * 2, myBitmap.getWidth() * 2, true);

                //Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);


                float prev_image_scale = mAttacher.getScale();

                //Matrix m = mAttacher.getDisplayMatrix();
                //imageHolder.setScaleX(prev_image_scale);
                //imageHolder.setScaleY(prev_image_scale);
                imageHolder.setImageBitmap(rotatedBitmap);

                //mAttacher.setDisplayMatrix(m);
                //mAttacher.setMinimumScale(mAttacher.getScale());

                //mAttacher.setMinimumScale(prev_image_scale);
                mAttacher.update();
                mAttacher.setScale(prev_image_scale);

                myBitmap    = null;


            }

        }

    }

    public void changeImage()
    {

        if(imageSetCursor!=null)
        {

            String imageFileName    = imageSetCursor.getString(imageSetCursor.getColumnIndex("image_name"));
            Log.d("Prescription img file: ", imageFileName);
            File imgFile            = new  File(imageFileName);

            BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inJustDecodeBounds = true;


                options.inSampleSize       =  3;


            if(imgFile.exists())
            {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);

                Matrix matrix = new Matrix();
                matrix.postRotate(90);

                //Bitmap scaledBitmap = Bitmap.createScaledBitmap(myBitmap, myBitmap.getHeight() * 2, myBitmap.getWidth() * 2, true);

                //Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

                rotatedBitmap = Bitmap.createBitmap(myBitmap , 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
                imageHolder.setImageBitmap(rotatedBitmap);

//                imageHolder.setPivotX(imageHolder.getWidth()/2);
//                imageHolder.setPivotY(imageHolder.getHeight()/2);



                //imageHolder.setRotation(90);
                //imageHolder.setScaleY(1.8f);
                //imageHolder.setScaleY(1.5f);

                mAttacher = new PhotoViewAttacher(imageHolder);
                mAttacher.setZoomable(true);
                mAttacher.setMaximumScale(15);
                mAttacher.setMidScale(5);

                //Toast.makeText(getApplicationContext(),mAttacher.getScale()+"",Toast.LENGTH_LONG).show();


//                mAttacher.setOnScaleChangeListener(new PhotoViewAttacher.OnScaleChangeListener() {
//                    @Override
//                    public void onScaleChange(float scaleFactor, float focusX, float focusY) {
//                        Toast.makeText(getApplicationContext(),mAttacher.getScale()+"",Toast.LENGTH_LONG).show();
//                    }
//                });
                //mAttacher.setMinScale(8);



                myBitmap    = null;


            }

        }
    }

    public void populateDoctorSearchAdapter(String s) {


        List<DoctorInfo> eInfo      = userDBHelper.getAutoCompleteDoctorInfo(s, "");
        doctorArray                 = new String[eInfo.size()];

        int str_index               = 0;

        for (int index = 0; index < eInfo.size();++index)
        {
            doctorArray[index]        = new String(eInfo.get(index).getDoctorName()+" | "+eInfo.get(index).getDoctorId());

        }

        ArrayAdapter adapter         = new ArrayAdapter(ASRPrescriptionImageBrowser.this, android.R.layout.simple_list_item_1, doctorArray);

        doctorArray = null;
        doctorAutoComplete.setAdapter(adapter);
        doctorAutoComplete.setThreshold(1);

    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {


            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            start_latitude  = mLastLocation.getLatitude();
            start_longitude = mLastLocation.getLongitude();

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_IMAGE_PICKER)
        {

            if(resultCode==100)
            {
                finish();
                onBackPressed();
            }

        }




    }

    @Override
    public void onBackPressed()
    {


        super.onBackPressed();
        //stopLocationUpdates();



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;

        }else if(id == android.R.id.home)
        {
            this.finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {

            stopLocationUpdates();

            String source_activity  = "";

            Intent main_menu = new Intent(ASRPrescriptionImageBrowser.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void createLocationRequest()
    {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {

            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);

            }

        }



    }



    @Override
    protected void onStart()
    {
        super.onStart();

//        if (mGoogleApiClient != null)
//        {
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();

    }

    @Override
    public void onConnectionSuspended(int i)
    {

        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location)
    {
        if(location!=null)
        {

            mCurrentLocation = location;
            recordLocation();


        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
