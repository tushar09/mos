package com.amadeyr.amadeyrsalesrepresentative;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acl on 5/12/15.
 */
public class APSCustomerCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;
    private List<String> incompleteList;
    private ArrayList<String> selectedForSubmission;
    private Context appContext;
    public APSCustomerCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags)
    {
        super(context, layout, c, from, to, flags);
        appContext = context;
        layout_id = layout;
        selectedForSubmission = new ArrayList<>();
        apcDate    = new APCDateHelper();
    }

    public void setIncompleteList(List a)
    {
        incompleteList = a;

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

//            String cusCode                  = cursor.getString(cursor.getColumnIndex("customer_id"));
//            String cusName                  = cursor.getString(cursor.getColumnIndex("trade_name"));

            TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewCustomerRowSerialID);
            TextView txtViewTradeName       = (TextView) view.findViewById(R.id.txtViewCustomerTradeName);
            TextView txtViewProprietaryName = (TextView) view.findViewById(R.id.txtViewCustomerProprietaryName);
            //TextView txtViewContact         = (TextView) view.findViewById(R.id.txtViewCustomerContactNo);
            ImageView ivStatusIndicator     = (ImageView) view.findViewById(R.id.ivStatusIndicator);
            final CheckBox chkBoxCustomer         = (CheckBox) view.findViewById(R.id.chkBoxCustomerVisitOnly);
            //Button   btnPreviousHistory     = (Button)   view.findViewById(R.id.btnCustomerHistory);

            if(chkBoxCustomer.isChecked())
            {

                chkBoxCustomer.setChecked(false);
            }


        if (cursor != null)
            {
                //
                txtViewSerialNo.setText(cursor.getPosition()+1+".");
                txtViewTradeName.setText(cursor.getString(cursor.getColumnIndex("trade_name"))+" | "+cursor.getString(cursor.getColumnIndex("address")));
                txtViewProprietaryName.setText(cursor.getString(cursor.getColumnIndex("proprietary_name")));
                //txtViewContact.setText(cursor.getString(cursor.getColumnIndex("contact_no")));
                final String customer_id = cursor.getString(cursor.getColumnIndex("customer_id"));
                final String cusName                  = cursor.getString(cursor.getColumnIndex("trade_name"));

                ivStatusIndicator.setVisibility(View.INVISIBLE);

                if(incompleteList!=null)
                {

                    if(incompleteList.contains(customer_id))
                    {
                        ivStatusIndicator.setBackground(appContext.getResources().getDrawable(R.drawable.orange_circle));
                        ivStatusIndicator.setVisibility(View.VISIBLE);
                    }

//                    for(int lIterator = 0 ; lIterator< incompleteList.size(); ++lIterator)
//                    {
//                        String present_id = (String) incompleteList.get(lIterator);
//
//                        if(present_id.contentEquals(customer_id))
//                        {
//                            ivStatusIndicator.setBackground(appContext.getDrawable(R.drawable.orange_circle));
//                            ivStatusIndicator.setVisibility(View.VISIBLE);
//                            break;
//                        }
//
//                    }
                }

                if(selectedForSubmission.contains(customer_id+"<sep>"+cusName)) chkBoxCustomer.setChecked(true);

                chkBoxCustomer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        if(chkBoxCustomer.isChecked())
                        {
                            selectedForSubmission.add(customer_id+"<sep>"+cusName);
                            //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();

                        }else
                        {
                            selectedForSubmission.remove(customer_id+"<sep>"+cusName);
                            //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                        }

                    }
                });




//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
            }

    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }
}
