package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncPrescriptionImageUploadTask;

/**
 * Created by acl on 2/23/16.
 */
public class PendingImageSubmissionNotificationActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        new AsyncPrescriptionImageUploadTask(getApplicationContext()).execute();
        finish();
    }
}
