package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;

import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by acl on 5/12/15.
 */
public class APCNetworkHandler {

    private String networkUrl;
    private Context appContext;
    private String productInfo;
    private ProductInfo pInfo;

    public APCNetworkHandler(String url, Context appContext) throws IOException {

        this.appContext = appContext;
        networkUrl = new String(url);



    }

    public String requestForProductData(String barcodeEntry) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
            new AccessAPCServerTask().execute(networkUrl, barcodeEntry);



            //pInfo           = new System.out.println((jObj));

            return productInfo;
        }else
        {
            Log.d("Network Error", "Connection Unavailable");

        }



        return null;

    }



    public void submitData() throws IOException {

        String testMsg = "Hello World";

        File myFile  = new File("/data/data/"+appContext.getPackageName()+"/databases","amadeyrProductDB");
        myFile.createNewFile();
        System.out.println("File created");
        FileOutputStream fOut = new FileOutputStream(myFile);
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
        myOutWriter.append(testMsg);
        myOutWriter.close();
        fOut.close();

    }


    public void stringToJson(String data)
    {




    }

    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {
        private ProgressDialog pd;
        protected void onPreExecute() {
            super.onPreExecute();

//            pd = new ProgressDialog();
//            pd.setTitle("Please Wait");
//            pd.setMessage("Submitting data to server ...");
//            pd.show();

        }
        @Override
        protected String doInBackground(String... urls) {

            try
            {

                String result  = getInfoWithUrl(urls[0],urls[1]);

                return result;

            }catch (IOException e)
            {

                return "Unsuccessfull";
            }

        }

        @Override
        protected void onPostExecute(String s)
        {
            pd.dismiss();
            System.out.println("SUCCESS !");
            //pd.dismiss();


           //productInfo = new String(s);
        }

        private String getInfoWithUrl(String serverUrl, String barCode) throws IOException {
            File root = Environment.getDataDirectory();
            System.out.println("Path: "+ root.getPath());

            //URL url = new URL(serverUrl);

            //HttpClient httpClient   = new DefaultHttpClient();
            //serverUrl               = serverUrl +"?term="+ barCode;


            // HttpGet request         = new HttpGet(serverUrl);


            //HttpResponse response   = null;

            //BufferedReader in=null;
            //String data = null            FileOutputStream output = null;
            FileInputStream fis = null;
            InputStream input = null;
            try {
                URL url = new URL(serverUrl);
                URLConnection connection = url.openConnection();
                input   = connection.getInputStream();
//                File root = Environment.getDataDirectory();
//                System.out.println("Path: "+ root.getPath());
                //fis     = new FileInputStream(new File(("/data/" + appContext.getPackageName() + "/databases/amadeyrProductDB")));
                System.out.println("APCNetworkHandler 1");
                FileOutputStream output  = new FileOutputStream(new File("/data/data/" + appContext.getPackageName() + "/database","amadeyrProductDB"));
                //output = appContext.openFileOutput("/data/" + appContext.getPackageName() + "/databases/amadeyrProductDB", Context.MODE_PRIVATE);

                int read;
                byte[] data = new byte[1024];
                while ((read = input.read(data)) != -1) {
                    System.out.println("datavar: "+read);
                    //fis.
                    output.write(data, 0, read);

                }

//                response = httpClient.execute(request);
//
//                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//                StringBuffer sb = new StringBuffer("");
//                String l = "";
//                String nl = System.getProperty("line.separator");
//
//                while((l=in.readLine())!=null)
//                {
//                    sb.append(l+nl);
//
//                }
//
//                in.close();;
//                data = sb.toString();
//                return data;

            }catch(Exception e){
                e.printStackTrace();


            } finally {

                if (fis != null) fis.close();
                if (input != null) input.close();
//                if(in!=null)
//                {
//                    try{
//
//                        in.close();
//                        return data;
//
//                    }catch(Exception e)
//                    {
//
//
//                    }
//
//
//                }


            }

            return "";
        }
    }


}
