package com.amadeyr.amadeyrsalesrepresentative;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRDoctorVisitArrayAdapter;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRDoctorVisitCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.ASROtherProductDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.VisitInfo;
import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncAutoSubmitDataTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by amadeyr on 11/4/15.
 */
public class DoctorVisitActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    //private EditText edtTextEmployeeID;
    private String username, user_token, postURL;
    private ListView lvOrderEntries;
    private APCDateHelper dateHelper;
    private APCProductDatabaseHelper apsProductDBHelper, asrUserDBHelper;
    private APCDatabaseHelper apsOrderDBHelper;
    private ASROtherProductDatabaseHelper otherProductDBHelper;
    private ASRDoctorVisitCursorAdapter apsOrderCursorAdapter;
    private APCCustomAutoCompleteView productAutoComplete;
    private SharedPreferences sp;
    private EditText edtTextDVQty, edtTextDVComments;
    public Spinner spnrItemSelector;
    private String[] productArray;
    public  String trade_name, customer_id, product_barcode, entry_id, visit_time, visit_id, product_id,product_name, order_comments,todays_date, delivery_date, order_date, employee_id,entry_mode, institute_code,institute_name, doctor_code,doctor_name, visited_with, category_name;


    private SimpleDateFormat dateFormatter;
    private Button btnDVAdd, btnDVFinish;
    public Menu menu;
    private final String ORDER_STATUS_COMPLETE      = "COMPLETE";
    private final String ORDER_STATUS_INCOMPLETE    = "INCOMPLETE";
    private final String ENTRY_MODE_EDIT            = "EDIT";
    private boolean sort_toggle_on;
    private ImageView imgViewClearAll;

    private double order_qty,start_latitude, start_longitude;
    //private ImageView imgViewSearch, imgViewCalendarPicker, imgViewCrossConfirm;

    //private APCDateHelper apcDateHelper;
    private DecimalFormat decFormatter;
    private DateTime jDTTimeHelper;

    private final int REQUEST_CODE                  = 100;
    private final int APC_EDIT_CODE                 = 1;
    private final int APC_REMOVE_CODE               = 2;
    private final int APC_SEARCH_CODE               = 500;
    private final int APC_SEARCH_CANCEL_CODE        = 600;
    private final int APC_ADD_CODE                  = 3;

    // Initializing variables for location services
    private Location mLastLocation, mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL              = 10000;
    private static int FASTEST_INTERVAL             = 5000;
    private static int DISPLACEMENT                 = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private android.support.v7.app.ActionBar bar;
    //private final String networkURL = "http://192.168.1.115/ACL_Automation/index.php/control/changeswitchstatus";
    private String networkURL; // = "http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.doctor_visit_screen);

        sort_toggle_on = false;

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        jDTTimeHelper           = new DateTime();
        dateHelper              = new APCDateHelper();
        dateFormatter           = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        decFormatter            = new DecimalFormat("0.00");
        employee_id             = getIntent().getStringExtra("employee_id");

        Calendar newCalendar    = Calendar.getInstance();
        todays_date             = dateFormatter.format(newCalendar.getTime());

        sp                      = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        username                = sp.getString("username", "");
        user_token              = sp.getString("user_token", "");
        postURL                 = sp.getString("postURL", "");
        username                = employee_id;

        //doctor_code             = getIntent().getStringExtra("doctor_code");
        //trade_name              = getIntent().getStringExtra("trade_name");
        entry_mode              = getIntent().getStringExtra("mode");

        doctor_code             = getIntent().getStringExtra("doctor_code");
        doctor_name             = getIntent().getStringExtra("doctor_name");
        institute_code          = getIntent().getStringExtra("institute_code");
        institute_name          = getIntent().getStringExtra("institute_name");

        order_date              = dateHelper.getCurrentDateStr();

        //txtViewBarTradeName     = (TextView) findViewById(R.id.txtViewOSBarTradeName);
       // txtViewBarDate          = (TextView) findViewById(R.id.txtViewOSBarCurrentDate);
       // txtViewProductSortTag   = (TextView) findViewById(R.id.txtViewTagTitle);

        //prepareOrderActionBar();


        edtTextDVQty = (EditText) findViewById(R.id.edtTextDVProductQty);

        edtTextDVQty.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                int result = i & EditorInfo.IME_MASK_ACTION;

                if (result == EditorInfo.IME_ACTION_DONE || result == EditorInfo.IME_ACTION_GO || result == EditorInfo.IME_ACTION_SEND || result == EditorInfo.IME_ACTION_NEXT)
                {


                    if(edtTextDVQty.getText().toString().isEmpty())
                    {

                        edtTextDVQty.setError("Please enter quantity");

                    }else if(Integer.parseInt(edtTextDVQty.getText().toString())==0)
                    {
                        edtTextDVQty.setError("Please enter quantity greater than 0");

                    }else
                    {
                        saveToDatabase(entry_mode);
                        clearEntryFields();
                        productAutoComplete.setEnabled(true);
                        spnrItemSelector.setEnabled(true);
                        disableEntryFields();
                        //edtTextDVComments.requestFocus();
                    }

                    //edtTextDVComments.requestFocus();
                    return true;

                }


                return false;
            }
        });


        edtTextDVComments = (EditText) findViewById(R.id.edtTextDVComments);

        edtTextDVComments.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                int result = i & EditorInfo.IME_MASK_ACTION;

                if (result == EditorInfo.IME_ACTION_NEXT || result == EditorInfo.IME_ACTION_DONE || result == EditorInfo.IME_ACTION_GO || result == EditorInfo.IME_ACTION_SEND) {

                    if (edtTextDVQty.getText().length() > 0) {
                        saveToDatabase(entry_mode);
                        clearEntryFields();
                        productAutoComplete.setEnabled(true);
                        spnrItemSelector.setEnabled(true);
                        disableEntryFields();
                        return true;

                    }


                }


                return false;
            }
        });

        //edtTextDeliveryDate     = (EditText) findViewById(R.id.edtTextDeliveryDate);

       // txtViewOSTradeName      = (TextView) findViewById(R.id.txtViewOSTradeName);
       // txtViewOSSubTotal       = (TextView) findViewById(R.id.txtViewOSSubtotal);
        lvOrderEntries          = (ListView) findViewById(R.id.lvDVEntries);

        spnrItemSelector        = (Spinner) findViewById(R.id.dv_spinner);




        btnDVAdd    = (Button) findViewById(R.id.btnDVAdd);
        btnDVFinish = (Button) findViewById(R.id.btnDVFinish);

        imgViewClearAll         = (ImageView) findViewById(R.id.imgViewClearDVSearchField);

        productAutoComplete     = (APCCustomAutoCompleteView) findViewById(R.id.actDVProductSearch);

        productAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));
        productAutoComplete.setFocusable(true);


        productAutoComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);

                }
            }
        });

        productAutoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });



        edtTextDVQty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);

                }
            }
        });

        edtTextDVQty.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });


        edtTextDVComments.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);

                }
            }
        });

        edtTextDVComments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnDVAdd.getVisibility() == View.GONE) {
                    btnDVAdd.setVisibility(View.VISIBLE);
                    btnDVFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });


        imgViewClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearEntryFields();
                disableEntryFields();
                productAutoComplete.setEnabled(true);
                spnrItemSelector.setEnabled(true);
                productAutoComplete.requestFocus();


            }
        });


        disableEntryFields();

        //apsProductDBHelper              = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        apsProductDBHelper              = ASRDatabaseFactory.getProductsDBInstance(getApplicationContext());
        //asrUserDBHelper                 = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        asrUserDBHelper                 = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());
        //otherProductDBHelper            = new ASROtherProductDatabaseHelper(getApplicationContext(),ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_NAME,null,ASROtherProductDatabaseHelper.OTHER_PRODUCT_DB_VERSION);
        otherProductDBHelper            = ASRDatabaseFactory.getOtherProductsDBInstance(getApplicationContext());
        //apsOrderDBHelper                = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        apsOrderDBHelper                = APCDatabaseHelper.getInstance(getApplicationContext());

        Cursor itemList                 = otherProductDBHelper.getAllCategory();

        ASRDoctorVisitArrayAdapter dvArray  = new ASRDoctorVisitArrayAdapter(this,R.layout.doctor_visit_spinner,new String[]{"Samples","Gifts","PPM"});
        dvArray.renderSpinnerValues(itemList);

        spnrItemSelector.setAdapter(dvArray);

        //fetching old orders or creating new
//
        if(entry_mode.contentEquals(ENTRY_MODE_EDIT))
        {
            visit_id                      = getIntent().getStringExtra("visit_id");

        }else
        {
            String lastIncompleteOrderNo    = apsOrderDBHelper.getLastIncompleteVisitOrderNo(doctor_code);

            if(!lastIncompleteOrderNo.contentEquals(""))
            {
                visit_id                    = lastIncompleteOrderNo;

            }else
            {
                int last_order_id           = apsOrderDBHelper.getLastVisitInsertID();
                visit_id                    = username+doctor_code+ String.format("%04d",last_order_id);

            }

        }


        Toast.makeText(getApplicationContext(),"Visit ID: "+visit_id,Toast.LENGTH_SHORT).show();
//
//
//
//        DateTime jDt                    = new DateTime(newCalendar.getTime());
//        System.out.println("JODA Time: "+jDt.getDayOfMonth());
//
//
//
//        //System.out.println("JODA Time: " + jDt.getDayOfMonth());
//        jDt = jDt.plusDays(2);
//
//        delivery_date = dateFormatter.format(jDt.toDate().getTime());
//
//
//
//        visit_date                      = dateHelper.getCurrentDateStr();
//        total                           = 0;
//        total_vat                       = 0;
//        total_commission                = 0;
//        main_product_price              = 0;
//        main_product_VAT                = 0;
//        main_product_commission         = 0;
        final Cursor orderCursor        = apsOrderDBHelper.getVisitEntries(visit_id);
        apsOrderCursorAdapter           = new ASRDoctorVisitCursorAdapter(getApplicationContext(),R.layout.dv_row_layout,orderCursor,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
//
//        int yyyy = jDt.getYear();
//        int mm   = jDt.getMonthOfYear()-1;
//        int dd   = jDt.getDayOfMonth();
//
//
//        if(orderCursor!=null)
//        {
//            if(orderCursor.getCount()>0)
//            {
//                orderCursor.moveToFirst();
//                total = orderCursor.getDouble(orderCursor.getColumnIndex("total"));
//                total_vat = orderCursor.getDouble(orderCursor.getColumnIndex("total_vat"));
//                total_commission = orderCursor.getDouble(orderCursor.getColumnIndex("total_commission"));
//                delivery_date = orderCursor.getString(orderCursor.getColumnIndex("delivery_date"));
//                try {
//                    jDt = new DateTime(dateHelper.getDateObjFromStringConventional(delivery_date).getTime());
//
//                    yyyy = jDt.getYear();
//                    mm   = jDt.getMonthOfYear()-1;
//                    dd   = jDt.getDayOfMonth();
//
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                //visit_date  = orderCursor.getString(orderCursor.getColumnIndex("visit_date"));
//            }
//        }
//
//
//
//
//        dp                    = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
//        {
//            @Override
//            public void onDateSet(DatePicker datePicker, int i, int i2, int i3)
//            {
//                Calendar newDate  = Calendar.getInstance();
//                newDate.set(i,i2,i3);
//
//                delivery_date = dateFormatter.format(newDate.getTime());
//                apsOrderDBHelper.updateDeliveryDateIfExists(visit_id,delivery_date);
//
//            }
//
//        },yyyy,mm,dd);
//
//
//
//
        lvOrderEntries.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                Cursor tmpCursor = (Cursor) lvOrderEntries.getAdapter().getItem(i);
                //String entryStatus          = tmpCursor.getString(tmpCursor.getColumnIndex("entry_status"));

                int entry_id = tmpCursor.getInt(tmpCursor.getColumnIndex("_id"));
                double qty = tmpCursor.getDouble(tmpCursor.getColumnIndex("qty"));
                //double price = tmpCursor.getDouble(tmpCursor.getColumnIndex("price"));
                //double vat = 0;
                //double commission = 0;

                //String offer                = tmpCursor.getString(tmpCursor.getColumnIndex("offer"));
                String item_name     = tmpCursor.getString(tmpCursor.getColumnIndex("item_name"));
                String comment       = tmpCursor.getString(tmpCursor.getColumnIndex("comments"));

                Intent openDialog   = new Intent(getApplicationContext(), ASRVisitEntryDialogActivity.class);

                openDialog.putExtra("_id", entry_id);
                openDialog.putExtra("mode", APC_EDIT_CODE);
                openDialog.putExtra("visit_id", visit_id);
                openDialog.putExtra("item_name", item_name);
                openDialog.putExtra("qty", qty);
                openDialog.putExtra("comments", comment);

                startActivityForResult(openDialog, REQUEST_CODE);

                return true;

            }


        });
//
//
//
        lvOrderEntries.setAdapter(apsOrderCursorAdapter);
        lvOrderEntries.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
//
//
//
//        txtViewOSTradeName.setText(trade_name);
//
//
//        txtViewOSSubTotal.setText(""+decFormatter.format(total));
//        // Sets focus on the search box and pop up the soft keyboard
//
//
//
//
        btnDVAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                btnDVAdd.setVisibility(View.GONE);
                btnDVFinish.setVisibility(View.VISIBLE);
                apsOrderDBHelper.getVisitTableDump();

            }
        });
//
        btnDVFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    //int order_item_count = lvOrderEntries.getAdapter().getCount();
//
//                    if (order_item_count == 0)
//                    {
//                        Toast.makeText(getApplicationContext(), "Cannot Finish ! This order is empty !", Toast.LENGTH_LONG).show();
//
//                    } else
//                    {
                        final Dialog orderConfirmationScreen = new Dialog(DoctorVisitActivity.this);
                        orderConfirmationScreen.setContentView(R.layout.confirm_order_screen);

                        LinearLayout l1 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO1);
                        LinearLayout l2 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO2);
                        LinearLayout l3 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO3);
                        LinearLayout l4 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO4);
                        LinearLayout l5 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO5);
                        LinearLayout l6 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO6);
                        LinearLayout l7 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llCO7);
                        LinearLayout l8 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.llC10);
                        LinearLayout l11 = (LinearLayout) orderConfirmationScreen.findViewById(R.id.lC11);



                        l1.setVisibility(View.GONE);
                        l2.setVisibility(View.GONE);
                        l3.setVisibility(View.GONE);
                        l4.setVisibility(View.GONE);
                        l5.setVisibility(View.GONE);
                        l6.setVisibility(View.GONE);
                        l8.setVisibility(View.GONE);
                        l11.setVisibility(View.GONE);

                        l7.setVisibility(View.VISIBLE);



//                        TextView confirmOrderDDate = (TextView) orderConfirmationScreen.findViewById(R.id.orderDeliveryDate);
//                        TextView confirmOrderSubtotal = (TextView) orderConfirmationScreen.findViewById(R.id.orderSubTotal);
//                        TextView confirmOrderTotalItems = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalItems);
//                        TextView confirmOrderTax = (TextView) orderConfirmationScreen.findViewById(R.id.orderTaxAmount);
//                        TextView confirmOrderCommission = (TextView) orderConfirmationScreen.findViewById(R.id.orderCommissionAmount);
//                        TextView confirmOrderTotal = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalAmount);
                        final EditText eTRemarks = (EditText) orderConfirmationScreen.findViewById(R.id.eTConfirmNotes);
                        Button btnConfirmOrderYes = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderYes);
                        Button btnConfirmOrderNo = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderNo);
                        RadioButton rbSelf          = (RadioButton) orderConfirmationScreen.findViewById(R.id.rbSelf);
                        final RadioButton rbSupervisor    = (RadioButton) orderConfirmationScreen.findViewById(R.id.rbSupervisor);
//                        confirmOrderDDate.setVisibility(View.GONE);
//                        confirmOrderSubtotal.setVisibility(View.GONE);
//                        confirmOrderTotalItems.setVisibility(View.GONE);
//
//                        //double tax          = 0.15 * total;
//                        //double tax          = 0.15 * total;b
//                        //                    double commission   = 0.05 * total;
//                        //double finalTotal = total + total_vat - total_commission;
//
//                        confirmOrderTax.setVisibility(View.GONE);
//                        confirmOrderCommission.setVisibility(View.GONE);
//                        confirmOrderTotal.setVisibility(View.GONE);






                        btnConfirmOrderYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                apsOrderDBHelper.updateVisitCompleteStatus(visit_id, ORDER_STATUS_COMPLETE);
                                String oRemarks     = eTRemarks.getText().toString();
                                String visitedWith  = "Self";

                                if(rbSupervisor.isChecked())
                                {
                                    visitedWith     = "Supervisor";

                                }

                                apsOrderDBHelper.addVisitRemarks(visit_id, oRemarks,visitedWith);

                                //new AsyncAutoSubmitDataTask("VISITS",postURL,getApplicationContext(),start_latitude,start_longitude).execute(user_token);

                                orderConfirmationScreen.dismiss();

                                onBackPressed();
                                finish();

                            }
                        });

                        btnConfirmOrderNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                orderConfirmationScreen.dismiss();
                            }
                        });


                        orderConfirmationScreen.setTitle("Confirm Visit");
                        orderConfirmationScreen.show();


                   // }

            }
        });
//
        productAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String searchItem   = productAutoComplete.getText().toString();
                String searchMode   = spnrItemSelector.getSelectedItem().toString();
                category_name       = searchMode;
                boolean entered     = apsOrderDBHelper.visitsExistsInOrder(visit_id, searchItem);

                if (entered)
                {
                    Toast.makeText(getApplicationContext(), "You have already entered this item once !", Toast.LENGTH_LONG).show();

                } else
                {

                    enableEntryFields();
                    productAutoComplete.setEnabled(false);
                    spnrItemSelector.setEnabled(false);

                    Cursor result = null;

                    if(searchMode.contentEquals("Samples"))
                    {

                        result = apsProductDBHelper.getOtherProductInfoFromName(searchMode,searchItem);

                    }else
                    {
                        result = otherProductDBHelper.getOtherProductInfoFromName(searchMode,searchItem);

                    }


                    if (result != null)
                    {
                        result.moveToFirst();

                        if(searchMode.contentEquals("Samples"))
                        {
                            product_id = result.getString(result.getColumnIndex("product_id"));

                        }else
                        {
                            product_id = result.getString(result.getColumnIndex("product_code"));

                        }

//                        Toast.makeText(getApplicationContext(),product_id,Toast.LENGTH_SHORT).show();

//                        double product_qty = result.getDouble(result.getColumnIndex("stock_qty"));
//                        double product_price = result.getDouble(result.getColumnIndex("price"));
//                        double product_vat = result.getDouble(result.getColumnIndex("vat"));
//                        double product_commission = result.getDouble(result.getColumnIndex("commission"));
//                        main_product_price = product_price;
//                        main_product_VAT = product_vat;
//                        main_product_commission = product_commission;
//                        String product_offer = result.getString(result.getColumnIndex("offer"));

                        //String visit_date = apsOrderDBHelper.getLastOrderDate(product_id, doctor_code);

//                        int no_of_days_since_prev_order = 0;
//
//                        try {
//                            Date last_order_date = dateHelper.getDateObjFromString(visit_date);
//                            Date current_date = dateHelper.getCurrentDateObj();
//
//                            Days days = Days.daysBetween(new DateTime(last_order_date), new DateTime(current_date));
//                            no_of_days_since_prev_order = days.getDays();
//
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//
//                        MenuItem qtyMenu = menu.findItem(R.id.stock_qty);
//                        MenuItem offerMenu = menu.findItem(R.id.action_offer);
//                        MenuItem priceMenu = menu.findItem(R.id.action_price);
//                        MenuItem daysAgo = menu.findItem(R.id.action_last_order);
//
//                        qtyMenu.setTitle("Stock Qty : " + product_qty);
//                        offerMenu.setTitle("Offer : " + product_offer);
//                        priceMenu.setTitle("Price : BDT " + product_price);
//                        daysAgo.setTitle("Last Order : " + no_of_days_since_prev_order + " days ago");



                    }
                }


            }
        });
//
//
//
//        txtViewProductSortTag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (sort_toggle_on)
//                {
//
//                    Cursor sortCursor = apsOrderDBHelper.getSortedOrderEntries(visit_id,"_id","DESC");
//                    apsOrderCursorAdapter.swapCursor(sortCursor);
//                    apsOrderCursorAdapter.notifyDataSetChanged();
//                    sort_toggle_on = false;
//
//                } else if (!sort_toggle_on)
//                {
//                    Cursor sortCursor = apsOrderDBHelper.getSortedOrderEntries(visit_id,"product_name","ASC");
//
//                    apsOrderCursorAdapter.swapCursor(sortCursor);
//                    apsOrderCursorAdapter.notifyDataSetChanged();
//
//                    sort_toggle_on = true;
//
//                }
//
//            }
//        });
//
//

        spnrItemSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {

                String populate_mode = spnrItemSelector.getAdapter().getItem(position).toString();
                populateSearchAdapter(populate_mode,"");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //String adapter_type = "";
        populateSearchAdapter("Samples","");

        // Location recording tasks
        prepareOrderActionBar();

    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            start_latitude  = tLocation.getLatitude();
            start_longitude = tLocation.getLongitude();

        }


//        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//        if(mLastLocation!=null)
//        {
//
//            start_latitude  = mLastLocation.getLatitude();
//            start_longitude = mLastLocation.getLongitude();
//
//
//            Toast.makeText(getApplicationContext(),"Current Location: "+start_latitude+","+start_longitude,Toast.LENGTH_SHORT).show();
//
//        }else
//        {
//
//            Toast.makeText(getApplicationContext(),"Location tracking may be disabled.",Toast.LENGTH_LONG).show();
//        }

    }

    private void hideKeyboard()
    {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void prepareOrderActionBar()
    {

        bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        //bar.setCustomView(R.layout.os_action_bar_layout);
        //bar.setTitle(trade_name);
        LayoutInflater mInflater    = LayoutInflater.from(this);
        View mCustomView            = mInflater.inflate(R.layout.custom_doctor_visit_action_bar_layout, null);
        TextView barTitle           = (TextView) mCustomView.findViewById(R.id.tvDVBarTitle);
        TextView barInstitute       = (TextView) mCustomView.findViewById(R.id.tvDVBarInstitute);
        //TextView barTakenBy         = (TextView) mCustomView.findViewById(R.id.tvDVBarTakenBy);


        barTitle.setText(doctor_name+ " | " + doctor_code);

        barInstitute.setText(institute_name + " | " + institute_code +" ("+username+")");

        bar.setCustomView(mCustomView);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayShowCustomEnabled(true);
        bar.show();
    }

    public void saveToDatabase(String mode)
    {

        boolean entry_status        = apsOrderDBHelper.visitExists(visit_id, mode);
        String oQty                 = edtTextDVQty.getText().toString();

        visited_with                = "";
        //String oBonus           = edtTextOSBonus.getText().toString();
        //String oBonus               = "";
        order_qty                   = Double.parseDouble(oQty.contentEquals("") ? "0":oQty);
        //order_bonus                 = Double.parseDouble(oBonus.contentEquals("") ? "0":oBonus);

        order_comments              = edtTextDVComments.getText().toString();

        //double productPrice     = Double.parseDouble(txtViewProduct   Price.getText().toString());
        //double productPrice         = main_product_price;
        //double productVAT           = main_product_VAT;
        //double productCommission    = main_product_commission;
        product_name                = productAutoComplete.getText().toString();


        String selected_cat_name    = spnrItemSelector.getSelectedItem().toString();
        Toast.makeText(getApplicationContext(),selected_cat_name,Toast.LENGTH_SHORT).show();

        VisitEntries entry          = new VisitEntries(visit_id,product_id,product_name,order_qty,order_comments,selected_cat_name);


//        total                       += order_qty * productPrice;
//        total_vat                   += productPrice *order_qty*(productVAT/100);
//        total_commission            += productPrice *order_qty*(productCommission/100);


        if(!entry_status)
        {


            recordLocation();
            LocalTime j         = jDTTimeHelper.toLocalTime();
            String strTime      = j.toString("HH:mm:ss");
           // String strTime = j.getHourOfDay()+":"+j.getMinuteOfHour()+":"+j.getSecondOfMinute();

            VisitInfo orderEntry = new VisitInfo(visit_id,doctor_code,doctor_name,this.order_date,institute_name,ORDER_STATUS_INCOMPLETE,start_latitude,start_longitude,0,0,employee_id,visited_with,strTime,APCDatabaseHelper.VISIT_TYPE_NORMAL);
            apsOrderDBHelper.insertVisit(orderEntry);

        }

        apsOrderDBHelper.insertVisitEntry(entry);



        Cursor updatedCursor     = apsOrderDBHelper.getVisitEntries(visit_id);

        apsOrderCursorAdapter.swapCursor(updatedCursor);
        apsOrderCursorAdapter.notifyDataSetChanged();

//        double tax               = total * (0.15);
//        double commision         = total * (0.05);
//
//        main_product_price       = 0;
//        main_product_commission  = 0;
//        main_product_VAT         = 0;
//
//        MenuItem qtyMenu        = menu.findItem(R.id.stock_qty);
//        MenuItem offerMenu      = menu.findItem(R.id.action_offer);
//        MenuItem priceMenu      = menu.findItem(R.id.action_price);
//        MenuItem daysAgo        = menu.findItem(R.id.action_last_order);
//
//        qtyMenu.setTitle("Stock Qty : -- ");
//        offerMenu.setTitle("Offer : --");
//        priceMenu.setTitle("Price : --");
//        daysAgo.setTitle("Last Order : --");
//
//
//        txtViewOSSubTotal.setText(""+decFormatter.format(total));
        lvOrderEntries.smoothScrollToPosition(0);

        this.sort_toggle_on = false;


    }

    public void populateSearchAdapter(String searchMode, String searchItem)
    {
        List<ProductInfo> pInfo = null;

        if(searchMode.contentEquals("Samples"))
        {

            pInfo = apsProductDBHelper.getAutoCompleteOtherProductInfo(searchMode,searchItem);

        }else
        {
            pInfo = otherProductDBHelper.getAutoCompleteOtherProductInfo(searchMode,searchItem);

        }


        productArray            = new String[pInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < pInfo.size();++index)
        {


            productArray[index]        = new String(pInfo.get(index).getProductName());
        }


        ArrayAdapter adapter            = new ArrayAdapter(DoctorVisitActivity.this, android.R.layout.simple_list_item_1, productArray);

        productAutoComplete.setAdapter(adapter);
        productAutoComplete.setThreshold(1);



    }
//

    public void enableEntryFields()
    {
        edtTextDVQty.requestFocus();
        edtTextDVQty.setEnabled(true);
        edtTextDVComments.setEnabled(true);
    }

    public void disableEntryFields()
    {

        productAutoComplete.requestFocus();
        edtTextDVQty.setFocusable(true);
        edtTextDVQty.setEnabled(false);
        edtTextDVComments.setEnabled(false);

    }


    public void clearEntryFields()
    {
        productAutoComplete.setText("");
        edtTextDVQty.setText("");
        edtTextDVComments.setText("");

    }


    public void updateListUI()
    {
        Cursor updatedProductEntries    = apsOrderDBHelper.getVisitEntries(visit_id);
        apsOrderCursorAdapter.swapCursor(updatedProductEntries);
        apsOrderCursorAdapter.notifyDataSetChanged();

    }
    public void updateTotalData()
    {

//        double tax               = total * (0.15);
//        double commision         = total * (0.05);
        //txtViewOSSubTotal.setText("" + decFormatter.format(total));
        lvOrderEntries.smoothScrollToPosition(0);
    }

    public void removeItemFromList(int entry_id)
    {
        apsOrderDBHelper.removeVisitEntry(entry_id);
        updateListUI();

    }
    //
    public void editItemInList(int entry_id, String order_no, String comments, double qty, double bonus)
    {
        apsOrderDBHelper.editEntry(entry_id,order_no,comments,qty,bonus);
        Cursor updatedProductEntries    = apsOrderDBHelper.getOrderEntries(order_no);
        apsOrderCursorAdapter.swapCursor(updatedProductEntries);
        apsOrderCursorAdapter.notifyDataSetChanged();

    }

    public void editVisitItemInList(int entry_id, String order_no, String comments, double qty)
    {
        apsOrderDBHelper.editVisitEntry(entry_id, order_no, comments, (int) qty);
        Cursor updatedProductEntries    = apsOrderDBHelper.getVisitEntries(order_no);
        apsOrderCursorAdapter.swapCursor(updatedProductEntries);
        apsOrderCursorAdapter.notifyDataSetChanged();

    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    //
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if(resultCode==APC_REMOVE_CODE)
        {
            int entry_id                = data.getIntExtra("_id",0);
//            double prevTotal            = data.getDoubleExtra("prevTotal", 0);
//            double prevVAT              = data.getDoubleExtra("prevVAT",0);
//            double prevCommission       = data.getDoubleExtra("prevCommission",0);

            Toast.makeText(getApplicationContext(), "Removed Product ID: " + entry_id, Toast.LENGTH_LONG).show();

            if(entry_id>0)
            {
                removeItemFromList(entry_id);

            }

//
//            total -= prevTotal;
//            total_vat -= prevVAT;
//            total_commission -= prevCommission;
//            apsOrderDBHelper.updateOrderTotal(visit_id,total,total_vat,total_commission);
          //  updateTotalData();


        }else if(resultCode==APC_ADD_CODE)
        {
//            boolean entry_status    = apsOrderDBHelper.orderExists(visit_id,"");
//            //String oQty             = data.getStringExtra("qty");
//            //String oBonus           = data.getStringExtra("bonus");
//            order_qty               = data.getDoubleExtra("qty",0);
//            order_bonus             = data.getDoubleExtra("bonus",0);
//            order_comments          = data.getStringExtra("comments");
//
//            // double productPrice     = Double.parseDouble(txtViewProductPrice.getText().toString());double productPrice     = Double.parseDouble(txtViewProductPrice.getText().toString());
//            double productPrice     = main_product_price;
//            double productVAT       = main_product_VAT;
//            double productCommission = main_product_commission;
//            product_name            = productAutoComplete.getText().toString();
//
//            OrderEntries entry      = new OrderEntries(visit_id,product_id,product_name,order_qty,productPrice,productVAT, productCommission,order_bonus,order_comments);
//
//            total                   += order_qty * productPrice;
//            total_vat               += total * (productVAT/100);
//            total_commission        += total * (productCommission/100);
//            delivery_date           = todays_date;
//
//
//            if(!entry_status)
//            {
//                OrderInfo orderEntry = new OrderInfo(visit_id,customer_id,trade_name,delivery_date,todays_date,total,ORDER_STATUS_INCOMPLETE,start_latitude,start_longitude,0,0,employee_id);
//                apsOrderDBHelper.insertOrder(orderEntry);
//
//            }else
//            {
//                apsOrderDBHelper.updateOrderTotal(visit_id,total,total_vat,total_commission);
//            }
//
//            apsOrderDBHelper.insertOrderEntry(entry);
//
//            Cursor updatedCursor     = apsOrderDBHelper.getOrderEntries(visit_id);
//
//            apsOrderCursorAdapter.swapCursor(updatedCursor);
//            apsOrderCursorAdapter.notifyDataSetChanged();
//
////            double tax               = total * (0.15);
////            double commision         = total * (0.05);
//
//            apsOrderDBHelper.updateOrderTotal(visit_id, total, total_vat, total_commission);
//
//            updateTotalData();
//            clearEntryFields();
//            disableEntryFields();


        }else if(resultCode==APC_EDIT_CODE)
        {
            int entry_id            = data.getIntExtra("_id", 0);
            //double prevTotal        = data.getDoubleExtra("prevTotal",0);
            //double currentTotal     = data.getDoubleExtra("currentTotal",0);
            String resultComment    = data.getStringExtra("comments");
            double editQty          = data.getDoubleExtra("qty", 0.0);
            //double bonustQty        = data.getDoubleExtra("bonus", 0.0);
            String edit_order_no    = data.getStringExtra("visit_id");

            if(entry_id>0)
            {
                editVisitItemInList(entry_id, edit_order_no, resultComment, editQty);

            }


            //apsOrderDBHelper.updateOrderTotal(visit_id,total,total_vat,total_commission);
            //updateTotalData();

        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.order_screen_menu_item, menu);
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent dbmanager = new Intent(DoctorVisitActivity.this,AndroidDatabaseManager.class);
            startActivity(dbmanager);

            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            //NavUtils.navigateUpFromSameTask(this);
            return true;


        }else if(id == R.id.return_to_main)
        {

            String source_activity  = "";

            Intent main_menu = new Intent(DoctorVisitActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            stopLocationUpdates();
            startActivity(main_menu);
            finish();




            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopLocationUpdates();

    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();


    }

    public void stopLocationUpdates()
    {
        if(mGoogleApiClient!=null)
        {
            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }


        }

    }


    protected void createLocationRequest()
    {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }


    @Override
    public void onConnected(Bundle bundle)
    {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        if(location!=null)
        {
            mCurrentLocation = location;
            recordLocation();


        }
    }
}
