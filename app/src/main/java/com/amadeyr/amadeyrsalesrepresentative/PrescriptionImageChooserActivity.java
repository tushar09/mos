package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRPrescriptionImageAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by acl on 2/15/16.
 */
public class PrescriptionImageChooserActivity extends Activity {


    private String SCREEN_TITLE = "Choose prescriptions for submission";
    private GridView grid;
    private Button btnOK;
    private APCDatabaseHelper dbHelper;
    private final int SUCCESS_CODE = 100;
    static final String[] MOBILE_OS = new String[] {
            "1", "2","3", "4" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
                                                                                                                                    super.onCreate(savedInstanceState);
        setContentView(R.layout.prescription_image_chooser);

//        android.support.v7.app.ActionBar bar = getSupportActionBar();
//        bar.setTitle(SCREEN_TITLE);
//        bar.setDisplayHomeAsUpEnabled(true);
//        bar.setIcon(R.drawable.iconb);
//        bar.show();
        dbHelper = APCDatabaseHelper.getInstance(getApplicationContext());

        String prescription_id = getIntent().getStringExtra("prescription_id");


        final Cursor c = dbHelper.getPrescriptionImages(prescription_id);

        grid  = (GridView) findViewById(R.id.prescription_grid);
        btnOK = (Button) findViewById(R.id.btnImgChooserOK);

        final ASRPrescriptionImageAdapter imgAdapter = new ASRPrescriptionImageAdapter(getApplicationContext(), R.layout.prescription_image_chooser_cell_layout,c,new String[]{},new int[]{});

        grid.setAdapter(imgAdapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        "OK", Toast.LENGTH_SHORT).show();

            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                ArrayList<String> selectedListOfOrders = imgAdapter.getSelectedListOfOrders();
                dbHelper.updateSelectStatusOfPrescriptionImages(selectedListOfOrders);


                if(c!=null)
                {
                    c.moveToFirst();


                    while(!c.isAfterLast())
                    {
                        String entry_id  = c.getString(c.getColumnIndex("_id"));


                        if(!selectedListOfOrders.contains(entry_id))
                        {
                            dbHelper.removeImageFromTable(entry_id);
                            String img_path         = c.getString(c.getColumnIndex("image_name"));

                            File imgFile            = new  File(img_path);
                            imgFile.delete();

                        }else
                        {
                            selectedListOfOrders.remove(entry_id);

                        }


                        c.moveToNext();
                    }

                }


                imgAdapter.clearSelectedListOfOrders();
                setResult(100);
                finish();

            }
        });

    }
}
