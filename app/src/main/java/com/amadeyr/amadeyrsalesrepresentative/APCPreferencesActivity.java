package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

//import com.google.zxing.integration.android.IntentIntegrator;
//import com.google.zxing.integration.android.IntentResult;

/**
 * Created by acl on 5/18/15.
 */
public class APCPreferencesActivity extends Activity {
    private TextView txtViewGetURL, txtViewPostURL;
    private EditText edtTextDownloadURL;
    private Button prefSet, btnPrefSync;
    private CheckBox enableDeviceCamera;
    SharedPreferences sharedPreferences;
    private com.amadeyr.amadeyrsalesrepresentative.APCProductDatabaseHelper apcProductDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);

        //apcProductDBHelper  = new APCProductDatabaseHelper(getApplicationContext(),"amadeyrProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        apcProductDBHelper = ASRDatabaseFactory.getProductsDBInstance(getApplicationContext());
        txtViewGetURL = (TextView) findViewById(R.id.edtTextGetURL);
        txtViewPostURL = (TextView) findViewById(R.id.edtTextPostURL);
        edtTextDownloadURL = (EditText) findViewById(R.id.edtTextDownloadURL);
        prefSet = (Button) findViewById(R.id.btnPrefSet);
        btnPrefSync = (Button) findViewById(R.id.btnPreferencesSync);
        enableDeviceCamera = (CheckBox) findViewById(R.id.chkBoxUseDeviceCamera);

        sharedPreferences = getSharedPreferences("apcpref", Context.MODE_PRIVATE);


        txtViewGetURL.setText(sharedPreferences.getString("getURL", ""));
        txtViewPostURL.setText(sharedPreferences.getString("postURL", ""));
        edtTextDownloadURL.setText(sharedPreferences.getString("downloadURL", ""));
        enableDeviceCamera.setChecked(sharedPreferences.getBoolean("scanMode", false));

        prefSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("getURL", txtViewGetURL.getText().toString());
                editor.putString("postURL", txtViewPostURL.getText().toString());
                editor.putString("downloadURL", edtTextDownloadURL.getText().toString());
                editor.putBoolean("scanMode", enableDeviceCamera.isChecked());
                editor.commit();

                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
            }
        });

        btnPrefSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //String url                      = sharedPreferences.getString("downloadURL","")+"/amadeyrProductDB.sqlite";
                String url = sharedPreferences.getString("downloadURL", "") + "/amadeyrProductDB.sqlite.zip";
                Log.e("download url", url);
                downloadProductDB(url);
            }
        });


    }

    public void downloadProductDB(String url) {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = connMgr.getActiveNetworkInfo();

        if (nInfo != null && nInfo.isConnected()) {
            new AccessAPCServerTask().execute(url);

        } else {
            Log.d("Network Error", "Connection Unavailable");

        }

    }

    private class AccessAPCServerTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pd;

        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(APCPreferencesActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Updating product database from server ...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                String result = getInfoWithUrl(urls[0]);
                return result;

            } catch (IOException e) {

                return "Unsuccessfull";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            pd.dismiss();
            System.out.println("SUCCESS !");
            apcProductDBHelper.createIndexes();
            //pd.dismiss();


            //productInfo = new String(s);
        }

        private String getInfoWithUrl(String serverUrl) throws IOException {

            FileOutputStream output = null;
            FileInputStream fis = null;
            InputStream input = null;
            try {
                URL url = new URL(serverUrl);
                URLConnection connection = url.openConnection();

                input = connection.getInputStream();

                System.out.println("APCNetworkHandler 1");

                //File downloadedDBFile = new File("/data/data/" + getPackageName() + "/databases","amadeyrProductDB");


                File downloadedDBFilePath = new File("/data/data/" + getPackageName() + "/files/");

                if (!downloadedDBFilePath.exists()) {
                    downloadedDBFilePath.mkdir();

                }

                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/files", "amadeyrProductDB.zip");

                downloadedDBFile.createNewFile();


                output = new FileOutputStream(downloadedDBFile);

                int read;
                byte[] data = new byte[1024];
                while ((read = input.read(data)) != -1) {
                    // System.out.println("datavar: "+read);

                    output.write(data, 0, read);

                }

                output.close();
                if (fis != null) fis.close();
                if (input != null) input.close();

                //pd.setMessage("Extracting database");

                APCFileOperationsHelper apf = new APCFileOperationsHelper(getApplicationContext());
                String _zipFileLocation = "/data/data/" + getPackageName() + "/files/amadeyrProductDB.zip";
                String _zipExtractedFileLocation = "/data/data/" + getPackageName() + "/databases/";
                String _extractedFileName = "amadeyrProductDB";
                try {
                    apf.unzip(_zipFileLocation, _zipExtractedFileLocation, _extractedFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                e.printStackTrace();


            } finally {

                if (fis != null) fis.close();
                if (input != null) input.close();


            }

            return "Done";
        }
    }


}
