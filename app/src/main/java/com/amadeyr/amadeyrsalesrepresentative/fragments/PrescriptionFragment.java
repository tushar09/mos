package com.amadeyr.amadeyrsalesrepresentative.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCOrderSCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.ASRDualSubmissionActivity;
import com.amadeyr.amadeyrsalesrepresentative.ASRPrescriptionImageBrowser;
import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.MainMenuActivity;
import com.amadeyr.amadeyrsalesrepresentative.OrderScreenActivity;
import com.amadeyr.amadeyrsalesrepresentative.R;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRPrescriptionSubmissionCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRVisitSCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncPrescriptionImageUploadTask;
import com.amadeyr.amadeyrsalesrepresentative.tasks.UpdateUserTokenTask;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

/**
 * Created by amadeyr on 1/26/16.
 */
public class PrescriptionFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener
{
    private Context appCtx;

    private ListView lvPreviousEntries;
    private Location mCurrentLocation;
    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private APCCustomAutoCompleteView actSearchField;
    private ASRPrescriptionSubmissionCursorAdapter apcCursorAdapter;
    private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;
    private SharedPreferences sp;
    private String product_name, product_id, product_barcode, user_role;
    private ImageView imgViewClearAll;

    private Button btnSearch, btnSubmit, btnSubmitAll, btnRemove;


    private final int REQUEST_CODE      = 100;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;

    private double latitude, longitude;

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] prescriptionArray;

    private HashMap<String,String> productNameMapper;

    private String screenTitle = "Submissions";
    private String userId;

    private final String DATA_SUBMISSION_TYPE_SELECTED  = "SELECTED";
    private final String DATA_SUBMISSION_TYPE_ALL       = "ALL";
    private final String ENTRY_MODE_EDIT            = "EDIT";
    private final String FRAGMENT_NAME              = "prescription";

    private Cursor lastSubmitted;

    public ArrayList<String> selectedForSubmission;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public ProgressDialog pd ;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        sp                      = getActivity().getSharedPreferences("apcpref",Context.MODE_PRIVATE);

        postURL                 = sp.getString("postURL","")+getResources().getString(R.string.postExtensionPrescription);

        user_role               = sp.getString("user_role", "");
        userId                  = sp.getString("username", "");

        appCtx                  = getActivity().getApplicationContext();
        pd                      = new ProgressDialog(getActivity());



        this.setPrescriptionAdapter();

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        if(mGoogleApiClient!=null) mGoogleApiClient.connect();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view               = inflater.inflate(R.layout.pending_submissions,container,false);

        if(appCtx==null) appCtx = getActivity().getApplicationContext();


        actSearchField          = (APCCustomAutoCompleteView) view.findViewById(R.id.apsPSSearch);

        actSearchField.addTextChangedListener(new CustomAutoCompleteTextChangedListener(getActivity(),"FRAGMENT",this.FRAGMENT_NAME));
        actSearchField.setThreshold(1);
        actSearchField.setFocusable(true);

        lvPreviousEntries       = (ListView) view.findViewById(R.id.lstViewAPSSubmissions);
        dbHelper                = APCDatabaseHelper.getInstance(appCtx);


        //Cursor orderEntries     = dbHelper.getPendingSubmissions(userId);
        Cursor pendingPrescriptionList    = null;
        pendingPrescriptionList           = dbHelper.getPendingPrescriptionList("");


        apcCursorAdapter        = new ASRPrescriptionSubmissionCursorAdapter(appCtx,R.layout.pending_row_layout,pendingPrescriptionList,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvPreviousEntries.setAdapter(apcCursorAdapter);

        btnSearch               = (Button)   view.findViewById(R.id.btnPSSearchShowAll);
        btnSubmit               = (Button)   view.findViewById(R.id.btnSendSelected);
        btnSubmitAll            = (Button)   view.findViewById(R.id.btnPSSubmitAll);
        btnRemove               = (Button)   view.findViewById(R.id.btnPSDeleteAll);
        imgViewClearAll         = (ImageView) view.findViewById(R.id.imgViewPSSearchClearButton);


        actSearchField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String searchKey = actSearchField.getText().toString();
                Cursor searchCursor = dbHelper.getEntryForNameFromPrescription(searchKey);
                apcCursorAdapter.swapCursor(searchCursor);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });


        actSearchField.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view)
            {
                actSearchField.setText("");
                return true;
            }
        });


        lvPreviousEntries.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
//                Toast.makeText(appCtx, "Item clicked!", Toast.LENGTH_SHORT).show();
//
                Cursor itemInfo = (Cursor) lvPreviousEntries.getAdapter().getItem(position);

                if (itemInfo != null) {


                    Intent editScreenStarter = new Intent(appCtx, ASRPrescriptionImageBrowser.class);

                    editScreenStarter.putExtra("mode", ENTRY_MODE_EDIT);
                    editScreenStarter.putExtra("group_id", itemInfo.getString(itemInfo.getColumnIndex("group_id")));

                    startActivity(editScreenStarter);


                }

                return true;
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ASRPrescriptionSubmissionCursorAdapter ap   = (ASRPrescriptionSubmissionCursorAdapter) lvPreviousEntries.getAdapter();
                selectedForSubmission                       = ap.getSelectedListOfOrders();




                try {

                    //new AsyncPrescriptionImageUploadTask(getActivity().getApplicationContext()).execute();

                    postDataToServer(DATA_SUBMISSION_TYPE_SELECTED);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        btnSubmitAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {


                    postDataToServer(DATA_SUBMISSION_TYPE_ALL);
                    Cursor newCursor    = null;

                    if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
                    {
                        newCursor           = dbHelper.getPendingSubmissions("");

                    }else
                    {

                        newCursor           = dbHelper.getPendingSubmissions(userId);
                    }

                    actSearchField.setText("");
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                final Dialog removalConfirmationScreen = new Dialog(getActivity());
                removalConfirmationScreen.setContentView(R.layout.confirm_order_removal);


                TextView confirmOrderDDate = (TextView) removalConfirmationScreen.findViewById(R.id.txtViewOrderRemovalMessage);

                Button btnConfirmRemovalYes = (Button) removalConfirmationScreen.findViewById(R.id.btnRemoveOrder);
                Button btnConfirmRemovalNo = (Button) removalConfirmationScreen.findViewById(R.id.btnCancelRemoval);

                selectedForSubmission   = apcCursorAdapter.getSelectedListOfOrders();

                confirmOrderDDate.append(selectedForSubmission.size() + " items ?");

                btnConfirmRemovalYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        removeFromSubmissionList();
                        removalConfirmationScreen.dismiss();
                        selectedForSubmission.clear();


                    }
                });

                btnConfirmRemovalNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removalConfirmationScreen.dismiss();
                    }
                });


                removalConfirmationScreen.setTitle("Remove Order");
                removalConfirmationScreen.show();





            }
        });

        imgViewClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actSearchField.setText("");
                //Cursor allEntries = dbHelper.getPendingSubmissions(userId);

                Cursor allEntries    = null;

                allEntries           = dbHelper.getPendingPrescriptionList("");

                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Cursor allEntries = dbHelper.getPendingSubmissions(userId);

                Cursor allEntries    = null;

                if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
                {
                    allEntries           = dbHelper.getPendingSubmissions("");

                }else
                {

                    allEntries           = dbHelper.getPendingSubmissions(userId);
                }


                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();


            }


        });





        return view;

    }


    private void removeImageFiles(Cursor prescription_images)
    {
        if (prescription_images != null)
        {
            prescription_images.moveToFirst();

            while (!prescription_images.isAfterLast())
            {
                String path = prescription_images.getString(prescription_images.getColumnIndex("image_name"));

                File f  = new File(path);
                f.delete();

                prescription_images.moveToNext();
            }
        }

    }

    public void removeFromSubmissionList()
    {

        int sizeOfList = selectedForSubmission.size();
        //JSONObject dataArray = new JSONObject();


        for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
        {
            String selectedOrderID = selectedForSubmission.get(lIterator).toString();
            dbHelper.removePrescription(selectedOrderID);
            Cursor c = dbHelper.getPrescriptionImages(selectedOrderID);
            removeImageFiles(c);


        }

        selectedForSubmission.clear();

        //Cursor updatedList = dbHelper.getPendingSubmissions(userId);
        Cursor updatedList    = null;

         updatedList           = dbHelper.getPendingPrescriptionList("");



        apcCursorAdapter.swapCursor(updatedList);
        apcCursorAdapter.notifyDataSetChanged();
        actSearchField.setText("");

    }

    public void callStringSearchMethod()
    {
        String searchString = actSearchField.getText().toString();
        this.populateSearchAdapter(searchString);
    }


    public void resetSelectedSubmissions()
    {

        ASRPrescriptionSubmissionCursorAdapter ap2   = (ASRPrescriptionSubmissionCursorAdapter) lvPreviousEntries.getAdapter();
        ap2.clearSelectedListOfOrders();
    }

    public void populateSearchAdapter(String searchItem)
    {
        Cursor results          = dbHelper.getAutocompletePrescriptionInfoForPending(searchItem);

        if(results!=null)
        {
            prescriptionArray              = new String[results.getCount()];
            int str_index           = 0;

            results.moveToFirst();


            while(!results.isAfterLast())
            {
                prescriptionArray[str_index]        = new String(results.getString(results.getColumnIndex("doctor_name")));
                str_index++;
                results.moveToNext();
            }


            ArrayAdapter adapter         = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, prescriptionArray);

            actSearchField.setAdapter(adapter);
            actSearchField.setThreshold(1);


        }





    }


    public void populateAPCAutocompleteArray(String searchTerm)
    {
        Cursor currentEntries = null;

        if(searchTerm.equals(""))
        {
            currentEntries = dbHelper.getDistinctEntries();

        }else
        {

            currentEntries  = dbHelper.getOrders(searchTerm);
        }



        if(currentEntries!=null)
        {
            currentEntries.moveToFirst();

            String[] barcodeArray = new String[currentEntries.getCount()];


            int string_arr_index = 0;

            for (int json_arr_index = 0; json_arr_index < currentEntries.getCount();++json_arr_index)
            {
                barcodeArray[json_arr_index]        = currentEntries.getString(currentEntries.getColumnIndex("trade_name"));
                currentEntries.moveToNext();
                //System.out.println("SearchActivity: "+jObj.getString("description"));
            }

            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, barcodeArray);
            actSearchField.setAdapter(adapter);


        }

    }

    public void postDataToServer(String sendType) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {




            new AccessAPCServerTask().execute(sendType);

        }else
        {
            Toast.makeText(appCtx,"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }



    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
           // new AccessAPCServerTask().execute("","");

        }else
        {
            Toast.makeText(appCtx,"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }


    public void setPrescriptionAdapter()
    {
        String user_id          = new String(userId);
        //dbHelper                = new APCDatabaseHelper(appCtx,"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper                = APCDatabaseHelper.getInstance(appCtx);

        Cursor pendingPrescriptionList    = null;

        pendingPrescriptionList           = dbHelper.getPendingPrescriptionList("");

        apcCursorAdapter                  = new ASRPrescriptionSubmissionCursorAdapter(getActivity().getApplicationContext(), R.layout.pending_row_layout,pendingPrescriptionList,new String[]{},new int[]{} , CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,getActivity(),PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(appCtx,
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks((ASRDualSubmissionActivity)getActivity())
                .addOnConnectionFailedListener((ASRDualSubmissionActivity)getActivity())
                .addApi(LocationServices.API).build();
    }



    private void recordLocation()
    {
        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            latitude  = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

        }

    }


    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        }



    }

    @Override
    public void onLocationChanged(Location location) {

        if(location!=null)
        {

            mCurrentLocation = location;
            recordLocation();


        }

    }

    private class AccessAPCServerTask extends AsyncTask<String,Void, String>
    {

        //private ProgressDialog pd;
        private boolean getAll = true;
        private ArrayList<String> image_filenames = new ArrayList<>();

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            recordLocation();
            stopLocationUpdates();
            pd.setTitle("Please Wait");
            pd.setMessage("Submitting data to server  ...");
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls)
        {
            String result="";
            String sendType = urls[0];
            recordLocation();

            List<String> submitted_order_ids = new ArrayList<>();

            try
            {

                if(sendType.contentEquals(DATA_SUBMISSION_TYPE_ALL))
                {

                    Cursor currentPending = dbHelper.getPendingPrescriptionGroup();
                    lastSubmitted = currentPending;
                    Cursor prescription_images= null;
                    if (currentPending != null) currentPending.moveToFirst();

                    JSONArray dataArray = new JSONArray();
//
                    while (!currentPending.isAfterLast())
                    {

                        JSONObject dataForSubmission = new JSONObject();

                        //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
                        //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
                        //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();
                        String post_order_no = currentPending.getString(currentPending.getColumnIndex("group_id"));


                        //String order_taken_by = post_order_no.substring(0,7);

                        String order_taken_by = currentPending.getString(currentPending.getColumnIndex("taken_by"));

                        dataForSubmission.put("group_id", currentPending.getString(currentPending.getColumnIndex("group_id")));
                        dataForSubmission.put("doctor_code", currentPending.getString(currentPending.getColumnIndex("doctor_code")));
                        dataForSubmission.put("creation_time", currentPending.getLong(currentPending.getColumnIndex("creation_time")));
                        dataForSubmission.put("start_lat", currentPending.getString(currentPending.getColumnIndex("start_lat")));
                        dataForSubmission.put("start_lon", currentPending.getString(currentPending.getColumnIndex("start_lon")));
                        dataForSubmission.put("submit_lat", latitude);
                        dataForSubmission.put("submit_lon", longitude);

                        //String pres_img_path    = currentPending.getString(currentPending.getColumnIndex("image_upload_path"));

                        //Log.d("prescription_image", pres_img_path.substring(pres_img_path.lastIndexOf("/")+1,pres_img_path.length()));
                        //dataForSubmission.put("image_file_name", pres_img_path.substring(pres_img_path.lastIndexOf("/") + 1, pres_img_path.length()));
                        //dataForSubmission.put("prescription_image_path", encodePhoto(currentPending.getString(currentPending.getColumnIndex("image_upload_path"))));
                        //dataForSubmission.put("prescription_image", encodePhoto(currentPending.getString(currentPending.getColumnIndex("image_upload_path"))));
                        dataForSubmission.put("taken_by", order_taken_by);


                        //dataForSubmission.put("remarks", currentPending.getString(currentPending.getColumnIndex("remarks")));
                        //dataForSubmission.put("taken_by",order_taken_by);
                        submitted_order_ids.add(post_order_no);

                        //dataForSubmission.put("submit_lat", currentPending.getString(currentPending.getColumnIndex("submit_lat")));
                        //dataForSubmission.put("submit_lon", currentPending.getString(currentPending.getColumnIndex("submit_lon")));


                        //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));


                        Cursor order_entries = dbHelper.getPrescriptionEntry(post_order_no);

                        JSONArray detailsArray = new JSONArray();

                        if (order_entries != null) {
                            order_entries.moveToFirst();

                            while (!order_entries.isAfterLast())
                            {
                                JSONObject dataEntries = new JSONObject();

                                //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                //dataEntries.put("master_group_id", order_entries.getString(order_entries.getColumnIndex("master_group_id")));

                                if(order_entries.getString(order_entries.getColumnIndex("product_code")).isEmpty())
                                {

                                    dataEntries.put("product_code", "(UKNP) | "+order_entries.getString(order_entries.getColumnIndex("product_name")));

                                }else
                                {

                                    dataEntries.put("product_code", order_entries.getString(order_entries.getColumnIndex("product_code")));

                                }

                                //dataEntries.put("product_code", order_entries.getString(order_entries.getColumnIndex("product_code")));


                                detailsArray.put(dataEntries);

                                dataEntries = null;
                                order_entries.moveToNext();
                            }
                        }

                        dataForSubmission.put("prescription_details", detailsArray.toString());

                        dataArray.put(dataForSubmission);


                        detailsArray = null;


                        // Create JSON for prescription Images

                        prescription_images  =   dbHelper.getPrescriptionImagesForSubmission(post_order_no);
                        JSONArray img_details_array =   new JSONArray();


                        if (prescription_images != null)
                        {
                            prescription_images.moveToFirst();

                            while (!prescription_images.isAfterLast())
                            {
                                JSONObject dataEntries = new JSONObject();

                                //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text
                                String file_path_in_device =     prescription_images.getString(prescription_images.getColumnIndex("image_name"));
                                //dataEntries.put("master_group_id", prescription_images.getString(prescription_images.getColumnIndex("group_id")));
                                dataEntries.put("file_name", file_path_in_device.substring(file_path_in_device.lastIndexOf("/")+1,file_path_in_device.length()-4));

                                img_details_array.put(dataEntries);

                                prescription_images.moveToNext();
                            }
                        }

                        dataForSubmission.put("image_list", img_details_array.toString());

                        img_details_array = null;

                        currentPending.moveToNext();
                    }

                    //Answers.getInstance().logCustom(new CustomEvent("Submitted Prescription Details").putCustomAttribute("Prescription details JSON", dataArray.toString()));
                    //Log.d("Prescription JSON before submit: ",dataArray.toString());
                    String TAG = "Prescription JSON ";
                    String sb = dataArray.toString();

//                    if (sb.length() > 4000) {
//                        Log.v(TAG, "sb.length = " + sb.length());
//                        int chunkCount = sb.length() / 4000;     // integer division
//                        for (int i = 0; i <= chunkCount; i++) {
//                            int max = 4000 * (i + 1);
//                            if (max >= sb.length()) {
//                                Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i));
//                            } else {
//                                Log.v(TAG, "chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i, max));
//                            }
//                        }
//                    } else {
                        Log.v(TAG, sb.toString());
                    //}

                    result = submitInfoToUrl(postURL, dataArray.toString());

                    if (result != null) {

                        JSONArray jArray = null;

                        try {
                            if (result.contentEquals("success"))
                            {

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {

                                    dbHelper.updatePrescriptionStatusToSubmitWithID(submitted_order_ids.get(s_it));
                                    dbHelper.updatePrescriptionSubmitLocation(submitted_order_ids.get(s_it), latitude, longitude);
                                }


                                ASRPrescriptionSubmissionCursorAdapter ap2   = (ASRPrescriptionSubmissionCursorAdapter) lvPreviousEntries.getAdapter();
                                ap2.clearSelectedListOfOrders();
                                selectedForSubmission = null;
                                //removeImageFile(prescription_images);


                            }else if(result.startsWith("[{"))
                            {

                                JSONArray errorArray    = new JSONArray(result);
                                JSONObject errorObj     = errorArray.getJSONObject(0);
                                String error_name       = errorObj.getString("error");

                                if(error_name.contentEquals("invalid token"))
                                {
                                    new UpdateUserTokenTask(getActivity().getApplicationContext(),null).getNewUserToken();

                                }

                                errorArray = null;
                                errorObj   = null;
                                error_name = null;

                            }else
                            {
                                resetSelectedSubmissions();
                            }


                        } catch (Exception e) {
                            resetSelectedSubmissions();
                            e.printStackTrace();
                        }


                    }

                    Log.d("VisitPendingSubmission", "JSON Created");

                }else if(sendType.contentEquals(DATA_SUBMISSION_TYPE_SELECTED))
                {
                    int sizeOfList = selectedForSubmission.size();
                    //JSONObject dataArray = new JSONObject();
                    Cursor prescription_images = null;
                    JSONArray dataArray = new JSONArray();

                    for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
                    {
                        String selectedOrderID  = selectedForSubmission.get(lIterator).toString();
                        Cursor orderList        = dbHelper.getPrescriptionGroup(selectedOrderID);

                        if(orderList!=null)
                        {
                            orderList.moveToFirst();


                            //JSONObject dataForSubmission = new JSONObject();
                            JSONObject dataForSubmission = new JSONObject();

                            String post_order_no = orderList.getString(orderList.getColumnIndex("group_id"));
                            //String order_taken_by = post_order_no.substring(0, 7);
                            String order_taken_by   = orderList.getString(orderList.getColumnIndex("taken_by"));

                            dataForSubmission.put("group_id", orderList.getString(orderList.getColumnIndex("group_id")));
                            dataForSubmission.put("doctor_code", orderList.getString(orderList.getColumnIndex("doctor_code")));
                            dataForSubmission.put("creation_time", orderList.getLong(orderList.getColumnIndex("creation_time")));
                            dataForSubmission.put("start_lat", orderList.getString(orderList.getColumnIndex("start_lat")));
                            dataForSubmission.put("start_lon", orderList.getString(orderList.getColumnIndex("start_lon")));
                            dataForSubmission.put("submit_lat", latitude);
                            dataForSubmission.put("submit_lon", longitude);
                            //String pres_img_path    = orderList.getString(orderList.getColumnIndex("image_upload_path"));
                            //Log.d("prescription_image", pres_img_path.substring(pres_img_path.lastIndexOf("/")+1,pres_img_path.length()));
                            //dataForSubmission.put("image_file_name", pres_img_path.substring(pres_img_path.lastIndexOf("/") + 1, pres_img_path.length()));
                            //dataForSubmission.put("prescription_image",encodePhoto(orderList.getString(orderList.getColumnIndex("image_upload_path"))));
                            dataForSubmission.put("taken_by",order_taken_by);

                            //dataForSubmission.put("submit_lat", orderList.getDouble(orderList.getColumnIndex("submit_lat")));
                            //dataForSubmission.put("submit_lon", orderList.getDouble(orderList.getColumnIndex("submit_lon")));

                            //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
                            submitted_order_ids.add(post_order_no);

                            Cursor order_entries = dbHelper.getPrescriptionEntry(post_order_no);

                            JSONArray detailsArray = new JSONArray();

                            if (order_entries != null)
                            {
                                order_entries.moveToFirst();

                                while (!order_entries.isAfterLast())
                                {
                                    JSONObject dataEntries = new JSONObject();

                                    //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                    //dataEntries.put("master_group_id", order_entries.getString(order_entries.getColumnIndex("master_group_id")));

                                    if(order_entries.getString(order_entries.getColumnIndex("product_code")).isEmpty())
                                    {

                                        dataEntries.put("product_code", "(UKNP) | "+order_entries.getString(order_entries.getColumnIndex("product_name")));

                                    }else
                                    {
                                        dataEntries.put("product_code", order_entries.getString(order_entries.getColumnIndex("product_code")));

                                    }


                                    detailsArray.put(dataEntries);
                                    dataEntries = null;
                                    order_entries.moveToNext();
                                }
                            }

                            dataForSubmission.put("prescription_details", detailsArray.toString());

                            detailsArray = null;


                            // Create JSON for prescription Images

                            prescription_images  =   dbHelper.getPrescriptionImagesForSubmission(post_order_no);
                            JSONArray img_details_array =   new JSONArray();


                            if (prescription_images != null)
                            {
                                prescription_images.moveToFirst();

                                while (!prescription_images.isAfterLast())
                                {
                                    JSONObject dataEntries = new JSONObject();

                                    //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text
                                    String file_path_in_device =     prescription_images.getString(prescription_images.getColumnIndex("image_name"));
                                    //dataEntries.put("master_group_id", prescription_images.getString(prescription_images.getColumnIndex("group_id")));
                                    dataEntries.put("file_name", file_path_in_device.substring(file_path_in_device.lastIndexOf("/")+1,file_path_in_device.length()-4));

                                    img_details_array.put(dataEntries);

                                    prescription_images.moveToNext();
                                }
                            }

                            dataForSubmission.put("image_list", img_details_array.toString());

                            img_details_array = null;

                            dataArray.put(dataForSubmission);




                            //System.out.println("JSONed Data: "+ dataForSubmission.toString());




                        }


                    }

                    result = submitInfoToUrl(postURL, dataArray.toString());
                    //Answers.getInstance().logCustom(new CustomEvent("Submitted Prescription").putCustomAttribute("Prescription JSON",dataArray.toString()));

                    if (result != null)
                    {

                        JSONArray jArray = null;

                        try
                        {
                            if (result.contentEquals("success"))
                            {

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {
                                    dbHelper.updatePrescriptionStatusToSubmitWithID(submitted_order_ids.get(s_it));
                                    dbHelper.updatePrescriptionSubmitLocation(submitted_order_ids.get(s_it), latitude, longitude);
                                }



                                ASRPrescriptionSubmissionCursorAdapter ap2   = (ASRPrescriptionSubmissionCursorAdapter) lvPreviousEntries.getAdapter();
                                ap2.clearSelectedListOfOrders();
                                selectedForSubmission = null;

                                //removeImageFile(prescription_images);

                            }else if(result.startsWith("[{"))
                            {

                                JSONArray errorArray    = new JSONArray(result);
                                JSONObject errorObj     = errorArray.getJSONObject(0);
                                String error_name       = errorObj.getString("error");

                                if(error_name.contentEquals("invalid token"))
                                {
                                    new UpdateUserTokenTask(getActivity().getApplicationContext(),null).getNewUserToken();

                                }

                                errorArray = null;
                                errorObj   = null;
                                error_name = null;

                            }else
                            {
                                resetSelectedSubmissions();
                            }

                        } catch (Exception e) {

                            resetSelectedSubmissions();

                            e.printStackTrace();
                        }


                    }



                }

            }catch (Exception e)
            {

                resetSelectedSubmissions();
                e.printStackTrace();
                return "Unsuccessfull";

            }

            return result;
        }


        private void removeImageFile(Cursor prescription_images)
        {
            if (prescription_images != null)
            {
                prescription_images.moveToFirst();

                while (!prescription_images.isAfterLast())
                {
                    String path = prescription_images.getString(prescription_images.getColumnIndex("image_name"));

                    File f  = new File(path);
                    f.delete();

                    prescription_images.moveToNext();
                }
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {


            super.onProgressUpdate(values);
            //pd.incrementProgressBy((int)values[0]);

        }

        @Override
        protected void onPostExecute(String s)
        {
            System.out.println("Server Response: "+s);
//            Answers.getInstance().logCustom(new CustomEvent("Visit Submission Response").putCustomAttribute("Server Reply", s));
            pd.dismiss();



            //Cursor productEntries   = dbHelper.getPendingVisits(userId);

            Cursor prescriptionEntries    = null;

            prescriptionEntries           = dbHelper.getPendingPrescriptionList("");

//            if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
//            {
//
//                productEntries           = dbHelper.getPendingVisits("");
//
//            }else
//            {
//                productEntries           = dbHelper.getPendingVisits(userId);
//
//            }

            apcCursorAdapter.swapCursor(prescriptionEntries);
            apcCursorAdapter.notifyDataSetChanged();


            new AsyncPrescriptionImageUploadTask(getActivity().getApplicationContext()).execute();

        }


//        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException {
//
//            //URL url = new URL(serverUrl);
//
//            //barCode                 = "132768165";
//            HttpClient httpClient   = new DefaultHttpClient();
//            HttpContext httpContext = new BasicHttpContext();
//            String postUrl          = networkURL;
//            HttpPost request        = new HttpPost(postUrl);
//
//            HttpResponse response   = null;
//
//            BufferedReader in=null;
//            String data = null;
//
//            try
//            {
//
//                StringEntity se = new StringEntity(dataArray);
//                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//                nameValuePairs.add(new BasicNameValuePair("aps",dataArray));
//                //nameValuePairs.add(new BasicNameValuePair("status","1"));
//
//                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//                //request.setEntity(se);
//                //request.setHeader("Accept", "application/json");
//                //request.setHeader("Content-type","application/json");
//                System.out.println("Post URL "+postUrl);
//                System.out.println("Submit pack: "+dataArray);
//                response = httpClient.execute(request, httpContext);
//                HttpEntity entity = response.getEntity();
//
//                String responseData     = EntityUtils.toString(entity);
////                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
////                StringBuffer sb = new StringBuffer("");
////                String l = "";
////                String nl = System.getProperty("line.separator");
////
////                while((l=in.readLine())!=null)
////                {
////                    sb.append(l+nl);
////
////                }
////
////                in.close();
////                data = sb.toString();
//                return responseData;
//
//            }finally
//            {
//
//
//            }
//
//
//
//
//        }

        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException {

            //dataArray = "Hello Saad !";
            System.out.println("Submit pack: "+dataArray);

            String user_token = sp.getString("user_token","");
            networkURL +="?token="+user_token;

            Log.i("Submit Post URL: ",networkURL);

            URL url                 = new URL(networkURL);
            HttpURLConnection conn  = (HttpURLConnection)url.openConnection();

            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("Content-encoding", "deflate");
            conn.setRequestProperty("Content-type", "application/octet-stream; charset=UTF-8");

            DeflaterOutputStream dos1 = new DeflaterOutputStream(conn.getOutputStream());
            dos1.write(dataArray.getBytes());
            dos1.flush();
            dos1.close();

            Log.e("GZip Visit", dos1.toString());
            Log.e("GZip Response Code", ""+conn.getResponseCode());

            BufferedReader in1 = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));

            String decodedString1   = "";
            String response         = "";

            while ((decodedString1 = in1.readLine()) != null)
            {
                Log.e("dump",decodedString1);
                if(decodedString1!=null) response = new String(decodedString1);
            }

            in1.close();

            return response;
        }
    }

    private String encodePhoto(String image_upload_path)
    {

        Log.d("Prescription img file: ", image_upload_path);
        String result           = "";
        File imgFile            = new  File(image_upload_path);

        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inJustDecodeBounds = true;
        options.inSampleSize       = 2;

        if(imgFile.exists())
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            myBitmap.compress(Bitmap.CompressFormat.PNG,100,baos);
            byte[] imageInBytes = baos.toByteArray();
            result              = Base64.encodeToString(imageInBytes, Base64.DEFAULT);

        }

        return result;


    }
}
