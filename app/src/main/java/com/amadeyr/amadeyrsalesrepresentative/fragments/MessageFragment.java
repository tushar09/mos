package com.amadeyr.amadeyrsalesrepresentative.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCMessageActivity;
import com.amadeyr.amadeyrsalesrepresentative.APSCustomerSelectionActivity;
import com.amadeyr.amadeyrsalesrepresentative.R;
import com.amadeyr.amadeyrsalesrepresentative.adapters.APCMessageCursorAdapter;

/**
 * Created by amadeyr on 10/6/15.
 */



public class MessageFragment extends Fragment
{
    private Context appCtx;
    private ListView lvMessageList;
    private APCMessageCursorAdapter msgAdapter;
    private APCDatabaseHelper msgDBHelper;
    private TextView replyMsgTitle, replyMsgBody, replyMsgTime, replyMsgDate;
    private EditText replyMsgSendMsg;
    private Button btnSend;
    private String fragementType;
    private SharedPreferences sp;

    private String user_id;
    private final String FRAGMENT_TYPE_INBOX    = "INBOX";
    private final String FRAGMENT_TYPE_OUTBOX   = "OUTBOX";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appCtx = getActivity().getApplicationContext();
        sp = appCtx.getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        user_id = sp.getString("username", "");
        msgDBHelper             = APCDatabaseHelper.getInstance(appCtx);
        this.setMessageAdapter();

    }

    // Method to set fragment type to whether it is for Inbox or Outbox

    public void setTypeOfFragment(String fragmentType)
    {
        this.fragementType = fragmentType;

    }

    // Creates the adapter object 
    public void setMessageAdapter()
    {

        //msgDBHelper             = new APCDatabaseHelper(appCtx,"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);


        Cursor msgListCursor    = null;

        if(this.fragementType.contentEquals(FRAGMENT_TYPE_INBOX))
        {
            msgListCursor       = msgDBHelper.getMsgInfoWithEmpCode(user_id);

        }else if(this.fragementType.contentEquals(FRAGMENT_TYPE_OUTBOX))
        {
            msgListCursor       = msgDBHelper.getSentMessagesWithEmpCode(user_id);

        }

        msgAdapter              = new APCMessageCursorAdapter(getActivity().getApplicationContext(),R.layout.message_row_layout,msgListCursor,new String[]{},new int[]{} , CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


    }

    public void reloadMessageData()
    {
        //user_id                 = "MYM-122";
        //msgDBHelper             = new APCDatabaseHelper(appCtx,"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        //msgDBHelper             = APCDatabaseHelper.getInstance(appCtx);

        Cursor msgListCursor    = null;

        if(this.fragementType.contentEquals(FRAGMENT_TYPE_INBOX))
        {
            msgListCursor       = msgDBHelper.getMsgInfoWithEmpCode(user_id);

        }else if(this.fragementType.contentEquals(FRAGMENT_TYPE_OUTBOX))
        {
            msgListCursor       = msgDBHelper.getSentMessagesWithEmpCode(user_id);

        }

        msgAdapter.swapCursor(msgListCursor);
        msgAdapter.notifyDataSetChanged();

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.message_screen,container,false);
        lvMessageList = (ListView) view.findViewById(R.id.lstViewMessages);
        lvMessageList.setAdapter(msgAdapter);

        lvMessageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



                final Dialog messageScreen = new Dialog(getActivity());
                messageScreen.requestWindowFeature(Window.FEATURE_NO_TITLE);
                messageScreen.setContentView(R.layout.message_view_screen);


                replyMsgTitle = (TextView) messageScreen.findViewById(R.id.txtViewMessageTitleView);
                replyMsgBody = (TextView) messageScreen.findViewById(R.id.txtViewMessageBodyView);
                replyMsgDate = (TextView) messageScreen.findViewById(R.id.txtViewMessagePubDateView);
                replyMsgTime = (TextView) messageScreen.findViewById(R.id.txtViewMsgPubTime);
                replyMsgSendMsg = (EditText) messageScreen.findViewById(R.id.edtTextReplyMessageText);
                btnSend = (Button) messageScreen.findViewById(R.id.btnReplytoMessage);


                Cursor tmpCursor = (Cursor) lvMessageList.getAdapter().getItem(position);
                //String entryStatus          = tmpCursor.getString(tmpCursor.getColumnIndex("entry_status"));
                String msg_title = tmpCursor.getString(tmpCursor.getColumnIndex("msg_title"));
                String msg_body = tmpCursor.getString(tmpCursor.getColumnIndex("msg_body"));
                String msg_pub_date = tmpCursor.getString(tmpCursor.getColumnIndex("publish_date"));
                String msg_pub_time = tmpCursor.getString(tmpCursor.getColumnIndex("pub_time"));



                msgDBHelper.messageRead(tmpCursor.getInt(tmpCursor.getColumnIndex("_id")));

                replyMsgTitle.setText(msg_title);
                replyMsgBody.setText(msg_body);
                replyMsgDate.setText(msg_pub_date);
                replyMsgTime.setText(msg_pub_time);

                btnSend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        messageScreen.dismiss();

//                        if (replyMsgSendMsg.getText().toString().isEmpty()) {
//                            replyMsgSendMsg.setError("No message provided !");
//
//                        } else
//                        {
//
//
//                        }


                    }
                });


                if (fragementType.contentEquals(FRAGMENT_TYPE_OUTBOX)) {
                    replyMsgSendMsg.setVisibility(View.GONE);
                    btnSend.setVisibility(View.GONE);
                }

                messageScreen.show();
                setMessageAdapter();
                msgAdapter.notifyDataSetChanged();

//                TextView confirmOrderSubtotal   = (TextView) orderConfirmationScreen.findViewById(R.id.orderSubTotal);
//                TextView confirmOrderTotalItems = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalItems);
//                TextView confirmOrderTax        = (TextView) orderConfirmationScreen.findViewById(R.id.orderTaxAmount);
//                TextView confirmOrderCommission = (TextView) orderConfirmationScreen.findViewById(R.id.orderCommissionAmount);
//                TextView confirmOrderTotal      = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalAmount);
//                Button btnConfirmOrderYes       = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderYes);
//                Button btnConfirmOrderNo        = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderNo);
//
//                confirmOrderDDate.setText(delivery_date);
//                confirmOrderSubtotal.setText("" + total);
//                confirmOrderTotalItems.setText("" + order_item_count);

//                //double tax          = 0.15 * total;
////                    double commission   = 0.05 * total;
//                double finalTotal   = total+total_vat-total_commission;
//
//                confirmOrderTax.setText(""+total_vat);
//                confirmOrderCommission.setText(""+total_commission);
//                confirmOrderTotal.setText(""+finalTotal);
//
//                btnConfirmOrderYes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        apsOrderDBHelper.updateCompleteStatus(visit_id, ORDER_STATUS_COMPLETE);
//                        Intent i = new Intent(OrderScreenActivity.this, APSCustomerSelectionActivity.class);
//                        startActivity(i);
//                        orderConfirmationScreen.dismiss();
//                        finish();
//                    }
//                });
//
//                btnConfirmOrderNo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        orderConfirmationScreen.dismiss();
//                    }
//                });
            }
        });


        return view;

    }
}
