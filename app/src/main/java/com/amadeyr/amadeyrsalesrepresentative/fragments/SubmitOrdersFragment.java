package com.amadeyr.amadeyrsalesrepresentative.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.APCOrderSCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.ASRDualSubmissionActivity;
import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRConfirmationMessageView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.MainMenuActivity;
import com.amadeyr.amadeyrsalesrepresentative.OrderScreenActivity;
import com.amadeyr.amadeyrsalesrepresentative.R;
import com.amadeyr.amadeyrsalesrepresentative.adapters.APCMessageCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRVisitSCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.amadeyr.amadeyrsalesrepresentative.tasks.UpdateUserTokenTask;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by amadeyr on 11/11/15.
 */
public class SubmitOrdersFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener
{
    private Context appCtx;
    
    private ListView lvPreviousEntries;
    private ProductInfo pInfo;
    private Location mCurrentLocation;
    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private APCCustomAutoCompleteView actSearchField;
    private APCOrderSCursorAdapter apcCursorAdapter;
    private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;
    private SharedPreferences sp;
    private String product_name, product_id, product_barcode, user_role;
    private ImageView imgViewClearAll;

    private Button btnSearch, btnSubmit, btnSubmitAll, btnRemove;


    private final int REQUEST_CODE      = 100;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;

    private double latitude, longitude;

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] orderArray;

    private HashMap<String,String> productNameMapper;

    private String screenTitle = "Submissions";
    private String userId;

    private final String DATA_SUBMISSION_TYPE_SELECTED  = "SELECTED";
    private final String DATA_SUBMISSION_TYPE_ALL       = "ALL";
    private final String ENTRY_MODE_EDIT            = "EDIT";
    private final String FRAGMENT_NAME              = "orders";

    private Cursor lastSubmitted;

    public ArrayList <String> selectedForSubmission;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public ProgressDialog pd ;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp                      = getActivity().getSharedPreferences("apcpref",Context.MODE_PRIVATE);

        postURL                 = sp.getString("postURL","")+getResources().getString(R.string.postExtension);

        user_role               = sp.getString("user_role", "");
        userId                  = sp.getString("username","");

        appCtx                  = getActivity().getApplicationContext();
        pd                      = new ProgressDialog(getActivity());



        this.setOrderAdapter();

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        if(mGoogleApiClient!=null) mGoogleApiClient.connect();

    }



    // Creates the adapter object
    public void setOrderAdapter()
    {
        String user_id          = new String(userId);
        //dbHelper                = new APCDatabaseHelper(appCtx,"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper                = APCDatabaseHelper.getInstance(appCtx);

        Cursor orderListCursor    = null;

        if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
        {
            orderListCursor           = dbHelper.getPendingSubmissions("");

        }else
        {

            orderListCursor           = dbHelper.getPendingSubmissions(user_id);
        }




        apcCursorAdapter          = new APCOrderSCursorAdapter(getActivity().getApplicationContext(), R.layout.pending_row_layout,orderListCursor,new String[]{},new int[]{} , CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,getActivity(),PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(appCtx,
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view               = inflater.inflate(R.layout.pending_submissions,container,false);

        if(appCtx==null) appCtx = getActivity().getApplicationContext();


        actSearchField          = (APCCustomAutoCompleteView) view.findViewById(R.id.apsPSSearch);

        actSearchField.addTextChangedListener(new CustomAutoCompleteTextChangedListener(getActivity(),"FRAGMENT",this.FRAGMENT_NAME));
        actSearchField.setThreshold(1);
        actSearchField.setFocusable(true);

        lvPreviousEntries       = (ListView) view.findViewById(R.id.lstViewAPSSubmissions);
        dbHelper                = APCDatabaseHelper.getInstance(appCtx);


        //Cursor orderEntries     = dbHelper.getPendingSubmissions(userId);
        Cursor orderEntries    = null;

        if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
        {
            orderEntries           = dbHelper.getPendingSubmissions("");
        }else
        {
            orderEntries           = dbHelper.getPendingSubmissions(userId);
        }


        apcCursorAdapter        = new APCOrderSCursorAdapter(appCtx,R.layout.pending_row_layout,orderEntries,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvPreviousEntries.setAdapter(apcCursorAdapter);

        btnSearch               = (Button)   view.findViewById(R.id.btnPSSearchShowAll);
        btnSubmit               = (Button)   view.findViewById(R.id.btnSendSelected);
        btnSubmitAll            = (Button)   view.findViewById(R.id.btnPSSubmitAll);
        btnRemove               = (Button)   view.findViewById(R.id.btnPSDeleteAll);
        imgViewClearAll         = (ImageView) view.findViewById(R.id.imgViewPSSearchClearButton);


        actSearchField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                String searchKey = actSearchField.getText().toString();
                Cursor searchCursor = dbHelper.getEntryForName(searchKey);
                apcCursorAdapter.swapCursor(searchCursor);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });


        actSearchField.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view)
            {
                actSearchField.setText("");
                return true;
            }
        });


        lvPreviousEntries.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(appCtx, "Item clicked!", Toast.LENGTH_SHORT).show();

                Cursor itemInfo = (Cursor) lvPreviousEntries.getAdapter().getItem(position);

                if (itemInfo != null) {


                    Intent editScreenStarter = new Intent(appCtx, OrderScreenActivity.class);

                    editScreenStarter.putExtra("mode", ENTRY_MODE_EDIT);
                    editScreenStarter.putExtra("order_no", itemInfo.getString(itemInfo.getColumnIndex("order_no")));
                    editScreenStarter.putExtra("customer_id", itemInfo.getString(itemInfo.getColumnIndex("customer_id")));
                    editScreenStarter.putExtra("trade_name", itemInfo.getString(itemInfo.getColumnIndex("customer_name")));
                    editScreenStarter.putExtra("employee_id", itemInfo.getString(itemInfo.getColumnIndex("taken_by")));

                    startActivity(editScreenStarter);


                }

                return true;
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                APCOrderSCursorAdapter ap   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
                selectedForSubmission       = ap.getSelectedListOfOrders();

                //Toast.makeText(getApplicationContext(),selectedForSubmission.size()+"",Toast.LENGTH_SHORT).show();


                try {

                    postDataToServer(DATA_SUBMISSION_TYPE_SELECTED);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        btnSubmitAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try
                {
                    postDataToServer(DATA_SUBMISSION_TYPE_ALL);
                    Cursor newCursor    = null;

                    if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
                    {
                        newCursor           = dbHelper.getPendingSubmissions("");

                    }else
                    {

                        newCursor           = dbHelper.getPendingSubmissions(userId);
                    }

                    actSearchField.setText("");
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                final Dialog removalConfirmationScreen = new Dialog(getActivity());
                removalConfirmationScreen.setContentView(R.layout.confirm_order_removal);


                TextView confirmOrderDDate = (TextView) removalConfirmationScreen.findViewById(R.id.txtViewOrderRemovalMessage);

                Button btnConfirmRemovalYes = (Button) removalConfirmationScreen.findViewById(R.id.btnRemoveOrder);
                Button btnConfirmRemovalNo = (Button) removalConfirmationScreen.findViewById(R.id.btnCancelRemoval);

                selectedForSubmission   = apcCursorAdapter.getSelectedListOfOrders();

                confirmOrderDDate.append(selectedForSubmission.size() + " items ?");

                btnConfirmRemovalYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        removeFromSubmissionList();
                        removalConfirmationScreen.dismiss();
                        selectedForSubmission.clear();


                    }
                });

                btnConfirmRemovalNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removalConfirmationScreen.dismiss();
                    }
                });


                removalConfirmationScreen.setTitle("Remove Order");
                removalConfirmationScreen.show();





            }
        });

        imgViewClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actSearchField.setText("");
                //Cursor allEntries = dbHelper.getPendingSubmissions(userId);

                Cursor allEntries    = null;

                if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
                {
                    allEntries           = dbHelper.getPendingSubmissions("");

                }else
                {

                    allEntries           = dbHelper.getPendingSubmissions(userId);
                }

                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Cursor allEntries = dbHelper.getPendingSubmissions(userId);

                Cursor allEntries    = null;

                if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
                {
                    allEntries           = dbHelper.getPendingSubmissions("");

                }else
                {

                    allEntries           = dbHelper.getPendingSubmissions(userId);
                }


                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();


            }


        });





        return view;

    }

    public void removeFromSubmissionList()
    {

        int sizeOfList = selectedForSubmission.size();
        //JSONObject dataArray = new JSONObject();


        for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
        {
            String selectedOrderID = selectedForSubmission.get(lIterator).toString();
            dbHelper.removeOrder(selectedOrderID);

        }

        selectedForSubmission.clear();

        //Cursor updatedList = dbHelper.getPendingSubmissions(userId);
        Cursor updatedList    = null;

        if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
        {
            updatedList           = dbHelper.getPendingSubmissions("");

        }else
        {

            updatedList           = dbHelper.getPendingSubmissions(userId);
        }


        apcCursorAdapter.swapCursor(updatedList);
        apcCursorAdapter.notifyDataSetChanged();
        actSearchField.setText("");

    }

    public void callStringSearchMethod()
    {
        String searchString = actSearchField.getText().toString();
        this.populateSearchAdapter(searchString);
    }


    public void resetSelectedSubmissions()
    {

        APCOrderSCursorAdapter ap2   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
        ap2.clearSelectedListOfOrders();
    }

    public void populateSearchAdapter(String searchItem)
    {
        List<OrderInfo> oInfo   = dbHelper.getAutoCompleteOrderInfoForPending(searchItem);
        orderArray              = new String[oInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < oInfo.size();++index)
        {
            orderArray[index]        = new String(oInfo.get(index).getCustomerName());

        }


        ArrayAdapter adapter         = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, orderArray);

        actSearchField.setAdapter(adapter);
        actSearchField.setThreshold(1);



    }


    public void populateAPCAutocompleteArray(String searchTerm)
    {
        Cursor currentEntries = null;

        if(searchTerm.equals(""))
        {
            currentEntries = dbHelper.getDistinctEntries();

        }else
        {

            currentEntries  = dbHelper.getOrders(searchTerm);
        }



        if(currentEntries!=null)
        {
            currentEntries.moveToFirst();

            String[] barcodeArray = new String[currentEntries.getCount()];


            int string_arr_index = 0;

            for (int json_arr_index = 0; json_arr_index < currentEntries.getCount();++json_arr_index)
            {
                barcodeArray[json_arr_index]        = currentEntries.getString(currentEntries.getColumnIndex("trade_name"));
                currentEntries.moveToNext();
                //System.out.println("SearchActivity: "+jObj.getString("description"));
            }

            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, barcodeArray);
            actSearchField.setAdapter(adapter);


        }

    }

    public void postDataToServer(String sendType) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {




            new AccessAPCServerTask().execute(sendType);

        }else
        {
            Toast.makeText(appCtx,"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }



    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
            new AccessAPCServerTask().execute("","");

        }else
        {
            Toast.makeText(appCtx,"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }


    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks((ASRDualSubmissionActivity)getActivity())
                .addOnConnectionFailedListener((ASRDualSubmissionActivity)getActivity())
                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {
        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {

            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            latitude  = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

        }

    }


    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        }



    }

    @Override
    public void onLocationChanged(Location location) {

        if(location!=null)
        {

            mCurrentLocation = location;
            recordLocation();


        }

    }



    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {

        //private ProgressDialog pd;
        private boolean getAll = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            recordLocation();
            stopLocationUpdates();
            pd.setTitle("Please Wait");
            pd.setMessage("Submitting data to server  ...");
            pd.setCancelable(false);
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls)
        {
            String result="";
            String sendType = urls[0];
            recordLocation();

            List<String> submitted_order_ids = new ArrayList<>();

            try
            {

                if(sendType.contentEquals(DATA_SUBMISSION_TYPE_ALL))
                {

                    Cursor currentPending   = apcCursorAdapter.getCursor();
                    lastSubmitted           = currentPending;

                    if (currentPending != null) currentPending.moveToFirst();


                    JSONArray dataArray             = new JSONArray();


//
                    while (!currentPending.isAfterLast())
                    {

                        JSONObject dataForSubmission    = new JSONObject();

                        //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
                        //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
                        //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();
                        String post_order_no = currentPending.getString(currentPending.getColumnIndex("order_no"));


                        //String order_taken_by = post_order_no.substring(0,7);

                        String order_taken_by = currentPending.getString(currentPending.getColumnIndex("taken_by"));


                        dataForSubmission.put("order_no", currentPending.getString(currentPending.getColumnIndex("order_no")));
                        dataForSubmission.put("customer_id", currentPending.getString(currentPending.getColumnIndex("customer_id")));
                        dataForSubmission.put("order_date", currentPending.getString(currentPending.getColumnIndex("order_date")));
                        dataForSubmission.put("delivery_date", currentPending.getString(currentPending.getColumnIndex("delivery_date")));
                        dataForSubmission.put("collection_date", currentPending.getString(currentPending.getColumnIndex("collection_date")));
                        dataForSubmission.put("order_time", currentPending.getString(currentPending.getColumnIndex("order_time")));
                        dataForSubmission.put("total_tp", currentPending.getString(currentPending.getColumnIndex("total")));
                        dataForSubmission.put("order_location_lat", currentPending.getString(currentPending.getColumnIndex("start_lat")));
                        dataForSubmission.put("order_location_lon", currentPending.getString(currentPending.getColumnIndex("start_lon")));
                        dataForSubmission.put("submitted_location_lat", latitude);
                        dataForSubmission.put("submitted_location_lon", longitude);

                        dataForSubmission.put("remarks", currentPending.getString(currentPending.getColumnIndex("remarks")));
                        dataForSubmission.put("invoice_type", currentPending.getString(currentPending.getColumnIndex("invoice_type")));
                        dataForSubmission.put("taken_by",order_taken_by);
                        submitted_order_ids.add(post_order_no);

                        //dataForSubmission.put("submit_lat", currentPending.getString(currentPending.getColumnIndex("submit_lat")));
                        //dataForSubmission.put("submit_lon", currentPending.getString(currentPending.getColumnIndex("submit_lon")));


                        //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));


                        Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);

                        JSONArray detailsArray = new JSONArray();

                        if (order_entries != null) {
                            order_entries.moveToFirst();

                            while (!order_entries.isAfterLast())
                            {
                                JSONObject dataEntries = new JSONObject();

                                //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
                                //dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
                                //dataEntries.put("trade_price", order_entries.getDouble(order_entries.getColumnIndex("price")));
                                dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
                                dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
                                //dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
                                dataEntries.put("remarks", order_entries.getString(order_entries.getColumnIndex("comments")));

                                detailsArray.put(dataEntries);

                                dataEntries = null;
                                order_entries.moveToNext();
                            }
                        }

                        dataForSubmission.put("order_details", detailsArray.toString());

                        dataArray.put(dataForSubmission);


                        detailsArray = null;

                        currentPending.moveToNext();
                    }

                    Answers.getInstance().logCustom(new CustomEvent("Submitted Order").putCustomAttribute("Order JSON",dataArray.toString()));
                    result = submitInfoToUrl(postURL, dataArray.toString());

                    if (result != null) {

                        JSONArray jArray = null;

                        try {
                            if (result.contentEquals("success")) {
                                //if(lastSubmitted!=null) lastSubmitted.moveToFirst();

                                //while(!lastSubmitted.isAfterLast())
                                //{
                                //    dbHelper.updateStatusToSubmitWithEntryID(lastSubmitted.getString(lastSubmitted.getColumnIndex("order_no")));
                                //    lastSubmitted.moveToNext();


                                //}

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {

                                    dbHelper.updateStatusToSubmitWithEntryID(submitted_order_ids.get(s_it));
                                    dbHelper.updateSubmitLocation(submitted_order_ids.get(s_it),latitude,longitude);
                                }



                                selectedForSubmission = null;


                                //dbHelper.updateStatusToSubmitWithEntryID(result);
                                //dbHelper.updateSubmitLocation(result,latitude,longitude);
                                //Cursor productEntries   = dbHelper.getPendingSubmissions();

                                //apcCursorAdapter.swapCursor(productEntries);
                                //apcCursorAdapter.notifyDataSetChanged();


                            }else if(result.startsWith("[{"))
                            {

                                JSONArray errorArray    = new JSONArray(result);
                                JSONObject errorObj     = errorArray.getJSONObject(0);
                                String error_name       = errorObj.getString("error");

                                if(error_name.contentEquals("invalid token"))
                                {
                                    new UpdateUserTokenTask(getActivity().getApplicationContext(),null).getNewUserToken();

                                }

                                errorArray = null;
                                errorObj   = null;
                                error_name = null;

                            }

                            resetSelectedSubmissions();

                        } catch (Exception e)
                        {

                            resetSelectedSubmissions();

                            e.printStackTrace();
                            return "Unsuccessfull Status Update";
                        }


                    }

                    Log.d("APCPendingSubmission", "JSON Created");

                }else if(sendType.contentEquals(DATA_SUBMISSION_TYPE_SELECTED))
                {
                    int sizeOfList = selectedForSubmission.size();
                    //JSONObject dataArray = new JSONObject();

                    JSONArray dataArray = new JSONArray();

                    for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
                    {
                        String selectedOrderID  = selectedForSubmission.get(lIterator).toString();
                        Cursor orderList        = dbHelper.getOrderByID(selectedOrderID);

                        if(orderList!=null)
                        {
                            orderList.moveToFirst();


                            //JSONObject dataForSubmission = new JSONObject();
                            JSONObject dataForSubmission = new JSONObject();

                            String post_order_no = orderList.getString(orderList.getColumnIndex("order_no"));
                            //
                            // String order_taken_by = post_order_no.substring(0, 7);

                            String order_taken_by    =  orderList.getString(orderList.getColumnIndex("taken_by"));

                            dataForSubmission.put("order_no", orderList.getString(orderList.getColumnIndex("order_no")));
                            dataForSubmission.put("customer_id", orderList.getString(orderList.getColumnIndex("customer_id")));
                            dataForSubmission.put("order_date", orderList.getString(orderList.getColumnIndex("order_date")));
                            dataForSubmission.put("delivery_date", orderList.getString(orderList.getColumnIndex("delivery_date")));
                            dataForSubmission.put("collection_date", orderList.getString(orderList.getColumnIndex("collection_date")));
                            dataForSubmission.put("order_time", orderList.getString(orderList.getColumnIndex("order_time")));
                            dataForSubmission.put("total_tp", orderList.getString(orderList.getColumnIndex("total")));
                            dataForSubmission.put("order_location_lat", orderList.getDouble(orderList.getColumnIndex("start_lat")));
                            dataForSubmission.put("order_location_lon", orderList.getDouble(orderList.getColumnIndex("start_lon")));
                            dataForSubmission.put("remarks", orderList.getString(orderList.getColumnIndex("remarks")));
                            dataForSubmission.put("invoice_type", orderList.getString(orderList.getColumnIndex("invoice_type")));
                            dataForSubmission.put("submitted_location_lat", latitude);
                            dataForSubmission.put("submitted_location_lon", longitude);
                            dataForSubmission.put("taken_by",order_taken_by);

                            //dataForSubmission.put("submit_lat", orderList.getDouble(orderList.getColumnIndex("submit_lat")));
                            //dataForSubmission.put("submit_lon", orderList.getDouble(orderList.getColumnIndex("submit_lon")));

                            //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
                            submitted_order_ids.add(post_order_no);

                            Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);

                            JSONArray detailsArray = new JSONArray();

                            if (order_entries != null)
                            {
                                order_entries.moveToFirst();

                                while (!order_entries.isAfterLast())
                                {
                                    JSONObject dataEntries = new JSONObject();

                                    //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                    dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
                                    //dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
                                    //dataEntries.put("price", order_entries.getDouble(order_entries.getColumnIndex("price")));
                                    dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
                                    dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
                                    //dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
                                    dataEntries.put("remarks", order_entries.getString(order_entries.getColumnIndex("comments")));

                                    detailsArray.put(dataEntries);

                                    dataEntries = null;
                                    order_entries.moveToNext();
                                }
                            }

                            dataForSubmission.put("order_details", detailsArray.toString());
                            dataArray.put(dataForSubmission);

                            //System.out.println("JSONed Data: "+ dataForSubmission.toString());


                            detailsArray = null;

                        }


                    }

                    Answers.getInstance().logCustom(new CustomEvent("Submitted Order").putCustomAttribute("Order JSON",dataArray.toString()));
                    result = submitInfoToUrl(postURL, dataArray.toString());
                    Answers.getInstance().logCustom(new CustomEvent("Result from server").putCustomAttribute("Result Response",result));

                    if (result != null)
                    {

                        JSONArray jArray = null;

                        try
                        {
                            if (result.contentEquals("success"))
                            {

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {
                                    dbHelper.updateStatusToSubmitWithEntryID(submitted_order_ids.get(s_it));
                                    dbHelper.updateSubmitLocation(submitted_order_ids.get(s_it),latitude,longitude);
                                }



                                APCOrderSCursorAdapter ap2   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
                                ap2.clearSelectedListOfOrders();
                                if(!selectedForSubmission.isEmpty()) new ASRConfirmationMessageView(getActivity()).showConfirmationMessage("Success", "Data was submitted to the server successfully"); selectedForSubmission = null;

                            }else if(result.startsWith("[{"))
                            {

                                JSONArray errorArray    = new JSONArray(result);
                                JSONObject errorObj     = errorArray.getJSONObject(0);
                                String error_name       = errorObj.getString("error");

                                if(error_name.contentEquals("invalid token"))
                                {
                                    new UpdateUserTokenTask(getActivity().getApplicationContext(),null).getNewUserToken();

                                }

                                errorArray = null;
                                errorObj   = null;
                                error_name = null;

                            }

                            resetSelectedSubmissions();

                        } catch (Exception e)
                        {
                            resetSelectedSubmissions();

                            e.printStackTrace();
                            return "Unsuccessfull Status Update";
                        }


                    }



                }

            }catch (Exception e)
            {

                resetSelectedSubmissions();
                e.printStackTrace();
                return "Unsuccessfull JSON Creation";

            }

            return result;
        }

        @Override
        protected void onProgressUpdate(Void... values) {



            super.onProgressUpdate(values);
            //pd.incrementProgressBy((int)values[0]);

        }

        @Override
        protected void onPostExecute(String s)
        {
            System.out.println("Server Response: " + s);
            pd.dismiss();


            Answers.getInstance().logCustom(new CustomEvent("Order Submission Response").putCustomAttribute("Server reply",s));

            if(!s.contentEquals("success"))
            {
                resetSelectedSubmissions();
                ASRErrorMessageView err = new ASRErrorMessageView(getActivity());
                err.showErrorMessage(getResources().getString(R.string.error401Title),getResources().getString(R.string.error401Message));
                return;
            }



            //Cursor productEntries   = dbHelper.getPendingSubmissions(userId);
            Cursor productEntries    = null;

            if(user_role.contentEquals(MainMenuActivity.TEAM_LEAD_ROLE))
            {
                productEntries           = dbHelper.getPendingSubmissions("");

            }else
            {

                productEntries           = dbHelper.getPendingSubmissions(userId);
            }


            apcCursorAdapter.swapCursor(productEntries);
            apcCursorAdapter.notifyDataSetChanged();





        }


        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException {

            //dataArray = "Hello Saad !";
            System.out.println("Submit pack: "+dataArray);

            String user_token = sp.getString("user_token","");

            //user_token = "sadksajdwkq-e27312783sdisadasjdbj1h27183127dhsadjasjdsjd";
            networkURL +="?token="+user_token;

            Log.i("Order Post URL: ",networkURL);

            URL url             = new URL(networkURL);
            HttpURLConnection conn  = (HttpURLConnection)url.openConnection();

            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("Content-encoding", "deflate");
            conn.setRequestProperty("Content-type", "application/octet-stream");
            DeflaterOutputStream dos1 = new DeflaterOutputStream(conn.getOutputStream());
            dos1.write(dataArray.getBytes());
            dos1.flush();
            dos1.close();

            Log.e("GZip", dos1.toString());
            Log.e("GZip Response Code", ""+conn.getResponseCode());

            BufferedReader in1 = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));

            String decodedString1 = "";
            String response = "";
            while ((decodedString1 = in1.readLine()) != null) {
                Log.e("dump",decodedString1);
                if(decodedString1!=null) response = new String(decodedString1);
            }
            in1.close();

            return response;
        }



    }




}
