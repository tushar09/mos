package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.tasks.UpdateUserTokenTask;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.apache.http.HttpException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;


public class LogInActivity extends ActionBarActivity {

    private EditText edtTextEmployeeID;
    private EditText edtTextPassword;
    private Button btnLogIn;
    private SharedPreferences sharedPreferences;
    private APCDatabaseHelper dbHelper;
    private SharedPreferences sp;
    private String requestURL, user_token;
    private String serverResponse;
    private final int CONNECTION_TIMEOUT_MILLISEC = 5000;
    private final String LOG_TAG = "LogInActivity";
    private String username, password;
    private boolean login_status;
    private ASRErrorMessageView am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        Fabric.with(this, new Answers());

        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_menu);



        am                      = new ASRErrorMessageView(LogInActivity.this);
        login_status            = false;
        sp                      = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        //sp.edit().putString("postURL","http://202.51.189.20/merged_sales/public").commit();
        //sp.edit().putString("postURL","http://192.168.1.101:8000").commit();
        sp.edit().putString("postURL","http://202.51.189.20/sales_force/public").commit();
        //sp.edit().putString("postURL","http://202.51.189.19/sales_force/public").commit();

        requestURL              = sp.getString("postURL","")+ASRConfig.LOG_IN_EXTENSION;



        if(tokenExists())
        {
            if(checkNetConnection())
            {

                username    = sp.getString("username","");
                password    = sp.getString("password","");

                if(new UpdateUserTokenTask(getApplicationContext(),null).didTokenDurationExpire())
                {
                    new AccessAPCServerTask().execute(requestURL);

                }else
                {
                    callNextActivity("");

                }


                //new UpdateUserTokenTask(getApplicationContext(),null).getNewUserToken();

            }else
            {
                callNextActivity("");

            }


        }

        edtTextEmployeeID   = (EditText) findViewById(R.id.edtTextHHSBarcode);
        edtTextPassword     = (EditText) findViewById(R.id.edtTextLogInPassword);

        btnLogIn            = (Button) findViewById(R.id.btnLogIn);


        //dbHelper            = new APCDatabaseHelper(getApplicationContext(),"apcDB", null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper            = APCDatabaseHelper.getInstance(getApplicationContext());
        dbHelper.getWritableDatabase();

//        if(!sp.getString("user_token","").isEmpty())
//        {
//            login_status    = true;
//            username        = sp.getString("user_name","");
//            password        = sp.getString("user_password","");
//            user_token      = sp.getString("user_token","");
//
//        }
        //downloadProductDB("http://192.168.1.117/html/downloads/amadeyrProductDB.sqlite");

        //APCNetworkHandler apn


        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                username    = edtTextEmployeeID.getText().toString().toUpperCase();
                password    = edtTextPassword.getText().toString();
                new AccessAPCServerTask().execute(requestURL);

            }
        });



//        if(login_status)
//        {
//            edtTextEmployeeID.setText(username);
//            edtTextPassword.setText(password);
//            ProgressDialog pd = new ProgressDialog(LogInActivity.this);
//            pd.setMessage("");
//
//
//        }



    }

    public void closeScreen()
    {

        this.finish();
    }

    private boolean tokenExists()
    {
        if(sp.getString("user_token","").isEmpty())
        {
            return false;

        }

        return true;
    }

    private void callNextActivity(String newHash)
    {
        //new APCDatabaseHelper(getApplicationContext(),APCDatabaseHelper.DATABASE_NAME,null,APCDatabaseHelper.DATABASE_VERSION).fillPrescriptionWithDummyData();
        //new APCDatabaseHelper(getApplicationContext(),APCDatabaseHelper.DATABASE_NAME,null,APCDatabaseHelper.DATABASE_VERSION).fillPrescriptionImagesWithDummyData();

        Intent i  = new Intent (LogInActivity.this,DeviceStatusActivity.class);
        i.putExtra("source_activity","LogIn");
        i.putExtra("newHash",newHash);

//        Intent i  = new Intent (LogInActivity.this,MainMenuActivity.class);
//        i.putExtra("source_activity","LogIn");
//        i.putExtra("newHash",newHash);
        startActivity(i);
        finish();

    }

    public boolean checkNetConnection()
    {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
            return true;

        }


        return false;

    }

    public void downloadProductDB(String url)
    {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
            new AccessAPCServerTask().execute(url);

        }else
        {
            Log.d("Network Error", "Connection Unavailable");

        }

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {

            Intent i = new Intent(LogInActivity.this,APCPreferencesActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        requestURL              = sp.getString("postURL","")+ASRConfig.LOG_IN_EXTENSION;
    }

    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {
        private ProgressDialog pd;
        private final String LOG_TAG = "AccessAPCServerTask";

        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = new ProgressDialog(LogInActivity.this);
            pd.setTitle("Please Wait");
            pd.setCancelable(false);
            pd.setMessage("Authenticating from server.");
            pd.show();

        }
        @Override
        protected String doInBackground(String... urls) {

            try
            {
                String result  = getInfoWithUrl(urls[0]);
                return result;



            }catch (IOException e)
            {

                return "Unsuccessfull";
            }

        }

        @Override
        protected void onPostExecute(String s)
        {
            pd.dismiss();

            Log.d(this.LOG_TAG, "LogIn Response: " + s);


            Answers.getInstance().logCustom(new CustomEvent("LogIn Response").putCustomAttribute("Server reply", s));

            SharedPreferences.Editor editor = sp.edit();


            //editor.commit();

            try {
                //s = "{\"token\":\"e8710013980281321\"}";

                if(!s.isEmpty())
                {
                    JSONObject jResponse = new JSONObject(s);

                    if(jResponse.has("token"))
                    {
                        editor.putString("user_token", jResponse.getString("token"));
                        editor.putString("last_log_in_date", new APCDateHelper().getCurrentDateStr());
                        editor.putString("username", username);
                        editor.putString("password", password);
                        editor.commit();
                        Log.e("Last Log In Date : " , sp.getString("last_log_in_date", ""));
                        callNextActivity(s);



                    }else if(jResponse.has("error"))
                    {
                        String errorStr = jResponse.getString("error");
                        Log.e("Log In Error: ", errorStr);

                        am.showErrorMessage("Log In Error", errorStr);
                        //Toast.makeText(getApplicationContext(),errorStr,Toast.LENGTH_LONG).show();

                    }

                }else
                {

                    am.showErrorMessage("Log-in Error","Incorrect password or username. Please retry");

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }



            Log.e("Server Response: ", s);

//            if(!s.contentEquals("Invalid"))
//            {
//
//                editor.putString("username",username);
//                editor.putString("password",password);
//                editor.commit();
//
//                Intent i  = new Intent (LogInActivity.this,MainMenuActivity.class);
//                i.putExtra("source_activity","LogIn");
//                i.putExtra("newHash",s);
//                startActivity(i);
//                closeScreen();
//
//
//
//            }else
//            {
//
//                Toast.makeText(getApplicationContext(),"Invalid Username and Password!",Toast.LENGTH_LONG).show();
//            }

            //pd.dismiss();


            //productInfo = new String(s);
        }

        private String getFormattedPostDataString(HashMap<String,String> postParameters)
        {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for(Map.Entry<String,String> entry: postParameters.entrySet())
            {
                if(first)
                    first = false;
                else
                    result.append("&");

                try {
                    result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    result.append("=");
                    result.append(URLEncoder.encode(entry.getValue(),"UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }

            return result.toString();

            //while(postParameters)


        }

        private String getInfoWithUrl(String serverUrl) throws IOException
        {
            String dbHashCode = "87421hjfksahaskncajhqkhdas34";

            HashMap<String,String> postParams = new HashMap<>();


            if(sp.getString("username","").isEmpty() && sp.getString("password","").isEmpty() )
            {
                username    = edtTextEmployeeID.getText().toString().toUpperCase();
                password    = edtTextPassword.getText().toString();

            }



            postParams.put("email", username);
            postParams.put("password",password);
            //postParams.put("dbHashCode",dbHashCode);

            URL url = null;
            String response="";

            serverUrl = new String(requestURL);
            try {


                url = new URL(serverUrl);

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setConnectTimeout(CONNECTION_TIMEOUT_MILLISEC);
                //conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                writer.write(getFormattedPostDataString(postParams));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK)
                {
                    String dataLine;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((dataLine=br.readLine())!=null)
                    {
                        response += dataLine;
                    }

                }else
                {
                    response = "";
                    throw new HttpException(responseCode+"");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return response;

//            FileOutputStream output = null;
//            FileInputStream fis = null;
//            InputStream input = null;
//            try {
//                URL url                     = new URL(serverUrl);
//                URLConnection connection    = url.openConnection();
//
//                input                       = connection.getInputStream();
//
//                Log.e("APCNetworkHandler 1");
//
//                File downloadedDBFile = new File("/data/data/" + getPackageName() + "/databases","amadeyrProductDB");
//
//                downloadedDBFile.createNewFile();
//
//                output  = new FileOutputStream(downloadedDBFile);
//
//                int read;
//                byte[] data = new byte[1024];
//
//                while ((read = input.read(data)) != -1)
//                {
//                    Log.e("datavar: "+read);
//
//                    output.write(data, 0, read);
//
//                }
//
//
//            }catch(Exception e){
//                e.printStackTrace();
//
//
//            } finally {
//
//                if (fis != null) fis.close();
//                if (input != null) input.close();
//
//
//            }
//
//            return "Done";
        }
    }

}
