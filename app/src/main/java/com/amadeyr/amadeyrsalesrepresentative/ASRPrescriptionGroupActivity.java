package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRPrescriptionGroupListCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.CustomerInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by amadeyr on 1/13/16.
 */
public class ASRPrescriptionGroupActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private ListView lvGroupList;
    private APCDatabaseHelper orderDBHelper;
    private APCDateHelper apcDateHelper;
    private ASRPrescriptionGroupListCursorAdapter apsCursorAdapter;
    private ASRErrorMessageView errorMsgObj;
    private Uri mCapturedImageURI;
    //private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;
    //private ImageView imgViewCustomerSelCross;
    private String product_name, product_id, product_barcode, employee_id, source_activity, group_id;
    private final String SCREEN_TITLE = "Prescription List";
    private int image_count;
    private Button btnAdd;
    private double start_latitude, start_longitude;

    private final int REQUEST_IMAGE_CAPTURE = 100;
    private final int APC_EDIT_CODE = 1;
    private final int APC_REMOVE_CODE = 2;

    private String getURL = "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL = "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] customerArray;

    private HashMap<String, String> productNameMapper;

    private Cursor lastSubmitted;


    // Initializing variables for location services


    private Location mLastLocation, mCurrentLocation;
    ;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prescription_layout_main);

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        SharedPreferences sp = getSharedPreferences("apcpref", Context.MODE_PRIVATE);

        image_count = 0;
        employee_id = getIntent().getStringExtra("employee_id");
        //employee_id             = sp.getString("username","");
        getURL = sp.getString("getURL", "") + "/autodata/description";
        postURL = sp.getString("postURL", "") + "/index.php/pos_request/datapush";
        source_activity = getIntent().getStringExtra("source_activity");
        apcDateHelper = new APCDateHelper();


        lvGroupList = (ListView) findViewById(R.id.lstViewPrescriptionList);
        btnAdd = (Button) findViewById(R.id.btnPRSAdd);
        orderDBHelper = APCDatabaseHelper.getInstance(getApplicationContext());
        errorMsgObj = new ASRErrorMessageView(ASRPrescriptionGroupActivity.this);

        //List<String> incompleteIDS  = orderDBHelper.getCustomersWithIncompleteOrders(employee_id);

        Cursor prescriptionEntries = null;

        try {
            prescriptionEntries = orderDBHelper.getIncompletePrescriptions();

        } catch (SQLiteException e) {
            e.printStackTrace();
            errorMsgObj.showErrorMessage("Error 101", "No database present ! Please select sync option from menu to download database");


        }


        apsCursorAdapter = new ASRPrescriptionGroupListCursorAdapter(getApplicationContext(), R.layout.prescription_group_layout, prescriptionEntries, new String[]{}, new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lvGroupList.setAdapter(apsCursorAdapter);


        lvGroupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor selectedCustomer = (Cursor) lvGroupList.getAdapter().getItem(position);
                String group_id_str = "";

                if (selectedCustomer != null) {
                    //selectedCustomer.moveToFirst();
                    group_id_str = selectedCustomer.getString(selectedCustomer.getColumnIndex("group_id"));

                }

                Log.d("Prescription group id: ", group_id_str);

                Intent imageSelectionScreen = new Intent(ASRPrescriptionGroupActivity.this, ASRPrescriptionImageBrowser.class);
                imageSelectionScreen.putExtra("group_id", group_id_str);
                startActivity(imageSelectionScreen);


            }
        });


        lvGroupList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                final int pos = position;
                final ASRErrorMessageView am = new ASRErrorMessageView(ASRPrescriptionGroupActivity.this);
                final ProgressDialog pd = new ProgressDialog(getApplicationContext());

                pd.setTitle("Removing prescription");
                pd.setMessage("Deleting image files ...");

                am.addExtraAction("Yes");
                am.getActionButton2Instance().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Cursor c = (Cursor) lvGroupList.getAdapter().getItem(pos);
                        String delete_group_id = c.getString(c.getColumnIndex("group_id"));
                        Cursor image_cursor = orderDBHelper.getImageSetForPrescription(delete_group_id);


                        if (image_cursor != null) {
                            image_cursor.moveToFirst();

                            while (!image_cursor.isAfterLast()) {
                                Log.d("To be deleted: ", image_cursor.getString(image_cursor.getColumnIndex("image_name")));
                                File imgFile = new File(image_cursor.getString(image_cursor.getColumnIndex("image_name")));
                                Log.d("To be deleted:", "" + imgFile.delete());
                                getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(image_cursor.getString(image_cursor.getColumnIndex("image_name"))))));
                                image_cursor.moveToNext();
                            }


                        }


                        orderDBHelper.removePrescription(delete_group_id);


                        apsCursorAdapter.swapCursor(orderDBHelper.getIncompletePrescriptions());
                        apsCursorAdapter.notifyDataSetChanged();
                        am.closeScreen();

                    }
                });

                am.showErrorMessage("Remove Prescription", "Do you want to remove this Prescription from your list?");


                return true;
            }
        });


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mGoogleApiClient != null) {
                    mGoogleApiClient.connect();
                }

                takePhoto();
            }
        });

        if (checkPlayServices()) {
            buildGoogleApiClient();
        }


    }


    public void takePhoto() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        int g_id                 = orderDBHelper.getLastGroupID();
//        group_id                 = ""+g_id;

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            String fileName = System.currentTimeMillis() + ".jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            mCapturedImageURI = getContentResolver()
                    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            values);
            takePictureIntent
                    .putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {

                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void recordLocation() {

        Location tLocation = null;

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mCurrentLocation == null) {

            tLocation = mLastLocation;

        } else {


            tLocation = mCurrentLocation;

        }


        if (tLocation != null) {
            start_latitude = mLastLocation.getLatitude();
            start_longitude = mLastLocation.getLongitude();

        }


    }

    protected synchronized void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    public void stopLocationUpdates() {

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        }


    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {

        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

            mCurrentLocation = location;
            recordLocation();


        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = connMgr.getActiveNetworkInfo();

        if (nInfo != null && nInfo.isConnected()) {
            if (currentPending != null) currentPending.moveToFirst();


            JSONArray dataArray = new JSONArray();


            while (!currentPending.isAfterLast()) {
                JSONObject dataForSubmission = new JSONObject();

                //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
                //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
                //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();

                dataForSubmission.put("customer_id", currentPending.getString(currentPending.getColumnIndex("customer_id")).trim());
                dataForSubmission.put("trade_name", currentPending.getString(currentPending.getColumnIndex("trade_name")));
                dataForSubmission.put("product_barcode", currentPending.getString(currentPending.getColumnIndex("product_barcode")));
                dataForSubmission.put("product_tag", currentPending.getString(currentPending.getColumnIndex("product_tag")));
                dataForSubmission.put("product_count", currentPending.getDouble(currentPending.getColumnIndex("product_count")));
                dataForSubmission.put("entry_date", currentPending.getString(currentPending.getColumnIndex("entry_date")));
                dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
                //dataForSubmission.put("entry_status",urls[8]);
                dataForSubmission.put("entry_comment", currentPending.getString(currentPending.getColumnIndex("comments")));
                dataArray.put(dataForSubmission);

                currentPending.moveToNext();
            }

            new AccessAPCServerTask().execute(postURL, dataArray.toString());

        } else {
            Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:

                if (resultCode == RESULT_OK) {
                    if (image_count == 0) {
                        group_id = "" + orderDBHelper.getLastGroupID();
                        orderDBHelper.insertPrescriptionGroup(group_id, start_latitude, start_longitude);

                    }

                    try {

                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor =
                                managedQuery(mCapturedImageURI, projection, null,
                                        null, null);
                        int column_index_data = cursor.getColumnIndexOrThrow(
                                MediaStore.Images.Media.DATA);
                        cursor.moveToFirst();
                        String picturePath = cursor.getString(column_index_data);
                        image_count++;

                        if (image_count > 0) {

                            orderDBHelper.insertPrescriptionGroupImages(group_id, picturePath, System.currentTimeMillis(), 0);

                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Photo could not be taken ! Please Retry !", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                    takePhoto();

                } else if (resultCode == RESULT_CANCELED) {
                    image_count = 0;
                    Cursor swapC = orderDBHelper.getIncompletePrescriptions();

                    apsCursorAdapter.swapCursor(swapC);
                    apsCursorAdapter.notifyDataSetChanged();
                    lvGroupList.smoothScrollToPosition(0);
                    swapC = null;

                }


        }

    }

    public void refreshTextViews() {
//        txtViewPSSProductID.setText("No Data");
//        txtViewPSSProductName.setText("No Data");
//        txtViewPSSProductBarcode.setText("No Data");
//        btnPSSOk.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;

        } else if (id == android.R.id.home) {
            this.finish();
            onBackPressed();
            return true;

        } else if (id == R.id.return_to_home) {
            stopLocationUpdates();
            String source_activity = "";

            Intent main_menu = new Intent(ASRPrescriptionGroupActivity.this, MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity", source_activity);

            startActivity(main_menu);
            finish();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        //Toast.makeText(getApplicationContext(),"Back pressed", Toast.LENGTH_SHORT).show();

//        if(btnOSAdd.getVisibility()==View.VISIBLE)
//        {
//            btnOSAdd.setVisibility(View.GONE);
//            btnOSFinish.setVisibility(View.VISIBLE);
//            return;
//        }

        super.onBackPressed();
        stopLocationUpdates();


    }

    @Override
    protected void onResume() {

        super.onResume();
        apsCursorAdapter.swapCursor(orderDBHelper.getIncompletePrescriptions());

        apsCursorAdapter.notifyDataSetChanged();


    }

    private class AccessAPCServerTask extends AsyncTask<String, Void, String> {

        private ProgressDialog pd;
        private boolean getAll = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(ASRPrescriptionGroupActivity.this);
            pd.setTitle("Please Wait");
            pd.setMessage("Submitting data to server  ...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls) {

            try {

                String result = submitInfoToUrl(urls[0], urls[1]);

                return result;

            } catch (IOException e) {

                return "Unsuccessfull";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            System.out.println("Server Response: " + s);
            pd.dismiss();

            if (s != null) {

                JSONArray jArray = null;

                try {


                    if (s.contentEquals("success")) {
                        if (lastSubmitted != null) lastSubmitted.moveToFirst();

                        while (!lastSubmitted.isAfterLast()) {
                            //dbHelper.updateStatusToSubmitWithEntryID(lastSubmitted.getString(lastSubmitted.getColumnIndex("_id")));

                            lastSubmitted.moveToNext();
                        }


                        // Cursor productEntries   = dbHelper.getPendingSubmissions();

                        // apsCursorAdapter.swapCursor(productEntries);
                        apsCursorAdapter.notifyDataSetChanged();


                    }


                } catch (Exception e) {


                }


            }


        }


        private String submitInfoToUrl(String networkURL, String dataArray) throws IOException {

            //URL url = new URL(serverUrl);

            //barCode                 = "132768165";
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext httpContext = new BasicHttpContext();
            String postUrl = networkURL;
            HttpPost request = new HttpPost(postUrl);


            HttpResponse response = null;

            BufferedReader in = null;
            String data = null;

            try {

                StringEntity se = new StringEntity(dataArray);
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("apc", dataArray));
                //nameValuePairs.add(new BasicNameValuePair("status","1"));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                //request.setEntity(se);
                //request.setHeader("Accept", "application/json");
                //request.setHeader("Content-type","application/json");
                System.out.println("Submit pack: " + dataArray);
                response = httpClient.execute(request, httpContext);
                HttpEntity entity = response.getEntity();

                String responseData = EntityUtils.toString(entity);
//                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//                StringBuffer sb = new StringBuffer("");
//                String l = "";
//                String nl = System.getProperty("line.separator");
//
//                while((l=in.readLine())!=null)
//                {
//                    sb.append(l+nl);
//
//                }
//
//                in.close();
//                data = sb.toString();
                return responseData;

            } finally {


            }


        }
    }

}
