package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.MessagesTabListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.fragments.MessageFragment;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by amadeyr on 10/6/15.
 */
public class APCMessageActivity extends ActionBarActivity{

    //private EditText edtTextEmployeeID;

    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCOrderSCursorAdapter apcCursorAdapter;


    private final int REQUEST_CODE              = 100;
    private final int APC_EDIT_CODE             = 1;
    private final int APC_REMOVE_CODE           = 2;
    private final String INBOX_TAB_TITLE        = "INBOX";
    private final String OUTBOX_TAB_TITLE       = "OUTBOX";

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";


    private String screenTitle = "Messages";
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocalTime jodaTimeHelper;
    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;
    private SharedPreferences sp;
    private String app_user_id, msgJSONString, user_token;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private MessageFragment  mf_inbox;
    public ProgressDialog pd ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_activity_layout);

        sp                      = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        jodaTimeHelper          = new LocalTime();
        pd                      = new ProgressDialog(this);

        postURL                 = sp.getString("postURL","");//+"/index.php/asp_api/postdata2";
        //getURL                  = sp.getString("getURL","")+"/getmessage";
        getURL                  = sp.getString("getURL","");
        //app_user_id             = sp.getString("username","1");
        app_user_id             = sp.getString("username","");
        user_token              = sp.getString("user_token","");;
        msgJSONString           = "";

//        if(checkPlayServices())
//        {
//            buildGoogleApiClient();
//        }

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle(screenTitle);
        bar.setIcon(R.drawable.iconb);
        bar.setNavigationMode(bar.NAVIGATION_MODE_TABS);
        //bar.setDisplayShowTitleEnabled(false);

        ActionBar.Tab inboxTab = bar.newTab();
        ActionBar.Tab outboxTab = bar.newTab();

        mf_inbox = new MessageFragment();
        mf_inbox.setTypeOfFragment("INBOX");


        MessageFragment  mf_outbox = new MessageFragment();
        mf_outbox.setTypeOfFragment("OUTBOX");

        inboxTab.setText(INBOX_TAB_TITLE)
                .setContentDescription("Tap to browse messages in your inbox.")
                .setTabListener(new MessagesTabListener<MessageFragment>(mf_inbox));

        outboxTab.setText(OUTBOX_TAB_TITLE)
                .setContentDescription("Tap to browse messages in your inbox.")
                .setTabListener(new MessagesTabListener<MessageFragment>(mf_outbox));

        bar.addTab(inboxTab);
        bar.addTab(outboxTab);

        bar.show();



//        SharedPreferences sp    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
//
//        getURL                  = sp.getString("getURL","")+"/autodata/description";
//        postURL                 = sp.getString("postURL","")+"/index.php/asp_api/postdata";
        //postURL                 = sp.getString("postURL","")+":8000/orders";





        //dbHelper                = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper                = APCDatabaseHelper.getInstance(getApplicationContext());
        dateHelper              = new APCDateHelper();

        //Cursor orderEntries     = dbHelper.getPendingSubmissions(app_user_id);
        //apcCursorAdapter        = new APCOrderSCursorAdapter(getApplicationContext(),R.layout.pending_row_layout,orderEntries,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


    }

    public void populateSearchAdapter(String searchItem)
    {
//        List<VisitInfo> oInfo = dbHelper.getAutoCompleteOrderInfoForPending(searchItem);
//        orderArray            = new String[oInfo.size()];
//
//        int str_index           = 0;
//
//        for (int index = 0; index < oInfo.size();++index)
//        {
//            orderArray[index]        = new String(oInfo.get(index).getOrderNo());
//            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());
//
//            //str_index+=2;
//
//            //System.out.println("SearchActivity: "+jObj.getString("description"));
//        }
//
//
//        ArrayAdapter adapter         = new ArrayAdapter(APCPendingSubmissionsActivity.this, android.R.layout.simple_list_item_1, orderArray);
//
//        actSearchField.setAdapter(adapter);
//        actSearchField.setThreshold(1);



    }


    public void populateAPCAutocompleteArray(String searchTerm)
    {
//        Cursor currentEntries = null;
//
//        if(searchTerm.equals(""))
//        {
//            currentEntries = dbHelper.getDistinctEntries();
//
//        }else
//        {
//
//            currentEntries  = dbHelper.getOrders(searchTerm);
//        }
//
//
//
//        if(currentEntries!=null)
//        {
//            currentEntries.moveToFirst();
//
//            String[] barcodeArray = new String[currentEntries.getCount()];
//
//
//            int string_arr_index = 0;
//
//            for (int json_arr_index = 0; json_arr_index < currentEntries.getCount();++json_arr_index)
//            {
//                barcodeArray[json_arr_index]        = currentEntries.getString(currentEntries.getColumnIndex("trade_name"));
//                currentEntries.moveToNext();
//                //System.out.println("SearchActivity: "+jObj.getString("description"));
//            }
//
//            ArrayAdapter adapter = new ArrayAdapter(APCPendingSubmissionsActivity.this, android.R.layout.simple_list_item_1, barcodeArray);
//            actSearchField.setAdapter(adapter);
//
//
//        }

    }

    public void postDataToServer(String sendType) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
//            if(currentPending!=null) currentPending.moveToFirst();
//
//
//            JSONArray dataArray = new JSONArray();
//
//
//            while(!currentPending.isAfterLast())
//            {
//                JSONObject dataForSubmission = new JSONObject();
//
//                //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
//                //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
//                //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();
//                String post_order_no = currentPending.getString(currentPending.getColumnIndex("visit_id"));
//
//                dataForSubmission.put("visit_id",currentPending.getString(currentPending.getColumnIndex("visit_id")));
//                dataForSubmission.put("doctor_code",currentPending.getString(currentPending.getColumnIndex("doctor_code")));
//                dataForSubmission.put("visit_date", currentPending.getString(currentPending.getColumnIndex("visit_date")));
//                dataForSubmission.put("delivery_date", currentPending.getString(currentPending.getColumnIndex("delivery_date")));
//                dataForSubmission.put("order_time", currentPending.getString(currentPending.getColumnIndex("order_time")));
//                dataForSubmission.put("total",currentPending.getString(currentPending.getColumnIndex("total")));
//                //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
//
//
//                Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);
//
//                JSONArray detailsArray = new JSONArray();
//
//                if(order_entries!=null)
//                {
//                    order_entries.moveToFirst();
//
//                    while(!order_entries.isAfterLast())
//                    {
//                        JSONObject dataEntries  = new JSONObject();
//
//                        //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text
//
//                        dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
//                        dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
//                        dataEntries.put("price", order_entries.getDouble(order_entries.getColumnIndex("price")));
//                        dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
//                        dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
//                        dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
//                        dataEntries.put("comments", order_entries.getString(order_entries.getColumnIndex("comments")));
//
//                        detailsArray.put(dataEntries);
//
//                    }
//                }
//
//                dataForSubmission.put("details_array",detailsArray.toString());
//
//                //dataForSubmission.put
//                //dataForSubmission.put("entry_status",urls[8]);
//                //dataForSubmission.put("entry_comment",currentPending.getString(currentPending.getColumnIndex("comments")));
//                dataArray.put(dataForSubmission);
//
//                currentPending.moveToNext();
//            }
//
//






            new AccessAPCServerTask().execute(sendType);

        }else
        {
            Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }

    public void pullMessageDataFromServer(int last_msg_id) throws JSONException
    {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {

            user_token          = sp.getString("user_token","");
            String pURL         = postURL+ASRConfig.PULL_MESSAGE_EXT+"?token="+user_token;

//            try {
//
//                ProgressDialog p = new ProgressDialog(APCMessageActivity.this);
//                p.setMessage("Pulling Messages From Server");
//                p.show();
//                msgJSONString       = getInfoFromUrl(pURL, ""+last_msg_id);
//                p.dismiss();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            new AccessAPCServerTask().execute("getmsg",""+last_msg_id);

        }else
        {
            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }

    }

    private String getInfoFromUrl(String pURL, String last_msg_id) throws IOException, JSONException {


        URL url                 = new URL(pURL);
        System.out.println("pURL: "+pURL);

        HttpClient httpClient   = new DefaultHttpClient();
        HttpPost request        = new HttpPost(pURL);
        HttpContext httpContext = new BasicHttpContext();
        HttpResponse response   = null;
        BufferedReader in       = null;

        String data             = null;

        JSONObject pdata        = new JSONObject();

        pdata.put("employee_code",app_user_id);
        pdata.put("last_msg_id", last_msg_id);


        try
        {

            StringEntity se = new StringEntity(pdata.toString());

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("msg_retrieve",pdata.toString()));

            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            System.out.println("Submit pack: "+pdata.toString());
            response = httpClient.execute(request, httpContext);
            HttpEntity entity = response.getEntity();

            String responseData     = EntityUtils.toString(entity);

            System.out.println("From Server: "+responseData);

            return responseData;


        }catch(Exception e){
            e.printStackTrace();


        } finally {

        }

        return "";



    }


    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {
//            if(currentPending!=null) currentPending.moveToFirst();
//
//
//            JSONArray dataArray = new JSONArray();
//
//
//            while(!currentPending.isAfterLast())
//            {
//                JSONObject dataForSubmission = new JSONObject();
//
//                //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
//                //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
//                //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();
//                String post_order_no = currentPending.getString(currentPending.getColumnIndex("visit_id"));
//
//                dataForSubmission.put("visit_id",currentPending.getString(currentPending.getColumnIndex("visit_id")));
//                dataForSubmission.put("doctor_code",currentPending.getString(currentPending.getColumnIndex("doctor_code")));
//                dataForSubmission.put("visit_date", currentPending.getString(currentPending.getColumnIndex("visit_date")));
//                dataForSubmission.put("delivery_date", currentPending.getString(currentPending.getColumnIndex("delivery_date")));
//                dataForSubmission.put("order_time", currentPending.getString(currentPending.getColumnIndex("order_time")));
//                dataForSubmission.put("total",currentPending.getString(currentPending.getColumnIndex("total")));
//                //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
//
//
//                Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);
//
//                JSONArray detailsArray = new JSONArray();
//
//                if(order_entries!=null)
//                {
//                    order_entries.moveToFirst();
//
//                    while(!order_entries.isAfterLast())
//                    {
//                        JSONObject dataEntries  = new JSONObject();
//
//                        //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text
//
//                        dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
//                        dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
//                        dataEntries.put("price", order_entries.getDouble(order_entries.getColumnIndex("price")));
//                        dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
//                        dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
//                        dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
//                        dataEntries.put("comments", order_entries.getString(order_entries.getColumnIndex("comments")));
//
//                        detailsArray.put(dataEntries);
//
//                    }
//                }
//
//                dataForSubmission.put("details_array",detailsArray.toString());
//
//                //dataForSubmission.put
//                //dataForSubmission.put("entry_status",urls[8]);
//                //dataForSubmission.put("entry_comment",currentPending.getString(currentPending.getColumnIndex("comments")));
//                dataArray.put(dataForSubmission);
//
//                currentPending.moveToNext();
//            }
//
//






            new AccessAPCServerTask().execute("","");

        }else
        {
            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.message_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.msg_refresh)
        {

            try {
                performMessageRefresh();

                //pullMessageDataFromServer();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;

        }else if(id == R.id.msg_compose)
        {
            buildComposeDialog("","");


            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            //NavUtils.navigateUpFromSameTask(this);
            return true;


        }

        return super.onOptionsItemSelected(item);
    }

    public void performMessageRefresh() throws JSONException {

        int last_msg_id     = 0;

        Cursor msgCursor    = dbHelper.getLastMsgStatus(app_user_id);

        if(msgCursor.getCount()>0)
        {
            msgCursor.moveToFirst();
            last_msg_id 	= msgCursor.getInt(msgCursor.getColumnIndex("msg_id"));
        }
        else
        {
            last_msg_id		= 0;
        }

        msgCursor.close();


        int success = 0;

        //String result
        pullMessageDataFromServer(last_msg_id);

//        if(msgJSONString!=null)
//        {
//
//            try{
//
//                //JSONObject json_data				= new JSONObject(msgJSONString);
//                JSONArray json_data				= new JSONArray(msgJSONString);
//
//                //success 							= Integer.parseInt(json_data.getString("success"));
//
////                if(success==1)
////                {
//                    //uid							= Integer.parseInt(json_data.getString("uid"));
//                    //JSONObject user_object 		= new JSONObject(json_data.getString("user"));
//                    //user_name				 		= user_object.getString("name");
//                    //user_email				 	= user_object.getString("email");
//                    //user_zilla				 	= user_object.getString("zilla");
//                    //user_upazilla				 	= user_object.getString("upazilla");
//                    //user_division 				= user_object.getString("division");
//
//
//			    	  /* inserting the messages in the local database */
//                    //int uid 						= Integer.parseInt(json_data.getString("uid"));
//                    int message_count 			    = json_data.length();
//
//                    if(message_count>0)
//                    {
//                        //dbHelper.onOpen(dbHelper.getWritableDatabase());
//
//                        //JSONArray  jArray = new JSONArray(json_data.getString("messages"));
//
//                        for(int i = 0; i<message_count;++i)
//                        {
//                            JSONObject msg_object = json_data.getJSONObject(i);
//
//                            dbHelper.addToMsgTable(msg_object.getString("msg_id"),
//                                    msg_object.getString("msg_title"),
//                                    msg_object.getString("msg_body"),
//                                    msg_object.getString("publish_date"),
//                                    "",
//                                    0,
//                                    0,
//                                    0,
//                                    1,0,msg_object.getString("employeecode"));
//
//                        }
//
//                        JSONObject last_msg = json_data.getJSONObject(message_count-1);
//
//                        if(last_msg_id==0)
//                        {
//                            dbHelper.addToLastMsgTable(null, last_msg.getString("msg_id"), app_user_id);
//                        }else
//                        {
//                            dbHelper.updateLastMsgTable(last_msg.getString("msg_id"), "" + last_msg_id, app_user_id);
//                        }
//
//                        if(mf_inbox !=null && mf_inbox.isVisible())
//                        {
//
//                            mf_inbox.reloadMessageData();
//                        }
//
//                    }
//
//
//
//
//
////                }else
////                {
////
////                    System.out.println(json_data.getString("error_msg"));
////
////                }
//
//
//            }
//            catch(NullPointerException e){
//                e.printStackTrace();
//            }
//            catch(JSONException e){
//                e.printStackTrace();
//            }
//
//
//
//        }else
//        {
//
//            Toast.makeText(getApplicationContext(),"No new messages", Toast.LENGTH_LONG).show();
//        }



    }

        public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }



    private void buildComposeDialog(String msgT, String msgB)
    {
        final Dialog messageScreen = new Dialog(APCMessageActivity.this);
        messageScreen.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageScreen.setContentView(R.layout.compose_message_screen);
        messageScreen.show();

        final EditText composeMsgTitle = (EditText) messageScreen.findViewById(R.id.edtTextComposeTitle);
        final EditText composeMsgBody  = (EditText) messageScreen.findViewById(R.id.edtTextComposeMsg);
        Button btnComposeSend    = (Button) messageScreen.findViewById(R.id.btnComposeSend);
        Button btnComposeCancel  = (Button) messageScreen.findViewById(R.id.btnComposeCancel);
        ImageView imgCross       = (ImageView) messageScreen.findViewById(R.id.imgViewComposeCross);

        composeMsgTitle.requestFocus();

        if(!msgT.isEmpty() && !msgB.isEmpty())
        {
            composeMsgTitle.setText(msgT);
            composeMsgBody.setText(msgB);

        }

        btnComposeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                messageScreen.dismiss();
            }
        });

        btnComposeSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String composeMsgTitleString    = composeMsgTitle.getText().toString();
                String composeMsgBodyString     = composeMsgBody.getText().toString();

                if(composeMsgTitleString.isEmpty())
                {
                    composeMsgTitle.setError("Please enter subject");
                }else if(composeMsgBodyString.isEmpty())
                {
                    composeMsgBody.setError("Please enter message");

                }else
                {
                    messageScreen.dismiss();
                    new AccessAPCServerTask().execute(composeMsgTitleString, composeMsgBodyString);
                }





            }
        });

        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                messageScreen.dismiss();
            }
        });



    }


    private void showSuccessDialog()
    {
        Toast.makeText(getApplicationContext(),"Your message was sent successfully.",Toast.LENGTH_LONG).show();

    }

    protected void buildGoogleApiClient()
    {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {

//        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//        if(mLastLocation!=null)
//        {
//
//            latitude  = mLastLocation.getLatitude();
//            longitude = mLastLocation.getLongitude();
//
////            double latitude  = mLastLocation.getLatitude();
////            double longitude = mLastLocation.getLongitude();
//
//            //Toast.makeText(getApplicationContext(),"Current Location: "+latitude+","+longitude,Toast.LENGTH_SHORT).show();
//
//        }else
//        {
//
//            //Toast.makeText(getApplicationContext(),"Location tracking may be disabled.",Toast.LENGTH_LONG).show();
//        }

    }

    @Override
    protected void onStart()
    {
        super.onStart();

//        if (mGoogleApiClient != null)
//        {
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //checkPlayServices();
    }

//    @Override
//    public void onConnected(Bundle bundle) {
//        recordLocation();
//    }
//
//    @Override
//    public void onConnectionSuspended(int i)
//    {
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//
//    }



    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {

        //private ProgressDialog pd;
        private boolean getAll = true;
        private String msgTitle, msgBody, taskType, last_msg_id;
        private int user_id ;
        private ProgressDialog pd;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            taskType = "";
            pd = new ProgressDialog(APCMessageActivity.this);
            pd.setCancelable(false);
            pd.setTitle("Please Wait");

            pd.setMessage("Sending request to server  ...");

            pd.show();

        }

        @Override
        protected String doInBackground(String... urls)
        {
            String result   = "";
            user_id  = 1;

            if(urls[0].contentEquals("getmsg"))
            {
                try
                {

                    taskType    = urls[0];
                    last_msg_id = urls[1];

                    //String pURL = postURL+"/message/getmessage";
                    user_token  = sp.getString("user_token", "");
                    String pURL = postURL+ASRConfig.PULL_MESSAGE_EXT+"?token="+user_token;
                    result      = getInfoFromUrl(pURL, last_msg_id);


                } catch (IOException e)
                {
                    e.printStackTrace();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else
            {

                msgTitle = urls[0];
                msgBody  = urls[1];

                String pUrl                  = postURL+ASRConfig.POST_MSG_EXT+"?token="+user_token;

                JSONArray dataArray          = new JSONArray();
                JSONObject dataForSubmission = new JSONObject();

                try
                {
                    dataForSubmission.put("msg_title", msgTitle);
                    dataForSubmission.put("msg_body", msgBody);
                    dataArray.put(dataForSubmission);
                    result = submitInfoToUrl(pUrl, dataArray.toString());

                } catch (JSONException e)
                {
                    e.printStackTrace();

                } catch (IOException e)
                {
                    e.printStackTrace();
                }


            }


            return result;
        }

        @Override
        protected void onProgressUpdate(Void... values) {



            super.onProgressUpdate(values);
            //pd.incrementProgressBy((int)values[0]);

        }

        @Override
        protected void onPostExecute(String s)
        {
            pd.dismiss();
            System.out.println("Server Response: "+s);

            if(taskType.contentEquals("getmsg"))
            {
                msgJSONString = s;

                if(msgJSONString!=null)
                {

                    try{

                        //JSONObject json_data				= new JSONObject(msgJSONString);
                        JSONArray json_data				= new JSONArray(msgJSONString);

                        //success 							= Integer.parseInt(json_data.getString("success"));

//                if(success==1)
//                {
                        //uid							= Integer.parseInt(json_data.getString("uid"));
                        //JSONObject user_object 		= new JSONObject(json_data.getString("user"));
                        //user_name				 		= user_object.getString("name");
                        //user_email				 	= user_object.getString("email");
                        //user_zilla				 	= user_object.getString("zilla");
                        //user_upazilla				 	= user_object.getString("upazilla");
                        //user_division 				= user_object.getString("division");


			    	  /* inserting the messages in the local database */
                        //int uid 						= Integer.parseInt(json_data.getString("uid"));
                        int message_count 			    = json_data.length();

                        if(message_count>0)
                        {
                            //dbHelper.onOpen(dbHelper.getWritableDatabase());

                            //JSONArray  jArray = new JSONArray(json_data.getString("messages"));

                            for(int i = 0; i<message_count;++i)
                            {
                                JSONObject msg_object = json_data.getJSONObject(i);

                                dbHelper.addToMsgTable(msg_object.getString("msg_id"),
                                        msg_object.getString("msg_title"),
                                        msg_object.getString("msg_body"),
                                        msg_object.getString("publish_date"),
                                        "",
                                        0,
                                        0,
                                        0,
                                        1,0,app_user_id);

                            }

                            JSONObject last_msg = json_data.getJSONObject(message_count-1);

                            if(Integer.parseInt(last_msg_id)==0)
                            {
                                dbHelper.addToLastMsgTable(null, last_msg.getString("msg_id"), app_user_id);
                            }else
                            {
                                dbHelper.updateLastMsgTable(last_msg.getString("msg_id"), "" + last_msg_id, app_user_id);
                            }

                            if(mf_inbox !=null && mf_inbox.isVisible())
                            {

                                mf_inbox.reloadMessageData();
                            }

                        }





//                }else
//                {
//
//                    System.out.println(json_data.getString("error_msg"));
//
//                }


                    }
                    catch(NullPointerException e){
                        e.printStackTrace();
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                    }



                }else
                {

                    Toast.makeText(getApplicationContext(),"No new messages", Toast.LENGTH_LONG).show();
                }




            }else
            {
                if(s.contentEquals("success"))
                {

                    int hOfDay = jodaTimeHelper.getHourOfDay();
                    int minOfHour = jodaTimeHelper.getMinuteOfHour();
                    String timePrefix = hOfDay >= 12 ? "pm":"am";
                    String prettyTime =  (hOfDay%12)+":"+minOfHour+" "+timePrefix;

                    dbHelper.addToMsgTable("",msgTitle,msgBody,dateHelper.getCurrentDateStr(),prettyTime,0,1,1,user_id,0,app_user_id);
                    showSuccessDialog();

                }else
                {
                    buildComposeDialog(msgTitle,msgBody);
                }


            }




        }


        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException {

            //URL url = new URL(serverUrl);

            //barCode                 = "132768165";
            HttpClient httpClient   = new DefaultHttpClient();
            HttpContext httpContext = new BasicHttpContext();
            String postUrl          = networkURL;
            HttpPost request        = new HttpPost(postUrl);

            HttpResponse response   = null;

            BufferedReader in=null;
            String data = null;

            Log.v("MessageActivity","Post URL: "+networkURL);


            try
            {
                StringEntity se = new StringEntity(dataArray);
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                //nameValuePairs.add(new BasicNameValuePair("user_id",app_user_id));
                nameValuePairs.add(new BasicNameValuePair("msg_details",dataArray));
                //nameValuePairs.add(new BasicNameValuePair("status","1"));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("Submit pack: "+dataArray);
                response = httpClient.execute(request, httpContext);
                HttpEntity entity = response.getEntity();

                String responseData     = EntityUtils.toString(entity);

                return responseData;

            }finally
            {


            }




        }
    }


}

