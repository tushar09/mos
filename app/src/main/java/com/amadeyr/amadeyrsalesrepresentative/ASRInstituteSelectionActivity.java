package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRInstituteListCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.InstituteInfo;

import java.util.List;

/**
 * Created by amadeyr on 11/4/15.
 */
public class ASRInstituteSelectionActivity extends ActionBarActivity{
    //private EditText edtTextEmployeeID;

    private ListView lvInstituteList;
    private InstituteInfo iInfo;
    private APCProductDatabaseHelper dbHelper;
    private APCDateHelper dateHelper,apcDateHelper;
    private APCCustomAutoCompleteView instituteSearch;
    private ASRInstituteListCursorAdapter apsCursorAdapter;
    private ImageView imgViewCross;



    private String user_id, source_activity;
    private final String SCREEN_TITLE = "Institute List";


    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] instituteArray;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.institute_selection);

        ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        SharedPreferences sp    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        getURL                  = sp.getString("getURL","")+"/autodata/description";
        postURL                 = sp.getString("postURL","")+"/index.php/pos_request/datapush";
        user_id                 = sp.getString("user_id", "SUP-100");
        source_activity         = getIntent().getStringExtra("source_activity");


        apcDateHelper           = new APCDateHelper();

        instituteSearch         = (APCCustomAutoCompleteView) findViewById(R.id.actInsituteSearch);
        instituteSearch.clearFocus();
        imgViewCross            = (ImageView) findViewById(R.id.imgViewCrossInstitute);
        lvInstituteList         = (ListView) findViewById(R.id.lstViewInstituteList);

        instituteSearch.setThreshold(1);

        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        //dbHelper                = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        dbHelper                = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());

        user_id                 = getIntent().getStringExtra("employee_id");

        Cursor instituteNames   = dbHelper.getInstituteList(user_id);

        apsCursorAdapter        = new ASRInstituteListCursorAdapter(getApplicationContext(),R.layout.institute_row_layout,instituteNames,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvInstituteList.setAdapter(apsCursorAdapter);
        //btnPSSubmit             = (Button)   findViewById(R.id.btnPSSubmit);
        //btnPSSearch             = (Button)   findViewById(R.id.btnPSSearch);

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instituteSearch.setText("");
                Cursor allEntries = dbHelper.getInstituteList(user_id);
                apsCursorAdapter.swapCursor(allEntries);
                apsCursorAdapter.notifyDataSetChanged();
            }
        });

        lvInstituteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor selectedCustomer = (Cursor) apsCursorAdapter.getItem(position);

                //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_LONG).show();

                Intent i = new Intent(ASRInstituteSelectionActivity.this, ASRDoctorSelectionActivity.class);
                i.putExtra("employee_id", selectedCustomer.getString(selectedCustomer.getColumnIndex("employee_id")));
                i.putExtra("institute_code", selectedCustomer.getString(selectedCustomer.getColumnIndex("institute_code")));
                i.putExtra("institute_name", selectedCustomer.getString(selectedCustomer.getColumnIndex("institute_name")));
                i.putExtra("source_activity", source_activity);

                startActivity(i);
            }
        });

        instituteSearch.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));

        instituteSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String searchKey    = instituteSearch.getText().toString();

                Cursor searchCursor = dbHelper.getInstituteInfo(searchKey);

                apsCursorAdapter.swapCursor(searchCursor);
                apsCursorAdapter.notifyDataSetChanged();


            }
        });




    }




    public void populateSearchAdapter(String searchItem)
    {

        String supervisor_id        = this.user_id;
        List<InstituteInfo> eInfo   = dbHelper.getAutoCompleteInstituteInfo(searchItem, user_id);
        instituteArray              = new String[eInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < eInfo.size();++index)
        {
            instituteArray[index]        = new String(eInfo.get(index).getInstituteName());
            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());

            //str_index+=2;

            //System.out.println("SearchActivity: "+jObj.getString("description"));
        }


        ArrayAdapter adapter         = new ArrayAdapter(ASRInstituteSelectionActivity.this, android.R.layout.simple_list_item_1, instituteArray);

        instituteSearch.setAdapter(adapter);
        instituteSearch.setThreshold(1);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {
            String source_activity  = "";

            Intent main_menu = new Intent(ASRInstituteSelectionActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();




            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

        super.onBackPressed();




    }





}
