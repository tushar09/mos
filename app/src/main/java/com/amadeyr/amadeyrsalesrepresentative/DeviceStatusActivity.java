package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.models.EmployeeInfo;
import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncDownloadAppUpdateTask;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;


public class DeviceStatusActivity extends ActionBarActivity {

    private Button gpsChange,
                   proceed,
                   update;

    private TextView calibUserName,
                     internetStatus, gpsStatus, bLevel;
    private String baseURL,
                   appUpdateUrl,
                   app_version_name,
                   server_version_name,
                   server_product_db_hash,
                   server_other_product_db_hash,
                   server_user_db_hash,
                   server_tl_hash,
                   employee_code,
                   user_designation,
                   user_token,
                   device_product_hash,
                   device_other_product_hash,
                   device_user_hash,
                    device_tl_hash;

    private APCDatabaseHelper userDBHelper;
    private int app_version_number;
    private CheckBox checkAll;

    private String email, password, user_name;
    private String rulesHash;
    private int user_id, net_status;
    private TextView contentTxt;
    private ProgressBar battery;
    private boolean sync_required, gps_status, product_required, other_product_required,user_required;
    private SharedPreferences settings;

    String currentversion = "";
    int warning = 0;
    //private Button mEnterMessageMenu;
    //private Button mExitButton;
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {


        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub
            int level = arg1.getIntExtra("level", 0);
            //level = 15;
            bLevel.setText(String.valueOf(level) + "%");
            bLevel.setTextColor(getResources().getColor(R.color.positive_confirmation_color));
            battery.setProgress(level);

            if (level < 20) {
                bLevel.setTextColor(getResources().getColor(R.color.battery_alert_color));
                warning = 1;

            } else {
                warning = 0;
            }

        }
    };


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_device_status);

        product_required = other_product_required = user_required = false;

        settings                    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);

        baseURL                     = new String(settings.getString("postURL", ""));
        //baseURL                     = "http://202.51.189.20";
        //baseURL                     = "http://192.168.1.132/asp_web/index.php/asp_api";
        appUpdateUrl                = baseURL + ASRConfig.GET_HASHES_EXTENSION;
        sync_required               = false;
        net_status                  = 0;
        user_token                  = new String(settings.getString("user_token", ""));
        user_name                   = new String(settings.getString("username", ""));
        userDBHelper                = APCDatabaseHelper.getInstance(getApplicationContext());
        //userDBHelper                = new APCDatabaseHelper(getApplicationContext(),APCDatabaseHelper.DATABASE_NAME_2,null,APCDatabaseHelper.DATABASE_VERSION);


        Cursor c1 = userDBHelper.getProductSyncHash();
        Cursor c2 = userDBHelper.getOtherProductSyncHash();
        Cursor c3 = userDBHelper.getTLHash(user_name);
        Cursor c4 = userDBHelper.getSRUserDBHash(user_name);

        device_product_hash             = "";
        device_other_product_hash       = "";
        device_tl_hash                  = "";
        device_user_hash                = "";

        if(c1!=null)
        {
            if(c1.getCount()>0)
            {
                c1.moveToFirst();
                device_product_hash             = c1.getString(c1.getColumnIndex("hashcode_latest"));

                if(device_product_hash==null) device_product_hash="";
            }

        }

        if(c2!=null)
        {
            if(c2.getCount()>0)
            {
                c2.moveToFirst();
                device_other_product_hash       = c2.getString(c2.getColumnIndex("hashcode_latest"));
                if(device_other_product_hash==null) device_other_product_hash="";
            }

        }

        if(c3!=null)
        {
            if(c3.getCount()>0)
            {
                c3.moveToFirst();
                device_tl_hash             = c3.getString(c3.getColumnIndex("hashcode_latest"));
                if(device_tl_hash==null) device_tl_hash="";
            }

        }

        if(c4!=null)
        {
            if(c4.getCount()>0)
            {
                c4.moveToFirst();
                device_user_hash           = c4.getString(c4.getColumnIndex("hashcode_latest"));
                if(device_user_hash==null) device_user_hash="";
            }

        }







        c1.close();
        c2.close();
        c3.close();
        c4.close();



        server_tl_hash                  = "";
        server_user_db_hash             = "";
        server_other_product_db_hash    = "";
        server_product_db_hash          = "";
        app_version_name                = "";
        gps_status                      = false;


//        if(productSyncCursor!=null)
//        {
//            if(productSyncCursor.getCount()==1)
//            {
//                device_product_hash = productSyncCursor.getString(productSyncCursor.getColumnIndex("hashcode_latest"));
//
//                productSyncCursor.close();
//
//                Cursor otherProductSyncCursor   = userDBHelper.getOtherProductSyncHash();
//
//                if(otherProductSyncCursor!=null)
//                {
//                    if (otherProductSyncCursor.getCount() == 1)
//                    {
//                        device_other_product_hash = otherProductSyncCursor.getString(otherProductSyncCursor.getColumnIndex("hashcode_latest"));
//                        otherProductSyncCursor.close();
//
//
//                    } else if (otherProductSyncCursor.getCount() == 0) {
//                        sync_required = true;
//
//                    }
//
//                }
//
//            }else if(productSyncCursor.getCount()==0)
//            {
//                sync_required       = true;
//
//            }
//
//        }





//        initialSyncUpdateCheck();
        initializeViews();

        this.registerReceiver(this.mBatInfoReceiver,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        try
        {


            String versionName  = BuildConfig.VERSION_NAME;
                    //getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            app_version_name    = BuildConfig.VERSION_NAME;
            app_version_number  = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            Toast.makeText(this, versionName, Toast.LENGTH_LONG).show();
            currentversion      = versionName;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        turnGPSOnOff();

        gpsChange.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                turnGPSOnOff();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ProgressDialog pd   = new ProgressDialog(DeviceStatusActivity.this);
                //String appDlUrl     = settings.getString("postURL", "")+ASRConfig.APP_UPDATE_URL_EXTENSION;
                new AsyncDownloadAppUpdateTask(getApplicationContext(),pd, user_token,server_version_name,AsyncDownloadAppUpdateTask.FETCH_TYPE_FILE,settings.getString("postURL","")).execute();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {

                                       public void onClick(View v) {

//                                           if(!gps_status || net_status==0)
//                                           {
//                                               ASRErrorMessageView amv = new ASRErrorMessageView(DeviceStatusActivity.this);
//
//                                               if(!gps_status)
//                                               {
//                                                   amv.showErrorMessage("Action not allowed", "GPS service is disabled. Please turn the GPS on device.");
//
//                                               }else if(net_status==0)
//                                               {
//                                                   amv.showErrorMessage("Action not allowed", "GPS service is disabled. Please turn the GPS on device.");
//
//                                               }
//
//                                           }

                                           Intent i = new Intent(DeviceStatusActivity.this, MainMenuActivity.class);
                                           i.putExtra("source_activity", "DeviceStatusActivity");

                                           sync_required = false;

                                           if(!device_product_hash.contentEquals(server_product_db_hash))
                                           {

                                               settings.edit().putBoolean("product_required", true).commit();
                                               sync_required = true;
                                               i.putExtra("latest_product_db_hash",server_product_db_hash);

                                           }else
                                           {

                                               settings.edit().putBoolean("product_required", false).commit();
                                               sync_required = true;
                                               i.putExtra("latest_product_db_hash","");

                                           }

                                           if(!device_other_product_hash.contentEquals(server_other_product_db_hash))
                                           {
                                               settings.edit().putBoolean("other_product_required", true).commit();
                                               sync_required = true;
                                               i.putExtra("latest_other_product_db_hash",server_other_product_db_hash);

                                           }else
                                           {
                                               settings.edit().putBoolean("other_product_required", false).commit();
                                               i.putExtra("latest_other_product_db_hash", "");
                                           }


                                           if(!device_tl_hash.contentEquals(server_tl_hash))
                                           {
                                               settings.edit().putBoolean("tl_hash_required", true).commit();
                                               sync_required = true;
                                               i.putExtra("latest_tl_hash",server_tl_hash);

                                           }else
                                           {
                                               settings.edit().putBoolean("tl_hash_required", false).commit();
                                           }


                                           if(!device_user_hash.contentEquals(server_user_db_hash))
                                           {
                                               settings.edit().putBoolean("user_db_required", true).commit();
                                               sync_required = true;
                                               i.putExtra("latest_user_db_hash",server_user_db_hash);

                                           }else
                                           {
                                               settings.edit().putBoolean("user_db_required", false).commit();
                                               i.putExtra("latest_user_db_hash", server_user_db_hash);
                                           }





                                           i.putExtra("newHash", "");
                                           i.putExtra("sync_required",sync_required);

                                           settings.edit().putBoolean("sync_required",sync_required).commit();
                                           finish();
                                           //i.putExtra("user_id", user_id);
                                           startActivity(i);
//                                           if (checkAll.isChecked()) {
//                                               Intent i = new Intent(DeviceStatusActivity.this, MainMenuActivity.class);
//                                               i.putExtra("email_id", email);
//                                               i.putExtra("password", password);
//                                               i.putExtra("user_id", user_id);
//
//                                               if (warning == 0) {
//                                                   startActivity(i);
//                                                   finish();
//
//                                               } else {
//                                                   Dialog warn = new Dialog(DeviceStatusActivity.this);
//                                                   warn.setTitle("Please Recharge");
//                                                   warn.show();
//                                                   warn.setCanceledOnTouchOutside(true);
//                                                   warn.setOnCancelListener(new DialogInterface.OnCancelListener() {
//
//
//                                                       public void onCancel(DialogInterface dialog) {
//                                                           // TODO Auto-generated method stub
//                                                           warning = 0;
//                                                       }
//                                                   });
//
//                                               }
//                                           } else {
//                                                     createCustomToast("দয়া করে ইন্টারনেট,জিপিএস ও ব্যাটারির চার্জ চেক করে All checks complete বক্সটিতে টিক দিন", Color.BLACK, true);
//
//                                           }
                                       }
                                   }


        );


        calibUserName.setText(user_name);

        new SetAllResources().execute();




    }




    private void initializeViews()
    {
        calibUserName   = (TextView) findViewById(R.id.txtCalibUser);
        gpsStatus       = (TextView) findViewById(R.id.txtCalibGpsStatus);
        internetStatus  = (TextView) findViewById(R.id.txtInternetStatus);
        bLevel          = (TextView) findViewById(R.id.tvBLevel);
        gpsChange       = (Button) findViewById(R.id.btnCalibGpsChange);
        checkAll        = (CheckBox) findViewById(R.id.chkAll);
        proceed         = (Button) findViewById(R.id.btnProceed);
        update          = (Button) findViewById(R.id.btnUpdate);

        calibUserName.setText(settings.getString("username", ""));
        user_designation    = settings.getString("user_designation", "");
        employee_code       = settings.getString("username", "");


        //gtrack = new GPSTracker(getApplicationContext());
        battery = (ProgressBar) this.findViewById(R.id.progressBar1);
        battery.setBackgroundResource(R.drawable.myprogressbar);
        battery.setMax(100);
    }



    private void initialSyncUpdateCheck()
    {

        Cursor productSyncCursor    = userDBHelper.getProductSyncHash();

        if(productSyncCursor!=null)
        {
            if(productSyncCursor.getCount()==1)
            {
                productSyncCursor.moveToFirst();
                device_product_hash = productSyncCursor.getString(productSyncCursor.getColumnIndex("hashcode_latest"));

                productSyncCursor.close();

                System.out.println("Device Product Hash: "+device_product_hash);

                Cursor otherProductSyncCursor   = userDBHelper.getOtherProductSyncHash();

                if(otherProductSyncCursor!=null)
                {
                    if (otherProductSyncCursor.getCount() == 1)
                    {
                        otherProductSyncCursor.moveToFirst();
                        device_other_product_hash = otherProductSyncCursor.getString(otherProductSyncCursor.getColumnIndex("hashcode_latest"));
                        otherProductSyncCursor.close();

                    } else if (otherProductSyncCursor.getCount() == 0)
                    {
                        sync_required = true;

                    }

                }

            }else if(productSyncCursor.getCount()==0)
            {
                sync_required       = true;

            }

        }

    }


    public void createCustomToast(String msg, int color, boolean mode) {

//        if(mode)
//        {
//            LayoutInflater inflater = getLayoutInflater();
//
//            View layout =
//                    inflater.inflate(R.layout.demo_toast,
//                            (ViewGroup) findViewById(R.id.frm_demo_toast));
//
//            TextView errMsg =  ((TextView) layout.findViewById(R.id.message));
//            errMsg.setTextColor(color);
//
//            Toast toast = new Toast(getBaseContext());
//            toast.setDuration(Toast.LENGTH_LONG * 2);
//
//            toast.setView(layout);
//
//            //toast.setGravity(Gravity.CENTER, 0,120);
//            toast.show();
//
//        }else
//        {
//            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
//
//
//        }
    }

    public void checkRulesUpdate() {
//        SharedPreferences settings =
//                PreferenceManager.getDefaultSharedPreferences(Collect.getInstance().getBaseContext());
//
//        String serverUrl =
//                settings.getString(PreferencesActivity.KEY_SERVER_URL,
//                        Collect.getInstance().getString(R.string.default_server_url));
//
//        System.out.println("Server URL : "  + serverUrl );
//        try{
//
//            rulesHash			 	= ArmsNetworkHelper.retrieveJSONstringFromServer(serverUrl+"/"+ArmsConfig.RMS_RULES_UPDATE_JSON, "");
//            JSONObject json_data	= null;
//
//            json_data = new JSONObject(rulesHash);
//            String hash_code_server = new String(json_data.getString("rules_hash"));
//
//            Collect.getInstance().updateRulesHash(hash_code_server);
//            //emptyLogInData 			= new HouseholdDbHelper(ArmsSettingsActivity.this,1);
//
//            Cursor c 					 = dbForRules.getRulesHash();
//
//            Log.d("Count of rules update table - ", ""+c.getCount());
//            if(c.getCount()==0)
//            {
//                dbForRules.addToRulesHash(1, json_data.getString("rules_update_id"), json_data.getString("title"), json_data.getString("update_time"), json_data.getString("rules_hash"));
//            }
//            //dbForRules.close();
//            c.close();
//            //System.out.println("Got Rules Hash: " + );
//        }catch(Exception e){
//
//
//        }

    }

    public String getIMEI() {

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String imei = new String(tm.getDeviceId());
        return imei;
    }

    public String getSIMNumber() {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String phone = new String(tm.getSimSerialNumber());
        return phone;

    }


    public String pingACLServerNew()
    {

        String serverUrl = new String(appUpdateUrl);

        String jsonResponse = "";

        try {

            String imei_id = "";
            String sim_id = "";

            Log.d("Net Status Before", "" + net_status);

            jsonResponse  = ArmsNetworkHelper.checkServer(appUpdateUrl, imei_id, sim_id, "", user_token);

            net_status = 0;

            Answers.getInstance().logCustom(new CustomEvent("Sync Information").putCustomAttribute("Sync JSON",jsonResponse));

        } catch (Exception e)
        {
            e.printStackTrace();
            return jsonResponse;
        }


        return jsonResponse;

    }

    public void parseResponseJSON(String jsonResponse)
    {


        if(!jsonResponse.contentEquals("nonet"))
        {
            net_status = 1;

            JSONArray response_array    = null;

            try {


                response_array = new JSONArray(jsonResponse);

                JSONObject response_object      = response_array.getJSONObject(0);

                server_version_name             = response_object.getString("version_no");
                server_product_db_hash          = response_object.getString("product_db_hash");
                server_other_product_db_hash    = response_object.getString("other_product_db_hash");
                server_user_db_hash             = response_object.getString("user_db_hash");
                server_tl_hash                  = response_object.getString("tl_hash");


            } catch (JSONException e)
            {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"Invalid JSON Response from Server",Toast.LENGTH_LONG).show();
            }
            //app_version_name        = version_obj.getString("version_no");
            Toast.makeText(getApplicationContext(),app_version_name,Toast.LENGTH_LONG).show();


        }

        if(net_status==0)
        {

            server_version_name             = app_version_name;
            server_product_db_hash          = device_product_hash;
            server_other_product_db_hash    = device_other_product_hash;
            server_user_db_hash             = device_user_hash;
            server_tl_hash                  = device_tl_hash;


        }

        Log.d("Net Status After", "" + net_status);

    }


    public void pingACLServer() {

        String serverUrl = new String(appUpdateUrl);

        try {

//            String imei_id 		 = getIMEI();
//            String sim_id 		 = getSIMNumber();
            String imei_id = "";
            String sim_id = "";


            //String gps_location	 = gtrack.getLatitude()+","+gtrack.getLongitude();

            Log.d("Net Status Before", "" + net_status);



            String jsonResponse  = ArmsNetworkHelper.checkServer(appUpdateUrl, imei_id, sim_id, "", user_token);

            net_status = 0;

            Answers.getInstance().logCustom(new CustomEvent("Sync Information").putCustomAttribute("Sync JSON",jsonResponse));

            if(!jsonResponse.contentEquals("nonet"))
            {
                net_status = 1;

                JSONArray response_array    = new JSONArray(jsonResponse);

                JSONObject response_object      = response_array.getJSONObject(0);

                server_version_name             = response_object.getString("version_no");
                server_product_db_hash          = response_object.getString("product_db_hash");
                server_other_product_db_hash    = response_object.getString("other_product_db_hash");
                server_user_db_hash             = response_object.getString("user_db_hash");
                server_tl_hash                  = response_object.getString("tl_hash");

                //app_version_name        = version_obj.getString("version_no");
                Toast.makeText(getApplicationContext(),app_version_name,Toast.LENGTH_LONG).show();




            }

            if(net_status==0)
            {

                server_version_name             = app_version_name;
                server_product_db_hash          = device_product_hash;
                server_other_product_db_hash    = device_other_product_hash;
                server_user_db_hash             = device_user_hash;
                server_tl_hash                  = device_tl_hash;


            }

            Log.d("Net Status After", "" + net_status);

        } catch (Exception e) {


        }
    }

    private void turnGPSOnOff() {
        String provider = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) {
            gpsStatus.setTextColor(getResources().getColor(R.color.order_no_entry));
            gpsStatus.setText("Disabled");
            gpsChange.setVisibility(View.VISIBLE);

        } else {
            gpsStatus.setTextColor(getResources().getColor(R.color.os_entry_button_bg_color));
            gps_status = true;
            //BengaliUnicodeString.getBengaliUTF(ArmsLogInActivity.this,"জিপিএস ক্ষমতা রহিত ",gpsStatus);
            //gpsStatus.setText("জিপিএস ক্ষমতা রহিত ");

            gpsStatus.setText("Enabled");
            gpsChange.setVisibility(View.GONE);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        turnGPSOnOff();
        new SetAllResources().execute();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        turnGPSOnOff();
        new SetAllResources().execute();
    }

    private class SetAllResources extends AsyncTask<Void, Void, Void> {
        ProgressDialog d;
        String version, jsonResponse;

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            jsonResponse = "";
            d = ProgressDialog.show(DeviceStatusActivity.this, "Checking Internet Availability", "Please Wait");
            d.show();
        }

        protected Void doInBackground(Void... params) {

            if (net_status != 1)
            {
                pingACLServer();
            }

            return null;
        }


        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            d.dismiss();

            //parseResponseJSON(jsonResponse);

            if (net_status == 1)
            {

                internetStatus.setTextColor(getResources().getColor(R.color.positive_confirmation_color));
                internetStatus.setText("Available");
                if(server_version_name==null) server_version_name = "";
                if(!app_version_name.contentEquals(server_version_name))
                {

                    update.setEnabled(true);
                    update.setBackgroundResource(R.drawable.os_entry_button_bg);
                    update.setTextColor(getResources().getColor(android.R.color.white));
                    proceed.setEnabled(false);
                    proceed.setBackgroundResource(R.drawable.update_button_style);


                }

                // Checks if the product hash and the other product hash suggested that a sync is required

                if(!sync_required)
                {

                    // In case if the status of the check suggest that there has been no change in product or other product
                    // then check the designation of the user.

//                    if(user_designation.contentEquals(EmployeeInfo.TEAM_LEADER))
//                    {
//                        // Execute the query to extract the latest hash code stored in the device for the team leader
//
//                        Cursor hash_cursor = userDBHelper.getTLHash(employee_code);
//
//
//                        // Check if result returned by query is null, to avoid nullPointerException
//
//                        if(hash_cursor!=null)
//                        {
//
//                            // If there exists a hash then store the hashcode inside the global variable
//
//                            if(hash_cursor.getCount()>0)
//                            {
//                                hash_cursor.moveToFirst();
//                                device_tl_hash = hash_cursor.getString(hash_cursor.getColumnIndex("hashcode_latest"));
//
//                            }
//
//                        }
//
//                        // Closes the cursor to avoid unwanted application crashes
//
//                        hash_cursor.close();
//
//                        // Matches the latest hash cached in the device with the current hash sent from the server
//                        // In case of a mismatich set sync_required to TRUE to suggest that an update of database is required
//                        System.out.println("Device TL Hash: "+ device_tl_hash);
//
//                        if(!device_tl_hash.contentEquals(server_tl_hash))
//                        {
//
//                            sync_required = true;
//
//                        }else
//                        {
//                            sync_required = false;
//
//                        }
//
//                    }else

                    //if(user_designation.contentEquals(EmployeeInfo.SALES_REPRESENTATIVE))
                    //{
                        // Execute the query to extract the latest hash code stored in the device for the sales representative

                        //Cursor hash_cursor = userDBHelper.getSRUserDBHash(employee_code);
                        Cursor hash_cursor = userDBHelper.getSRUserDBHash();

                        // Check if result returned by query is null, to avoid nullPointerException

                        if(hash_cursor!=null)
                        {
                            // If there exists a hash then store the hashcode inside the global variable

                            if(hash_cursor.getCount()>0)
                            {
                                hash_cursor.moveToFirst();
                                device_user_hash = hash_cursor.getString(hash_cursor.getColumnIndex("hashcode_latest"));

                            }

                        }

                        hash_cursor.close();

//                        if(!device_tl_hash.contentEquals(server_user_db_hash))
//                        {
//
//                            sync_required = true;
//
//                        }else
//                        {
//                            sync_required = false;
//
//                        }

                   // }

                }



                //new versionCheck().execute();
//
//                if(!version.equalsIgnoreCase(currentversion))
//                {
//                    //Toast.makeText(ArmsLogInActivity.this,"Please update ....new application version is available" ,Toast.LENGTH_LONG ).show();
//                    //Toast.makeText(ArmsLogInActivity.this,"New updates are available! Please go to the settings section once you log in." ,Toast.LENGTH_LONG ).show();
//                    //Toast.makeText(ArmsLogInActivity.this,"New updates are available! Please go to the settings section once you log in." ,Toast.LENGTH_LONG ).show();
//                    createCustomToast("এপ্লিকেশনটি আপডেট করা জরুরি!। Settings এ গিয়ে Update বোতামটি চাপুন", Color.RED, true);
//                }

            } else
            {
                internetStatus.setTextColor(Color.RED);
                internetStatus.setText("Unavailable");
            }





            // TODO: Add a Listview for households
        }
    }

    /*
    public void performAllNetworkOperations(){
    	try{
    		this.HouseholdJSONString = ArmsNetworkHelper.retrieveJSONstringFromServer(ArmsConfig.RMS_JSON_CONTROLLER_STAFF,staff_id.toString());
    		System.out.println("RDG String: "+ HouseholdJSONString);
    	}catch(Exception e){


    	}
    	if(HouseholdJSONString!=null)
    	{
	    	houseHoldDB.onOpen(houseHoldDB.getWritableDatabase());

	    	JSONArray jArray = null;

			//int ITEM_ID = 0;
			try{
			      jArray = new JSONArray(HouseholdJSONString);
			      JSONObject json_data=null;

			      for(int i=0;i<jArray.length();++i)
			      {
			    	  	json_data = jArray.getJSONObject(i);
			    	  	houseHoldDB.addToStaffTable(i+1,
								  json_data.getString("id"),
								  json_data.getString("username"),
								  "",
								  "",
								  json_data.getString("version_no"));
			      }


		          houseHoldDB.close();

		          //System.out.println("family_id:(" + ITEM_ID + ") --- ");
			}
			catch(NullPointerException e){
				e.printStackTrace();
			}
			catch(JSONException e){
				e.printStackTrace();
			}
			catch (ParseException e) {
			      e.printStackTrace();
			}
    	}



    }
*/
    class versionCheck extends AsyncTask {
        ProgressDialog d;

        String app_version_from_server;

        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            d = ProgressDialog.show(DeviceStatusActivity.this, "fetching date", "please wait");
            d.setCancelable(false);
        }

        protected Object doInBackground(Object... params) {
            // TODO Auto-generated method stub

            JSONArray jArray = null;
            try {

                 app_version_from_server  = ArmsNetworkHelper.Updatecheck(baseURL+"/version");

                //System.out.println("family_id:(" + ITEM_ID + ") --- ");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Object result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            d.dismiss();


            if(!app_version_from_server.contentEquals(app_version_name))
            {
                update.setEnabled(true);
                update.setBackgroundResource(R.drawable.os_entry_button_bg);
                update.setTextColor(getResources().getColor(android.R.color.white));
            }

        }

    }

/*
    private class SetAllResources extends AsyncTask<Void , Void , Void> {
		 ProgressDialog d ;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			d = ProgressDialog.show(ArmsLogInActivity.this,"fetching date", "please wait");
		}

		protected Void doInBackground(Void... params) {
			performAllNetworkOperations();
			return null;
		}


		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			d.dismiss();
			Intent i = new Intent(getApplicationContext(), AmadeyrArmsActivity.class);
        	i.putExtra("staff_id", staff_id.toString());
        	i.putExtra("current_version", "");
        	i.putExtra("new_version","");
        	i.putExtra("email_id", email);
        	i.putExtra("password", password);
        	i.putExtra("user_id", user_id);

        	startActivity(i);
        	finish();
			// TODO: Add a Listview for households
		}
	}


    protected void onDestroy() {
    	try{
	        if (houseHoldDB != null) {
	            houseHoldDB.close();
	        }
    	}catch (Exception e) {
			// TODO: handle exception
		}
        super.onDestroy();

    }

    */


    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            startActivity(new Intent(DeviceStatusActivity.this,APCPreferencesActivity.class));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
