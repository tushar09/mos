package com.amadeyr.amadeyrsalesrepresentative;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderEntries;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.amadeyr.amadeyrsalesrepresentative.tasks.AsyncAutoSubmitDataTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalTime;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by acl on 5/12/15.
 */
public class OrderScreenActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener
{

    //private EditText edtTextEmployeeID;
    private String username,user_token, post_url;
    private Button btnScan, btnExpand;
    private ListView lvOrderEntries;
    private com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo pInfo;
    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCProductDatabaseHelper apsProductDBHelper, asrUserDBHelper;
    private APCDatabaseHelper apsOrderDBHelper;
    private Location mCurrentLocation;
    private APCSimpleCursorAdapter apcCursorAdapter;
    private APSOrderScreenCursorAdapter apsOrderCursorAdapter;
    private APCCustomAutoCompleteView productAutoComplete;
    private SharedPreferences sp;
    private EditText edtTextDESCount,edtTextDESComment, edtTextDESBarcode, edtTextDeliveryDate, edtTextOSQty, edtTextOSOffer, edtTextOSBonus, edtTextOSComments;
    private TextView txtViewOSTradeName, txtViewProductQty, txtViewProductPrice, txtViewProductOffer, txtViewProductLastOrder, txtViewOrderNo, txtViewOSSubTotal, txtViewOSRunningTotal, txtViewOSCommision, txtViewOSTax, txtViewBarTradeName, txtViewBarDate, txtViewProductSortTag;
    private Spinner spnrDESTag;
    private String[] productArray;
    public  String trade_name, customer_id, customer_name, product_barcode, entry_id, order_id, product_id,product_name, order_comments,todays_date, delivery_date, order_date, employee_id,entry_mode, collection_date, invoice_type;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog dp,dp2;
    private Button btnOSAdd, btnDESSubmit, btnDESProceed, btnDESSearch, btnOSFinish;
    public  Menu menu, dateMenu;
    private final String ORDER_STATUS_COMPLETE      = "COMPLETE";
    private final String ORDER_STATUS_INCOMPLETE    = "INCOMPLETE";
    private final String ENTRY_MODE_EDIT            = "EDIT";
    private boolean sort_toggle_on;
    private ImageView imgViewClearAll;
    private PopupMenu datePopUp;

    private double total,total_vat, total_commission, order_qty,order_bonus, main_product_price, main_product_VAT, main_product_commission,start_latitude, start_longitude;
    private ImageView imgViewSearch, imgViewCalendarPicker, imgViewCrossConfirm;

    private APCDateHelper apcDateHelper;
    private DecimalFormat decFormatter;

    private final int REQUEST_CODE                  = 100;
    private final int APC_EDIT_CODE                 = 1;
    private final int APC_REMOVE_CODE               = 2;
    private final int APC_SEARCH_CODE               = 500;
    private final int APC_SEARCH_CANCEL_CODE        = 600;
    private final int APC_ADD_CODE                  = 3;

    // Initializing variables for location services


    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private android.support.v7.app.ActionBar bar;
    //private final String networkURL = "http://192.168.1.115/ACL_Automation/index.php/control/changeswitchstatus";
    private String networkURL; // = "http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_screen);


        //View dateV = findViewById(R.id.action_delivery_date);

        //registerForContextMenu(dateV);

        sort_toggle_on = false;

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }


        dateHelper              = new APCDateHelper();
        dateFormatter           = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        decFormatter            = new DecimalFormat("0.00");
        employee_id             = getIntent().getStringExtra("employee_id");

        Calendar newCalendar    = Calendar.getInstance();
        todays_date             = dateFormatter.format(newCalendar.getTime());
        collection_date         = "";
        invoice_type            = OrderInfo.INVOICE_TYPE_CASH;

        sp                      = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        username                = sp.getString("username", "nouser");

        post_url                = sp.getString("postURL","");
        username                = employee_id;
        user_token              = sp.getString("user_token", "");

        customer_id             = getIntent().getStringExtra("customer_id");
        trade_name              = getIntent().getStringExtra("trade_name");
        entry_mode              = getIntent().getStringExtra("mode");

        txtViewBarTradeName     = (TextView) findViewById(R.id.txtViewOSBarTradeName);
        txtViewBarDate          = (TextView) findViewById(R.id.txtViewOSBarCurrentDate);
        txtViewProductSortTag   = (TextView) findViewById(R.id.txtViewTagTitle);

        prepareOrderActionBar();

        //txtViewBarTradeName.setText(trade_name);
        //txtViewBarDate.setText(todays_date);

        edtTextOSQty            = (EditText) findViewById(R.id.edtTextOSProductQty);

        edtTextOSQty.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                int result = i & EditorInfo.IME_MASK_ACTION;

                if (result == EditorInfo.IME_ACTION_DONE || result == EditorInfo.IME_ACTION_GO || result == EditorInfo.IME_ACTION_SEND)
                {

                    if(edtTextOSQty.getText().toString().isEmpty())
                    {

                        edtTextOSQty.setError("Please enter quantity");

                    }else if(Integer.parseInt(edtTextOSQty.getText().toString())==0)
                    {
                        edtTextOSQty.setError("Please enter quantity greater than 0");

                    }else
                    {
                        //edtTextOSComments.requestFocus();
                        saveToDatabase(entry_mode);
                        clearEntryFields();
                        productAutoComplete.setEnabled(true);
                        disableEntryFields();
                       // return true;









                    }


//                    saveToDatabase();
//                    //edtTextDESBarcode.requestFocus();
//                    clearEntryFields();
//                    disableEntryFields();
                    return true;

                }


                return false;
            }
        });

       // edtTextOSBonus          = (EditText) findViewById(R.id.edtTextBonus);
        //edtTextOSOffer          = (EditText) findViewById(R.id.edtTextOSOffer);

        edtTextOSComments       = (EditText) findViewById(R.id.edtTextOSComments);

        edtTextOSComments.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                int result = i & EditorInfo.IME_MASK_ACTION;

                if (result==EditorInfo.IME_ACTION_NEXT || result == EditorInfo.IME_ACTION_DONE || result == EditorInfo.IME_ACTION_GO || result == EditorInfo.IME_ACTION_SEND)
                {
                    if(edtTextOSQty.getText().length()>0)
                    {
                        saveToDatabase(entry_mode);
                        //edtTextDESBarcode.requestFocus();
                        clearEntryFields();
                        productAutoComplete.setEnabled(true);
                        disableEntryFields();
                        return true;

                    }

                }


                return false;
            }
        });

        //edtTextDeliveryDate     = (EditText) findViewById(R.id.edtTextDeliveryDate);

        txtViewOSTradeName      = (TextView) findViewById(R.id.txtViewOSTradeName);
        //txtViewProductQty       = (TextView) findViewById(R.id.txtViewProductQty);
        //txtViewProductPrice     = (TextView) findViewById(R.id.txtViewProductPrice);
        //txtViewProductOffer     = (TextView) findViewById(R.id.txtViewProductOffers);
        //txtViewProductLastOrder = (TextView) findViewById(R.id.txtViewProductLastOrder);
        //txtViewOrderNo          = (TextView) findViewById(R.id.txtViewOSOrderNo);
        txtViewOSSubTotal       = (TextView) findViewById(R.id.txtViewOSSubtotal);
        //txtViewOSTax             = (TextView) findViewById(R.id.txtViewOSTax);
        //txtViewOSCommision      = (TextView) findViewById(R.id.txtViewOSCommission);
        //txtViewOSRunningTotal   = (TextView) findViewById(R.id.txtViewOSRunningTotal);

        //imgViewCalendarPicker   = (ImageView) findViewById(R.id.imgViewCalendarPicker);
        lvOrderEntries          = (ListView) findViewById(R.id.lstViewOrderEntries);


        btnExpand               = (Button) findViewById(R.id.btnOSExpand);
        btnOSAdd                = (Button) findViewById(R.id.btnOSAdd);
        btnOSFinish             = (Button) findViewById(R.id.btnOSFinish);

        imgViewClearAll         = (ImageView) findViewById(R.id.imgViewClearProductSearchField);

       // imgViewCrossConfirm     = (ImageView) findViewById(R.id.imgViewCrossConfirm);


       // productAutoComplete     = (APCCustomAutoCompleteView) findViewById(R.id.actProductSearch);

        productAutoComplete     = (APCCustomAutoCompleteView) findViewById(R.id.actProductSearch);

        productAutoComplete.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));
        productAutoComplete.setFocusable(true);


        productAutoComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);

                }
            }
        });

        productAutoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });



        edtTextOSQty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);

                }
            }
        });

        edtTextOSQty.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });


        edtTextOSComments.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus && btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);

                }
            }
        });

        edtTextOSComments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (btnOSAdd.getVisibility() == View.GONE) {
                    btnOSAdd.setVisibility(View.VISIBLE);
                    btnOSFinish.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });


        imgViewClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearEntryFields();
                disableEntryFields();
                productAutoComplete.setEnabled(true);
                productAutoComplete.requestFocus();

            }
        });


        disableEntryFields();

        //apsProductDBHelper              = new APCProductDatabaseHelper(getApplicationContext(),"apsProductDB",null,APCProductDatabaseHelper.PD_DATABASE_VERSION);
        apsProductDBHelper              = ASRDatabaseFactory.getProductsDBInstance(getApplicationContext());
        //asrUserDBHelper                 = new APCProductDatabaseHelper(getApplicationContext(),APCProductDatabaseHelper.USER_DATABASE_NAME,null,APCProductDatabaseHelper.USER_DATABASE_VERSION);
        asrUserDBHelper                 = ASRDatabaseFactory.getUserDBInstance(getApplicationContext());
        //apsOrderDBHelper                = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        apsOrderDBHelper                = APCDatabaseHelper.getInstance(getApplicationContext());


        //fetching old orders or creating new

        if(entry_mode.contentEquals(ENTRY_MODE_EDIT))
        {
              order_id                      = getIntent().getStringExtra("order_no");

        }else
        {
            String lastIncompleteOrderNo    = apsOrderDBHelper.getLastIncompleteOrderNo(customer_id);

            if(!lastIncompleteOrderNo.contentEquals(""))
            {
                order_id                    = lastIncompleteOrderNo;

            }else
            {
                int last_order_id           = apsOrderDBHelper.getLastInsertID();
                order_id                    = username+customer_id+ String.format("%04d",last_order_id);

            }

        }



        DateTime jDt                    = new DateTime(newCalendar.getTime());
        System.out.println("JODA Time: "+jDt.getDayOfMonth());



        //System.out.println("JODA Time: " + jDt.getDayOfMonth());
        //jDt = jDt.plusDays(2);

        delivery_date = dateFormatter.format(jDt.toDate().getTime());



        order_date                      = dateHelper.getCurrentDateStr();
        total                           = 0;
        total_vat                       = 0;
        total_commission                = 0;
        main_product_price              = 0;
        main_product_VAT                = 0;
        main_product_commission         = 0;
        final Cursor orderCursor        = apsOrderDBHelper.getOrderEntries(order_id);
        apsOrderCursorAdapter           = new APSOrderScreenCursorAdapter(getApplicationContext(),R.layout.os_row_layout,orderCursor,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        int yyyy = jDt.getYear();
        int mm   = jDt.getMonthOfYear()-1;
        int dd   = jDt.getDayOfMonth();


        if(orderCursor!=null)
        {
            if(orderCursor.getCount()>0)
            {
                orderCursor.moveToFirst();
                total = orderCursor.getDouble(orderCursor.getColumnIndex("total"));
                total_vat = orderCursor.getDouble(orderCursor.getColumnIndex("total_vat"));
                total_commission = orderCursor.getDouble(orderCursor.getColumnIndex("total_commission"));
                delivery_date = orderCursor.getString(orderCursor.getColumnIndex("delivery_date"));
                try {
                    jDt = new DateTime(dateHelper.getDateObjFromStringConventional(delivery_date).getTime());

                    yyyy = jDt.getYear();
                    mm   = jDt.getMonthOfYear()-1;
                    dd   = jDt.getDayOfMonth();

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //order_date  = orderCursor.getString(orderCursor.getColumnIndex("order_date"));
            }
        }




        dp                    = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i2, int i3)
            {
                Calendar newDate  = Calendar.getInstance();
                newDate.set(i, i2, i3);

                delivery_date = dateFormatter.format(newDate.getTime());
                apsOrderDBHelper.updateDeliveryDateIfExists(order_id,delivery_date);

            }

        },yyyy,mm,dd);

        //dp.getDatePicker().setMinDate(System.currentTimeMillis()-1000);




        dp2                    = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i2, int i3)
            {
                Calendar newDate  = Calendar.getInstance();
                newDate.set(i,i2,i3);

                collection_date = dateFormatter.format(newDate.getTime());
                apsOrderDBHelper.updateCollectionDateIfExists(order_id, collection_date);

            }

        },yyyy,mm,dd);

        //dp2.getDatePicker().setMinDate(System.currentTimeMillis()-1000);


        lvOrderEntries.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                Cursor tmpCursor = (Cursor) lvOrderEntries.getAdapter().getItem(i);
                //String entryStatus          = tmpCursor.getString(tmpCursor.getColumnIndex("entry_status"));

                int entry_id = tmpCursor.getInt(tmpCursor.getColumnIndex("_id"));
                double qty = tmpCursor.getDouble(tmpCursor.getColumnIndex("qty"));
                double price = tmpCursor.getDouble(tmpCursor.getColumnIndex("price"));
                double vat = 0;
                double commission = 0;

                //String offer                = tmpCursor.getString(tmpCursor.getColumnIndex("offer"));
                String product_name     = tmpCursor.getString(tmpCursor.getColumnIndex("product_name"));
                String bonus            = tmpCursor.getString(tmpCursor.getColumnIndex("bonus"));
                String comment          = tmpCursor.getString(tmpCursor.getColumnIndex("comments"));

                Intent openDialog = new Intent(getApplicationContext(), APCEditEntryDialogActivity.class);

                openDialog.putExtra("_id", entry_id);
                openDialog.putExtra("mode", APC_EDIT_CODE);
                openDialog.putExtra("order_no", order_id);
                openDialog.putExtra("product_name", product_name);
                openDialog.putExtra("qty", qty);
                openDialog.putExtra("price", price);
                openDialog.putExtra("bonus", bonus);
                openDialog.putExtra("comments", comment);

                startActivityForResult(openDialog, REQUEST_CODE);

                return true;

            }


        });



        lvOrderEntries.setAdapter(apsOrderCursorAdapter);
        lvOrderEntries.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });



        txtViewOSTradeName.setText(trade_name);
       // txtViewOrderNo.setText(order_id);
        //double tax               = total * (0.15);
        //final double commision         = total * (0.05);



        txtViewOSSubTotal.setText(""+decFormatter.format(total));
//        txtViewOSTax.setText(""+tax);
//        txtViewOSCommision.setText(""+commision);
//        txtViewOSRunningTotal.setText(total+tax-commision+"");

        // Sets focus on the search box and pop up the soft keyboard



//        if(productAutoComplete.requestFocus())
//        {
//            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//
//        }



//        btnExpand.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LinearLayout expandHolder = (LinearLayout) findViewById(R.id.llExpandedInfoHolder);
//                if (expandHolder.getVisibility() == View.VISIBLE) {
//
//                    expandHolder.setVisibility(View.GONE);
//
//                } else if (expandHolder.getVisibility() == View.GONE) {
//                    expandHolder.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        btnOSAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //saveToDatabase();
                //clearEntryFields();
                //disableEntryFields();
                hideKeyboard();
                btnOSAdd.setVisibility(View.GONE);
                btnOSFinish.setVisibility(View.VISIBLE);

            }
        });

        final DatePickerDialog dp3;

        dp3                    = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i2, int i3)
            {
                Calendar newDate  = Calendar.getInstance();
                newDate.set(i,i2,i3);

                collection_date = dateFormatter.format(newDate.getTime());
                apsOrderDBHelper.updateCollectionDateIfExists(order_id, collection_date);
                btnOSFinish.performClick();

            }

        },yyyy,mm,dd);

        btnOSFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(collection_date.isEmpty())
                {


                    dp3.setTitle("No collection date !");
                    //dp3.getDatePicker().setMinDate(System.currentTimeMillis()-10000);
                    dp3.show();

                }else {

                    int order_item_count = lvOrderEntries.getAdapter().getCount();


                    if (order_item_count == 0)
                    {
                        ASRErrorMessageView noEntryError = new ASRErrorMessageView(OrderScreenActivity.this);
                        noEntryError.showErrorMessage(getString(R.string.errorMessageNoProductTitle), getString(R.string.errorMessageNoProduct));
                        //Toast.makeText(getApplicationContext(), "Cannot Finish ! This order is empty !", Toast.LENGTH_LONG).show();

                    } else
                    {

                        final Dialog orderConfirmationScreen = new Dialog(OrderScreenActivity.this);
                        orderConfirmationScreen.setContentView(R.layout.confirm_order_screen);


                        TextView confirmOrderDDate = (TextView) orderConfirmationScreen.findViewById(R.id.orderDeliveryDate);
                        TextView confirmCollectionDDate = (TextView) orderConfirmationScreen.findViewById(R.id.orderCollectionDate);
                        TextView confirmOrderSubtotal = (TextView) orderConfirmationScreen.findViewById(R.id.orderSubTotal);
                        TextView confirmOrderTotalItems = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalItems);
                        TextView confirmOrderTax = (TextView) orderConfirmationScreen.findViewById(R.id.orderTaxAmount);
                        TextView confirmOrderCommission = (TextView) orderConfirmationScreen.findViewById(R.id.orderCommissionAmount);
                        TextView confirmOrderTotal = (TextView) orderConfirmationScreen.findViewById(R.id.orderTotalAmount);
                        final EditText eTRemarks          = (EditText) orderConfirmationScreen.findViewById(R.id.eTConfirmNotes);
                        final RadioButton rbCash              = (RadioButton) orderConfirmationScreen.findViewById(R.id.invCash);
                        final RadioButton rbCredit      = (RadioButton) orderConfirmationScreen.findViewById(R.id.invCredit);
                        Button btnConfirmOrderYes = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderYes);
                        Button btnConfirmOrderNo = (Button) orderConfirmationScreen.findViewById(R.id.btnConfirmOrderNo);

                        confirmOrderDDate.setText(delivery_date);
                        confirmCollectionDDate.setText(collection_date);
                        confirmOrderSubtotal.setText("" + total);
                        confirmOrderTotalItems.setText("" + order_item_count);

                        //double tax          = 0.15 * total;
                        //                    double commission   = 0.05 * total;
                        double finalTotal = total + total_vat - total_commission;

                        confirmOrderTax.setText("" + decFormatter.format(total_vat));
                        confirmOrderCommission.setText("" + decFormatter.format(total_commission));
                        confirmOrderTotal.setText("" + decFormatter.format(finalTotal));

                        btnConfirmOrderYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                recordLocation();
                                stopLocationUpdates();

                                apsOrderDBHelper.updateCompleteStatus(order_id, ORDER_STATUS_COMPLETE);
                                String oRemarks     =  eTRemarks.getText().toString();
                                String inv_type     = "";
                                if(!oRemarks.isEmpty()) apsOrderDBHelper.addRemarks(order_id,oRemarks);
                                if(rbCredit.isChecked()) apsOrderDBHelper.updateInvoiceType(order_id, OrderInfo.INVOICE_TYPE_CREDIT);

                                //new AsyncAutoSubmitDataTask("ORDERS",post_url,getApplicationContext(),start_latitude,start_longitude).execute(user_token);
                                //Intent i = new Intent(OrderScreenActivity.this, APSCustomerSelectionActivity.class);
                                //i.putExtra("employee_id", employee_id);
                                //startActivity(i);
                                orderConfirmationScreen.dismiss();

                                onBackPressed();
                                finish();

                            }
                        });

                        btnConfirmOrderNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                orderConfirmationScreen.dismiss();
                            }
                        });


                        orderConfirmationScreen.setTitle("Confirm Order");
                        orderConfirmationScreen.show();


                        //apsOrderDBHelper.updateCompleteStatus(order_id,ORDER_STATUS_COMPLETE);
                        //Intent i = new Intent(OrderScreenActivity.this, APSCustomerSelectionActivity.class);
                        //startActivity(i);
                        //finish();
                    }
                }
            }
        });

        productAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                String searchItem = productAutoComplete.getText().toString();

                boolean entered = apsOrderDBHelper.entryExistsInOrder(order_id, searchItem);

                if (entered)
                {

                    new ASRErrorMessageView(OrderScreenActivity.this).showErrorMessage(getResources().getString(R.string.errorDuplicateEntryTitle), getResources().getString(R.string.errorDuplicateEntryMessage));


                    //Toast.makeText(getApplicationContext(), "You have already entered this product once !", Toast.LENGTH_LONG).show();

                } else
                {
                    enableEntryFields();
                    productAutoComplete.setEnabled(false);
                    Cursor result = apsProductDBHelper.getProductInfoFromNameOrBarcode(searchItem);

                    if (result != null) {
                        result.moveToFirst();
                        product_id = result.getString(result.getColumnIndex("product_id"));
                        double product_qty = 0;
                        //double product_qty = result.getDouble(result.getColumnIndex("stock_qty"));
                        double product_price = result.getDouble(result.getColumnIndex("price"));
                        double product_vat = result.getDouble(result.getColumnIndex("vat"));
                        double product_commission = result.getDouble(result.getColumnIndex("commission"));
                        main_product_price = product_price;
                        main_product_VAT = product_vat;
                        main_product_commission = product_commission;
                        String product_offer = "";
                        //String product_offer = result.getString(result.getColumnIndex("offer"));
                        //String order_date = apsOrderDBHelper.getLastOrderDate(product_id, customer_id);

//                        int no_of_days_since_prev_order = 0;
//
//                        try {
//                            Date last_order_date = dateHelper.getDateObjFromString(order_date);
//                            Date current_date = dateHelper.getCurrentDateObj();
//
//                            Days days = Days.daysBetween(new DateTime(last_order_date), new DateTime(current_date));
//                            no_of_days_since_prev_order = days.getDays();
//
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }


                        MenuItem qtyMenu    = menu.findItem(R.id.stock_qty);
                        MenuItem offerMenu  = menu.findItem(R.id.action_offer);
                        MenuItem priceMenu  = menu.findItem(R.id.action_price);
                        MenuItem daysAgo    = menu.findItem(R.id.action_last_order);

                        qtyMenu.setTitle("Stock Qty : " + product_qty);
                        offerMenu.setTitle("Offer : " + product_offer);
                        priceMenu.setTitle("Price : BDT " + product_price);
                        //daysAgo.setTitle("Last Order : " + no_of_days_since_prev_order + " days ago");


                        //txtViewProductQty.setText("" + product_qty);
                        //txtViewProductPrice.setText("" + product_price);
                        //txtViewProductOffer.setText(product_offer);


                        //btnPSSOk.setEnabled(true);

                    }
                }


            }
        });



        txtViewProductSortTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sort_toggle_on)
                {

                    Cursor sortCursor = apsOrderDBHelper.getSortedOrderEntries(order_id,"_id","DESC");
                    apsOrderCursorAdapter.swapCursor(sortCursor);
                    apsOrderCursorAdapter.notifyDataSetChanged();
                    sort_toggle_on = false;

                } else if (!sort_toggle_on)
                {
                    Cursor sortCursor = apsOrderDBHelper.getSortedOrderEntries(order_id,"product_name","ASC");

                    apsOrderCursorAdapter.swapCursor(sortCursor);
                    apsOrderCursorAdapter.notifyDataSetChanged();

                    sort_toggle_on = true;

                }

            }
        });


        populateSearchAdapter("");

        // Location recording tasks


    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                                .addConnectionCallbacks(this)
                                .addOnConnectionFailedListener(this)
                                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {

        Location tLocation  = null;

        mLastLocation       = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mCurrentLocation==null)
        {

            tLocation = mLastLocation;

        }else
        {


            tLocation = mCurrentLocation;

        }


        if(tLocation!=null)
        {
            start_latitude  = tLocation.getLatitude();
            start_longitude = tLocation.getLongitude();

        }



    }

    private void hideKeyboard()
    {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null)
        {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void prepareOrderActionBar()
    {

        bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        //bar.setCustomView(R.layout.os_action_bar_layout);
        //bar.setTitle(trade_name);
        LayoutInflater mInflater    = LayoutInflater.from(this);
        View mCustomView            = mInflater.inflate(R.layout.custom_action_bar_layout, null);
        TextView title              = (TextView) mCustomView.findViewById(R.id.tvABTitle);
        TextView subTitle           = (TextView) mCustomView.findViewById(R.id.tvABSubtitle);


        title.setText(trade_name);
        subTitle.setText("Taken by: "+username);

        bar.setCustomView(mCustomView);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayShowCustomEnabled(true);
        bar.show();
    }

    public void saveToDatabase(String mode)
    {

        boolean entry_status        = apsOrderDBHelper.orderExists(order_id,mode);
        String oQty                 = edtTextOSQty.getText().toString();
        //String oBonus           = edtTextOSBonus.getText().toString();
        String oBonus               = "";
        order_qty                   = Double.parseDouble(oQty.contentEquals("") ? "0":oQty);
        order_bonus                 = Double.parseDouble(oBonus.contentEquals("") ? "0":oBonus);

        order_comments              = edtTextOSComments.getText().toString();

        //double productPrice     = Double.parseDouble(txtViewProduct   Price.getText().toString());
        double productPrice         = main_product_price;
        double productVAT           = main_product_VAT;
        double productCommission    = main_product_commission;
        product_name                = productAutoComplete.getText().toString();

        OrderEntries entry          = new OrderEntries(order_id,product_id,product_name,order_qty,productPrice,productVAT,productCommission,order_bonus,order_comments);


        total                       += order_qty * productPrice;
        total_vat                   += productPrice *order_qty*(productVAT/100);
        total_commission            += productPrice *order_qty*(productCommission/100);

        //delivery_date               = todays_date;


        if(!entry_status)
        {
            if(ArmsNetworkHelper.getGPSStatus(getApplicationContext()))
            {

                recordLocation();
                DateTime jDt                    = new DateTime();
                LocalTime j                     = jDt.toLocalTime();
                String strTime                  = j.toString("HH:mm:ss");
                //String strTime                  = j.getHourOfDay()+":"+j.getMinuteOfHour()+":"+j.getSecondOfMinute();

                OrderInfo orderEntry = new OrderInfo(order_id,customer_id,trade_name,delivery_date,this.order_date,total,ORDER_STATUS_INCOMPLETE,start_latitude,start_longitude,0,0,employee_id,collection_date,invoice_type);
                orderEntry.setOrderTime(strTime);

                System.out.println("Collection Date: " + orderEntry.getCollectionDate());

                apsOrderDBHelper.insertOrder(orderEntry);

                jDt         = null;
                j           = null;

            }else
            {
                final ASRErrorMessageView am = new ASRErrorMessageView(OrderScreenActivity.this);
                am.addExtraAction(getResources().getString(R.string.lblBtnTurnOn));
                am.showErrorMessage(getResources().getString(R.string.errorMsgTitleLocationDisabled),getResources().getString(R.string.errorMsgBodyLocationDisabled));
                Button gpsEnabledButton = am.getActionButton2Instance();
                gpsEnabledButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        am.closeScreen();
                    }
                });

                return;

            }





        }else
        {
            apsOrderDBHelper.updateOrderTotal(order_id,total,total_vat,total_commission);
        }

        apsOrderDBHelper.insertOrderEntry(entry);

        Cursor updatedCursor     = apsOrderDBHelper.getOrderEntries(order_id);

        apsOrderCursorAdapter.swapCursor(updatedCursor);
        apsOrderCursorAdapter.notifyDataSetChanged();

       // double tax               = total * (0.15);
       // double commision         = total * (0.05);

        main_product_price       = 0;
        main_product_commission  = 0;
        main_product_VAT         = 0;

        MenuItem qtyMenu        = menu.findItem(R.id.stock_qty);
        MenuItem offerMenu      = menu.findItem(R.id.action_offer);
        MenuItem priceMenu      = menu.findItem(R.id.action_price);
        MenuItem daysAgo        = menu.findItem(R.id.action_last_order);

        qtyMenu.setTitle("Stock Qty : -- ");
        offerMenu.setTitle("Offer : --");
        priceMenu.setTitle("Price : --");
        daysAgo.setTitle("Last Order : --");


        txtViewOSSubTotal.setText(""+decFormatter.format(total));
        //txtViewOSTax.setText(""+tax);
        //txtViewOSCommision.setText(""+commision);
        //txtViewOSRunningTotal.setText(total+tax+commision+"");
        lvOrderEntries.smoothScrollToPosition(0);

        this.sort_toggle_on = false;


    }

    public void populateSearchAdapter(String searchItem)
    {
        List<ProductInfo> pInfo = apsProductDBHelper.getAutoCompleteProductInfo(searchItem);
        productArray            = new String[pInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < pInfo.size();++index)
        {

            productArray[index]        = new String(pInfo.get(index).getProductName());

        }


        ArrayAdapter adapter            = new ArrayAdapter(OrderScreenActivity.this, android.R.layout.simple_list_item_1, productArray);

        productAutoComplete.setAdapter(adapter);
        productAutoComplete.setThreshold(1);



    }
//

    public void enableEntryFields()
    {
        edtTextOSQty.requestFocus();
        edtTextOSQty.setEnabled(true);
       // edtTextOSBonus.setEnabled(true);
       // edtTextOSOffer.setEnabled(true);
        edtTextOSComments.setEnabled(true);
        btnExpand.setEnabled(true);
        //btnOSAdd.setEnabled(true);
    }

    public void disableEntryFields()
    {

        productAutoComplete.requestFocus();
        edtTextOSQty.setFocusable(true);
        edtTextOSQty.setEnabled(false);
//        edtTextOSBonus.setEnabled(false);
      //  edtTextOSOffer.setEnabled(false);

        edtTextOSComments.setEnabled(false);
        btnExpand.setEnabled(false);
        //btnOSAdd.setEnabled(false);


    }


    public void clearEntryFields()
    {
        productAutoComplete.setText("");
        edtTextOSQty.setText("");
      //  edtTextOSBonus.setText("");
    //    edtTextOSOffer.setText("");
        edtTextOSComments.setText("");
       // txtViewProductLastOrder.setText("");
      //  txtViewProductQty.setText("");
       // txtViewProductOffer.setText("");
        //txtViewProductPrice.setText("");

    }


    public void updateListUI()
    {
        Cursor updatedProductEntries    = apsOrderDBHelper.getOrderEntries(order_id);
        apsOrderCursorAdapter.swapCursor(updatedProductEntries);
        apsOrderCursorAdapter.notifyDataSetChanged();
        //lvOrderEntries.setSelection(apcCursorAdapter.getCount()-1);

    }
    public void updateTotalData()
    {

        double tax               = total * (0.15);
        double commision         = total * (0.05);
        txtViewOSSubTotal.setText("" + decFormatter.format(total));
//        txtViewOSTax.setText(""+tax);
//        txtViewOSCommision.setText(""+commision);
//        txtViewOSRunningTotal.setText(total+tax+commision+"");
        lvOrderEntries.smoothScrollToPosition(0);
    }

    public void removeItemFromList(int entry_id)
    {
        apsOrderDBHelper.removeEntry(entry_id);
        updateListUI();

    }
//
    public void editItemInList(int entry_id, String order_no, String comments, double qty, double bonus)
    {
        apsOrderDBHelper.editEntry(entry_id, order_no, comments, qty, bonus);
        //Cursor updatedProductEntries    = dbHelper.getEntryForProduct(product_barcode);
        Cursor updatedProductEntries    = apsOrderDBHelper.getOrderEntries(order_no);
        apsOrderCursorAdapter.swapCursor(updatedProductEntries);
        apsOrderCursorAdapter.notifyDataSetChanged();

    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

//
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if(resultCode==APC_REMOVE_CODE)
        {
            int entry_id                = data.getIntExtra("_id",0);
            double prevTotal            = data.getDoubleExtra("prevTotal", 0);
            double prevVAT              = data.getDoubleExtra("prevVAT",0);
            double prevCommission       = data.getDoubleExtra("prevCommission",0);

            Toast.makeText(getApplicationContext(), "Removed Product ID: " + entry_id, Toast.LENGTH_LONG).show();

            if(entry_id>0)
            {
                removeItemFromList(entry_id);

            }

            total -= prevTotal;
            total_vat -= prevVAT;
            total_commission -= prevCommission;
            apsOrderDBHelper.updateOrderTotal(order_id,total,total_vat,total_commission);
            updateTotalData();


        }else if(resultCode==APC_ADD_CODE)
        {
            boolean entry_status    = apsOrderDBHelper.orderExists(order_id,"");
            //String oQty             = data.getStringExtra("qty");
            //String oBonus           = data.getStringExtra("bonus");
            order_qty               = data.getDoubleExtra("qty",0);
            order_bonus             = data.getDoubleExtra("bonus",0);
            order_comments          = data.getStringExtra("comments");

           // double productPrice     = Double.parseDouble(txtViewProductPrice.getText().toString());double productPrice     = Double.parseDouble(txtViewProductPrice.getText().toString());
            double productPrice     = main_product_price;
            double productVAT       = main_product_VAT;
            double productCommission = main_product_commission;
            product_name            = productAutoComplete.getText().toString();

            OrderEntries entry      = new OrderEntries(order_id,product_id,product_name,order_qty,productPrice,productVAT, productCommission,order_bonus,order_comments);

            total                   += order_qty * productPrice;
            total_vat               += total * (productVAT/100);
            total_commission        += total * (productCommission/100);
            delivery_date           = todays_date;


            if(!entry_status)
            {
                OrderInfo orderEntry = new OrderInfo(order_id,customer_id,trade_name,delivery_date,todays_date,total,ORDER_STATUS_INCOMPLETE,start_latitude,start_longitude,0,0,employee_id,collection_date,invoice_type);


                apsOrderDBHelper.insertOrder(orderEntry);

            }else
            {
                apsOrderDBHelper.updateOrderTotal(order_id,total,total_vat,total_commission);
            }



            apsOrderDBHelper.insertOrderEntry(entry);

            Cursor updatedCursor     = apsOrderDBHelper.getOrderEntries(order_id);

            apsOrderCursorAdapter.swapCursor(updatedCursor);
            apsOrderCursorAdapter.notifyDataSetChanged();

            double tax               = total * (0.15);
            double commision         = total * (0.05);




//            txtViewOSSubTotal.setText(""+total);
//            txtViewOSTax.setText(""+tax);
//            txtViewOSCommision.setText(""+commision);
//            txtViewOSRunningTotal.setText(total+tax+commision+"");
//            lvOrderEntries.smoothScrollToPosition(0);

            apsOrderDBHelper.updateOrderTotal(order_id, total, total_vat, total_commission);
            //apsOrderDBHelper.updateOrderTotalVatCommission(order_id,total_vat,total_commission);

            updateTotalData();
            clearEntryFields();
            disableEntryFields();


        }else if(resultCode==APC_EDIT_CODE)
        {
            int entry_id            = data.getIntExtra("_id", 0);
            double prevTotal        = data.getDoubleExtra("prevTotal",0);
            double currentTotal     = data.getDoubleExtra("currentTotal",0);
            String resultComment    = data.getStringExtra("comments");
            double editQty          = data.getDoubleExtra("qty", 0.0);
            double bonustQty        = data.getDoubleExtra("bonus", 0.0);
            String edit_order_no    = data.getStringExtra("order_no");

            if(entry_id>0)
            {
                editItemInList(entry_id,edit_order_no,resultComment,editQty,bonustQty);

            }

            total += currentTotal;
            total -= prevTotal;

            apsOrderDBHelper.updateOrderTotal(order_id,total,total_vat,total_commission);
            updateTotalData();

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_screen_menu_item, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {

            return true;

        }else if (id == R.id.action_due_view)
        {

        }else if (id == R.id.action_delivery_date)
        {


            datePopUp = new PopupMenu(OrderScreenActivity.this, findViewById(id));
            datePopUp.inflate(R.menu.date_context_menu);

//            Menu dDate = datePopUp.getMenu();
//
//            MenuItem mi_d_date = dDate.findItem(R.id.delivery_date_picker);
//            MenuItem mi_c_date = dDate.findItem(R.id.collection_date_picker);




            datePopUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int id2 = item.getItemId();

                    if (id2 == R.id.delivery_date_picker)
                    {
                        //Toast.makeText(getApplicationContext(), "Delivery Date", Toast.LENGTH_SHORT).show();
                        dp.setTitle("Select delivery date");
                        //dp.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                        dp.show();

                    } else if (id2 == R.id.collection_date_picker)
                    {
                        dp2.setTitle("Select collection date");
                        //dp2.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                        dp2.show();
                    }

                    Menu dDate          = datePopUp.getMenu();
                    MenuItem mi_d_date  = dDate.findItem(R.id.delivery_date_picker);
                    mi_d_date.setTitle(delivery_date);


                    return false;
                }
            });

            datePopUp.show();
            //dp.setTitle("Select delivery date");
            //dp.show();

        }else if(id == android.R.id.home)
        {

            finish();
            onBackPressed();
            //NavUtils.navigateUpFromSameTask(this);
            return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {


        //Toast.makeText(getApplicationContext(),"Back pressed", Toast.LENGTH_SHORT).show();

//        if(btnOSAdd.getVisibility()==View.VISIBLE)
//        {
//            btnOSAdd.setVisibility(View.GONE);
//            btnOSFinish.setVisibility(View.VISIBLE);
//            return;
//        }

        super.onBackPressed();
        stopLocationUpdates();


    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();


    }

    @Override
    public void onConnected(Bundle bundle)
    {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        recordLocation();


    }

    protected void createLocationRequest() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setSmallestDisplacement(0);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }



    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }



    public void stopLocationUpdates()
    {

        if(mGoogleApiClient!=null)
        {

            if(mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }

        }



    }

    @Override
    public void onLocationChanged(Location location) {

        if(location!=null)
        {

            mCurrentLocation = location;
            recordLocation();


        }

    }

}