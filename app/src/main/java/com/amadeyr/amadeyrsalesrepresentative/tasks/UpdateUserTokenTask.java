package com.amadeyr.amadeyrsalesrepresentative.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDateHelper;
import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;

import org.apache.http.HttpException;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by amadeyr on 12/22/15.
 */
public class UpdateUserTokenTask
{
    private Context ctx;
    private ProgressDialog pd;
    private SharedPreferences sp;
    private String url;
    private String username,password, last_log_in_date;
    private APCDateHelper dtHelper;

    public UpdateUserTokenTask(Context appCtx, ProgressDialog progressD)
    {
        dtHelper  = new APCDateHelper();
        ctx = appCtx;
        pd  = progressD;
        sp  = ctx.getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        url = sp.getString("postURL","")+ ASRConfig.LOG_IN_EXTENSION;
        username = sp.getString("username","");
        password = sp.getString("password","");
        last_log_in_date = sp.getString("last_log_in_date","");

    }

    public boolean didTokenDurationExpire()
    {
        Date last_login_date;
        last_login_date            = null;

        try
        {
            //todays_date             = dtHelper.getCurrentDateObj();
            last_login_date         = dtHelper.getDateObjFromString(last_log_in_date);

        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        DateTime today          = new DateTime();
        DateTime lastLogInDate  = new DateTime(last_login_date);
        //lastLogInDate           = lastLogInDate.minusDays(10);
        Duration duration       = new Duration(lastLogInDate,today);


        int timeSpan = (int) duration.getStandardDays();

        if(timeSpan>4) return true;


        return false;
    }

    public boolean getNewUserToken()
    {
//        pd.setMessage("Retrieving new token ...");
//        pd.show();
        if(!didTokenDurationExpire()) return false;

//        Date last_login_date;
//        last_login_date            = null;
//
//        try
//        {
//            //todays_date             = dtHelper.getCurrentDateObj();
//            last_login_date         = dtHelper.getDateObjFromString(last_log_in_date);
//
//        } catch (ParseException e)
//        {
//            e.printStackTrace();
//        }
//
//        DateTime today          = new DateTime();
//        DateTime lastLogInDate  = new DateTime(last_login_date);
//        Duration duration       = new Duration(lastLogInDate,today);
//
//
//        int timeSpan = (int) duration.getStandardDays();
//
//        System.out.println("Token is "+timeSpan+" days old.");
//
//
//
//        if(timeSpan<4)
//        {
//           return false;
//
//        }

        if(!username.isEmpty() && !password.isEmpty())
        {
            try
            {
                String resultJSON  = getInfoWithUrl(url);

                if(resultJSON.isEmpty()) return false;

                saveUserToken(resultJSON);
               // pd.dismiss();
                return true;

            }catch (IOException e)
            {
               // pd.dismiss();
                return false;

            }


        }else
        {

            //pd.dismiss();
            Toast.makeText(ctx,"No username-password found in cache. Log Out and Log In again!",Toast.LENGTH_LONG).show();

        }

        return false;

    }


    private String getInfoWithUrl(String serverUrl) throws IOException
    {


        HashMap<String,String> postParams = new HashMap<>();
        postParams.put("email", username);
        postParams.put("password",password);
        //postParams.put("dbHashCode",dbHashCode);

        URL url = null;

        //boolean status = false;
        String response="";


        try {


            url = new URL(this.url);

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
            //conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
            writer.write(getFormattedPostDataString(postParams));
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK)
            {
                String dataLine;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while ((dataLine=br.readLine())!=null)
                {
                    response += dataLine;
                }


            }else
            {
                response = "";
                //status = false;
                throw new HttpException(responseCode+"");
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }


        return response;


    }

    private String getFormattedPostDataString(HashMap<String,String> postParameters)
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for(Map.Entry<String,String> entry: postParameters.entrySet())
        {
            if(first)
                first = false;
            else
                result.append("&");

            try {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(),"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

        return result.toString();

        //while(postParameters)


    }

    private void saveUserToken(String tokenJSON)
    {

       // pd.dismiss();



        SharedPreferences.Editor editor = sp.edit();


        //editor.commit();

        try {
            //s = "{\"token\":\"e8710013980281321\"}";

            if(!tokenJSON.isEmpty())
            {
                JSONObject jResponse = new JSONObject(tokenJSON);

                if(jResponse.has("token"))
                {
                    editor.putString("user_token", jResponse.getString("token"));
                    editor.putString("last_log_in_date",dtHelper.getCurrentDateStr());
                    editor.commit();

                    System.out.println("Token updated successfully");

                }else if(jResponse.has("error"))
                {
                    String errorStr = jResponse.getString("error");
                    System.out.println("Log In Error: "+errorStr);

                    Toast.makeText(ctx,"Unable to re-sign !",Toast.LENGTH_LONG).show();

                }

            }else
            {

                Toast.makeText(ctx,"No JSON found for token !",Toast.LENGTH_LONG).show();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }



        System.out.println("Server Response: "+tokenJSON);

    }

}
