package com.amadeyr.amadeyrsalesrepresentative.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.ASRErrorMessageView;
import com.google.android.gms.analytics.ecommerce.Product;

import org.apache.http.HttpException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by amadeyr on 12/6/15.
 */
public class AsyncDownloadAppUpdateTask extends AsyncTask
{
    private Context context;
    private ProgressDialog pd;
    private String user_token, DATA_FETCH_TYPE;
    public final static String FETCH_TYPE_FILE     = "FILE";
    public final static String FETCH_TYPE_DL_URL   = "URL";

    private  String app_new_version;
    private String downloadBaseURL;
    private final String LOG_TAG = "AsyncDownloadAppUpdateTask";

    public AsyncDownloadAppUpdateTask(Context context, ProgressDialog pd, String user_token, String app_new_version, String DATA_FETCH_TYPE, String url)
    {
        SharedPreferences sp    = context.getSharedPreferences(ASRConfig.DEFAULT_PREFERENCES_NAME,Context.MODE_PRIVATE);
        this.context 		    = context;
        this.pd                 = pd;
        this.user_token         = user_token;
        this.app_new_version    = app_new_version;
        this.DATA_FETCH_TYPE    = DATA_FETCH_TYPE;
        //this.downloadBaseURL    = sp.getString("postURL","");
        //this.downloadBaseURL    = "http://192.168.1.132/asp_web";

        this.downloadBaseURL    = url;

    }

    private String getFormattedPostDataString(HashMap<String,String> postParameters)
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for(Map.Entry<String,String> entry: postParameters.entrySet())
        {
            if(first)
                first = false;
            else
                result.append("&");

            try {
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(),"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


        }

        return result.toString();

        //while(postParameters)


    }

    private boolean getConnectionStatus()
    {
        ConnectivityManager connMngr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo            = connMngr.getActiveNetworkInfo();


        if(nInfo != null && nInfo.isConnected())
        {
            //Toast.makeText(context,"No Internet Connection Available",Toast.LENGTH_LONG).show();
            return true;
        }

        return false;


    }


    // Returns the app download url when requested with parameters

    private String getAppDownloadURL(HashMap<String,String> postParameters)
    {

        if(!getConnectionStatus()) return "";

        StringBuilder response = new StringBuilder();
        HttpURLConnection urlConnection  = null;
        InputStream is;

        try {

            URL url         = new URL(this.downloadBaseURL+ASRConfig.APP_UPDATE_URL_EXTENSION);
            urlConnection   = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.writeBytes(getFormattedPostDataString(postParameters));
            wr.flush();
            wr.close();

            is = urlConnection.getInputStream();

            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            String line;

            while((line=rd.readLine())!=null)
            {
                response.append(line);
            }



        } catch (MalformedURLException e)
        {
            e.printStackTrace();
            return "";

        } catch (IOException e)
        {
            e.printStackTrace();
            return "";

        }finally
        {
            if(urlConnection!=null) urlConnection.disconnect();


        }


        return response.toString();
    }



    private boolean downloadAndInstallUpdate(HashMap<String,String> postParameters) throws IOException
    {
        FileOutputStream fos 	= null;
        FileInputStream fis  	= null;
        InputStream input 		= null;

        try
        {

           // URL url 					= new URL(downloadBaseURL+ASRConfig.APP_UPDATE_URL_EXTENSION+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
            URL url 					= new URL(downloadBaseURL+ASRConfig.APP_UPDATE_URL_EXTENSION+"?token="+user_token);
            //File destinationFolderRoot 	= new File(Environment.getExternalStorageDirectory()+"/"+ASRConfig.DOWNLOAD_DIRECTORY_NAME);
            File desinationFolder 		= new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),ASRConfig.DOWNLOAD_DIRECTORY_NAME);

//            if(!destinationFolderRoot.exists())
//            {
//                destinationFolderRoot.mkdir();
//
//            }

            boolean a = false;
            String  b = "";

            if(!desinationFolder.exists())
            {

                a = desinationFolder.mkdirs();
                //if(a){};
            }

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setConnectTimeout(ASRConfig.CONNECTION_TIMEOUT_VALUE);
            //conn.setRequestMethod("POST");
            //conn.setDoInput(true);
            //conn.setDoOutput(true);

//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
//            writer.write(getFormattedPostDataString(postParameters));
//            writer.flush();
//            writer.close();
//            os.close();

            int responseCode = conn.getResponseCode();

            if(responseCode != HttpURLConnection.HTTP_FORBIDDEN)
            {
                int read;

                byte [] data = new byte[1024];

                input   = conn.getInputStream();
                fos	    = new FileOutputStream(desinationFolder+"/"+ASRConfig.DOWNLOAD_FILE_NAME);

                while ((read=input.read(data))!=-1)
                {
                    fos.write(data,0,read);
                }

                return true;
            }



//            URLConnection conn 		= url.openConnection();
//
//            input 					= conn.getInputStream();
//
//            fos						= new FileOutputStream(desinationFolder+"/"+ASRConfig.DOWNLOAD_FILE_NAME);
//
//            Log.d(LOG_TAG, "Url DL Path" + url.getPath());
//
//            int read;
//            byte [] data = new byte[1024];
//
//            while((read=input.read(data))!=-1)
//            {
//
//                Log.d(LOG_TAG,"Data: "+read);
//                fos.write(data,0,read);
//
//            }

            return false;

        } catch (MalformedURLException e)
        {
            e.printStackTrace();

        } catch (IOException e)
        {
            //new ASRErrorMessageView(pd.getOwnerActivity()).showErrorMessage("File Error","Problem at creating files/directories for downloading updates.");
            e.printStackTrace();

        }finally
        {
            if(fis!=null) fis.close();
            if(input!=null) input.close();


        }



        return false;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd.setMessage("Downloading the new version of the app. Please wait ... ");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected Object doInBackground(Object[] params)
    {
        HashMap<String,String> postParams = new HashMap<>();
        postParams.put("user_token",this.user_token);

        try
        {
           if(this.DATA_FETCH_TYPE.contentEquals(this.FETCH_TYPE_DL_URL))
           {
               return getAppDownloadURL(postParams);

           }else if(this.DATA_FETCH_TYPE.contentEquals(this.FETCH_TYPE_FILE))
           {
               return downloadAndInstallUpdate(postParams);

           }


        } catch (IOException e)
        {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    protected void onPostExecute(Object o)
    {
        super.onPostExecute(o);

        pd.dismiss();


        if(this.DATA_FETCH_TYPE.contentEquals(this.FETCH_TYPE_DL_URL))
        {

            String result = (String) o;


            if(result.isEmpty())
            {
                //Toast.makeText(context,"Could not fetch download URL",Toast.LENGTH_LONG).show();

            }else
            {

                new AsyncDownloadAppUpdateTask(context,pd,this.user_token,this.app_new_version,FETCH_TYPE_FILE,result).execute();


            }

        }else if(this.DATA_FETCH_TYPE.contentEquals(this.FETCH_TYPE_FILE))
        {
            boolean status = (boolean) o;

            if(status)
            {
                File desinationFolder 		= new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),ASRConfig.DOWNLOAD_DIRECTORY_NAME);
                File appAPK                 = new File(desinationFolder,ASRConfig.DOWNLOAD_FILE_NAME);
                Intent i                    = new Intent(Intent.ACTION_VIEW);

                i.setDataAndType(Uri.fromFile(appAPK),"application/vnd.android.package-archive");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                //pd.getOwnerActivity().finish();

            }else
            {
               // Toast.makeText(context,"Download failed !",Toast.LENGTH_LONG).show();
            }


        }






    }
}
