package com.amadeyr.amadeyrsalesrepresentative.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;
import com.amadeyr.amadeyrsalesrepresentative.managers.ASRLocationManager;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRLocationHolder;

/**
 * Created by acl on 2/18/16.
 */
public class AsyncAutoAttendanceUpdaterTask extends AsyncTask
{

    private APCDatabaseHelper dbHelper;
    private String              postURL;
    private Context appCtx;
    private String entry_id;

    public AsyncAutoAttendanceUpdaterTask(Context ctx, String entry_id)
    {
        appCtx               = ctx;
        this.entry_id        = entry_id;
        dbHelper             = APCDatabaseHelper.getInstance(ctx);
    }


    @Override
    protected Object doInBackground(Object[] params)
    {
        ASRLocationHolder geo_coordinates = new ASRLocationHolder();

        ASRLocationManager alm = new ASRLocationManager(appCtx);
        //alm.setAppContext(appCtx);
        int retry_count        = 0;

        while(retry_count<3)
        {

            if(alm.getLocationReadyStatus())
            {
                geo_coordinates.setLatitude(alm.getLatitude());
                geo_coordinates.setLongitude(alm.getLongitude());
                alm.stopLocationUpdates();
                return geo_coordinates;

            }else
            {

                try
                {
                    Thread.sleep(5000);
                    retry_count++;
                } catch (InterruptedException e)
                {

                    e.printStackTrace();
                }

            }


        }

        alm.stopLocationUpdates();

        return geo_coordinates;
    }

    @Override
    protected void onPostExecute(Object o) {

        ASRLocationHolder result_coordinates = (ASRLocationHolder) o;

        dbHelper.updateAttendanceEntryLocationsWithEntryID(entry_id, result_coordinates.getLatitude(), result_coordinates.getLongitude());

        super.onPostExecute(o);
    }
}
