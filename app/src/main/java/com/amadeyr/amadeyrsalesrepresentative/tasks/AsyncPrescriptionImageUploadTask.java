package com.amadeyr.amadeyrsalesrepresentative.tasks;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.content.ContentBody;
//import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by acl on 2/16/16.
 */
public class AsyncPrescriptionImageUploadTask extends AsyncTask
{
    private APCDatabaseHelper   dbHelper;
    private String              postURL;
    private Context appCtx;
    public AsyncPrescriptionImageUploadTask(Context ctx)
    {
        SharedPreferences sp = ctx.getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        postURL              = sp.getString("postURL","")+ ASRConfig.PRESCRIPTION_IMAGE_SUBMISSION_EXT;
        dbHelper             = APCDatabaseHelper.getInstance(ctx);
        appCtx               = ctx.getApplicationContext();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(appCtx,"Thread astarted",Toast.LENGTH_LONG).show();

    }

    @Override
    protected Object doInBackground(Object[] params) {

        Cursor c    = dbHelper.getAllPendingPrescriptionImages();
        Log.v("Total pending images", ""+c.getCount());

        if(c!=null)
        {
            c.moveToFirst();

            while(!c.isAfterLast())
            {
                String file_path            = c.getString(c.getColumnIndex("image_name"));
                Log.v("Upload Image Path", file_path);
                try
                {
                    boolean upload_status   = postImageWithFileName(file_path);

                    if(upload_status)
                    {
                        String image_entry_id = c.getString(c.getColumnIndex("_id"));
                        dbHelper.updatePrescriptionImageStatus(image_entry_id);
                        removeImageFile(file_path);

                    }

                    c.moveToNext();

                } catch (IOException e)
                {
                    c.moveToNext();
                    e.printStackTrace();
                }



            }


        }

        return null;
    }


    private void removeImageFile(String path)
    {
        File f = new File(path);
        f.delete();

    }




    @TargetApi(Build.VERSION_CODES.KITKAT)

    private boolean postImageWithFileName(String file_path) throws IOException
    {

        String file_name            = file_path.substring(file_path.lastIndexOf("/")+1,file_path.length()-4);
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inJustDecodeBounds = true;
        options.inSampleSize       = 3;

        Bitmap bm                   = BitmapFactory.decodeFile(file_path,options);
        ByteArrayOutputStream  bos  = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.PNG,0,bos);

        HttpClient httpclient       = new DefaultHttpClient();
        HttpPost httppost           =  new HttpPost(this.postURL);

        InputStream in              = new ByteArrayInputStream(bos.toByteArray());
        ContentBody img             = new InputStreamBody(in,"image/jpeg",file_name);
        MultipartEntity mpEntity    = new MultipartEntity();

        mpEntity.addPart("prescription_image", img);

        httppost.setEntity(mpEntity);
        HttpResponse response       = httpclient.execute(httppost);

        int status_code             = response.getStatusLine().getStatusCode();

        String sb = EntityUtils.toString(response.getEntity());

        Log.v("Upload Status",""+status_code);
        //Log.v("Upload Response", EntityUtils.toString(response.getEntity()));

        String TAG = "Upload Response";

        if (sb.length() > 4000)
        {
                        Log.v(TAG, "sb.length = " + sb.length());

            int chunkCount = sb.length() / 4000;     // integer division

                        for (int i = 0; i <= chunkCount; i++) {
                            int max = 4000 * (i + 1);
                            if (max >= sb.length())
                            {
                                Log.v(TAG, sb.substring(4000 * i));
                            } else {
                                Log.v(TAG, sb.substring(4000 * i, max));
                            }
                        }

        } else {

            Log.v(TAG, sb.toString());
        }


        if(status_code!=200)
        {
            return false;
        }

        httpclient.getConnectionManager().shutdown();

        return true;
        //httpclient.getConnectionManager().


    }
}
