package com.amadeyr.amadeyrsalesrepresentative.tasks;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.amadeyr.amadeyrsalesrepresentative.APCDatabaseHelper;
import com.amadeyr.amadeyrsalesrepresentative.ASRConfig;
import com.amadeyr.amadeyrsalesrepresentative.ArmsNetworkHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

/**
 * Created by amadeyr on 1/3/16.
 */
public class AsyncAutoSubmitDataTask extends AsyncTask {

    private APCDatabaseHelper OrderDBHelper;
    private String submissionDataType;
    private String networkURL;
    private Context appCtx;
    private List<String> submitted_order_ids, submitted_visit_ids;
    private double latitude, longitude;

    public AsyncAutoSubmitDataTask(String type, String url, Context ctx, double lat, double lon)
    {
        this.submissionDataType = new String (type);
        submitted_order_ids     = new ArrayList<>();
        submitted_visit_ids     = new ArrayList<>();
        OrderDBHelper           = APCDatabaseHelper.getInstance(ctx);
        networkURL              = url;
        appCtx                  = ctx;
        latitude                = lat;
        longitude               = lon;
    }


    private String prepareJSON(Cursor pendingSubmissions) throws JSONException {

        if (pendingSubmissions != null) pendingSubmissions.moveToFirst();

        JSONArray dataArray             = new JSONArray();

        while (!pendingSubmissions.isAfterLast())
        {

            JSONObject dataForSubmission    = new JSONObject();
            String post_order_no            = pendingSubmissions.getString(pendingSubmissions.getColumnIndex("order_no"));
            //String order_taken_by           = post_order_no.substring(0,7);

            String order_taken_by           = pendingSubmissions.getString(pendingSubmissions.getColumnIndex("taken_by"));

            dataForSubmission.put("order_no", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("order_no")));
            dataForSubmission.put("customer_id", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("customer_id")));
            dataForSubmission.put("order_date", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("order_date")));
            dataForSubmission.put("delivery_date", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("delivery_date")));
            dataForSubmission.put("collection_date", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("collection_date")));
            dataForSubmission.put("order_time", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("order_time")));
            dataForSubmission.put("total_tp", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("total")));
            dataForSubmission.put("order_location_lat", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("start_lat")));
            dataForSubmission.put("order_location_lon", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("start_lon")));
            dataForSubmission.put("submitted_location_lat", latitude);
            dataForSubmission.put("submitted_location_lon", longitude);
            dataForSubmission.put("remarks", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("remarks")));
            dataForSubmission.put("invoice_type", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("invoice_type")));
            dataForSubmission.put("taken_by",order_taken_by);
            submitted_order_ids.add(post_order_no);


            Cursor order_entries = OrderDBHelper.getOrderDetailsEntries(post_order_no);

            JSONArray detailsArray = new JSONArray();

            if (order_entries != null) {
                order_entries.moveToFirst();

                while (!order_entries.isAfterLast())
                {
                    JSONObject dataEntries = new JSONObject();

                    dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
                    dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
                    dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));

                    dataEntries.put("remarks", order_entries.getString(order_entries.getColumnIndex("comments")));

                    detailsArray.put(dataEntries);
                    dataEntries = null;
                    order_entries.moveToNext();
                }
            }

            dataForSubmission.put("order_details", detailsArray.toString());

            dataArray.put(dataForSubmission);


            detailsArray = null;

            pendingSubmissions.moveToNext();
        }


        return dataArray.toString();
    }

    private String prepareVisitsJSON(Cursor pendingSubmissions) throws JSONException {

        if (pendingSubmissions != null) pendingSubmissions.moveToFirst();

        JSONArray dataArray             = new JSONArray();

        while (!pendingSubmissions.isAfterLast())
        {

            JSONObject dataForSubmission    = new JSONObject();

            String post_order_no = pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visit_id"));
            //String order_taken_by = post_order_no.substring(0,7);
            String order_taken_by   = pendingSubmissions.getString(pendingSubmissions.getColumnIndex("taken_by"));

            dataForSubmission.put("visit_id", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visit_id")));
            dataForSubmission.put("doctor_code", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("doctor_code")));
            dataForSubmission.put("visit_date", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visit_date")));
            dataForSubmission.put("visit_time", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visit_time")));
            dataForSubmission.put("visit_location_lat", pendingSubmissions.getDouble(pendingSubmissions.getColumnIndex("visit_start_lat")));
            dataForSubmission.put("visit_location_lon", pendingSubmissions.getDouble(pendingSubmissions.getColumnIndex("visit_start_lon")));
            dataForSubmission.put("remarks", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visit_remarks")));
            dataForSubmission.put("visited_with", pendingSubmissions.getString(pendingSubmissions.getColumnIndex("visited_with")));
            dataForSubmission.put("submitted_location_lat", latitude);
            dataForSubmission.put("submitted_location_lon", longitude);
            dataForSubmission.put("taken_by",order_taken_by);

            submitted_visit_ids.add(post_order_no);

            Cursor order_entries = OrderDBHelper.getVisitDetailsEntries(post_order_no);

            JSONArray detailsArray = new JSONArray();

            if (order_entries != null)
            {
                order_entries.moveToFirst();

                while (!order_entries.isAfterLast())
                {
                    JSONObject dataEntries = new JSONObject();

                    dataEntries.put("visit_id", order_entries.getString(order_entries.getColumnIndex("visit_id")));
                    dataEntries.put("item_code", order_entries.getString(order_entries.getColumnIndex("item_code")));
                    dataEntries.put("category_name", order_entries.getString(order_entries.getColumnIndex("category_name")));
                    dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
                    dataEntries.put("comments", order_entries.getString(order_entries.getColumnIndex("comments")));


                    detailsArray.put(dataEntries);

                    dataEntries = null;
                    order_entries.moveToNext();
                }
            }

            dataForSubmission.put("visit_details", detailsArray.toString());
            dataArray.put(dataForSubmission);

            detailsArray = null;
            pendingSubmissions.moveToNext();
        }


        return dataArray.toString();
    }


    private String submitDataToServer(String type, String dataArray, String user_token) throws IOException {

        //dataArray = "Hello Saad !";
        System.out.println("Submit pack: " + dataArray);

        if(type.contentEquals("ORDERS"))
        {
            networkURL += ASRConfig.ORDER_SAVE_EXTENSION+"?token="+user_token;

            Log.i("Order Post URL: ", networkURL);

        }else if(type.contentEquals("VISITS"))
        {
            networkURL +=ASRConfig.DOCTOR_VISIT_SAVE_EXTENSION+"?token="+user_token;

            Log.i("Order Visit URL: ", networkURL);

        }




        URL url             = new URL(networkURL);
        HttpURLConnection conn  = (HttpURLConnection)url.openConnection();

        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestProperty("Content-encoding", "deflate");
        conn.setRequestProperty("Content-type", "application/octet-stream");
        DeflaterOutputStream dos1 = new DeflaterOutputStream(conn.getOutputStream());
        dos1.write(dataArray.getBytes());
        dos1.flush();
        dos1.close();

        Log.e("GZip", dos1.toString());
        Log.e("GZip Response Code", ""+conn.getResponseCode());

        BufferedReader in1 = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));

        String decodedString1 = "";
        String response = "";

        while ((decodedString1 = in1.readLine()) != null)
        {
            Log.e("dump",decodedString1);
            if(decodedString1!=null) response = new String(decodedString1);
        }
        in1.close();

        return response;


    }

    @Override
    protected Object doInBackground(Object[] params)
    {

        if(!ArmsNetworkHelper.getNetStatus(appCtx)) return false;

        String user_token           = (String) params[0];

        Cursor pendingSubmissions   = null;
        Cursor pendingVisits        = null;

        int noOfRetry               = 3;

        pendingSubmissions  = OrderDBHelper.getPendingSubmissions("");
        pendingVisits       = OrderDBHelper.getPendingVisits("");

//        if(this.submissionDataType.contentEquals("ORDERS"))
//        {
//
//            pendingSubmissions = OrderDBHelper.getPendingSubmissions("");
//
//        }else if(this.submissionDataType.contentEquals("VISITS"))
//        {
//
//            pendingSubmissions = OrderDBHelper.getPendingVisits("");
//
//        }



        try
        {
            String orderDataToSubmit = "";
            String visitDataToSubmit = "";

            orderDataToSubmit   = prepareJSON(pendingSubmissions);
            visitDataToSubmit   = prepareVisitsJSON(pendingVisits);

//            if(this.submissionDataType.contentEquals("ORDERS"))
//            {
//                dataToSubmit = prepareJSON(pendingSubmissions);
//
//            }else if(this.submissionDataType.contentEquals("VISITS"))            {
//
//                dataToSubmit = prepareVisitsJSON(pendingSubmissions);
//
//            }

           // for(int i=0;i<noOfRetry;++i)
           // {
                String server_response  = submitDataToServer("ORDERS",orderDataToSubmit,user_token);

                if(server_response.contentEquals("success"))
                {

                //    for(int j=0;j<noOfRetry;++j)
                //    {
                        String visit_response = submitDataToServer("VISITS",visitDataToSubmit,user_token);

                        if(visit_response.contentEquals("success"))
                        {
                            return true;

                        }

//                        else
//                        {
//                            try
//                            {
//
//                                Thread.sleep(10000);
//
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//                        }

                    }


                //}

//                else
//                {
//                    try
//                    {
//
//                        Thread.sleep(10000);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                }



            //}



        } catch (JSONException e)
        {
            e.printStackTrace();
            return false;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        return false;
    }


    @Override
    protected void onPostExecute(Object o) {

        super.onPostExecute(o);
        boolean status = (boolean) o;

        if(status)
        {

            //if(this.submissionDataType.contentEquals("ORDERS"))
            if(!submitted_order_ids.isEmpty())
            {
                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                {
                    OrderDBHelper.updateStatusToSubmitWithEntryID(submitted_order_ids.get(s_it));
                    OrderDBHelper.updateSubmitLocation(submitted_order_ids.get(s_it),latitude,longitude);
                }

                submitted_order_ids.clear();

            }

            if(!submitted_visit_ids.isEmpty())
            {

                for (int s_it=0; s_it<submitted_visit_ids.size();++s_it)
                {
                    OrderDBHelper.updateVisitStatusToSubmitWithEntryID(submitted_visit_ids.get(s_it));
                    OrderDBHelper.updateVisitSubmitLocation(submitted_visit_ids.get(s_it), latitude, longitude);
                }

                submitted_visit_ids.clear();
            }


        }

    }
}
