package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.models.OrderInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;

import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by acl on 5/17/15.
 */
public class APCPendingSubmissionsActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{

    //private EditText edtTextEmployeeID;

    private ListView lvPreviousEntries;
    private ProductInfo pInfo;
    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private APCCustomAutoCompleteView actSearchField;
    private APCOrderSCursorAdapter apcCursorAdapter;
    private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;

    private String product_name, product_id, product_barcode, userId;
    private ImageView imgViewClearAll;

    private Button btnSearch, btnSubmit, btnSubmitAll, btnRemove;


    private final int REQUEST_CODE      = 100;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;

    private double latitude, longitude;

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] orderArray;

    private HashMap<String,String> productNameMapper;

    private String screenTitle = "Submissions";

    private final String DATA_SUBMISSION_TYPE_SELECTED  = "SELECTED";
    private final String DATA_SUBMISSION_TYPE_ALL       = "ALL";
    private final String ENTRY_MODE_EDIT            = "EDIT";

    private Cursor lastSubmitted;

    public ArrayList <String> selectedForSubmission;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public ProgressDialog pd ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_submissions);

        if(checkPlayServices())
        {
            buildGoogleApiClient();
        }

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle(screenTitle);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        selectedForSubmission = new ArrayList<>();


        SharedPreferences sp    = getSharedPreferences("apcpref",Context.MODE_PRIVATE);
        pd                      = new ProgressDialog(this);
        getURL                  = sp.getString("getURL","")+"/autodata/description";
        //postURL                 = sp.getString("postURL","")+"/index.php/asp_api/postdata";
        postURL                 = sp.getString("postURL","")+"/orders/store";

        userId                  = "MYM-122";

        apcDateHelper           = new APCDateHelper();


        actSearchField          = (APCCustomAutoCompleteView) findViewById(R.id.apsPSSearch);

        actSearchField.addTextChangedListener(new CustomAutoCompleteTextChangedListener(this));
        actSearchField.setThreshold(1);
        actSearchField.setFocusable(true);

        lvPreviousEntries       = (ListView) findViewById(R.id.lstViewAPSSubmissions);
//        txtViewPSSProductID      = (TextView) findViewById(R.id.txtViewPSSProductID);
//        txtViewPSSProductName    = (TextView) findViewById(R.id.txtViewPSSProductName);
//        txtViewPSSProductBarcode = (TextView) findViewById(R.id.txtViewPSSProductBarcode);


        //dbHelper                = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper                = APCDatabaseHelper.getInstance(getApplicationContext());

        Cursor orderEntries     = dbHelper.getPendingSubmissions(userId);
        apcCursorAdapter        = new APCOrderSCursorAdapter(getApplicationContext(),R.layout.pending_row_layout,orderEntries,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        lvPreviousEntries.setAdapter(apcCursorAdapter);
       // btnSubmit               = (Button)   findViewById(R.id.btnPSSubmit);
        btnSearch               = (Button)   findViewById(R.id.btnPSSearchShowAll);
        btnSubmit               = (Button)   findViewById(R.id.btnSendSelected);
        btnSubmitAll            = (Button)   findViewById(R.id.btnPSSubmitAll);
        btnRemove               = (Button)   findViewById(R.id.btnPSDeleteAll);
        imgViewClearAll         = (ImageView) findViewById(R.id.imgViewPSSearchClearButton);


        actSearchField.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String searchKey = actSearchField.getText().toString();
                Cursor searchCursor = dbHelper.getEntryForName(searchKey);
                apcCursorAdapter.swapCursor(searchCursor);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });


        actSearchField.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                actSearchField.setText("");
                return true;
            }
        });



        lvPreviousEntries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "Item clicked!", Toast.LENGTH_SHORT).show();

                Cursor itemInfo = (Cursor) lvPreviousEntries.getAdapter().getItem(position);

                if (itemInfo != null) {

                    Intent editScreenStarter = new Intent(APCPendingSubmissionsActivity.this, OrderScreenActivity.class);

                    editScreenStarter.putExtra("mode", ENTRY_MODE_EDIT);
                    editScreenStarter.putExtra("order_no", itemInfo.getString(itemInfo.getColumnIndex("order_no")));
                    editScreenStarter.putExtra("customer_id", itemInfo.getString(itemInfo.getColumnIndex("customer_id")));
                    editScreenStarter.putExtra("trade_name", itemInfo.getString(itemInfo.getColumnIndex("customer_name")));
                    editScreenStarter.putExtra("employee_id", itemInfo.getString(itemInfo.getColumnIndex("taken_by")));

                    startActivity(editScreenStarter);


                }


            }
        });
        //btnPSSOk.setEnabled(false);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                APCOrderSCursorAdapter ap   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
                selectedForSubmission       = ap.getSelectedListOfOrders();

                //Toast.makeText(getApplicationContext(),selectedForSubmission.size()+"",Toast.LENGTH_SHORT).show();

                try {

                    postDataToServer(DATA_SUBMISSION_TYPE_SELECTED);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Cursor currentPending = apcCursorAdapter.getCursor();s
                //lastSubmitted = currentPending;
                //try {


                    //postDataToServer(null);
//                    Cursor newCursor = dbHelper.getPendingSubmissions();
//                    apcCursorAdapter.swapCursor(newCursor);
//                    lvPreviousEntries.setAdapter(apcCursorAdapter);
                  //  actSearchField.setText("");
                //} catch (JSONException e) {
                //    e.printStackTrace();
               // }

            }
        });

        btnSubmitAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Cursor currentPending = apcCursorAdapter.getCursor();
                //lastSubmitted = currentPending;
                try
                {
                    postDataToServer(DATA_SUBMISSION_TYPE_ALL);
                    Cursor newCursor = dbHelper.getPendingSubmissions(userId);
//                    apcCursorAdapter.swapCursor(newCursor);
//                    lvPreviousEntries.setAdapter(apcCursorAdapter);
                    actSearchField.setText("");
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                final Dialog removalConfirmationScreen = new Dialog(APCPendingSubmissionsActivity.this);
                removalConfirmationScreen.setContentView(R.layout.confirm_order_removal);


                TextView confirmOrderDDate = (TextView) removalConfirmationScreen.findViewById(R.id.txtViewOrderRemovalMessage);

                Button btnConfirmRemovalYes = (Button) removalConfirmationScreen.findViewById(R.id.btnRemoveOrder);
                Button btnConfirmRemovalNo = (Button) removalConfirmationScreen.findViewById(R.id.btnCancelRemoval);

                selectedForSubmission   = apcCursorAdapter.getSelectedListOfOrders();

                confirmOrderDDate.append(selectedForSubmission.size() + " items ?");

                btnConfirmRemovalYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        removeFromSubmissionList();
                        removalConfirmationScreen.dismiss();
                        selectedForSubmission.clear();


                    }
                });

                btnConfirmRemovalNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removalConfirmationScreen.dismiss();
                    }
                });


                removalConfirmationScreen.setTitle("Remove Order");
                removalConfirmationScreen.show();





            }
        });

        imgViewClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actSearchField.setText("");
                Cursor allEntries = dbHelper.getPendingSubmissions(userId);
                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Cursor allEntries = dbHelper.getPendingSubmissions(userId);

                apcCursorAdapter.swapCursor(allEntries);
                apcCursorAdapter.notifyDataSetChanged();


            }


        });





    }

    public void removeFromSubmissionList()
    {

        int sizeOfList = selectedForSubmission.size();
        //JSONObject dataArray = new JSONObject();


        for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
        {
            String selectedOrderID = selectedForSubmission.get(lIterator).toString();
            dbHelper.removeOrder(selectedOrderID);

        }

        selectedForSubmission.clear();

        Cursor updatedList = dbHelper.getPendingSubmissions(userId);
        apcCursorAdapter.swapCursor(updatedList);
        apcCursorAdapter.notifyDataSetChanged();
        actSearchField.setText("");

    }

    public void populateSearchAdapter(String searchItem)
    {
        List<OrderInfo> oInfo   = dbHelper.getAutoCompleteOrderInfoForPending(searchItem);
        orderArray              = new String[oInfo.size()];

        int str_index           = 0;

        for (int index = 0; index < oInfo.size();++index)
        {
            orderArray[index]        = new String(oInfo.get(index).getCustomerName());
            //barcodeArray[str_index + 1]    = new String(pInfo.get(index).getProductName());

            //str_index+=2;

            //System.out.println("SearchActivity: "+jObj.getString("description"));
        }


        ArrayAdapter adapter         = new ArrayAdapter(APCPendingSubmissionsActivity.this, android.R.layout.simple_list_item_1, orderArray);

        actSearchField.setAdapter(adapter);
        actSearchField.setThreshold(1);



    }


    public void populateAPCAutocompleteArray(String searchTerm)
    {
        Cursor currentEntries = null;

        if(searchTerm.equals(""))
        {
            currentEntries = dbHelper.getDistinctEntries();

        }else
        {

            currentEntries  = dbHelper.getOrders(searchTerm);
        }



        if(currentEntries!=null)
        {
            currentEntries.moveToFirst();

            String[] barcodeArray = new String[currentEntries.getCount()];


            int string_arr_index = 0;

            for (int json_arr_index = 0; json_arr_index < currentEntries.getCount();++json_arr_index)
            {
                barcodeArray[json_arr_index]        = currentEntries.getString(currentEntries.getColumnIndex("trade_name"));
                currentEntries.moveToNext();
                //System.out.println("SearchActivity: "+jObj.getString("description"));
            }

            ArrayAdapter adapter = new ArrayAdapter(APCPendingSubmissionsActivity.this, android.R.layout.simple_list_item_1, barcodeArray);
            actSearchField.setAdapter(adapter);


        }

    }

    public void postDataToServer(String sendType) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {

            new AccessAPCServerTask().execute(sendType);

        }else
        {
            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }



    public void postDataToServer(Cursor currentPending) throws JSONException {

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo           = connMgr.getActiveNetworkInfo();

        if(nInfo !=null && nInfo.isConnected())
        {

            new AccessAPCServerTask().execute("","");

        }else
        {
            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_LONG).show();
            Log.d("Network Error", "Connection Unavailable");

        }


    }



    public void refreshTextViews()
    {
//        txtViewPSSProductID.setText("No Data");
//        txtViewPSSProductName.setText("No Data");
//        txtViewPSSProductBarcode.setText("No Data");
//        btnPSSOk.setEnabled(false);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            //NavUtils.navigateUpFromSameTask(this);
            return true;


        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {

                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else
            {

                Toast.makeText(getApplicationContext(),
                        "This device does not support location recording.", Toast.LENGTH_LONG)
                        .show();

            }

            return false;
        }

        return true;

    }

    protected void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void recordLocation()
    {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLastLocation!=null)
        {

            latitude  = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();

//            double latitude  = mLastLocation.getLatitude();
//            double longitude = mLastLocation.getLongitude();

            //Toast.makeText(getApplicationContext(),"Current Location: "+latitude+","+longitude,Toast.LENGTH_SHORT).show();

        }else
        {

            //Toast.makeText(getApplicationContext(),"Location tracking may be disabled.",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (mGoogleApiClient != null)
        {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        Cursor orderEntries     = dbHelper.getPendingSubmissions(userId);
        apcCursorAdapter.swapCursor(orderEntries);
        apcCursorAdapter.notifyDataSetChanged();

    }

    @Override
    public void onConnected(Bundle bundle) {
        recordLocation();
    }

    @Override
    public void onConnectionSuspended(int i)
    {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }



    private class AccessAPCServerTask extends AsyncTask<String,Void,String>
    {

        //private ProgressDialog pd;
        private boolean getAll = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pd.setTitle("Please Wait");
            pd.setMessage("Submitting data to server  ...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... urls)
        {
            String result="";
            String sendType = urls[0];
            recordLocation();

            List<String> submitted_order_ids = new ArrayList<>();

            try
            {

                if(sendType.contentEquals(DATA_SUBMISSION_TYPE_ALL))
                {

                    Cursor currentPending   = apcCursorAdapter.getCursor();
                    lastSubmitted           = currentPending;

                    if (currentPending != null) currentPending.moveToFirst();


                    JSONArray dataArray             = new JSONArray();


//
                    while (!currentPending.isAfterLast())
                    {

                        JSONObject dataForSubmission    = new JSONObject();

                        //String trimmedProductBarcode     = currentPending.getString(currentPending.getColumnIndex("product_barcode"));
                        //trimmedProductBarcode            = trimmedProductBarcode.replace(" ","");
                        //String trimmedProductName        = currentPending.getString(currentPending.getColumnIndex("trade_name")).trim();
                        String post_order_no = currentPending.getString(currentPending.getColumnIndex("order_no"));


                        String order_taken_by = post_order_no.substring(0,7);

                        dataForSubmission.put("order_no", currentPending.getString(currentPending.getColumnIndex("order_no")));
                        dataForSubmission.put("customer_id", currentPending.getString(currentPending.getColumnIndex("customer_id")));
                        dataForSubmission.put("order_date", currentPending.getString(currentPending.getColumnIndex("order_date")));
                        dataForSubmission.put("delivery_date", currentPending.getString(currentPending.getColumnIndex("delivery_date")));
                        dataForSubmission.put("order_time", currentPending.getString(currentPending.getColumnIndex("order_time")));
                        dataForSubmission.put("total_tp", currentPending.getString(currentPending.getColumnIndex("total")));
                        dataForSubmission.put("order_location_lat", currentPending.getString(currentPending.getColumnIndex("start_lat")));
                        dataForSubmission.put("order_location_lon", currentPending.getString(currentPending.getColumnIndex("start_lon")));
                        dataForSubmission.put("submitted_location_lat", latitude);
                        dataForSubmission.put("submitted_location_lon", longitude);

                        dataForSubmission.put("remarks", currentPending.getString(currentPending.getColumnIndex("remarks")));
                        dataForSubmission.put("taken_by",order_taken_by);
                        submitted_order_ids.add(post_order_no);

                        //dataForSubmission.put("submit_lat", currentPending.getString(currentPending.getColumnIndex("submit_lat")));
                        //dataForSubmission.put("submit_lon", currentPending.getString(currentPending.getColumnIndex("submit_lon")));


                        //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));


                        Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);

                        JSONArray detailsArray = new JSONArray();

                        if (order_entries != null) {
                            order_entries.moveToFirst();

                            while (!order_entries.isAfterLast())
                            {
                                JSONObject dataEntries = new JSONObject();

                                //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
                                //dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
                                //dataEntries.put("trade_price", order_entries.getDouble(order_entries.getColumnIndex("price")));
                                dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
                                dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
                                //dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
                                dataEntries.put("remarks", order_entries.getString(order_entries.getColumnIndex("comments")));

                                detailsArray.put(dataEntries);

                                dataEntries = null;
                                order_entries.moveToNext();
                            }
                        }

                        dataForSubmission.put("order_details", detailsArray.toString());

                        dataArray.put(dataForSubmission);


                        detailsArray = null;

                        currentPending.moveToNext();
                    }


                    result = submitInfoToUrl(postURL, dataArray.toString());

                    if (result != null) {

                        JSONArray jArray = null;

                        try {
                            if (result.contentEquals("success")) {
                                //if(lastSubmitted!=null) lastSubmitted.moveToFirst();

                                //while(!lastSubmitted.isAfterLast())
                                //{
                                //    dbHelper.updateStatusToSubmitWithEntryID(lastSubmitted.getString(lastSubmitted.getColumnIndex("order_no")));
                                //    lastSubmitted.moveToNext();


                                //}

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {

                                    dbHelper.updateStatusToSubmitWithEntryID(submitted_order_ids.get(s_it));
                                    dbHelper.updateSubmitLocation(submitted_order_ids.get(s_it),latitude,longitude);
                                }


                                APCOrderSCursorAdapter ap2   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
                                ap2.clearSelectedListOfOrders();
                                selectedForSubmission = null;

                                //dbHelper.updateStatusToSubmitWithEntryID(result);
                                //dbHelper.updateSubmitLocation(result,latitude,longitude);
                                //Cursor productEntries   = dbHelper.getPendingSubmissions();

                                //apcCursorAdapter.swapCursor(productEntries);
                                //apcCursorAdapter.notifyDataSetChanged();


                            }


                        } catch (Exception e) {

                            e.printStackTrace();
                        }


                    }

                    Log.d("APCPendingSubmission", "JSON Created");

                }else if(sendType.contentEquals(DATA_SUBMISSION_TYPE_SELECTED))
                {
                    int sizeOfList = selectedForSubmission.size();
                    //JSONObject dataArray = new JSONObject();

                    JSONArray dataArray = new JSONArray();

                    for(int lIterator = 0; lIterator<sizeOfList; ++lIterator )
                    {
                        String selectedOrderID  = selectedForSubmission.get(lIterator).toString();
                        Cursor orderList        = dbHelper.getOrderByID(selectedOrderID);

                        if(orderList!=null)
                        {
                            orderList.moveToFirst();


                            //JSONObject dataForSubmission = new JSONObject();
                            JSONObject dataForSubmission = new JSONObject();

                            String post_order_no = orderList.getString(orderList.getColumnIndex("order_no"));
                            String order_taken_by = post_order_no.substring(0, 7);

                            dataForSubmission.put("order_no", orderList.getString(orderList.getColumnIndex("order_no")));
                            dataForSubmission.put("customer_id", orderList.getString(orderList.getColumnIndex("customer_id")));
                            dataForSubmission.put("order_date", orderList.getString(orderList.getColumnIndex("order_date")));
                            dataForSubmission.put("delivery_date", orderList.getString(orderList.getColumnIndex("delivery_date")));
                            dataForSubmission.put("order_time", orderList.getString(orderList.getColumnIndex("order_time")));
                            dataForSubmission.put("total_tp", orderList.getString(orderList.getColumnIndex("total")));
                            dataForSubmission.put("order_location_lat", orderList.getDouble(orderList.getColumnIndex("start_lat")));
                            dataForSubmission.put("order_location_lon", orderList.getDouble(orderList.getColumnIndex("start_lon")));
                            dataForSubmission.put("remarks", orderList.getString(orderList.getColumnIndex("remarks")));
                            dataForSubmission.put("submitted_location_lat", latitude);
                            dataForSubmission.put("submitted_location_lon", longitude);
                            dataForSubmission.put("taken_by",order_taken_by);

                            //dataForSubmission.put("submit_lat", orderList.getDouble(orderList.getColumnIndex("submit_lat")));
                            //dataForSubmission.put("submit_lon", orderList.getDouble(orderList.getColumnIndex("submit_lon")));

                            //dataForSubmission.put("entry_time", apcDateHelper.getTimeFromMilliseconds(currentPending.getLong(currentPending.getColumnIndex("entry_time"))));
                            submitted_order_ids.add(post_order_no);

                            Cursor order_entries = dbHelper.getOrderDetailsEntries(post_order_no);

                            JSONArray detailsArray = new JSONArray();

                            if (order_entries != null)
                            {
                                order_entries.moveToFirst();

                                while (!order_entries.isAfterLast())
                                {
                                    JSONObject dataEntries = new JSONObject();

                                    //product_id text, product_name text, price numeric, entry_time text, qty numeric, bonus numeric, comments text

                                    dataEntries.put("product_id", order_entries.getString(order_entries.getColumnIndex("product_id")));
                                    //dataEntries.put("product_name", order_entries.getString(order_entries.getColumnIndex("product_name")));
                                    //dataEntries.put("price", order_entries.getDouble(order_entries.getColumnIndex("price")));
                                    dataEntries.put("entry_time", order_entries.getString(order_entries.getColumnIndex("entry_time")));
                                    dataEntries.put("qty", order_entries.getDouble(order_entries.getColumnIndex("qty")));
                                    //dataEntries.put("bonus", order_entries.getDouble(order_entries.getColumnIndex("bonus")));
                                    dataEntries.put("remarks", order_entries.getString(order_entries.getColumnIndex("comments")));

                                    detailsArray.put(dataEntries);

                                    dataEntries = null;
                                    order_entries.moveToNext();
                                }
                            }

                            dataForSubmission.put("order_details", detailsArray.toString());
                            dataArray.put(dataForSubmission);

                            //System.out.println("JSONed Data: "+ dataForSubmission.toString());


                            detailsArray = null;

                        }


                    }

                    result = submitInfoToUrl(postURL, dataArray.toString());

                    if (result != null)
                    {

                        JSONArray jArray = null;

                        try
                        {
                            if (result.contentEquals("success"))
                            {

                                for (int s_it=0; s_it<submitted_order_ids.size();++s_it)
                                {
                                    dbHelper.updateStatusToSubmitWithEntryID(submitted_order_ids.get(s_it));
                                    dbHelper.updateSubmitLocation(submitted_order_ids.get(s_it),latitude,longitude);
                                }

                                APCOrderSCursorAdapter ap2   = (APCOrderSCursorAdapter) lvPreviousEntries.getAdapter();
                                ap2.clearSelectedListOfOrders();
                                selectedForSubmission = null;
                            }

                        } catch (Exception e) {

                            e.printStackTrace();
                        }


                    }



                }

            }catch (Exception e)
            {
                e.printStackTrace();
                return "Unsuccessfull";

            }

            return result;
        }

        @Override
        protected void onProgressUpdate(Void... values) {



            super.onProgressUpdate(values);
            //pd.incrementProgressBy((int)values[0]);

        }

        @Override
        protected void onPostExecute(String s)
        {
            System.out.println("Server Response: "+s);
            pd.dismiss();

            Cursor productEntries   = dbHelper.getPendingSubmissions(userId);

            apcCursorAdapter.swapCursor(productEntries);
            apcCursorAdapter.notifyDataSetChanged();




        }


        private String submitInfoToUrl(String networkURL,String dataArray) throws IOException{

            //URL url = new URL(serverUrl);

            //barCode                 = "132768165";
            HttpClient httpClient   = new DefaultHttpClient();
            HttpContext httpContext = new BasicHttpContext();
            String postUrl          = networkURL;
            HttpPost request        = new HttpPost(postUrl);




            HttpResponse response   = null;

            BufferedReader in=null;
            String data = null;

            try
            {

                StringEntity se = new StringEntity(dataArray);
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("aps",dataArray));
                //nameValuePairs.add(new BasicNameValuePair("status","1"));

                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                //request.setEntity(se);
                //request.setHeader("Accept", "application/json");
                //request.setHeader("Content-type","application/json");
                System.out.println("Submit pack: "+dataArray);
                response = httpClient.execute(request, httpContext);
                HttpEntity entity = response.getEntity();

                String responseData     = EntityUtils.toString(entity);
//                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//                StringBuffer sb = new StringBuffer("");
//                String l = "";
//                String nl = System.getProperty("line.separator");
//
//                while((l=in.readLine())!=null)
//                {
//                    sb.append(l+nl);
//
//                }
//
//                in.close();
//                data = sb.toString();
                return responseData;

            }finally
            {


            }




        }
    }


}
