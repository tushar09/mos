package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by amadeyr on 9/15/15.
 */
public class APSOrderScreenCursorAdapter extends SimpleCursorAdapter {

    private APCDateHelper apcDate;
    private int layout_id;
    private DecimalFormat decFormatter;

    public APSOrderScreenCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        layout_id = layout;
        apcDate    = new APCDateHelper();
        decFormatter    = new DecimalFormat("0.00");
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor)
    {

        //TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewOSRowSerialID);
        TextView txtViewProductName     = (TextView) view.findViewById(R.id.txtViewOSRowProductName);
        TextView txtViewPrice           = (TextView) view.findViewById(R.id.txtViewOSRowPrice);
        TextView txtViewQty             = (TextView) view.findViewById(R.id.txtViewOSRowQty);
        TextView txtViewTotal           = (TextView) view.findViewById(R.id.txtViewOSRowTotal);


        if (cursor != null)
        {
            //

            String order_no     = cursor.getString(cursor.getColumnIndex("order_no"));
            String trade_name   = cursor.getString(cursor.getColumnIndex("product_name"));
            String order_date   = cursor.getString(cursor.getColumnIndex("order_date"));
            int qty             = (int) cursor.getDouble(cursor.getColumnIndex("qty"));
            Double price        = cursor.getDouble(cursor.getColumnIndex("price"));
            Double item_total   = qty * price;
            Double total        = cursor.getDouble(cursor.getColumnIndex("total"));

            //int position        = cursor.getPosition()+1;

//            if(position<10)
//            {
//                txtViewSerialNo.setText("0"+position);
//            }else
//            {
//                txtViewSerialNo.setText("0"+position);
//            }


            txtViewProductName.setText(trade_name);
            txtViewPrice.setText(""+price);
            txtViewQty.setText(""+qty);
            txtViewTotal.setText(""+decFormatter.format(item_total));


//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
        }

    }
}


