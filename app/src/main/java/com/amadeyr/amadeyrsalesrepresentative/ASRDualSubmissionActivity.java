package com.amadeyr.amadeyrsalesrepresentative;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.MessagesTabListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.fragments.MessageFragment;
import com.amadeyr.amadeyrsalesrepresentative.fragments.PrescriptionFragment;
import com.amadeyr.amadeyrsalesrepresentative.fragments.SubmitOrdersFragment;
import com.amadeyr.amadeyrsalesrepresentative.fragments.VisitOrdersFragment;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by amadeyr on 11/11/15.
 */
public class ASRDualSubmissionActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener
{
    private ListView lvPreviousEntries;
    private ProductInfo pInfo;
    private APCDatabaseHelper dbHelper;
    private APCDateHelper dateHelper;
    private APCDateHelper apcDateHelper;
    private APCCustomAutoCompleteView actSearchField;
    private APCOrderSCursorAdapter apcCursorAdapter;
    private TextView txtViewPSSProductName,txtViewPSSProductID, txtViewPSSProductBarcode;

    private String product_name, product_id, product_barcode;
    private ImageView imgViewClearAll;

    private Button btnSearch, btnSubmit, btnSubmitAll, btnRemove;

    private String TAB_1_TITLE        = "ORDERS";
    private String TAB_2_TITLE        = "VISITS";
    private String TAB_3_TITLE        = "PRESCRIPTIONS";


    private final int REQUEST_CODE      = 100;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;

    private double latitude, longitude;

    private String getURL =   "";//http://202.51.189.20/rpo_latest/public/autodata/description";
    private String postURL =  "";//http://192.168.1.122/pos_report/index.php/pos_request/datapush";

    private String[] orderArray;

    private HashMap<String,String> productNameMapper;

    private String screenTitle = "Submissions";

    private final String DATA_SUBMISSION_TYPE_SELECTED  = "SELECTED";
    private final String DATA_SUBMISSION_TYPE_ALL       = "ALL";
    private final String ENTRY_MODE_EDIT            = "EDIT";

    private Cursor lastSubmitted;
    private LocalTime jodaTimeHelper;
    public ArrayList<String> selectedForSubmission;
    private SharedPreferences sp;



    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static int UPDATE_INTERVAL  = 10000;
    private static int FASTEST_INTERVAL = 5000;
    private static int DISPLACEMENT     = 10;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public ProgressDialog pd ;
    private SubmitOrdersFragment mf_order;
    private VisitOrdersFragment  mf_doctor_visit;
    private PrescriptionFragment mf_prescriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_activity_layout);

        sp                      = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        jodaTimeHelper          = new LocalTime();
        pd                      = new ProgressDialog(this);
        pd.setCancelable(false);

        postURL                 = sp.getString("postURL","")+getResources().getString(R.string.postExtension);//+"/index.php/asp_api/postdata2";
        //getURL                  = sp.getString("getURL","")+"/index.php/asp_api/getmsgdata";
        //app_user_id             = sp.getString("username","1");
        //app_user_id             = "MYM-122";
        //msgJSONString           = "";
        TAB_1_TITLE             = getResources().getString(R.string.tabTitleOrders);
        TAB_2_TITLE             = getResources().getString(R.string.tabTitleVisits);
        TAB_3_TITLE             = getResources().getString(R.string.tabTitlePrescriptions);

        //dbHelper                = new APCDatabaseHelper(getApplicationContext(),"apsDB",null,APCDatabaseHelper.DATABASE_VERSION);
        dbHelper                = APCDatabaseHelper.getInstance(getApplicationContext());
        dateHelper              = new APCDateHelper();
        //Cursor orderEntries     = dbHelper.getPendingSubmissions();
        //apcCursorAdapter        = new APCOrderSCursorAdapter(getApplicationContext(),R.layout.pending_row_layout,orderEntries,new String[]{},new int[]{}, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);



//        if(checkPlayServices())
//        {
//            buildGoogleApiClient();
//        }

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle(getResources().getString(R.string.barTitleSubmissions));
        bar.setIcon(R.drawable.iconb);
        bar.setNavigationMode(bar.NAVIGATION_MODE_TABS);
        //bar.setDisplayShowTitleEnabled(false);

        ActionBar.Tab orderTab          = bar.newTab();
        ActionBar.Tab doctorVisitTab    = bar.newTab();
        ActionBar.Tab prescriptionTab   = bar.newTab();

        mf_order = new SubmitOrdersFragment();
        //mf_order.setTypeOfFragment("ORDERS");

        mf_doctor_visit = new VisitOrdersFragment();
        //mf_doctor_visit.setTypeOfFragment("DOCTOR VISITS");

        mf_prescriptions = new PrescriptionFragment();

        orderTab.setText(TAB_1_TITLE)

                .setContentDescription("Tap to browse messages in your inbox.")
                .setTabListener(new MessagesTabListener<SubmitOrdersFragment>(mf_order));

        doctorVisitTab.setText(TAB_2_TITLE)
                .setContentDescription("Tap to browse messages in your inbox.")
                .setTabListener(new MessagesTabListener<VisitOrdersFragment>(mf_doctor_visit));

        prescriptionTab.setText(TAB_3_TITLE)
                .setContentDescription("Tap to browse messages in your inbox.")
                .setTabListener(new MessagesTabListener<PrescriptionFragment>(mf_prescriptions));



        bar.addTab(orderTab);
        bar.addTab(doctorVisitTab);
        bar.addTab(prescriptionTab);
        bar.show();



//        SharedPreferences sp    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
//
//        getURL                  = sp.getString("getURL","")+"/autodata/description";
//        postURL                 = sp.getString("postURL","")+"/index.php/asp_api/postdata";
        //postURL                 = sp.getString("postURL","")+":8000/orders";

        Cursor pendingImages = dbHelper.getAllPendingPrescriptionImages();

        if(pendingImages!=null)
        {
            if(pendingImages.getCount()>0)
            {
                createNotificationMessage();

            }

        }




    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void createNotificationMessage()
    {

        Intent intent = new Intent(this, PendingImageSubmissionNotificationActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        // Build notification
        // Actions are just fake
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification noti = new Notification.Builder(this)
                .setContentTitle("Image Submission Pending")
                .setContentText("Click on this notification to start submission").setSmallIcon(R.drawable.submitsmall)
                .setContentIntent(pIntent)

                .setSound(uri)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);

    }

    public void invokeOrderFragmentAutocompleteSearch()
    {
        this.mf_order.callStringSearchMethod();

    }


    public void invokeFragmentAutocompleteSearch(String fragName)
    {

        if(fragName.contentEquals("orders"))
        {
            this.mf_order.callStringSearchMethod();

        }else if(fragName.contentEquals("visits"))
        {
            this.mf_doctor_visit.callStringSearchMethod();

        }else if(fragName.contentEquals("prescription"))
        {
            this.mf_prescriptions.callStringSearchMethod();

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            //NavUtils.navigateUpFromSameTask(this);
            return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }




}
