package com.amadeyr.amadeyrsalesrepresentative;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.amadeyr.amadeyrsalesrepresentative.models.CustomerInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.DoctorInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.EmployeeInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.InstituteInfo;
import com.amadeyr.amadeyrsalesrepresentative.models.ProductInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by acl on 5/12/15.
 */
public class APCProductDatabaseHelper extends SQLiteOpenHelper
{

    public static final String PRODUCT_DATABASE_NAME        = "apsProductDB";
    public static final String USER_DATABASE_NAME           = "asrUserDB";
    public static  int PD_DATABASE_VERSION                  = 2;
    public static  int USER_DATABASE_VERSION                = 2;

    private Context application_ctx;
    // Initializing table names

    private static final String CUSTOMER_TABLE                  = "aps_customer";
    private static final String PRODUCT_TABLE                   = "aps_product";
    private static final String OTHER_PRODUCT_TABLE             = "aps_other_products";

    private static final String PRODUCT_CATEGORY_TABLE          = "aps_product_category";
    private static final String DUE_TABLE           = "aps_dues";
    private static final String INSTITUTES_TABLE    = "aps_institutes";
    private static final String MARKET_TREE_TABLE   = "aps_marketing_tree";
    private static final String DOCTORS_TABLE       = "aps_doctor_list";

    private static APCProductDatabaseHelper sInstance;
    //private static final String table_name    2     = ""

//    private static final String CREATE_ENTRIES_TABLE       = "create table "+table_name1+"(_id integer primary key,doctor_code text,trade_name text,product_barcode text, product_tag text,product_count double, entry_date date, entry_time long, entry_status text, comments text)";
//    private static final String CREATE_PRODUCT_TABLE       = "create table "+table_name2+"(_id integer primary key,doctor_code text,trade_name text,product_barcode text)";
//    private static final String CREATE_SYNC_TABLE          = "create table "+table_name3+"(_id integer primary key,sync_version int)";

    public static synchronized  APCProductDatabaseHelper getInstance(Context c, String dbName, int db_version)
    {
        if(sInstance==null)
        {
            sInstance = new APCProductDatabaseHelper(c,dbName,null,db_version);
        }

        return sInstance;

    }

    public APCProductDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        application_ctx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {


    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }


    }

    public void fillWithDummyData()
    {

    }

    public void createIndexes()
    {
        SQLiteDatabase db       =   this.getWritableDatabase();

        String sqlQuery         =   "CREATE INDEX idx_aps_pname ON aps_product(trade_name)";
        db.execSQL(sqlQuery);
        db.close();
    }

    public void insertCustomer(CustomerInfo c)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();


        cvalues.put("doctor_code",c.getCustomerId());
        cvalues.put("trade_name",c.getTradeName());
        cvalues.put("proprietary_name", c.getProprietaryName());
        cvalues.put("contact_no",c.getContactNo());
        cvalues.put("address",c.getAddress());


        //cvalues.put("product_barcode",p.getProductBarcode());

        db.insert(CUSTOMER_TABLE, null, cvalues);
        db.close();

    }

    public void insertProduct(ProductInfo p)
    {
        SQLiteDatabase db       =   this.getWritableDatabase();
        ContentValues cvalues   =   new ContentValues();

        cvalues.put("product_id", p.getProductID());
        cvalues.put("trade_name",p.getProductName());
        cvalues.put("generic_name", p.getGenericName());
        cvalues.put("unit",p.getUnit());
        cvalues.put("stock_qty",p.getProductStock());
        cvalues.put("price",p.getProductPrice());
        cvalues.put("offer",p.getOffer());

        //cvalues.put("product_barcode",p.getProductBarcode());

        db.insert(PRODUCT_TABLE, null, cvalues);
        db.close();

    }



    public Cursor getAllProductInfo()
    {

        SQLiteDatabase db       = this.getWritableDatabase();
        String query        = "SELECT * FROM " + PRODUCT_TABLE;

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getInstituteList(String employee_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM "+INSTITUTES_TABLE+" WHERE employee_id='"+employee_id+"' ORDER BY institute_name ASC";

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getDoctorsList(String institute_code)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        //String query        =  "SELECT * FROM "+DOCTORS_TABLE+" WHERE institute_code='"+institute_code+"' ORDER BY doctor_name ASC";
        String query        =  "SELECT * FROM "+DOCTORS_TABLE+" ORDER BY doctor_name ASC";
        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getDoctorsListForSR(String employee_code)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+DOCTORS_TABLE+" WHERE employee_id='"+employee_code+"' ORDER BY doctor_name ASC";

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }



    public Cursor getAllCategory()
    {

        SQLiteDatabase db   = this.getWritableDatabase();
        String query        = "SELECT * FROM " + PRODUCT_CATEGORY_TABLE;

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }




    public Cursor getAllCustomerInfo()
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM " + CUSTOMER_TABLE;

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public Cursor getAllDueInfo()
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM " + DUE_TABLE;
        Cursor results      =  db.rawQuery(query,null);
        return results;


    }

    public Cursor getDueInfoFromCustomerID(String customer_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM " + DUE_TABLE+" WHERE doctor_code=\""+customer_id+"\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }

    public boolean isTeamLeader(String employee_code)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM " + MARKET_TREE_TABLE+" WHERE parent_id=\""+employee_code+"\" LIMIT 1";
        Cursor results      =  db.rawQuery(query,null);

        if(results!=null)
        {
            if(results.getCount()==1) return true;

        }

        return false;


    }

    public Cursor getDueInfoFromDate(String customer_id, String date)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM " + DUE_TABLE+" WHERE doctor_code=\""+customer_id+"\" AND date="+date;

        Cursor results      =  db.rawQuery(query,null);

        return results;


    }



    public Cursor getProductInfoFromBarcode(String barcode)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM apc_product WHERE product_barcode=\""+barcode+"\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }

    public Cursor getProductInfoFromName(String name)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM "+PRODUCT_TABLE+ " WHERE trade_name LIKE \"%"+name+"%\"";
        Cursor results      =  db.rawQuery(query,null);
        db.close();
        return results;

    }




    public Cursor getProductInfoFromNameOrBarcode(String searchString)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM aps_product WHERE trade_name = \""+searchString+"\" OR generic_name = \""+searchString+"\"";

        Cursor results      =  db.rawQuery(query,null);

        return results;
    }

    public Cursor getOtherProductInfoFromName(String mode, String searchString)
    {
        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "";

        if(mode.contentEquals("Samples"))
        {

            query = "SELECT * FROM aps_product WHERE trade_name = \""+searchString+"\" OR generic_name = \""+searchString+"\"";

        }else
        {

            query = "SELECT * FROM "+OTHER_PRODUCT_TABLE+" JOIN aps_product_category ON aps_other_products.category_code=aps_product_category.category_code WHERE aps_product_category.category_name='"+mode+"' AND aps_other_products.product_name LIKE \"%"+searchString+"%\"";

        }


        Cursor results      =  db.rawQuery(query,null);

        return results;
    }




    public List<ProductInfo> getAutoCompleteProductInfo(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM "+PRODUCT_TABLE+ " WHERE trade_name LIKE \"%"+searchTerm+"%\" ORDER BY trade_name ASC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);

        List<ProductInfo> searchResults = new ArrayList<ProductInfo>();

        if(results.moveToFirst())
        {
            do
            {
                String product_id               = results.getString(results.getColumnIndex("product_id"));
                String product_name             = results.getString(results.getColumnIndex("trade_name"));
                String product_generic_name     = results.getString(results.getColumnIndex("generic_name"));
                String product_unit             = results.getString(results.getColumnIndex("unit"));
                //double product_stock            = results.getDouble(results.getColumnIndex("stock_qty"));
                double product_stock            = 0;
                double product_price            = results.getDouble(results.getColumnIndex("price"));
                String product_offer            = "";
                //String product_offer            = results.getString(results.getColumnIndex("offer"));

                ProductInfo pInfo               = new ProductInfo();

                //pInfo.setProductBarcode(product_barcode);
                pInfo.setProductID(product_id);
                pInfo.setProductName(product_name);
                pInfo.setGenericName(product_generic_name);
                pInfo.setUnit(product_unit);
                pInfo.setProductStock(product_stock);
                pInfo.setProductPrice(product_price);
                pInfo.setOffer(product_offer);

                searchResults.add(pInfo);

            }while(results.moveToNext());


        }

        results.close();
        db.close();



        return searchResults;
    }

    public List<ProductInfo> getAutoCompleteOtherProductInfo(String type,String searchTerm)
    {



        SQLiteDatabase db       =  this.getWritableDatabase();
        String query            =  "";


        if(type.contentEquals("Samples"))
        {
            query        = "SELECT * FROM "+PRODUCT_TABLE+ " WHERE trade_name LIKE \"%"+searchTerm+"%\" ORDER BY trade_name ASC LIMIT 0,20";

        }else
        {
            query        = "SELECT * FROM "+OTHER_PRODUCT_TABLE+ " JOIN aps_product_category ON aps_other_products.category_code=aps_product_category.category_code WHERE aps_product_category.category_name='"+type+"' AND aps_other_products.product_name LIKE \"%"+searchTerm+"%\" ORDER BY aps_other_products.product_name DESC LIMIT 0,20";

        }


        Cursor results      =  db.rawQuery(query,null);

        List<ProductInfo> searchResults = new ArrayList<ProductInfo>();

        if(results.moveToFirst())
        {
            do
            {

                ProductInfo pInfo               = new ProductInfo();

                if(type.contentEquals("Samples"))
                {

                    String product_id               = results.getString(results.getColumnIndex("product_id"));
                    String product_name             = results.getString(results.getColumnIndex("trade_name"));
                    String product_generic_name     = results.getString(results.getColumnIndex("generic_name"));
                    String product_unit             = results.getString(results.getColumnIndex("unit"));
                    double product_stock            = 0;
                    //double product_stock            = results.getDouble(results.getColumnIndex("stock_qty"));
                    double product_price            = results.getDouble(results.getColumnIndex("price"));
                    String product_offer            = "";
                    //String product_offer            = results.getString(results.getColumnIndex("offer"));



                    //pInfo.setProductBarcode(product_barcode);
                    pInfo.setProductID(product_id);
                    pInfo.setProductName(product_name);
                    pInfo.setGenericName(product_generic_name);
                    pInfo.setUnit(product_unit);
                    pInfo.setProductStock(product_stock);
                    pInfo.setProductPrice(product_price);
                    pInfo.setOffer(product_offer);


                }else
                {
                    String product_id               = results.getString(results.getColumnIndex("product_code"));
                    String product_name             = results.getString(results.getColumnIndex("product_name"));




                    //pInfo.setProductBarcode(product_barcode);
                    pInfo.setProductID(product_id);
                    pInfo.setProductName(product_name);
//                    pInfo.setGenericName(product_generic_name);
//                    pInfo.setUnit(product_unit);
//                    pInfo.setProductStock(product_stock);
//                    pInfo.setProductPrice(product_price);
//                    pInfo.setOffer(product_offer);



                }


                searchResults.add(pInfo);

            }while(results.moveToNext());


        }

        results.close();
        db.close();



        return searchResults;
    }




    public List<DoctorInfo> getAutoCompleteDoctorInfo(String searchTerm,String inst_code)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        //String query        = "SELECT * FROM "+DOCTORS_TABLE+ " WHERE doctor_name LIKE \"%"+searchTerm+"%\" AND institute_code='"+inst_code+"' ORDER BY doctor_name DESC LIMIT 0,20";
        String query        = "SELECT * FROM "+DOCTORS_TABLE+ " WHERE doctor_name LIKE \"%"+searchTerm+"%\" ORDER BY doctor_name ASC";
        Cursor results      =  db.rawQuery(query,null);

        List<DoctorInfo> searchResults = new ArrayList<DoctorInfo>();

        if(results.moveToFirst())
        {
            do
            {
                String doctor_code              = results.getString(results.getColumnIndex("doctor_code"));
                //String institute_code           = results.getString(results.getColumnIndex("institute_code"));
                String institute_code           = "";
                String doctor_name              = results.getString(results.getColumnIndex("doctor_name"));

                DoctorInfo pInfo = new DoctorInfo();

                //pInfo.setProductBarcode(product_barcode);
                pInfo.setDoctorId(doctor_code);
                pInfo.setInstituteCode(institute_code);
                pInfo.setDoctorName(doctor_name);

                searchResults.add(pInfo);

            }while(results.moveToNext());


        }

        results.close();
        db.close();



        return searchResults;
    }

    public List<InstituteInfo> getAutoCompleteInstituteInfo(String searchTerm, String u_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        = "SELECT * FROM "+INSTITUTES_TABLE+ " WHERE institute_name LIKE \"%"+searchTerm+"%\" ORDER BY institute_name DESC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);

        List<InstituteInfo> searchResults = new ArrayList<InstituteInfo>();

        if(results.moveToFirst())
        {
            do
            {
                String institute_code               = results.getString(results.getColumnIndex("institute_code"));
                String employee_id                  = results.getString(results.getColumnIndex("employee_id"));
                String institute_name               = results.getString(results.getColumnIndex("institute_name"));
                String institute_address            = results.getString(results.getColumnIndex("institute_address"));


                InstituteInfo pInfo               = new InstituteInfo();

                //pInfo.setProductBarcode(product_barcode);
                pInfo.setInstituteId(institute_code);
                pInfo.setEmployeeCode(employee_id);
                pInfo.setInstituteName(institute_name);
                pInfo.setAddress(institute_address);

                searchResults.add(pInfo);

            }while(results.moveToNext());


        }

        results.close();
        db.close();



        return searchResults;
    }


    public Cursor getCustomerInfo(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+CUSTOMER_TABLE+ " WHERE trade_name LIKE \"%"+searchTerm+"%\" OR proprietary_name LIKE \"%"+searchTerm+"%\" ORDER BY trade_name ASC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);



        return results;

    }

    public Cursor getDoctorInfo(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+DOCTORS_TABLE+ " WHERE doctor_name LIKE \"%"+searchTerm+"%\" OR doctor_code LIKE \"%"+searchTerm+"%\" ORDER BY doctor_name DESC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);



        return results;

    }

    public Cursor getInstituteInfo(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+INSTITUTES_TABLE+ " WHERE institute_name LIKE \"%"+searchTerm+"%\" OR institute_code LIKE \"%"+searchTerm+"%\" ORDER BY institute_code DESC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);



        return results;

    }


    public Cursor getEmployeeList(String searchTerm)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+MARKET_TREE_TABLE+ " WHERE employee_id LIKE \"%"+searchTerm+"%\" ORDER BY employee_id DESC LIMIT 0,5";

        Cursor results      =  db.rawQuery(query,null);

        return results;

    }


    public Cursor getCustomerList(String sales_rep_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+CUSTOMER_TABLE+ " WHERE employee_id='"+sales_rep_id+"' ORDER BY trade_name ASC";

        Cursor results      =  db.rawQuery(query,null);



        return results;

    }


    public Cursor getEmployeesUnderSupervisor(String supervisor_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT _id, employee_id FROM "+MARKET_TREE_TABLE+ " WHERE parent_id='"+supervisor_id+"'";
        Cursor results      =  db.rawQuery(query,null);

        return results;

    }



    public List<CustomerInfo> getAutoCompleteCustomerInfo(String searchTerm, String employee_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        //String query        = "SELECT * FROM "+CUSTOMER_TABLE+ " WHERE (trade_name LIKE \"%"+searchTerm+"%\" OR proprietary_name LIKE \"%"+searchTerm+"%\") AND employee_id='"+employee_id+"' ORDER BY trade_name ASC LIMIT 0,20";
        String query        = "SELECT * FROM "+CUSTOMER_TABLE+ " WHERE (trade_name LIKE \"%"+searchTerm+"%\" OR proprietary_name LIKE \"%"+searchTerm+"%\") AND employee_id='"+employee_id+"' ORDER BY trade_name ASC";
        //String query        = "SELECT * FROM "+CUSTOMER_TABLE+ " WHERE (trade_name LIKE \"%"+searchTerm+"%\" OR proprietary_name LIKE \"%"+searchTerm+"%\" OR address LIKE \"%\"+searchTerm+\"%\") AND employee_id='"+employee_id+"' ORDER BY trade_name ASC";

        Cursor results      =  db.rawQuery(query,null);

        List<CustomerInfo> searchResults = new ArrayList<CustomerInfo>();

        if(results.moveToFirst())
        {
            do
            {
                String customer_id              = results.getString(results.getColumnIndex("customer_id"));
                String trade_name               = results.getString(results.getColumnIndex("trade_name"));
                String proprietary_name         = results.getString(results.getColumnIndex("proprietary_name"));
                String contact_no               = results.getString(results.getColumnIndex("contact_no"));
                String address                  = results.getString(results.getColumnIndex("address"));

                CustomerInfo cInfo              = new CustomerInfo();

                //pInfo.setProductBarcode(product_barcode);
                cInfo.setCustomerId(customer_id);
                cInfo.setTradeName(trade_name);
                cInfo.setProprietaryName(proprietary_name);
                cInfo.setContactNo(contact_no);
                cInfo.setAddress(address);

                searchResults.add(cInfo);

            }while(results.moveToNext());





        }

        results.close();
        db.close();



        return searchResults;
    }


    public List<EmployeeInfo> getAutoCompleteEmployeeInfo(String searchTerm, String supervisor_id)
    {

        SQLiteDatabase db   =  this.getWritableDatabase();
        String query        =  "SELECT * FROM "+MARKET_TREE_TABLE+ " WHERE employee_id LIKE \"%"+searchTerm+"%\" AND parent_id='"+supervisor_id+"'ORDER BY employee_id ASC LIMIT 0,20";

        Cursor results      =  db.rawQuery(query,null);

        List<EmployeeInfo> searchResults = new ArrayList<EmployeeInfo>();

        if(results.moveToFirst())
        {
            do
            {
                String employee_id              = results.getString(results.getColumnIndex("employee_id"));
                EmployeeInfo eInfo              = new EmployeeInfo();
                eInfo.setEmployeeId(employee_id);

                searchResults.add(eInfo);

            }while(results.moveToNext());





        }

        results.close();
        db.close();



        return searchResults;
    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public List<String> getDoctorsWithIncompleteVisits(String employee_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        List<String> outputIDs = new ArrayList();

        String query            =  "SELECT doctor_code,doctor_name FROM aps_doctor_visit_master WHERE visit_status='INCOMPLETE' AND visit_id LIKE '"+employee_id+"%' ORDER BY doctor_code ASC";
        Cursor results          =  db.rawQuery(query, null);

        if(results!=null)
        {
            results.moveToFirst();

            while(!results.isAfterLast())
            {
                outputIDs.add(results.getString(results.getColumnIndex("doctor_code")));
                results.moveToNext();

            }

            return outputIDs;

        }


        return null;
    }
}

