package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by acl on 5/12/15.
 */
public class APCOrderSCursorAdapter extends SimpleCursorAdapter {


    private Context appContext;
    private APCDateHelper apcDate;
    private int layout_id;
    public ArrayList<String> selectedForSubmission;
    public DateTime jDt;
    public DateTimeFormatter formatter;
    public APCOrderSCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        selectedForSubmission = new ArrayList<>();
        appContext  = context;
        layout_id   = layout;
        apcDate     = new APCDateHelper();


    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {



        TextView txtViewSerialNo        = (TextView) view.findViewById(R.id.txtViewPSRowSerialID);
        TextView txtViewTradeName       = (TextView) view.findViewById(R.id.txtViewPSRowTradeName);
        TextView txtViewDate            = (TextView) view.findViewById(R.id.txtViewPSRowDate);
        TextView txtViewOrderNo         = (TextView) view.findViewById(R.id.txtViewPSRowOrderNo);
        final CheckBox chkSubmission    = (CheckBox) view.findViewById(R.id.chkBoxSelect);



        if(chkSubmission.isChecked())
        {

            chkSubmission.setChecked(false);
        }

        String order_no                 = "";

        if (cursor != null)
        {
            //

            order_no            = cursor.getString(cursor.getColumnIndex("order_no"));
            String trade_name   = cursor.getString(cursor.getColumnIndex("customer_name"));
            String order_date   = cursor.getString(cursor.getColumnIndex("order_date"));
            String order_time   = cursor.getString(cursor.getColumnIndex("order_time"));
            //Double total        = cursor.getDouble(cursor.getColumnIndex("total"));

            formatter           = DateTimeFormat.forPattern("yyyy-MM-dd");
            jDt                 = formatter.parseDateTime(order_date);


            formatter           = DateTimeFormat.forPattern("dd MMM yy");


            int position        = cursor.getPosition()+1;

            if(position<10)
            {
                txtViewSerialNo.setText(""+position);
            }else
            {
                txtViewSerialNo.setText("0"+position);
            }

            txtViewTradeName.setText(trade_name);
            txtViewDate.setText(formatter.print(jDt)+" | "+order_time);
            txtViewOrderNo.setText(order_no);



//                btnPreviousHistory.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)
//                    {
//                        Toast.makeText(context,"Test",Toast.LENGTH_LONG).show();
//                    }
//                });

//                if (entry_status.contentEquals("Submitted")) {
//                    txtViewDESRowStatus.setTextColor(Color.GREEN);
//
//                } else {
//                    txtViewDESRowStatus.setTextColor(Color.RED);
//
//                }
        }

        final String finalOrder_no = order_no;

        if(selectedForSubmission.contains(finalOrder_no)) chkSubmission.setChecked(true);

        chkSubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(chkSubmission.isChecked())
                {

                    selectedForSubmission.add(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();


                }else
                {
                    selectedForSubmission.remove(finalOrder_no);
                    //Toast.makeText(appContext,selectedForSubmission.size()+"",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public ArrayList<String> getSelectedListOfOrders()
    {
        return selectedForSubmission;
    }

    public void clearSelectedListOfOrders()
    {
        selectedForSubmission.clear();
    }
}
