package com.amadeyr.amadeyrsalesrepresentative.CustomViews;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by amadeyr on 11/25/15.
 */
public class ASRErrorMessageView
{

    private Activity cActivity;
    private Button btnAction2;

    private Dialog alertScreen;

    public ASRErrorMessageView(Activity ac)
    {
        cActivity = ac;
        alertScreen = new Dialog(cActivity);
        alertScreen.setContentView(R.layout.default_error_layout);
        btnAction2 = (Button) alertScreen.findViewById(R.id.btnAction2);
    }

    public Button getActionButton2Instance()
    {
        return btnAction2;
    }

    public void showErrorMessage(String messageTitle, String messageBody)
    {

        alertScreen.setTitle(messageTitle);
        //ImageView errClose  = (ImageView) alertScreen.findViewById(R.id.ivErrClose);
        TextView errMsgView = (TextView) alertScreen.findViewById(R.id.txtViewErrorMessage);
        Button btnClose     = (Button) alertScreen.findViewById(R.id.btnErrClose);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertScreen.dismiss();
            }
        });

        errMsgView.setText(messageBody);
        alertScreen.show();


    }

    public void addExtraAction(String btnTitle)
    {
        btnAction2.setText(btnTitle);
        btnAction2.setVisibility(View.VISIBLE);


    }

    public void closeScreen()
    {

        alertScreen.dismiss();
    }
}
