package com.amadeyr.amadeyrsalesrepresentative.CustomViews;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.amadeyr.amadeyrsalesrepresentative.R;

/**
 * Created by amadeyr on 12/15/15.
 */
public class ASRConfirmationMessageView
{

    private Activity cActivity;

    public ASRConfirmationMessageView(Activity ac)
    {
        cActivity = ac;
    }


    public void showConfirmationMessage(String messageTitle, String messageBody)
    {
        final Dialog alertScreen = new Dialog(cActivity);
        alertScreen.setTitle(messageTitle);
        alertScreen.setContentView(R.layout.default_confirmation_layout);

        //ImageView errClose  = (ImageView) alertScreen.findViewById(R.id.ivErrClose);
        TextView errMsgView = (TextView) alertScreen.findViewById(R.id.txtViewConfirmationMessage);
        Button btnClose     = (Button) alertScreen.findViewById(R.id.btnConfirmationClose);



        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertScreen.dismiss();
            }
        });

        errMsgView.setText(messageBody);
        alertScreen.show();


    }
}
