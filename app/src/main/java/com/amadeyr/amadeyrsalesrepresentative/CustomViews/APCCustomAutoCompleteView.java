package com.amadeyr.amadeyrsalesrepresentative.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Created by acl on 5/24/15.
 */
public class APCCustomAutoCompleteView extends AutoCompleteTextView
{

    public APCCustomAutoCompleteView(Context context)
    {
        super(context);
    }

    public APCCustomAutoCompleteView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
    }


    public APCCustomAutoCompleteView(Context context, AttributeSet attrs, int defStyle)
    {
       super(context,attrs,defStyle);
    }
    @Override
    protected void performFiltering(CharSequence text, int keyCode)
    {
        String filterText = "";
        super.performFiltering(filterText, keyCode);
    }

    @Override
    protected void replaceText(CharSequence text) {
        super.replaceText(text);
    }
}
