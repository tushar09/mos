package com.amadeyr.amadeyrsalesrepresentative;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by acl on 5/13/15.
 */
public class APCEditEntryDialogActivity extends Activity
{

    private Spinner spnrEditDialogTag;
    private ImageView imgViewClose, imgViewCloseAlert;
    private EditText edtTextQty, edtTextBonus, edtTextComments;
    private TextView tvProductName;
    private Button btnDone, btnRemove, btnYes, btnNo;
    private final int APC_EDIT_CODE         = 1;
    private final int APC_REMOVE_CODE       = 2;
    private final int APC_ADD_CODE          = 3;
    private int entry_id, customer_id, operation_mode;
    private double prevQty, prevBonus, prevTotal, currentTotal, prevVAT, prevCommission;
    private double qty, bonus, price, vat, commission;
    private String order_no,comments, product_name;
    private LinearLayout llEdit, llAlert;
    private DecimalFormat decFormatter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.edit_entry);

        llEdit                      = (LinearLayout) findViewById(R.id.llNormalEditScr);
        llAlert                     = (LinearLayout) findViewById(R.id.llAlertScr);

        decFormatter                = new DecimalFormat("0.00");
        tvProductName               = (TextView) findViewById(R.id.txtViewDialogProductName);
        edtTextQty                  = (EditText) findViewById(R.id.edtTextQty);
        edtTextComments             = (EditText) findViewById(R.id.edtTextEditComments);
        edtTextBonus                = (EditText) findViewById(R.id.edtTextBonus);
        btnDone                     = (Button)   findViewById(R.id.btnEditSave);
        btnRemove                   = (Button)   findViewById(R.id.btnOSRemove);
        btnYes                      = (Button)   findViewById(R.id.btnConfirmYes);
        btnNo                       = (Button)   findViewById(R.id.btnConfirmNo);
        imgViewClose                = (ImageView) findViewById(R.id.imgViewCross);
        imgViewCloseAlert           = (ImageView) findViewById(R.id.imgViewCancelAlert);

        Intent receivedIntent       = getIntent();
        order_no                    = receivedIntent.getStringExtra("order_no");
        entry_id                    = receivedIntent.getIntExtra("_id", 0);
        operation_mode              = receivedIntent.getIntExtra("mode",0);
        product_name                = receivedIntent.getStringExtra("product_name");
        prevQty                     = receivedIntent.getDoubleExtra("qty", 0);
        prevBonus                   = receivedIntent.getDoubleExtra("bonus", 0);
        price                       = receivedIntent.getDoubleExtra("price", 0);
        prevVAT                     = receivedIntent.getDoubleExtra("vat", 0);
        prevCommission              = receivedIntent.getDoubleExtra("commission", 0);
        prevTotal                   = prevQty * price;


        if(operation_mode==APC_EDIT_CODE)
        {
            tvProductName.setText(product_name);
            edtTextQty.setText(""+ (int) prevQty);
            edtTextBonus.setText(""+ prevBonus);
            edtTextComments.setText(receivedIntent.getStringExtra("comments"));

        }

        imgViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgViewCloseAlert.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    });

        if(edtTextQty.requestFocus())
        {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        }


        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String bonusEntry   = edtTextBonus.getText().toString();
                String commentEntry = edtTextComments.getText().toString();
                String qtyEntry     = edtTextQty.getText().toString();


                double qtyEntryD    = Double.parseDouble(qtyEntry.isEmpty() ? "0":qtyEntry);
                currentTotal        = qtyEntryD * price;
                Intent resultIntent = new Intent();

                resultIntent.putExtra("_id",entry_id);
                resultIntent.putExtra("order_no",order_no);
                resultIntent.putExtra("bonus",Double.parseDouble(bonusEntry.isEmpty() ? "0":bonusEntry));
                resultIntent.putExtra("qty", Double.parseDouble(qtyEntry.isEmpty() ? "0" : qtyEntry));
                resultIntent.putExtra("prevTotal",prevTotal);
                resultIntent.putExtra("prevVAT",prevVAT);
                resultIntent.putExtra("prevCommission",prevCommission);
                resultIntent.putExtra("currentTotal",currentTotal);
                resultIntent.putExtra("comments", commentEntry);


                if(operation_mode==APC_EDIT_CODE)
                {
                    setResult(APC_EDIT_CODE,resultIntent);

                }else if(operation_mode==APC_ADD_CODE)
                {
                    setResult(APC_ADD_CODE,resultIntent);
                }

                finish();


            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                llAlert.setVisibility(View.VISIBLE);
                llEdit.setVisibility(View.GONE);
//                Intent resultIntent = new Intent();
//                resultIntent.putExtra("_id",entry_id);
//                resultIntent.putExtra("prevTotal",prevTotal);
//                setResult(APC_REMOVE_CODE,resultIntent);
//                finish();

            }
        });


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("_id",entry_id);
                resultIntent.putExtra("prevTotal",prevTotal);
                setResult(APC_REMOVE_CODE,resultIntent);
                finish();

            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llAlert.setVisibility(View.GONE);
                llEdit.setVisibility(View.VISIBLE);

            }
        });



//        btnRemove.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                Intent resultIntent = new Intent();
//                resultIntent.putExtra("_id", entry_id);
//                setResult(APC_REMOVE_CODE, resultIntent);
//                finish();
//
//            }
//        });


    }

}
