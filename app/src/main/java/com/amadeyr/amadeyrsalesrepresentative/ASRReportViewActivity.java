package com.amadeyr.amadeyrsalesrepresentative;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.amadeyr.amadeyrsalesrepresentative.CustomListeners.CustomAutoCompleteTextChangedListener;
import com.amadeyr.amadeyrsalesrepresentative.CustomViews.APCCustomAutoCompleteView;
import com.amadeyr.amadeyrsalesrepresentative.adapters.ASRInstituteListCursorAdapter;
import com.amadeyr.amadeyrsalesrepresentative.models.ASRDatabaseFactory;

/**
 * Created by amadeyr on 2/1/16.
 */
public class ASRReportViewActivity extends ActionBarActivity
{
    private final String SCREEN_TITLE = "Reports";
    private String user_id, user_token, postURL;


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports_layout);

        ActionBar bar = getSupportActionBar();
        bar.setTitle(SCREEN_TITLE);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setIcon(R.drawable.iconb);
        bar.show();

        SharedPreferences sp    = getSharedPreferences("apcpref", Context.MODE_PRIVATE);
        postURL                 = sp.getString("postURL","");
        user_id                 = sp.getString("user_id", "");
        user_token              = sp.getString("user_token","");


        WebView reportWV        = (WebView)findViewById(R.id.reportWebView);
        reportWV.setWebViewClient(new MyWebViewClient());
        reportWV.getSettings().setJavaScriptEnabled(true);

        reportWV.loadUrl(postURL + ASRConfig.REPORT_EXTENSION + "?token=" + user_token);
        reportWV.requestFocus();
        //reportWV.loadUrl("http://www.amadeyr.com");


        //user_token  = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiaXNzIjoiaHR0cDpcL1wvbG9jYWxob3N0OjgwMDBcL1wvYXV0aG1vYiIsImlhdCI6MTQ1NDQxMjU1OSwiZXhwIjoxNDU0ODQ0NTU5LCJuYmYiOjE0NTQ0MTI1NTksImp0aSI6IjViMzQxZWQ4M2Y3MTFlYmQ1Y2QyNTkyOWQ3OWQ5ZDc1In0.k6XrgEBS-YO7YbYjTBjCNjLS0wdK3z-U3Qgh7exMrA4";

        //reportWV.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        //reportWV.loadUrl("http://192.168.1.101:8000/esalesreport?token="+user_token);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_button_only, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {

            return true;

        }else if(id == android.R.id.home)
        {
            finish();
            onBackPressed();
            return true;

        }else if(id == R.id.return_to_home)
        {
            String source_activity  = "";

            Intent main_menu = new Intent(ASRReportViewActivity.this,MainMenuActivity.class);

            main_menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            main_menu.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            main_menu.putExtra("source_activity",source_activity);

            startActivity(main_menu);
            finish();




            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {

        super.onBackPressed();




    }

}
